<?php

namespace App\Http\Middleware;

use App\models\Organization;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class loggedin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty(Session::has('xaraSession'))) {
            $organization = Organization::find(1);
            return response()->view('sign_in', compact('organization'));
        }
        return $next($request);
    }

}
