<?php

use App\Contribution;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LoanaccountsController;
use App\Journal;
use App\Journaldump;
use App\Loanaccount;
use App\Loanposting;
use App\Loanrepayment;
use App\Loantransaction;
use App\Memberfee;
use App\Particular;
use App\Receipt;
use App\Savingtransaction;
use App\Sharetransaction;
use App\Vehicle;
use App\vehicletransaction;
use Illuminate\Support\Facades\Auth;

class Datamigration extends Controller {

	/**
	 * Display a listing of the resource.
	 * GET /datamigration
	 *
	 * @return Response
	 */
	public function loans(){
		$loanaccounts = Loanaccount::where('is_disbursed', true)->get();

		foreach($loanaccounts as $loanaccount){
			$loanproduct = $loanaccount->loanproduct;

			$posting = Loanposting::getPostingAccount($loanproduct, 'disbursal');

			$journal = new Journal;
			$trans_no = $journal->getTransactionNumber();
			$journal->date = $loanaccount->date_disbursed;
			$journal->trans_no = $trans_no;
			$journal->account_id = $posting['credit'];
			$journal->amount = $loanaccount->amount_disbursed;
			$journal->initiated_by = 'system';
			$journal->type = 'credit';
			$journal->description = '';

		}
	}


	public function contributions(){
	  $transactions = Contribution::where('date', '>', '2017-12-31')->where('type', 'credit')->get();
	  //return sizeof($transactions);
	  foreach ($transactions as $trans) {
	    // code...
	    $member = $trans->member_id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 31;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = 'system';
	    $journal->type = "credit";
	    $journal->description = "Daily Contributions";
	    $journal->receiptNo = $trans->batch_transaction_no;
			$journal->void = $trans->is_void;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 39;
	    $journal->user_id = $trans->user_id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 17;
	    $journal2->amount = $trans->amount;
	    $journal2->initiated_by = 'system';
	    $journal2->type = "debit";
	    $journal2->description = "Daily Contributions";
	    $journal2->receiptNo = $trans->batch_transaction_no;
			$journal2->void = $trans->is_void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 39;
	    $journal2->user_id = $trans->user_id;
	    $journal2->member_id = $member;
	    $journal2->save();

	  }
	  return "Contribution Data Transfer Successful";
	}

	public function contributionarrears(){
	  $transactions = vehicletransaction::where('category', 'cla')->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    $member = Vehicle::find($trans->vehicle_id)->member->id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 31;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = $trans->user_id;
	    $journal->type = "credit";
	    $journal->description = "Contribution Arrears";
	    $journal->receiptNo = $trans->receiptno;
		$journal->void = $trans->void;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 39;
	    $journal->user_id = $trans->user_id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 17;
	    $journal2->amount = $trans->amount;
	    $journal2->initiated_by = $trans->user_id;
	    $journal2->type = "debit";
	    $journal2->description = "Contribution Arrears";
	    $journal2->receiptNo = $trans->receiptno;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 39;
	    $journal2->user_id = $trans->user_id;
	    $journal2->member_id = $member;
	    $journal2->save();

	  }
	  return "Contribution Arrears Data Transfer Successful";
	}


	public function shares(){
	  $transactions = Sharetransaction::where('pay_for', 'shares')->where('type', 'credit')->where('date', '>', '2017-12-31')->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    $member = $trans->shareaccount->member->id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 27;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = 'system';
	    $journal->type = "credit";
	    $journal->description = "Member Share Capital";
	    $journal->receiptNo = $trans->batch_transaction_no;
		$journal->void = $trans->void;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 36;
	    $journal->user_id = $trans->user_id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 17;
	    $journal2->amount = $trans->amount;
	    $journal2->initiated_by = 'system';
	    $journal2->type = "debit";
	    $journal2->description = "Member Share Capital";
	    $journal2->receiptNo = $trans->batch_transaction_no;
		$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 36;
	    $journal2->user_id = $trans->user_id;
	    $journal2->member_id = $member;
	    $journal2->save();

	  }
	  return "Share Capital Data Transfer Successful";
	}

	public function sharesdebit(){
	  $transactions = Sharetransaction::where('pay_for', 'shares')->where('type', 'debit')->where('date', '>', '2017-12-31')->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    $member = $trans->shareaccount->member->id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 17;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = 'system';
	    $journal->type = "credit";
	    $journal->description = $trans->description;
	    $journal->receiptNo = $trans->batch_transaction_no;
			$journal->void = $trans->void;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 36;
	    $journal->user_id = $trans->user_id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 27;
	    $journal2->amount = $trans->amount;
	    $journal2->initiated_by = 'system';
	    $journal2->type = "debit";
	    $journal2->description = $trans->description;
	    $journal2->receiptNo = $trans->batch_transaction_no;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 36;
	    $journal2->user_id = $trans->user_id;
	    $journal2->member_id = $member;
	    $journal2->save();

	  }
	  return "Share Capital Debits Data Transfer Successful";
	}

	public function memberfees(){
		$transactions = Sharetransaction::where('pay_for', 'membership')->where('type', 'credit')->where('date', '>', '2017-12-31')->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    $member = $trans->shareaccount->member->id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 36;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = 'system';
	    $journal->type = "credit";
	    $journal->description = "Membership fee";
	    $journal->receiptNo = $trans->batch_transaction_no;
			$journal->void = $trans->void;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 4;
	    $journal->user_id = $trans->user_id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 17;
	    $journal2->amount = $trans->amount;
	    $journal2->initiated_by = 'system';
	    $journal2->type = "debit";
	    $journal2->description = "Membership fee";
	    $journal2->receiptNo = $trans->batch_transaction_no;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 4;
	    $journal2->user_id = $trans->user_id;
	    $journal2->member_id = $member;
	    $journal2->save();

	  }
	  return "Membership Data Transfer Successful";
	}

	public function memberfeesdebit(){
		$transactions = Sharetransaction::where('pay_for', 'membership')->where('type', 'debit')->where('date', '>', '2017-12-31')->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    $member = $trans->shareaccount->member->id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 17;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = 'system';
	    $journal->type = "credit";
	    $journal->description =$trans->description;
	    $journal->receiptNo = $trans->batch_transaction_no;
			$journal->void = $trans->void;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 4;
	    $journal->user_id = $trans->user_id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 36;
	    $journal2->amount = $trans->amount;
	    $journal2->initiated_by = 'system';
	    $journal2->type = "debit";
	    $journal2->description = $trans->description;
	    $journal2->receiptNo = $trans->batch_transaction_no;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 4;
	    $journal2->user_id = $trans->user_id;
	    $journal2->member_id = $member;
	    $journal2->save();

	  }
	  return "Membership Debit Data Transfer Successful";
	}



	public function memberfees2(){
	  $transactions = Memberfee::all();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    //$member = Vehicle::find($trans->vehicle_id)->member->id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 36;
	    $journal->amount = $trans->member_registration_fee;
	    $journal->initiated_by = Auth::user()->username;
	    $journal->type = "credit";
	    $journal->description = "Membership fee";
	    $journal->receiptNo = $trans->batch_transaction_no;
			$journal->void = $trans->void;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 4;
	    $journal->user_id = Auth::user()->id;
	    $journal->member_id = $trans->account_id;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 17;
	    $journal2->amount = $trans->member_registration_fee;
	    $journal2->initiated_by = Auth::user()->username;
	    $journal2->type = "debit";
	    $journal2->description = "Membership fee";
	    $journal2->receiptNo = $trans->batch_transaction_no;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 4;
	    $journal2->user_id = Auth::user()->id;
	    $journal2->member_id = $trans->account_id;
	    $journal2->save();

	  }
	  return "Membership Data Transfer Successful";
	}

	// public function memberfees3(){
	//   $transactions = Sharetransaction::where('pay_for', 'membership')->get();
	//   //return $transactions;
	//   foreach ($transactions as $trans) {
	//     // code...
	//     $member = Shareaccount::find($trans->shareaccount_id)->member->id;
	//
	// 		if($trans->type == 'debit'){
	// 			$journal = new Journal;
	//
	// 			$trans_no = $journal->getTransactionNumber();
	// 			$journal->date = $trans->date;
	// 			$journal->trans_no = $trans_no;
	// 			$journal->account_id = 24;
	// 			$journal->amount = $trans->amount;
	// 			$journal->initiated_by = Auth::user()->username;
	// 			$journal->type = "credit";
	// 			$journal->description = "Membership fee";
	// 			$journal->receiptNo = $trans->batch_transaction_no;
	// 			$journal->void = $trans->void;
	// 			$journal->created_at = $trans->created_at;
	// 			$journal->updated_at = $trans->updated_at;
	// 			$journal->particulars_id = 4;
	// 			$journal->user_id = Auth::user()->id;
	// 			$journal->member_id = $member;
	// 			$journal->save();
	//
	// 			$journal2 = new Journal;
	// 			$journal2->date = $trans->date;
	// 			$journal2->trans_no = $trans_no;
	// 			$journal2->account_id = 36;
	// 			$journal2->amount = $trans->amount;
	// 			$journal2->initiated_by = Auth::user()->username;
	// 			$journal2->type = "debit";
	// 			$journal2->description = "Membership fee";
	// 			$journal2->receiptNo = $trans->batch_transaction_no;
	// 			$journal2->void = $trans->void;
	// 			$journal2->created_at = $trans->created_at;
	// 			$journal2->updated_at = $trans->updated_at;
	// 			$journal2->particulars_id = 4;
	// 			$journal2->user_id = Auth::user()->id;
	// 			$journal2->member_id = $member;
	// 			$journal2->save();
	// 		}else{
	// 			$journal = new Journal;
	//
	// 			$trans_no = $journal->getTransactionNumber();
	// 			$journal->date = $trans->date;
	// 			$journal->trans_no = $trans_no;
	// 			$journal->account_id = 36;
	// 			$journal->amount = $trans->amount;
	// 			$journal->initiated_by = Auth::user()->username;
	// 			$journal->type = "credit";
	// 			$journal->description = "Membership fee";
	// 			$journal->receiptNo = $trans->batch_transaction_no;
	// 			$journal->void = $trans->void;
	// 			$journal->created_at = $trans->created_at;
	// 			$journal->updated_at = $trans->updated_at;
	// 			$journal->particulars_id = 4;
	// 			$journal->user_id = Auth::user()->id;
	// 			$journal->member_id = $member;
	// 			$journal->save();
	//
	// 			$journal2 = new Journal;
	// 			$journal2->date = $trans->date;
	// 			$journal2->trans_no = $trans_no;
	// 			$journal2->account_id = 17;
	// 			$journal2->amount = $trans->amount;
	// 			$journal2->initiated_by = Auth::user()->username;
	// 			$journal2->type = "debit";
	// 			$journal2->description = "Membership fee";
	// 			$journal2->receiptNo = $trans->batch_transaction_no;
	// 			$journal2->void = $trans->void;
	// 			$journal2->created_at = $trans->created_at;
	// 			$journal2->updated_at = $trans->updated_at;
	// 			$journal2->particulars_id = 4;
	// 			$journal2->user_id = Auth::user()->id;
	// 			$journal2->member_id = $member;
	// 			$journal2->save();
	// 		}
	//
	//
	//   }
	//   return "Membership Data Transfer Successful";
	// }

	public function devloanprincipal(){
		$loans = Loanaccount::where("loanproduct_id", 1)->lists('id');
	  $transactions = Vehicletransaction::where('category', 'lp')->whereIn('category_id', $loans)->get();

		$particular = Particular::where('name', 'like', '%'.'Dev Loan '.'%')->first();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    $member = Vehicle::find($trans->vehicle_id)->member->id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 1;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = $trans->user_id;
	    $journal->type = "credit";
	    $journal->description = "Principal Repayment";
	    $journal->receiptNo = $trans->receiptno;
			$journal->void = $trans->void;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->user_id = $trans->user_id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 17;
	    $journal2->amount = $trans->amount;
	    $journal2->initiated_by = $trans->user_id;
	    $journal2->type = "debit";
	    $journal2->description = "Principal Repayment";
	    $journal2->receiptNo = $trans->receiptno;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->user_id = $trans->user_id;
	    $journal2->member_id = $member;
	    $journal2->save();

	  }
	  return "Dev Loan Data Transfer Successful";
	}

	public function tyreloanprincipal(){
		$loans = Loanaccount::where("loanproduct_id", 2)->lists('id');
	  $transactions = Vehicletransaction::where('category', 'lp')->whereIn('category_id', $loans)->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    $member = Vehicle::find($trans->vehicle_id)->member->id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 2;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = $trans->user_id;
	    $journal->type = "credit";
	    $journal->description = "Principal Repayment";
	    $journal->receiptNo = $trans->receiptno;
			$journal->void = $trans->void;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->user_id = $trans->user_id;
	    $journal->member_id = $member;
	    $journal->save();

			$journal3 = new Journal;
	    $journal3->date = $trans->date;
	    $journal3->trans_no = $trans_no;
	    $journal3->account_id = 35;
	    $journal3->amount = $trans->amount;
	    $journal3->initiated_by = $trans->user_id;
	    $journal3->type = "credit";
	    $journal3->description = "Tyre Sales(Loan repayment)";
	    $journal3->receiptNo = $trans->receiptno;
			$journal3->void = $trans->void;
	    $journal3->created_at = $trans->created_at;
	    $journal3->updated_at = $trans->updated_at;
	    $journal3->user_id = $trans->user_id;
	    $journal3->member_id = $member;
	    $journal3->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 17;
	    $journal2->amount = $trans->amount;
	    $journal2->initiated_by = $trans->user_id;
	    $journal2->type = "debit";
	    $journal2->description = "Principal Repayment";
	    $journal2->receiptNo = $trans->receiptno;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->user_id = $trans->user_id;
	    $journal2->member_id = $member;
	    $journal2->save();

	  }
	  return "Tyre Loan Data Transfer Successful";
	}


	public function loaninterests(){
	  $transactions = Vehicletransaction::where('category', 'li')->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    $member = Vehicle::find($trans->vehicle_id)->member->id;

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 30;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = $trans->user_id;
	    $journal->type = "credit";
	    $journal->description = "Interest";
			$journal->void = $trans->void;
	    $journal->receiptNo = $trans->receiptno;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 28;
	    $journal->user_id = $trans->user_id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 17;
	    $journal2->amount = $trans->amount;
	    $journal2->initiated_by = $trans->user_id;
	    $journal2->type = "debit";
	    $journal2->description = "Interest repayment";
	    $journal2->receiptNo = $trans->receiptno;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 28;
	    $journal2->user_id = $trans->user_id;
	    $journal2->member_id = $member;
	    $journal2->save();

	  }
	  return "Interest Data Transfer Successful";
	}

	public function loaninterests2(){
	  $transactions = Loanrepayment::where('interest_paid', '>', 0)->get();
	  //return $transactions;
		$count = 1;
		$see = [];
	  foreach ($transactions as $trans) {
	    // code...
	    $member = $trans->loanaccount->member->id;
			$trn = Loantransaction::find($trans->transaction_id);

			if(!empty($trn)){
				$receipt = $trn->batch_transaction_no;
				array_push($see, $receipt);
			}else{
				$receipt = null;
			}

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 30;
	    $journal->amount = $trans->interest_paid;
	    $journal->initiated_by = Auth::user()->username;
	    $journal->type = "credit";
	    $journal->description = "Interest";
			$journal->void = $trans->void;
	    $journal->receiptNo = $receipt;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 28;
	    $journal->user_id = Auth::user()->id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 17;
	    $journal2->amount = $trans->interest_paid;
	    $journal2->initiated_by = Auth::user()->username;
	    $journal2->type = "debit";
	    $journal2->description = "Interest repayment";
	    $journal2->receiptNo = $receipt;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 28;
	    $journal2->user_id = Auth::user()->id;
	    $journal2->member_id = $member;
	    $journal2->save();


	  }
	  return "Interest Data Transfer Successful";
	}

	public function loaninterests3(){
	  $transactions = Loanrepayment::whereBetween('date', array('2018-01-01', '2018-12-31'))->where('interest_paid', '<', 0)->get();
	  //return $transactions;
		$count = 1;
		$see = [];
	  foreach ($transactions as $trans) {
	    // code...
	    $member = $trans->loanaccount->member->id;
			$trn = Loantransaction::find($trans->transaction_id);

			if(!empty($trn)){
				$receipt = $trn->batch_transaction_no;
				array_push($see, $receipt);
			}else{
				$receipt = null;
			}

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 30;
	    $journal->amount = $trans->principal_paid;
	    $journal->initiated_by = Auth::user()->username;
	    $journal->type = "debit";
	    $journal->description = "Interest overpayment correction";
			$journal->void = $trans->void;
	    $journal->receiptNo = $receipt;
	    $journal->created_at = $trans->created_at;
	    $journal->updated_at = $trans->updated_at;
	    $journal->particulars_id = 28;
	    $journal->user_id = Auth::user()->id;
	    $journal->member_id = $member;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 1;
	    $journal2->amount = $trans->principal_paid;
	    $journal2->initiated_by = Auth::user()->username;
	    $journal2->type = "credit";
	    $journal2->description = "Interest overpayment to principal";
	    $journal2->receiptNo = $receipt;
			$journal2->void = $trans->void;
	    $journal2->created_at = $trans->created_at;
	    $journal2->updated_at = $trans->updated_at;
	    $journal2->particulars_id = 28;
	    $journal2->user_id = Auth::user()->id;
	    $journal2->member_id = $member;
	    $journal2->save();


	  }
	  return "Interest Data Transfer Successful";
	}


	public function devdisbursements(){
	  $transactions = Loanaccount::where('void', 0)->where('loanproduct_id', 1)->where('date_disbursed', '>=', '2018-01-01')->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date_disbursed;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 1;
	    $journal->amount = $trans->amount_disbursed;
	    $journal->initiated_by = "System";
	    $journal->type = "debit";
	    $journal->description = "Loan disbursement";
	    $journal->particulars_id = 31;
	    $journal->user_id = Auth::user()->id;
		$journal->created_at = $trans->created_at;
		$journal->updated_at = $trans->updated_at;
	    $journal->member_id = $trans->member_id;
	    $journal->save();

	    $journal2 = new Journal;
	    $journal2->date = $trans->date_disbursed;
	    $journal2->trans_no = $trans_no;
	    $journal2->account_id = 12;
	    $journal2->amount = $trans->amount_disbursed;
	    $journal2->initiated_by = "System";
	    $journal2->type = "credit";
	    $journal2->description = "Loan disbursement";
	    $journal2->particulars_id = 31;
	    $journal2->user_id = Auth::user()->id;
			$journal2->created_at = $trans->created_at;
			$journal2->updated_at = $trans->updated_at;
	    $journal2->member_id = $trans->member_id;
	    $journal2->save();

	  }
	  return "Dev Disbursements Transfer Successful";
	}

	public function tyredisbursements(){
	  $transactions = Loanaccount::where('void', 0)->where('loanproduct_id', 2)->where('date_disbursed', '>=','2018-01-01')->get();//->whereBetween('date_disbursed', array('2018-01-01', '2018-12-31'))->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date_disbursed;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 2;
	    $journal->amount = $trans->amount_disbursed;
	    $journal->initiated_by = "System";
	    $journal->type = "debit";
	    $journal->description = "Tyre loan disbursement";
	    $journal->particulars_id = 31;
	    $journal->user_id = Auth::user()->id;
	    $journal->member_id = $trans->member_id;
		$journal->created_at = $trans->created_at;
		$journal->updated_at = $trans->updated_at;
	    $journal->save();

	  }
	  return "Tyre Disbursements Transfer Successful";
	}
	public function Disbursements(){
	  $transactions = Loanaccount::where('void', 0)->where('loanproduct_id', 2)->where('date_disbursed', '>=','2019-01-01')->get();
	  return $transactions;
	  foreach ($transactions as $trans) {
	    // code...

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date_disbursed;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = 2;
	    $journal->amount = $trans->amount_disbursed;
	    $journal->initiated_by = "System";
	    $journal->type = "debit";
	    $journal->description = "Tyre loan disbursement";
	    $journal->particulars_id = 31;
	    $journal->user_id = Auth::user()->id;
	    $journal->member_id = $trans->member_id;
		$journal->created_at = $trans->created_at;
		$journal->updated_at = $trans->updated_at;
	    $journal->save();

	  }
	  return "tyre 2 Disbursements Transfer Successful";
	}


	public function sales(){
	  $transactions = Journaldump::whereIn('particulars_id', array(41, 46, 47, 85, 87))->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = $trans->account_id;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = $trans->initiated_by;
	    $journal->type = $trans->type;
	    $journal->description = $trans->description;
	    $journal->void = $trans->void;
	    $journal->particulars_id = $trans->particulars_id;
	    $journal->user_id = $trans->user_id;
	    $journal->receiptNo = $trans->receiptNo;
			$journal->created_at = $trans->created_at;
			$journal->updated_at = $trans->updated_at;
	    $journal->member_id = $trans->member_id;
	    $journal->save();

	  }
	  return "Sales Transfer Successful";
	}



	public function expenses(){
		$arr = array(70, 98, 100, 18, 103, 66, 11, 14, 15, 17, 19, 20, 21, 25, 43, 65, 90,
		 62, 91, 61, 60, 22, 102, 99, 107, 101, 69, 67, 64, 16, 63, 40, 68, 108, 89, 5, 101);
	  $transactions = Journaldump::whereIn('particulars_id', $arr)->get();
	  //return sizeof($transactions);
	  foreach ($transactions as $trans) {
	    // code...

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = $trans->account_id;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = $trans->initiated_by;
	    $journal->type = $trans->type;
	    $journal->description = $trans->description;
	    $journal->void = $trans->void;
	    $journal->particulars_id = $trans->particulars_id;
	    $journal->user_id = $trans->user_id;
	    $journal->receiptNo = $trans->receiptNo;
			$journal->created_at = $trans->created_at;
			$journal->updated_at = $trans->updated_at;
	    $journal->member_id = $trans->member_id;
	    $journal->save();

	  }
	  return "Expenses Transfer Successful";
	}


	public function incomes(){
		$arr = array(51, 56, 88, 57, 58, 48, 54, 42, 49, 52, 105, 55, 50, 53, 59, 7);
	  $transactions = Journaldump::whereIn('particulars_id', $arr)->get();
	  //return sizeof($transactions);
	  foreach ($transactions as $trans) {
	    // code...

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = $trans->account_id;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = $trans->initiated_by;
	    $journal->type = $trans->type;
	    $journal->description = $trans->description;
	    $journal->void = $trans->void;
	    $journal->particulars_id = $trans->particulars_id;
	    $journal->user_id = $trans->user_id;
	    $journal->receiptNo = $trans->receiptNo;
			$journal->created_at = $trans->created_at;
			$journal->updated_at = $trans->updated_at;
	    $journal->member_id = $trans->member_id;
	    $journal->save();

	  }
	  return "Incomes Transfer Successful";
	}


	public function contribIncome(){
	  $transactions = Journaldump::where('void', 0)->where('particulars_id', 39)->where('date', '<=', '2018-08-01')->get();
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...

	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = $trans->account_id;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = $trans->initiated_by;
	    $journal->type = $trans->type;
	    $journal->description = $trans->description;
	    $journal->void = $trans->void;
	    $journal->particulars_id = $trans->particulars_id;
	    $journal->user_id = $trans->user_id;
	    $journal->receiptNo = $trans->receiptNo;
			$journal->created_at = $trans->created_at;
			$journal->updated_at = $trans->updated_at;
	    $journal->member_id = $trans->member_id;
	    $journal->save();

	  }
	  return "Contrib Income Transfer Successful";
	}


	public function pastTrans(){
		$arr = array(70, 98, 100, 18, 103, 66, 11, 14, 15, 17, 19, 20, 21, 25, 43, 65, 90,
		 62, 91, 61, 60, 22, 102, 99, 107, 101, 69, 67, 64, 16, 63, 40, 68, 108, 89, 5, 101, 41, 46, 47, 85, 87,
		 39,51, 56, 88, 57, 58, 48, 54, 42, 49, 52, 105, 55, 50, 53, 59, 7);

		$transactions =Journaldump::whereNotIn('particulars_id', $arr)->where('date', '<', '2018-08-01')->where('date', '>', '2017-12-31')->get();
		return $transactions;
	  //return $transactions;
	  foreach ($transactions as $trans) {
	    // code...
	    $journal = new Journal;

	    $trans_no = $journal->getTransactionNumber();
	    $journal->date = $trans->date;
	    $journal->trans_no = $trans_no;
	    $journal->account_id = $trans->account_id;
	    $journal->amount = $trans->amount;
	    $journal->initiated_by = $trans->initiated_by;
	    $journal->type = $trans->type;
	    $journal->description = $trans->description;
	    $journal->void = $trans->void;
	    $journal->particulars_id = $trans->particulars_id;
	    $journal->user_id = $trans->user_id;
	    $journal->receiptNo = $trans->receiptNo;
			$journal->created_at = $trans->created_at;
			$journal->updated_at = $trans->updated_at;
	    $journal->member_id = $trans->member_id;
	    $journal->save();

	  }
	  return "Contrib Income Transfer Successful";
	}


	public function nonReport(){
		$receipts = Vehicletransaction::where('category', 'sv')->lists('receiptno');
		$transactions = Savingtransaction::whereNotIn('batch_transaction_no', $receipts)->get();
		return $transactions;
	}

	public function transferred(){
		$transactions =Journaldump::where('particulars_id', 80)->get();
		return $transactions;
	}
	/**
	 * Show the form for creating a new resource.
	 * GET /datamigration/create
	 *
	 * @return Response
	 */



	public function Vehicle()
	{
		$vehicle = Vehicle::lists('id');

		$missing = Vehicletransaction::whereNotIn('vehicle_id', $vehicle)->lists('vehicle_id');

		$v1 = Vehicle::find(92);
		$v2 = Vehicle::find(117);

		if(empty($v1)){
			$vehicle = new Vehicle;
			$vehicle->id = 92;
			$vehicle->regno = "KAP567P";
			$vehicle->member_id = 265;
			$vehicle->void = 1;
			$vehicle->save();
		}
		if(empty($v2)){
			$vehicle = new Vehicle;
			$vehicle->id = 117;
			$vehicle->regno = "KAL630J";
			$vehicle->member_id = 360;
			$vehicle->void = 1;
			$vehicle->save();
		}

		return "Missing vehicles added";
	}


	public function sumBank()
	{
		$sum = 1300125.78;

		$journal = new Journal;

		$trans_no = $journal->getTransactionNumber();
		$journal->date = '2017-12-31';
		$journal->trans_no = $trans_no;
		$journal->account_id = 12;
		$journal->amount = $sum;
		$journal->initiated_by = 'System';
		$journal->type = "debit";
		$journal->description = "Bank account balance as at 2017-12-31";
		$journal->user_id = Auth::user()->id;
		$journal->save();

		return "Total sum of ".$sum." credited to bank as at 2017-12-31";
	}

	public function sumCash()
	{
		$sum = 885;

		$journal = new Journal;

		$trans_no = $journal->getTransactionNumber();
		$journal->date = '2017-12-31';
		$journal->trans_no = $trans_no;
		$journal->account_id = 17;
		$journal->amount = $sum;
		$journal->initiated_by = 'System';
		$journal->type = "debit";
		$journal->description = "Cash account balance as at 2017-12-31";
		$journal->user_id = Auth::user()->id;
		$journal->save();

		return "Total sum of ".$sum." credited to cash as at 2017-12-31";
	}

	public function sumDev()
	{
		$sum = Loanaccount::where('void', 0)->where('date_disbursed', '<=', '2017-12-31')->where('loanproduct_id', 1)->sum('amount_disbursed');
		$sum = 5456501;

		$journal = new Journal;

		$trans_no = $journal->getTransactionNumber();
		$journal->date = '2017-12-31';
		$journal->trans_no = $trans_no;
		$journal->account_id = 1;
		$journal->amount = $sum;
		$journal->initiated_by = 'System';
		$journal->type = "debit";
		$journal->description = "Developmental Loan amount as at 2017-12-31";
		$journal->user_id = Auth::user()->id;
		$journal->save();

		return "Total sum of ".$sum." credited to Developmental loan account as at 2017-12-31";
	}


	public function sumTyre()
	{
		$sum = Loanaccount::where('void', 0)->where('date_disbursed', '<=', '2017-12-31')->where('loanproduct_id', 2)->sum('amount_disbursed');

		$journal = new Journal;

		$trans_no = $journal->getTransactionNumber();
		$journal->date = '2017-12-31';
		$journal->trans_no = $trans_no;
		$journal->account_id = 2;
		$journal->amount = $sum;
		$journal->initiated_by = 'System';
		$journal->type = "debit";
		$journal->description = "Tyre Loan amount as at 2017-12-31";
		$journal->user_id = Auth::user()->id;
		$journal->save();

		return "Total sum of ".$sum." credited to Tyre loan account as at 2017-12-31";

	}

	public function sumSaving()
	{
		$sum = Savingtransaction::where('void', 0)->where('date', '<=', '2017-12-31')->sum('amount');

		$journal = new Journal;

		$trans_no = $journal->getTransactionNumber();
		$journal->date = '2017-12-31';
		$journal->trans_no = $trans_no;
		$journal->account_id = 24;
		$journal->amount = $sum;
		$journal->initiated_by = 'System';
		$journal->type = "credit";
		$journal->description = "Savings deposit as at 2017-12-31";
		$journal->user_id = Auth::user()->id;
		$journal->save();

		return "Total sum of ".$sum." credited to Savings deposit account as at 2017-12-31";
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /datamigration
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /datamigration/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /datamigration/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /datamigration/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /datamigration/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function correctInterests(){
		$loanaccounts = Loanaccount::where('backdateable', 0)->where('void', 0)->where('loanproduct_id', 1)->get();

		foreach ($loanaccounts as $loanaccount) {
			// code...

			#TODO: change loanarrear to loanBalance
			$loanaccount = Loanaccount::getLoanBalance($loanaccount);
			$startDate = date('Y-m-01', strtotime($loanaccount->date_disbursed));
			$day = date('d', strtotime($loanaccount->date_disbursed));
			$endDuration = date('Y-m-d', strtotime('+1 month', strtotime($startDate)));
			$count = 1;

			if($day < date('t', strtotime($endDuration))){							//if repayment date is less than last day use it
				$endDuration = date('Y-m-'.$day, strtotime($endDuration));
			}else{
				$endDuration = date('Y-m-t', strtotime($endDuration));
			}

			//return $loanaccounts;
			$size = sizeof($loanaccount->dates);
			for ($count = 0; $count < $size; $count++) {
				$arrayData = $loanaccount->dates;
				$endDuration = date('Y-m-d', strtotime('-1 day', strtotime($endDuration)));
				$data = $arrayData[$endDuration];

				$receipt = 'IC'-Receipt::getReceiptno();

				if($data['interestArr'] < 0){
					$transaction = new Loantransaction;
					$transaction->loanaccount()->associate($loanaccount);
					$transaction->date = $endDuration;
					$transaction->description = 'Interest overpayments moved to principal';
					$transaction->amount =(-1 * $data['interestArr']);
					$transaction->type = 'credit';
					$transaction->is_field = 0;
					$transaction->payment_via = "System";
					$transaction->batch_transaction_no = $receipt;
					$transaction->save();

					$repayment = new Loanrepayment;
					$repayment->loanaccount()->associate($loanaccount);
					$repayment->date = $endDuration;
					$repayment->principal_paid = (-1 * $data['interestArr']);
					$repayment->interest_paid = $data['interestArr'];
					$repayment->void = 0;
					$repayment->transaction_id = $transaction->id;
					$repayment->save();
				}

				$endDuration = date('Y-m-d', strtotime('+1 month', strtotime(date('Y-m-01', strtotime($endDuration)))));
				if($day < date('t', strtotime($endDuration))){							//if repayment date is less than last day use it
					$endDuration = date('Y-m-'.$day, strtotime($endDuration));
				}else{
					$endDuration = date('Y-m-t', strtotime($endDuration));
				}

			//	$count++;
				$loanaccount = Loanaccount::getLoanBalance($loanaccount);
			}
	}
		return "Success";
	}

}
