<?php namespace App\Http\Controllers;

use App\models\Account;
use App\models\Branch;
use App\models\Journal;
use App\models\Member;
use App\models\Particular;
use App\models\Savingposting;
use App\models\Savingtransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class JournalsController extends Controller {

	/*
	 * Display a listing of journals
	 *
	 * @return Response
	 */
	public function index()
	{
		$journals = Journal::all();

		return view('journals.index', compact('journals'));
	}

	/*
	 * Show the form for creating a new journal
	 *
	 * @return Response
	 */
	public function create()
	{
		$particulars = Particular::all();
		foreach ($particulars as $key => $particular) {
				if ($particular->name == "Expense (Loan Insurance)" || $particular->id == '32') {
						unset($particulars[$key]);
				}
		}
        /*$accounts = Account::all();
        return View::make('journals.create', compact('accounts','particulars'));*/

		$members = Member::all();
		return view('journals.create', compact('particulars', 'members'));
	}

	/*
	 * Store a newly created journal in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($data = $request->all(), Journal::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$part= $request->input('particular'); if(!isset($part)){$part=1;}


        $partCount = Particular::where('id',$part)->count();

	if(isset($part) && !empty($part) && $partCount>0){
		$particular = Particular::find($part);
       $data = array(
            'date' => $request->input('date'),
            'debit_account' => $particular->debitaccount_id,
            'credit_account' => $particular->creditaccount_id,
            'description' => $request->input('description'),
            'amount' => $request->input('amount'),
            'initiated_by' => $request->input('user'),
            'particulars_id' => $request->input('particular'),
            'narration' => $request->input('narration')
        );
			//return	$this->savingtransactions();

			//	return $data1;
        $journal = new Journal;

        $journal->journal_entry($data);
	}
        if ($request->has('expense')) {
            return Redirect::to('budget/expenses');
        } elseif ($request->has('income')) {
            return Redirect::to('budget/incomes');
        } else {
            return Redirect::route('journals.index');
        }
	}

	/*
	 * Display the specified journal.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$journal = Journal::findOrFail($id);

		return view('journals.show', compact('journal'));
	}

	/*
	 * Show the form for editing the specified journal.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$journal = Journal::find($id);

		return view('journals.edit', compact('journal'));
	}

	/*
	 * Update the specified journal in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$journal = Journal::findOrFail($id);

		$validator = Validator::make($data = $request->all(), Journal::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$branch = Branch::findOrFail($request->input('branch_id'));
		$account = Account::findOrFail($request->input('account_id'));


		$journal->branch()->associate($branch);
		$journal->account()->associate($account);

		$journal->date = $request->input('date');
		$journal->trans_no = $request->input('trans_no');
		$journal->initiated_by = $request->input('initiated_by');
		$journal->amount = $request->input('amount');
		$journal->type = $request->input('type');
		$journal->description = $request->input('description');
		$journal->update();

		return Redirect::route('journals.index');
	}

	/*
	 * Remove the specified journal from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$journal = Journal::findOrFail($id);

		$journal->void = TRUE;
		$journal->update();

		return Redirect::route('journals.index');
	}
public function savingtransactions(Request $request){


	$savingtransactions= Savingtransaction::all();
    return	$savingaccount_id= Savingtransaction::where('savingaccount_id','>','0')->pluck('savingaccount_id');
	$credit_account= Savingposting::where('credit_account','=',$savingaccount_id)->get();
    $debit_account= Savingposting::where('debit_account','=',$savingaccount_id)->get();

    foreach ($savingtransactions as $savingtransaction) {

	$data1 = array(
			 'date' => $request->input('date'),
			 // 'debit_account' => 9,
			 // 'credit_account' => 5,
		 	 'debit_account' => $debit_account,
			 'credit_account' => $credit_account,
			 'description' => $savingtransaction->type,
			 'amount' =>$savingtransaction->amount,
			 'initiated_by' => $request->input('user'),
			 'particulars_id' =>$savingtransaction->id,
			 'narration' => $request->input('narration')
				);
}
    return $data1;
		}

}
