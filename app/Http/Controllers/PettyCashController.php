<?php
namespace App\Http\Controllers;

use App\models\Account;
use App\models\AccountTransaction;
use App\models\CuriousPetty;
use App\models\Journal;
use App\models\Member;
use App\models\Particular;
use App\models\PettycashItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class PettyCashController extends Controller{

    /*
     * Display a listing of expenses
     *
     * @return Response
     */
    public function index(){
        Session::forget('newTransaction');
        Session::forget('trItems');

        $accounts = Account::all();
        $assets = Account::where('category', 'ASSET')->where('name', 'not like', '%'.'Loan'.'%')->get();
        $liabilities = Account::where('category', 'LIABILITY')->get();
        $petty = Account::where('name', 'LIKE', '%petty%')->get();
        $petty_account = Account::where('name', 'LIKE', '%'.'petty cash'.'%')->where('active', 1)->first();

        if($petty_account != null){
            $acID = $petty_account->id;

            $query = new AccountTransaction;
            $ac_transactions = $query->where(function($query) use ($acID){
                $query->where('account_debited', $acID)
                    ->orWhere('account_credited', $acID);
            })->orderBy('transaction_date','DESC')->get();
        }

        //return $ac_transactions;

        return View::make('petty_cash.index', compact('accounts', 'assets', 'liabilities', 'petty', 'petty_account', 'ac_transactions'));
    }

    /*
     * Show the form for creating a new expense
     *
     * @return Response
     */
    public function create()
    {

    }

    /*
     * Store a newly created expense in storage.
     *
     * @return Response
     */
    public function store()
    {

    }

    /*
     * Display the specified expense.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /*
     * Show the form for editing the specified expense.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /*
     * Update the specified expense in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

    }

    /*
     * Remove the specified expense from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

    }


    /*
     * SHOW RECEIPT TRANSACTIONS
     */
    public function receiptTransactions($id){
        $items = PettycashItem::where('ac_trns', $id)->get();

        return View::make('petty_cash.receiptTransactions', compact('items'));
    }


    /*
     * ADD MONEY TO PETTY CASH ACCOUNT
     */
    public function addMoney(Request $request){
        $ac_name = Account::where('id', $request->get('ac_from'))->first();
        $amount = $request->get('amount');
        if(Account::getAccountBalanceAtDate($ac_name, date('Y-m-d')) < $request->get('amount')){
            return Redirect::back()->with('error', 'Insufficient funds in From Account selected!');
        }

        $particular = Particular::where('name', 'like', '%'.'Petty Cash'.'%')->first();
        $data = array(
            'date' => date("Y-m-d"),
            'debit_account' => $request->get('ac_to'),
            'credit_account' => $request->get('ac_from'),
            'description' => "Transferred cash from $ac_name->name account to Petty Cash Account",
            'narration' => 220,
            'particulars_id' => $particular->id,
            'batch_transaction_no' => $request->get('reference'),
            'amount' => $request->get('amount'),
            'initiated_by' => Auth::user()->username
        );

        //DB::table('accounts')->where('id', $request->get('ac_from'))->decrement('balance', $request->get('amount'));
        //DB::table('accounts')->where('id', $request->get('ac_to'))->increment('balance', $request->get('amount'));

        $acTransaction = new AccountTransaction;
        $journal = new Journal;

        $tr = $acTransaction->createTransaction($data);
        $trans_no = $journal->journal_entry($data);

        // $trans = AccountTransaction::find($tr);
        //
        // $trans->journal_trans_no = $trans_no;
        //
        // $trans->update();

        return Redirect::action('PettyCashController@index')->with('success', "KES. $amount Successfully Transferred from $ac_name->name to Petty Cash!");
    }

    /*
     * ADD MONEY TO PETTY CASH FROM OWNER'S CONTRIBUTION
     */
    public function addContribution(Request $request){
        $ac_name = Account::where('id', $request->get('cont_acFrom'))->first();
        $contAmount = $request->get('cont_amount');
        $contName = $request->get('cont_name');

        $particular = Particular::where('name', 'like', '%'.'Petty Cash'.'%')->first();
        if(empty($particular)){
            $particular = new Particular;
            $particular->name = "Petty Cash";
            $particular->save();
        }

        $data = array(
            'date' => date("Y-m-d"),
            'debit_account' => $request->get('cont_acTo'),
            'credit_account' => $request->get('cont_acFrom'),
            'description' => "Transferred Money to Petty Cash Account from $contName",
            'amount' => $request->get('cont_amount'),
            'particulars_id' => $particular->id,
            'initiated_by' => Auth::user()->username
        );

        DB::table('accounts')->where('id', $request->get('cont_acFrom'))
            ->decrement('balance', $request->get('cont_amount'));
        DB::table('accounts')->where('id', $request->get('cont_acTo'))
            ->increment('balance', $request->get('cont_amount'));

        $acTransaction = new AccountTransaction;
        $journal = new Journal;

        $acTransaction->createTransaction($data);
        $journal->journal_entry($data);

        return Redirect::action('PettyCashController@index')->with('success', "KES. $contAmount Transferred to Petty Cash Account from $contName");
    }

    /*
     * CREATE NEW PETTY CASH TRANSACTION
     */
    public function newTransaction(Request $request){

        Session::put('newTransaction', [
            'transactTo'=>$request->get('transact_to'),
            'trDate'=>$request->get('tr_date'),
            'description'=>$request->get('description'),
            'expense_ac'=>$request->get('expense_ac'),
            'credit_ac'=>$request->get('credit_ac')
        ]);

        $newTr = Session::get('newTransaction');
        //return $request->get();
        if($request->get('item') != NULL){
            Session::push('trItems', array(
                'item_name' => Particular::find($request->get('item'))->name,
                'description' => $request->get('desc'),
                'quantity' => $request->get('qty'),
                'unit_price' => $request->get('unit_price'),
                'particulars_id' => $request->get('item'),
                'receipt' => $request->get('receipt')
            ));
        }

        $trItems = Session::get('trItems');

        $particulars = Particular::whereNotNull('debitaccount_id')->whereNotNull('creditaccount_id')->get();
        foreach ($particulars as $key => $particular) {
            if ($particular->name == "Expense (Loan Insurance)" || $particular->id == '32' || $particular->debitAccount->category != 'EXPENSE') {
                unset($particulars[$key]);
            }
        }


        return View::make('petty_cash.transactionItems', compact('newTr', 'particulars','trItems'));
    }


    /*
     * Remove Petty Cash Transaction Item
     */
    public function removeTransactionItem($count){
        /*Session::put('newTransaction', [
            'transactTo'=>$request->get('transact_to'),
            'trDate'=>$request->get('tr_date')
        ]);*/
        $newTr = Session::get('newTransaction');

        $items = Session::get('trItems');
        unset($items[$count]);
        $newItems = array_values($items);
        Session::put('trItems', $newItems);

        //return Session::get('trItems');
        $trItems = Session::get('trItems');
        $particulars = Particular::whereNotNull('debitaccount_id')->whereNotNull('creditaccount_id')->get();
        foreach ($particulars as $key => $particular) {
            if ($particular->name == "Expense (Loan Insurance)" || $particular->id == '32' || $particular->debitAccount->category != 'EXPENSE') {
                unset($particulars[$key]);
            }
        }

        return View::make('petty_cash.transactionItems', compact('newTr', 'trItems', 'particulars'));
    }


    /*
     * Commit Transaction
     */
    public function commitTransaction(){

        $newTr = Session::get('newTransaction');
        $trItems = Session::get('trItems');
        $particulars = Particular::whereNotNull('debitaccount_id')->whereNotNull('creditaccount_id')->get();
        foreach ($particulars as $key => $particular) {
            if ($particular->name == "Expense (Loan Insurance)" ||  $particular->debitAccount->category != 'EXPENSE') {
                unset($particulars[$key]);
            }
        }

        if($trItems == NULL){
            $notice = 'Please select some entries';
            return View::make('petty_cash.transactionItems', compact('newTr', 'trItems','particulars', 'notice'));
        }

        $total = 0;
        foreach($trItems as $trItem){
            $total += ($trItem['quantity'] * $trItem['unit_price']);
        }

        $petty_account = Account::where('name', 'like','%'.'Petty Cash'.'%')->first();

        if($total > Account::getAccountBalanceAtDate($petty_account, date('Y-m-d'))){
            //return Redirect::back()->;
            $warn = 'Not enough funds in petty cash account';
            return View::make('petty_cash.transactionItems', compact('newTr', 'trItems','particulars','warn'));
        }

        $particular = Particular::where('name', 'like', '%'.'Petty Cash'.'%')->first();
        if(empty($particular)){
            $particular = new Particular;
            $particular->name = "Petty Cash";
            $particular->save();
        }

        //DB::table('accounts')->where('id', $newTr['credit_ac'])->decrement('balance', $total);
        //DB::table('accounts')->where('id', $newTr['expense_ac'])->increment('balance', $total);

        $narr = Member::where('name', 'like', '%'.'Cash Customer'.'%')->pluck('id');
        foreach($trItems as $trItem){
            $data = array(
                'date' => $trItem['date'],
                'debit_account' => Particular::find($trItem['particulars_id'])->debitaccount_id,
                'credit_account' => $trItem['credit_ac'],
                'description' => $trItem['description'],
                'narration' => 0,
                'particulars_id' => $trItem['particulars_id'],
                'batch_transaction_no' => $trItem['receipt'],
                'amount' => $trItem['quantity'] * $trItem['unit_price'],
                'initiated_by' => Auth::user()->username
            );



            $acTransaction = new AccountTransaction;
            $journal = new Journal;

            $trId = $acTransaction->createTransaction($data);
            //$trId = $tr->id;
            $trans_no = $journal->journal_entry($data);
            // $tr->journal_trans_no = $trans_no;
            // $tr->update();

            $pettyCashItem = new PettycashItem;

            $pettyCashItem->ac_trns = $trId;
            $pettyCashItem->item_name = $trItem['item_name'];
            $pettyCashItem->description = $trItem['description'];
            $pettyCashItem->quantity = $trItem['quantity'];
            $pettyCashItem->unit_price = $trItem['unit_price'];

            $pettyCashItem->save();
        }

        Session::forget('newTransaction');
        Session::forget('trItems');

        return Redirect::action('PettyCashController@index');
    }

    public function deletePetty($id){
        $ac_trans = AccountTransaction::findOrFail($id);

        if($ac_trans->journal_trans_no != null){
            $journals = Journal::where('trans_no', '=', $ac_trans->journal_trans_no)->get();

            if(sizeof($journals) > 2){
                $curious = new CuriousPetty;
                $curious->pettyId = $id;
                $curious->save();
            }

            foreach ($journals as $journal) {
                $journal->void = 1;
                $journal->update();
            }

        }else{
            $pettyAc = Account::where('name', 'like', '%'.'Petty Cash'.'%')->first();
            if($ac_trans->account_credited == $pettyAc->id)
                $type = 'credit';
            else
                $type = 'debit';

            $pettyentry = Journal::where('account_id', '=', $pettyAc->id)
                ->where('type', $type)
                ->where('date', '=', $ac_trans->transaction_date)
                ->where('amount', '=', $ac_trans->transaction_amount)
                ->where('void', '=', 0)
                ->first();
            if(!empty($pettyentry))
                $journals = Journal::where('trans_no', '=', $pettyentry->trans_no)->get();
            else
                $journals = array();

            if(sizeof($journals) > 2){
                $curious = new CuriousPetty;
                $curious->pettyId = $id;
                $curious->save();
            }

            foreach ($journals as $journal) {
                $journal->void = 1;
                $journal->update();
            }

        }

        $pettyItem = PettycashItem::where('ac_trns', $id)->delete();
        $ac_trans->delete();

        return Redirect::back()->with('success', 'Petty cash entry deleted successfully!');
    }

    public static function createPetty($data, $trans = null){
        if ($trans == null){
            $trans = AccountTransaction::createTransaction($data);
        }

        $pettyCashItem = new PettycashItem;

        $pettyCashItem->ac_trns = $trans->id;
        $pettyCashItem->item_name = Particular::find($data['particulars_id'])->name;
        $pettyCashItem->description = $data['description'];
        $pettyCashItem->quantity = $data['amount'];
        $pettyCashItem->unit_price = 1;

        $pettyCashItem->save();

    }

}
