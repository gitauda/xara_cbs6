<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class VendorsController extends Controller {

	/*
	 * Display a listing of vendors
	 *
	 * @return Response
	 */
	public function index()
	{
		$vendors = Vendor::all();

		return View::make('vendors.index', compact('vendors'));
	}

	/*
	 * Show the form for creating a new vendor
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('vendors.create');
	}

	/*
	 * Store a newly created vendor in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($data = $request->all(), Vendor::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$vendor = new Vendor;

		$vendor->name = $request->get('name');
		$vendor->email = $request->get('email');
		$vendor->phone = $request->get('phone');
		$vendor->description = $request->get('description');
		$vendor->status = $request->get('status');
		$vendor->save();

		return Redirect::route('vendors.index');
	}

	/*
	 * Display the specified vendor.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$vendor = Vendor::findOrFail($id);

		return View::make('vendors.show', compact('vendor'));
	}

	/*
	 * Show the form for editing the specified vendor.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$vendor = Vendor::find($id);

		return View::make('vendors.edit', compact('vendor'));
	}

	/*
	 * Update the specified vendor in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$vendor = Vendor::findOrFail($id);

		$validator = Validator::make($data = $request->all(), Vendor::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$vendor->name = $request->get('name');
		$vendor->email = $request->get('email');
		$vendor->phone = $request->get('phone');
		$vendor->description = $request->get('description');
		$vendor->status = $request->get('status');

		$vendor->update();

		return Redirect::route('vendors.index');
	}

	/*
	 * Remove the specified vendor from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Vendor::destroy($id);

		return Redirect::route('vendors.index');
	}

}
