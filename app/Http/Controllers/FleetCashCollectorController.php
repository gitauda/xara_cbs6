<?php

/**
 * FleetCashController Class
 *
 * Implements actions regarding fleet management
 */
class FleetCashCollectorController extends Controller
{
    /**
    * display a list of collectors
    */
    public function index(){
        $collectors=FleetCashCollector::all();
        return View::make('fleetMgnt.fleetCashCollectors.index',compact('collectors'));
    }

    /**
    * display the edit page
    */
    
    public function show($id){

        $collector = FleetCashCollector::findorfail($id);
        $vehicles = Vehicle::all();
       Audit::logAuditwithfewargs('fleet_cash_collector_view', Confide::User()->username.' viewed collector '.$collector->name, '0');
       return View::make('fleetMgnt.fleetCashCollectors.show', compact('collector','vehicles'));
    }

    public function edit($id){
        $collector = FleetCashCollector::findorfail($id);
        $vehicles = Vehicle::where('id','!=',$collector->vehicle->id)->get(); 
       return View::make('fleetMgnt.fleetCashCollectors.edit', compact('collector','vehicles'));
    }


     /**
    * updates the collections
    */
    public function update($id){

        $collector = FleetCashCollector::findorfail($id);
        //associate($account);
        $collector->name = Input::get('collector');
        $collector->registration_date =  Input::get('reg_date');
        $collector->update();

        Audit::logAuditwithfewargs('fleet_cash_collector_update', Confide::User()->username.' updated collector '.$collector->name, '0');

        return Redirect::route('fleetCashCollectors.index')->with('success','Collector '.$collector->name.' successfully updated');

    }




    /**
     * Displays the form for collection creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {

        $vehicles = Vehicle::all(); 
        
        return View::make('fleetMgnt.fleetCashCollectors.create', compact('vehicles'));
    }

    /**
     * Stores new collection
     *
     * @return  Illuminate\Http\Response
     */
    public function store()
    {

        $collector = new FleetCashCollector;
        //associate($account);
        $collector->name = Input::get('collector');
        $collector->registration_date =  Input::get('reg_date');
        $collector->save();

        Audit::logAuditwithfewargs('fleet_cash_collector_new', Confide::User()->username.' added collector '.$collector->name, '0');

        return Redirect::route('fleetCashCollectors.index')->with('success','Collector '.$collector->name.' successfully added');
    }

    /**
    * Delete the collection
    *
    */

    public function destroy($id){

        $collector = FleetCashCollector::find($id);

        $collector->delete();

        Audit::logAuditwithfewargs('fleet_cash_collector_delete',Confide::User()->username.' deleted collector '.$collector->name, '0');

        return Redirect::route('fleetCashCollectors.index')->with('success','Collector successfully removed');
    }


}