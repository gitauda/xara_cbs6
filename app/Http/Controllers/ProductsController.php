<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\models\Loanproduct;
use App\models\Product;
use App\models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ProductsController extends Controller {

    /*
     * Display a listing of products
     *
     * @return Response
     */
    public function index()
    {
        $products = Product::all();

        return View::make('products.index', compact('products'));
    }

    /*
     * Show the form for creating a new product
     *
     * @return Response
     */
    public function create()
    {

        $vendors = Vendor::all();

        return View::make('products.create', compact('vendors'));
    }

    /*
     * Store a newly created product in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Product::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }


        $file = $request->file('image');

        $filename = str_random(12);

        $ext = $request->file('image')->getClientOriginalExtension();
        $photo = $filename.'.'.$ext;


        $destination = public_path().'/uploads/images';


        $request->file('image')->move($destination, $photo);

        $vendor = Vendor::find($request->get('vendor_id'));

        $product = new Product;

        $product->vendor()->associate($vendor);
        $product->name = $request->get('name');
        $product->image = $photo;
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->status = "active";
        $product->save();

        return Redirect::route('products.index');

    }

    /*
     * Display the specified product.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return View::make('products.show', compact('product'));
    }

    /*
     * Show the form for editing the specified product.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $product = Product::find($id);
        $vendors = Vendor::all();

        return View::make('products.edit', compact('product', 'vendors'));
    }

    /*
     * Update the specified product in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $product = Product::findOrFail($id);

        $validator = Validator::make($data = $request->all(), Product::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $vendor = Vendor::find($request->get('vendor_id'));

        $product->vendor()->associate($vendor);
        $product->name = $request->get('name');

        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->status = 'active';
        $product->update();

        return Redirect::route('products.index');
    }

    /*
     * Remove the specified product from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return Redirect::route('products.index');
    }


    public function shop(){

        $products = Product::all();

        $loans = Loanproduct::all();

        return View::make('shop.index', compact('products', 'loans'));
    }

}
