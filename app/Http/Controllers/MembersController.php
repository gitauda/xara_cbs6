<?php
namespace App\Http\Controllers;

use AfricasTalkingGatewayException;
use App\models\AfricasTalkingGateway;
use App\models\Audit;
use App\models\Bank;
use App\models\BBranch;
use App\models\Branch;
use App\models\Charge;
use App\models\Document;
use App\models\Group;
use App\Http\Controllers\Controller;
use App\models\Kin;
use App\models\Member;
use App\models\Organization;
use App\models\Savingaccount;
use App\models\Savingproduct;
use App\models\Shareaccount;
use App\models\Vehicle;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MembersController extends Controller {

    /*
     * Display a listing of members
     *
     * @return Response
     */
    public function index()
    {
        $members = Member::all();
        return View::make('members.index', compact('members'));
    }


    public function members(){
        //$members = Member::all();
        $pdf = PDF::loadView('pdf.blank')->setPaper('a4','portrait');
        return $pdf->stream('MemberList.pdf');
    }


    /*
     * Show the form for creating a new member
     *
     * @return Response
     */
    public function create()
    {
        $this->middleware('limit');
        $count = DB::table('members')->count();
        $mno   =  001;
        if($count > 0){
            $member = Member::orderBy('id', 'DESC')->first();
            $m = preg_replace('/\D/', '', $member->membership_no);
            $mno   =  sprintf("%03d",$m+1);
        }else{
            $mno   =  001;
        }
        $banks = Bank::all();
        $bbranches = BBranch::all();
        $branches = Branch::all();
        $groups = Group::all();
        $savingproducts = Savingproduct::all();
        $charges = Charge::where('category', 'member')->get();


        return View::make('members.create', compact('branches', 'mno', 'banks', 'bbranches', 'groups', 'savingproducts','charges'));
    }

    /*
     * Store a newly created member in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Member::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }


        $member = new Member;

        if($request->get('branch_id') != null){

            $branch = Branch::findOrFail($request->input('branch_id'));
            $member->branch()->associate($branch);
        }

        if($request->get('group_id') != null){

            $group = Group::findOrFail($request->input('group_id'));
            $member->group()->associate($group);
        }
        if($request->hasFile('photo')){
            $destination = public_path().'/uploads/photos';

            $filename = Str::random(12);

            $ext = $request->file('photo')->getClientOriginalExtension();
            $photo = $filename.'.'.$ext;


            $request->file('photo')->move($destination, $photo);


            $member->photo = $photo;

        }
        if($request->hasFile('signature')){

            $destination = public_path().'/uploads/photos';

            $filename = Str::random(12);

            $ext = $request->file('signature')->getClientOriginalExtension();
            $photo = $filename.'.'.$ext;


            $request->file('signature')->move($destination, $photo);


            $member->signature = $photo;

        }


        $member->name = $request->input('mname');
        $member->id_number = $request->input('mid_number');
        $member->membership_no = $request->input('membership_no');
        if($request->input('member_type') != null){
            $member->member_type= $request->input('member_type');
        }
        $member->phone = $request->input('phone');
        $member->email = $request->input('email');
        $member->address = $request->input('address');
        $member->bank_id = $request->input('bank_id');
        $member->bank_branch_id = $request->input('bbranch_id');
        $member->bank_account_number = $request->input('bank_acc');
        $member->monthly_remittance_amount = $request->input('monthly_remittance_amount');
        $member->gender = $request->input('gender');
        if($request->input('active') == '1'){
            $member->is_active = TRUE;
        } else {
            $member->is_active = FALSE;
        }

        $member->save();
        $member_id = $member->id;

        //if($request->input('share_account') == '1'){
        Shareaccount::createAccount($member_id);
        //}

        $insertedId = $member->id;

        Kin::where('member_id', $insertedId)->delete();
        $name_arr = array($request->input('name'));
        for ($i=0; $i <count($name_arr) ; $i++) {
            # code...

            if(($request->input('name')[$i] != '' || $request->input('name')[$i] != null)){
                $kin = new Kin;
                $kin->member_id=$insertedId;
                $kin->name = $request->input('name')[$i];
                $kin->goodwill = $request->input('goodwill')[$i];
                $kin->rship = $request->input('relationship')[$i];
                $kin->contact = $request->input('contact')[$i];
                $kin->id_number = $request->input('id_number')[$i];

                $kin->save();

            }
            $reg_arr = array($request->input('regno'));
            for ($k=0; $k <count($reg_arr) ; $k++) {
                # code...
                if(($request->input('regno')[$k] != '' || $request->input('regno')[$k] != null)){
                    $vehicle = new Vehicle;
                    $vehicle->member_id=$insertedId;
                    $vehicle->regno = $request->input('regno')[$k];
                    $vehicle->make = $request->input('make')[$k];

                    $charge = Charge::find($request->input('fee')[$k]);
                    if(!empty($charge))
                    {
                        $vehicle->registration_fee = $charge->amount;
                    }

                    $vehicle->save();

                    //Charge::applyFee($request->input('fee')[$k]);

                }
            }



        }

        $files = $request->file('path');
        $j = 0;

        if($files != '' || $files != null){

            foreach($files as $file){

                if ( $request->hasFile('path')){
                    $document= new Document;

                    $document->member_id = $insertedId;
                    $name = $file->getClientOriginalName();
                    $file = $file->move('public/uploads/documents/', $name);
                    $input['file'] = '/public/uploads/documents/'.$name;
                    $extension = pathinfo($name, PATHINFO_EXTENSION);
                    $document->path = $name;
                    $document->save();
                    $j=$j+1;
                }
            }

        }


        Audit::logAudit(date('Y-m-d'), Auth::user()->username, 'member creation', 'Member', '0');


        return Redirect::route('members.index');
    }

    /*
     * Display the specified member.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $member = Member::findOrFail($id);

        $documents = $id ? Document::where('employee_id',$id)->get() : Document::where('member_id',$id);
        #echo '<pre>'; print_r($member->kin); echo '</pre>'; die;
        $savingaccounts = $member->savingaccounts;
        $shareaccount = $member->shareaccount;

        if(Auth::user()->user_type == 'member'){
            return View::make('css.showmembercss',compact('member', 'savingaccounts', 'shareaccount'));
        }else{
            return View::make('members.show',	compact('member', 'documents', 'savingaccounts', 'shareaccount'));
        }
    }

    /*
     * Show the form for editing the specified member.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        $branches = Branch::all();
        $kins = Kin::where('member_id',$id)->get();
        $banks = Bank::all();
        $bbranches = BBranch::where('bank_id',$member->bank_id)->get();
        $documents = Document::where('member_id',$id)->get();
        $vehicles = Vehicle::where('member_id',$id)->get();
        $count = Document::where('member_id',$id)->count();
        $countk = Kin::where('member_id',$id)->count();
        $countv = Vehicle::where('member_id',$id)->count();
        $groups = Group::all();
        $charges = Charge::where('category', 'member')->get();

        if(Auth::user()->hasRole('member')){
            return View::make('css.editmembercss', compact('charges','member', 'vehicles', 'countk', 'countv', 'kins','banks', 'bbranches','count', 'documents', 'branches', 'groups'));
        }else{

            return View::make('members.edit', compact('charges','member', 'vehicles','countk', 'countv','kins','banks', 'bbranches','count', 'documents', 'branches', 'groups'));
        }
    }



    /*
     * Update the specified member in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $member = Member::findOrFail($id);
        $data = $request->all();
        #echo "<pre>"; print_r($data); echo "</pre>"; die();

        $validator = Validator::make($data, Member::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        if($request->get('branch_id') != null){

            $branch = Branch::findOrFail($request->get('branch_id'));
            $member->branch()->associate($branch);
        }

        if($request->get('group_id') != null){

            $group = Group::findOrFail($request->get('group_id'));
            $member->group()->associate($group);
        }

        //$member->photo = $request->input('photo');
        //$member->signature = $request->input('signature');

        if($request->hasFile('photo')){

            $destination = public_path().'/uploads/photos';

            $filename = Str::random(12);

            $ext = $request->file('photo')->getClientOriginalExtension();
            $photo = $filename.'.'.$ext;


            $request->file('photo')->move($destination, $photo);


            $member->photo = $photo;



        }


        if($request->hasFile('signature')){

            $destination = public_path().'/uploads/photos';

            $filename = Str::random(12);

            $ext = $request->file('signature')->getClientOriginalExtension();
            $photo = $filename.'.'.$ext;


            $request->file('signature')->move($destination, $photo);
            $member->signature = $photo;

        }


        $member->name = $request->input('mname');
        $member->id_number = $request->input('mid_number');
        $member->membership_no = $request->input('membership_no');
        if($request->input('member_type') != null){
            $member->member_type= $request->input('member_type');
        }
        $member->phone = $request->input('phone');
        $member->email = $request->input('email');
        $member->address = $request->input('address');
        $member->bank_id = $request->input('bank_id');
        $member->bank_branch_id = $request->input('bbranch_id');
        $member->bank_account_number = $request->input('bank_acc');
        $member->monthly_remittance_amount = $request->input('monthly_remittance_amount');
        $member->gender = $request->input('gender');
        $member->update();

        $insertedId = $member->id;


        Kin::where('member_id', $insertedId)->delete();
        #TODO: check if the name is an array first
        $name_arr1 = $request->input('name');
        for ($i=0; $i < count($name_arr1) ; $i++) {
            # code...

            if(($request->input('name')[$i] != '' || $request->input('name')[$i] != null)){
                $kin = new Kin;
                $kin->member_id=$insertedId;
                $kin->name = $request->input('name')[$i];
                $kin->goodwill = $request->input('goodwill')[$i];
                $kin->rship = $request->input('relationship')[$i];
                $kin->contact = $request->input('contact')[$i];
                $kin->id_number = $request->input('id_number')[$i];

                $kin->save();

            }
        }


        $vehiclecount = Vehicle::where('member_id', $insertedId)->count();
        // dd($request->input('vid')[0]);
        if($vehiclecount > 0){

            for ($v=0; $v<count($request->input('vid')) ; $v++) {
                # code...

                $veh = Vehicle::find($request->input('vid')[$v]);
                $veh->regno = $request->input('regnoupd')[$v];
                $veh->make = $request->input('makeupd')[$v];

                $veh->update();
            }

        }

        for ($k=0; $k <count($request->input('regno')) ; $k++) {
            # code...

            if(($request->input('regno')[$k] != '' || $request->input('regno')[$k] != null)){
                $vehicle = new Vehicle;
                $vehicle->member_id=$insertedId;
                $vehicle->regno = $request->input('regno')[$k];
                $vehicle->make = $request->input('make')[$k];

                $vehicle->save();

            }
        }


        $files = $request->file('path');
        $j = 0;

        if($files != '' || $files != null){

            foreach($files as $file){

                if ( $request->hasFile('path')){
                    $document= new Document;

                    $document->member_id = $insertedId;
                    $name = $file->getClientOriginalName();
                    $file = $file->move('public/uploads/documents/', $name);
                    $input['file'] = '/public/uploads/documents/'.$name;
                    $extension = pathinfo($name, PATHINFO_EXTENSION);
                    $document->path = $name;
                    $document->save();
                    $j=$j+1;
                }
            }

        }
        $email = $member->email;
        $phone = $member->phone;
        $name = $member->name;
        $organization = Organization::find(1);

        if($phone != null){

            include(base_path() . '/resources/views/AfricasTalkingGateway.php');
            //App::make('App\AfricasTalkingGateway');
            // Specify your login credentials

            /**TODO UPDATE THESE KEYS */
            $username   = "lixnet";
            $apikey     = "a8d19ab5cfe8409bf737a4ef53852ab515560e31fcac077c3e6bb579cc2681e6";
            // Specify the numbers that you want to send to in a comma-separated list
            // Please ensure you include the country code (+254 for Kenya in this case)
            $recipients = $phone;
            // And of course we want our recipients to know what we really do
            $message    = "Hello ".$name.",
			Your self service portal account has been activated
			Below is your login credentials:
			1. Username - ".$member->membership_no."

			Regards,
			".$organization->name;
            // Create a new instance of our awesome gateway class
            $gateway    = new AfricasTalkingGateway($username, $apikey);
            // Any gateway error will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block
            try
            {
                // Thats it, hit send and we'll take care of the rest.
                $results = $gateway->sendMessage($recipients, $message);




                foreach($results as $result) {
                    // status is either "Success" or "error message"
                    echo " Number: " .$result->number;
                    echo " Status: " .$result->status;
                    echo " MessageId: " .$result->messageId;
                    echo " Cost: "   .$result->cost."\n";
                }
            }
            catch ( AfricasTalkingGatewayException $e )
            {
                echo "Encountered an error while sending: ".$e->getMessage();
            }}

        if(Auth::user()->user_type == 'member'){
            return Redirect::to('/member')->withFlashMessage('You have successfully updated your membership details!');
        }else{
            return Redirect::route('members.index');
        }

    }


    /*
     * Remove the specified member from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Member::destroy($id);

        return Redirect::route('members.index');
    }



    public function loanaccounts($id)
    {
        $member = Member::findOrFail($id);

        return View::make('members.loanaccounts', compact('member'));
    }


    public function activateportal($id){

        $member = Member::find($id);
        $organization = Organization::find(1);


        $password = strtoupper(Str::random(8));



        $email = $member->email;
        $phone = $member->phone;
        $name = $member->name;

        //  if($phone != null){

        /*include(app_path() . '\views\AfricasTalkingGateway.php');
    // Specify your login credentials
    $username   = "kenkode";
    $apikey     = "7876fef8a4303ec6483dfa47479b1d2ab1b6896995763eeb620b697641eba670";
    // Specify the numbers that you want to send to in a comma-separated list
    // Please ensure you include the country code (+254 for Kenya in this case)
    $recipients = $phone;
    // And of course we want our recipients to know what we really do
    $message    = "Hello ".$name.",
    Your self service portal account has been activated
    Below is your login credentials:
    1. Username - ".$member->membership_no."
    2. Password - ".$password."
    Regards,
    ".$organization->name;
    // Create a new instance of our awesome gateway class
    $gateway    = new AfricasTalkingGateway($username, $apikey);
    // Any gateway error will be captured by our custom Exception class below,
    // so wrap the call in a try-catch block
    try
    {
      // Thats it, hit send and we'll take care of the rest.
      $results = $gateway->sendMessage($recipients, $message);
      DB::table('users')->insert(
	  array('email' => $member->email,
	  'phone' => $member->phone,
	  'username' => $member->membership_no,
	  'password' => Hash::make($password),
	  'user_type'=>'member',
	  'confirmation_code'=> md5(uniqid(mt_rand(), true)),
	  'confirmed'=> 1
		)
);


        $member->is_css_active = true;
		$member->update();

       return Redirect::back()->with('notice', 'Member has been activated and login credentials sent to their phone number');

      /*foreach($results as $result) {
        // status is either "Success" or "error message"
        echo " Number: " .$result->number;
        echo " Status: " .$result->status;
        echo " MessageId: " .$result->messageId;
        echo " Cost: "   .$result->cost."\n";
      }*/
        /* }
         catch ( AfricasTalkingGatewayException $e )
         {
           echo "Encountered an error while sending: ".$e->getMessage();
         }*/

        if($email != null){

            DB::table('users')->insert(
                array('email' => $member->email,
                    'username' => $member->membership_no,
                    'password' => Hash::make($password),
                    'user_type'=>'member',
                    'confirmation_code'=> md5(uniqid(mt_rand(), true)),
                    'confirmed'=> 1
                )
            );

            $member->is_css_active = true;
            $member->update();





            Mail::send( 'emails.password', array('password'=>$password, 'name'=>$name), function( $message ) use ($member)
            {
                $message->to($member->email )->subject( 'Self Service Portal Credentials' );
            });


            return Redirect::back()->with('notice', 'Member has been activated and login credentials sent to their email address');
        }

        else{

            return Redirect::back()->with('notice', 'Member has not been activated kindly update phone number');
        }
    }


    public function config()
    {
        $inactivity_duration= DB::table('configuration')->where('id', '=', 1)->pluck('value');
        return view('members/config',compact('inactivity_duration'));
    }

    public function configure(Request $request)
    {
        $iduration= $request->get('inactivity_duration');
        if(!empty($iduration)){
            DB::update('update x_configuration set value = ? where id = ?',[$iduration,1]);
        }
        $inactivity_duration=DB::table('configuration')->where('id', '=', 1)->pluck('value');
        return view('members/config',compact('inactivity_duration'));
    }

    public function deactivateportal($id){


        $member = Member::find($id);

        DB::table('users')->where('username', '=', $member->membership_no)->delete();

        $member->is_css_active = false;
        $member->update();

        $email=$member->email;
        $password=strtoupper(Str::random(6));
        $name=$member->name;


        /*if($member->phone != null){

        include(app_path() . '\views\AfricasTalkingGateway.php');
        // Specify your login credentials
        $username   = "kenkode";
        $apikey     = "7876fef8a4303ec6483dfa47479b1d2ab1b6896995763eeb620b697641eba670";
        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        $recipients = $phone;
        // And of course we want our recipients to know what we really do
        $message    = "Hello ".$name.",
        Your self service portal account has been deactivated
        Regards,
        ".$organization->name;
        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);
        // Any gateway error will be captured by our custom Exception class below,
        // so wrap the call in a try-catch block
        try
        {
          // Thats it, hit send and we'll take care of the rest.
          $results = $gateway->sendMessage($recipients, $message);

           return Redirect::back()->with('notice', 'Member has been successfully deactivated');

          /*foreach($results as $result) {
            // status is either "Success" or "error message"
            echo " Number: " .$result->number;
            echo " Status: " .$result->status;
            echo " MessageId: " .$result->messageId;
            echo " Cost: "   .$result->cost."\n";
          }*/
        /* }
         catch ( AfricasTalkingGatewayException $e )
         {
           echo "Encountered an error while sending: ".$e->getMessage();
         }

             if($email != null){

             DB::table('users')->insert(
         array('email' => $member->email,
           'username' => $member->membership_no,
           'password' => Hash::make($password),
           'user_type'=>'member',
           'confirmation_code'=> md5(uniqid(mt_rand(), true)),
           'confirmed'=> 1
             )
     );
     */





        Mail::send( 'emails.deactivate', array('name'=>$name), function( $message ) use ($member)
        {


            //$message->from ('kevohm.km@gmail.com','Kelvin Gikonyo');

            $message->to($member->email )->subject( 'Self Service Portal Deactivation' );
        });


        return Redirect::back()->with('notice', 'Member has been successfully deactivated');



    }

    public function savingtransactions($acc_id){
        $account = Savingaccount::findorfail($acc_id);
        $balance = Savingaccount::getAccountBalance($account);
        return View::make('css.savingtransactions', compact('account', 'balance'));
    }
    public function loanaccounts2(){
        $mem = Auth::user()->username;
        $id = DB::table('members')->where('membership_no', '=', $mem)->pluck('id');
        $member = Member::findOrFail($id);
        return View::make('css.loanaccounts', compact('member'));
    }
    public function deletedoc(Request $request){

        //$id = DB::table('members')->where('membership_no', '=', $mem)->pluck('id');\

        Document::destroy($request->input('id'));

        return 0;

    }

    public function loanHistory($id) {
        $member = Member::findOrFail($id);
        return View::make('members.loanhistory', compact('member'));
    }

    public function loanHistoryReport($id) {
        $organization = Organization::find(1);
        $member = Member::findOrFail($id);

        $pdf = PDF::loadView('pdf.loanreports.loanhistory', compact('organization','member'))->setPaper('a4','portrait');
        return $pdf->stream('Remittance.pdf');
    }

    public function summary($id)
    {
        $member = Member::findOrFail($id);
        return View::make('members.summary', compact('member'));
    }
}
