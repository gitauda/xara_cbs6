<?php namespace App\Http\Controllers;

use App\models\Account;
use App\models\Contribution;
use App\Http\Controllers\Controller;
use App\models\Journal;
use App\models\Loanproduct;
use App\models\Loanrepayment;
use App\models\Loantransaction;
use App\models\Savingaccount;
use App\models\Savingtransaction;
use App\models\Shareaccount;
use App\models\Sharetransaction;
use App\models\Vehicle;
use App\models\Vehicleincome;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class VehicleIncomesController extends Controller {

    /*
     * Display a listing of accounts
     *
     * @return Response
     */
    public function index()
    {
        $vehicles = Vehicleincome::all();

        return View::make('vehicleincomes.index', compact('vehicles'));
    }

    /*
     * Show the form for creating a new account
     *
     * @return Response
     */
    public function create()
    {
        $vehicles = Vehicle::all();
        $ainc     = Account::where('category','LIABILITY')->get();
        $asset    = Account::where('category','ASSET')->get();
        $equities    = Account::where('category','EQUITY')->get();
        $loanproducts    = Loanproduct::all();

        return view('vehicleincomes.create', compact('vehicles','ainc','asset','equities','loanproducts'));
    }

    /*
     * Store a newly created account in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //return $request->input('eq_id');
        $validator = Validator::make($data = $request->all(), Vehicleincome::$rules);
        #echo '<pre>'; print_r($data); echo '</pre>'; die();
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        /*Record Additional Payments*/
        $assign = Vehicle::where('id',$request->input('vehicle_id'))->first();
        $sum_amount=0;
        for ($i=0; $i < count($data['payment_names']) ; $i++) {
            if(($data['payment_names'][$i] != '' ||
                $data['payment_names'][$i] != null)){
                $date = $request->input('date');

                $saving = Savingaccount::where('member_id',$assign->member_id) ->first();

                $savingaccount = Savingaccount::findOrFail($saving->id);

                $date = $request->input('date');

                $saving_amount =  $data['amount'][$i];
                $type = "credit";
                $description = $data['payment_names'][$i]. " from vehicle income";
                $transacted_by = Auth::user()->username;
                $category = 'Vehicle';
                $member = $assign->member_id;

                $savingtransaction = new Savingtransaction;

                $savingtransaction->date = $date;
                $savingtransaction->savingaccount()->associate($savingaccount);
                $savingtransaction->amount = $saving_amount;
                $savingtransaction->type = $type;
                $savingtransaction->description = $description;
                $savingtransaction->transacted_by = $transacted_by;
                $savingtransaction->void = 0;
                if($type == 'credit'){
                    $savingtransaction->payment_via = $category;
                }
                $savingtransaction->save();

                $vehsav = Vehicleincome::where('id',$assign->id)->first();
                $vehsav->savingtransaction_id = $savingtransaction->id;
                $vehsav->update();
                $vid = $assign->id;

                Savingtransaction::vtransact($date, $savingaccount, $category, $saving_amount, $type, $description, $transacted_by,$member,$vid);
            }
            $sum_amount+=$data['amount'][$i];
        }
        // check if code exists
        $total_amount= $request->get('offamt') +
            $request->get('fee_amount') +
            $request->get('shares') +
            $request->get('petrol_investment') +
            $request->get('loans') +
            $request->get('savings') +
            $request->get('insurance')+
            $sum_amount;

        $assign = Vehicle::where('id',$request->input('vehicle_id'))->first();
        /*Record Total Income*/
        $vehicle = new Vehicleincome;
        $vehicle->vehicle_id = $assign->id;
        $vehicle->member_id = $assign->member_id;
        $vehicle->amount  = $total_amount;
        $vehicle->date = $request->input('date');
        $vehicle->asset_account_id = $request->input('asset_id');
        $vehicle->income_account_id = $request->input('ainc_id');
        $vehicle->equity_account_id = $request->input('eq_id');
        $vehicle->save();

        $cont = 0;
        /*Record Vehicle Daily Contribution*/
        if(!empty($request->input('offamt')) ){
            $offm = str_replace( ',', '', $request->input('offamt'));
            $veh = Vehicle::findOrFail($request->input('vehicle_id'));

            $contribution = new Contribution;
            $contribution->member_id = $veh->member_id;
            $contribution->vehicleincome_id = $vehicle->id;
            $contribution->amount = str_replace( ',', '', $request->input('offamt'));
            $contribution->date = $request->input('date');
            $contribution->type= 'credit';
            $contribution->is_void = 0;
            $contribution->save();

            $cont = $contribution->id;
        }
        /*Record Savings/Commissions*/
        $date = $request->input('date');
        $saving = Savingaccount::where('member_id',$assign->member_id)
            ->first();
        $savingaccount = Savingaccount::findOrFail($saving->id);
        $date = $request->input('date');

        $saving_amount = $request->input('savings');
        $type = "credit";
        $description = "Savings/Commissions from vehicle income";
        $transacted_by = Auth::user()->username;
        $category = 'Vehicle';
        $member = $assign->member_id;

        $savingtransaction = new Savingtransaction;
        $savingtransaction->date = $date;
        $savingtransaction->savingaccount()->associate($savingaccount);
        $savingtransaction->amount = $saving_amount;
        $savingtransaction->type = $type;
        $savingtransaction->description = $description;
        $savingtransaction->transacted_by = $transacted_by;
        $savingtransaction->void = 0;
        if($type == 'credit'){
            $savingtransaction->payment_via = $category;
        }
        $savingtransaction->save();

        $vehsav = Vehicleincome::where('id',$vehicle->id)->first();
        $vehsav->savingtransaction_id = $savingtransaction->id;
        $vehsav->update();
        $vid = $vehicle->id;

        Savingtransaction::vtransact($date, $savingaccount, $category, $saving_amount, $type, $description, $transacted_by,$member,$vid);
        /*Record Loan Repayment*/
        if(!empty($request->input('loanproduct_id'))){
            if(!empty($request->input('loans')) ){
                $loanamt = str_replace( ',', '', $request->input('loans'));
            }else{
                $loanamt = 0.00;
            }
            $loanaccount_id = $request->input('loanproduct_id');
            Loanrepayment::vrepayLoan($date,$loanamt,$loanaccount_id,$member,$vid);
        }
        /*Record Shares*/
        if($request->input('shares') != 0.00 || !empty($request->input('shares'))){
            $share = Shareaccount::where('member_id',$assign->member_id)
                ->first();
            $shareaccount = Shareaccount::findOrFail($share->id);
            /*Record the share transaction*/
            $sharetransaction = new Sharetransaction;
            $sharetransaction->date = date("Y-m-d");
            $sharetransaction->shareaccount()->associate($shareaccount);
            if(!empty($request->input('shares'))){
                $sharetransaction->amount =$request->input('shares');
            }
            $sharetransaction->amount = 0;
            $sharetransaction->type = "credit";
            $sharetransaction->pay_for='shares';
            $sharetransaction->description = "Shares from vehicle income";
            $sharetransaction->save();

            $vehshare = Vehicleincome::where('id',$vehicle->id)->first();
            $vehshare->sharetransaction_id = $sharetransaction->id;
            $vehshare->update();
        }
        /*Petrol Station Investment*/
        $share3 = Shareaccount::where('member_id',$assign->member_id)
            ->first();
        $shareaccount3 = Shareaccount::findOrFail($share3->id);
        $sharetransaction3 = new Sharetransaction;
        $sharetransaction3->date = date("Y-m-d");
        $sharetransaction3->shareaccount()->associate($shareaccount3);

        if(!empty($request->input('petrol_investment'))){
            $sharetransaction3->amount =$request->input('petrol_investment');
        }

        $sharetransaction3->amount =0;
        $sharetransaction3->type = "credit";
        $sharetransaction3->pay_for='petrol';
        $sharetransaction3->description = "Petrol Station Investment";
        $sharetransaction3->save();

        $vehshare3 = Vehicleincome::where('id',$vehicle->id)->first();
        $vehshare3->sharetransaction_id = $sharetransaction3->id;
        $vehshare3->update();
        /*Pick Accounts*/
        $acc = Account::where('name','Cash Account')->first();
        if($request->input('petrol_investment') > 0 ){
            $data = array(
                'date' => $data['date'],
                'debit_account' =>$request->input('asset_id'),
                'credit_account' => $request->input('eq_id'),
                'description' => 'Petrol Station Investment',
                'amount' => $request->input('petrol_investment'),
                'initiated_by' => Auth::user()->username,
                'cid' => $cont
            );
            $journal = new Journal;
            /**TODO create journal_contentry */
            $journal->journal_entry($data);
        }
        /*Insurance Payment*/
        $share33 = Shareaccount::where('member_id',$assign->member_id)
            ->first();
        $shareaccount33 = Shareaccount::findOrFail($share33->id);
        $sharetransaction33 = new Sharetransaction;
        $sharetransaction33->date = date("Y-m-d");
        $sharetransaction33->shareaccount()->associate($shareaccount33);
        if(!empty($request->input('insurance'))){
            $sharetransaction33->amount =$request->input('insurance');
        }
        $sharetransaction33->amount = 0;
        $sharetransaction33->type = "credit";
        $sharetransaction33->pay_for='others';
        $sharetransaction33->description = "Insurance Payment";
        $sharetransaction33->save();

        $vehshare33 = Vehicleincome::where('id',$vehicle->id)->first();
        $vehshare33->sharetransaction_id = $sharetransaction33->id;
        $vehshare33->update();
        //$acc= Account::where('category','=','Cash Account')->first();
        /*Membership Fee Payment*/
        $share333 = Shareaccount::where('member_id',$assign->member_id)
            ->first();
        $shareaccount333 = Shareaccount::findOrFail($share333->id);
        $sharetransaction333 = new Sharetransaction;
        $sharetransaction333->date = date("Y-m-d");
        $sharetransaction333->shareaccount()->associate($shareaccount333);
        if(!empty($request->input('fee_amount'))){
            $sharetransaction333->amount =$request->input('fee_amount');
        }
        $sharetransaction333->amount = 0;
        $sharetransaction333->type = "credit";
        $sharetransaction333->pay_for= 'membership';
        $sharetransaction333->description = "Membership Fee Payment";
        $sharetransaction333->save();

        $vehshare333 = Vehicleincome::where('id',$vehicle->id)->first();
        $vehshare333->sharetransaction_id = $sharetransaction333->id;
        $vehshare333->update();
        /*Pick Accounts*/
        $acc = Account::where('name','Cash Account')->first();
        if($request->input('offamt') > 0 ){
            $dt = array(
                'member_id'=>$assign->member_id,
                'date'=>$request->input('date'),
                'amount'=>$request->input('offamt'),
                'type'=>'debit'
            );
        }
        //Contribution::nadd($dt, $request->input('asset_id'), $request->input('ainc_id') );
        return Redirect::route('vehicleincomes.index');
    }

    /**
     * Display the specified account.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $vehicle = Vehicle::findOrFail($id);
        return View::make('vehicleincomes.show', compact('vehicle'));
    }

    /**
     * Show the form for editing the specified account.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $vehicle      = Vehicleincome::find($id);
        $veh          = Vehicle::where('id',$vehicle->vehicle_id)->first();
        $loanproducts = DB::table('loanaccounts')
            ->join('loanproducts', 'loanaccounts.loanproduct_id', '=', 'loanproducts.id')
            ->where('loanaccounts.member_id',$vehicle->member_id)
            ->where('loanaccounts.is_approved',1)
            ->where('loanaccounts.is_rejected',0)
            ->select('loanaccounts.id as id','loanproducts.name as name')
            ->get();
        $ainc        = Account::where('category','LIABILITY')->get();
        $asset       = Account::where('category','ASSET')->get();
        $equities    = Account::where('category','EQUITY')->get();
        $savingtransaction     = Savingtransaction::where('id',$vehicle->savingtransaction_id)->first();
        $sharetransaction      = Sharetransaction::where('id',$vehicle->sharetransaction_id)->first();
        $loantransaction       = Loantransaction::where('id',$vehicle->loantransaction_id)->first();
        $officecontribution    = Journal::where('id',$vehicle->equity_credit_journal_id)->first();
        $vehs = Vehicle::all();
        return View::make('vehicleincomes.edit', compact('veh','vehicle','loanproducts','equities','vehs','ainc','asset','savingtransaction','sharetransaction','loantransaction','officecontribution'));
    }

    /**
     * Update the specified account in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $vehicle = Vehicleincome::findOrFail($id);
        $validator = Validator::make($data = $request->all(), Vehicleincome::$rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $vid = $id;

        $assign = Vehicle::where('id',$request->input('vehicle_id'))->first();
        $vehicle->vehicle_id = $request->input('vehicle_id');
        $vehicle->amount     = str_replace( ',', '', $request->input('amount'));
        $vehicle->date = $request->input('date');
        $vehicle->asset_account_id = $request->input('asset_id');
        $vehicle->income_account_id = $request->input('ainc_id');
        $vehicle->equity_account_id = $request->input('eq_id');
        $vehicle->update();
        $lamt = 0.00;

        if($request->input('loanproduct_id') != '' ){
            $lamt = str_replace( ',', '', $request->input('loans'));
        }else{
            $lamt = 0.00;
        }

        if($request->input('offamt') > 0 ){
            $offm = str_replace( ',', '', $request->input('offamt'));


            $cont = Contribution::where('vehicleincome_id',$id)->first();
            $cont->is_void = 1;
            $cont->update();

            if($cont->debit_journal_id != null && $cont->credit_journal_id != null){
                $journal = Journal::findOrFail($cont->credit_journal_id);
                $journal->void = 1;
                $journal->update();

                $journal = Journal::findOrFail($cont->debit_journal_id);
                $journal->void = 1;
                $journal->update();
            }

            $contribution = new Contribution;
            $contribution->member_id = $assign->member_id;
            $contribution->vehicleincome_id = $id;
            $contribution->amount = str_replace( ',', '', $request->input('offamt'));
            $contribution->date = $request->input('date');

            if($request->input('reasons') != null || $request->input('reasons') != ''){
                $contribution->type = 'credit';
            }else{
                $contribution->type = 'debit';
            }
            $contribution->is_void = 0;
            $contribution->reason = $request->input('reasons');
            $contribution->save();
        }else if($request->input('offamt') <= 0 ){
            $offm = 200;

            $veh = Vehicle::findOrFail($request->input('vehicle_id'));

            $cont = Contribution::where('vehicleincome_id',$id)->first();
            $cont->is_void = 1;
            $cont->update();

            if($cont->debit_journal_id != null && $cont->credit_journal_id != null){
                $journal = Journal::findOrFail($cont->credit_journal_id);
                $journal->void = 1;
                $journal->update();

                $journal = Journal::findOrFail($cont->debit_journal_id);
                $journal->void = 1;
                $journal->update();
            }

            $contribution = new Contribution;
            $contribution->member_id = $assign->member_id;
            $contribution->vehicleincome_id = $id;
            $contribution->amount = str_replace( ',', '', $request->input('offamt'));
            $contribution->date = $request->input('date');
            if($request->input('reasons') != null || $request->input('reasons') != ''){
                $contribution->type = 'credit';
            }else{
                $contribution->type = 'debit';
            }
            $contribution->is_void = 0;
            $contribution->reason = $request->input('reasons');
            $contribution->save();
        }

        $amt = str_replace( ',', '', $request->input('amount'))-$lamt-$offm-str_replace( ',', '', $request->input('shares'));

        $date = $request->input('date');
        $transAmount = $amt;

        $saving = Savingaccount::where('member_id',$assign->member_id)->first();

        $savingaccount = Savingaccount::findOrFail($saving->id);
        $date = $request->input('date');
        $amount = 0.00;

        $amount = $amt;
        $type = "credit";
        $description = "Savings/Commissions from vehicle income";
        $transacted_by = Auth::user()->username;
        $category = 'Vehicle';
        $member = $assign->member_id;

        $saving = Savingtransaction::findOrFail($vehicle->savingtransaction_id);
        $saving->void = 1;
        $saving->update();

        $savingtransaction = new Savingtransaction;
        $savingtransaction->date = $date;
        $savingtransaction->savingaccount()->associate($savingaccount);
        $savingtransaction->amount = $amount;
        $savingtransaction->type = $type;
        $savingtransaction->description = $description;
        $savingtransaction->transacted_by = $transacted_by;
        $saving->void = 0;
        if($type == 'credit'){
            $savingtransaction->payment_via = $category;
        }
        $savingtransaction->save();

        $vehsav = Vehicleincome::where('id',$id)->first();
        $vehsav->savingtransaction_id = $savingtransaction->id;
        $vehsav->update();
        $vid = $vehicle->id;

        $journal = Journal::findOrFail($vehicle->savings_credit_journal_id);
        $journal->void = 1;
        $journal->update();

        $journal = Journal::findOrFail($vehicle->savings_debit_journal_id);
        $journal->void = 1;
        $journal->update();

        Savingtransaction::vtransact($date, $savingaccount, $category, $amount, $type, $description, $transacted_by,$member,$vid);

        if($request->input('loanproduct_id') != '' ){
            $loanamt = str_replace( ',', '', $request->input('loans'));

            $loanaccount_id = $request->input('loanproduct_id');

            $journal = Journal::findOrFail($vehicle->interest_credit_journal_id);
            $journal->void = 1;
            $journal->update();

            $journal = Journal::findOrFail($vehicle->interest_debit_journal_id);
            $journal->void = 1;
            $journal->update();

            $journal = Journal::findOrFail($vehicle->principal_credit_journal_id);
            $journal->void = 1;
            $journal->update();

            $journal = Journal::findOrFail($vehicle->principal_debit_journal_id);
            $journal->void = 1;
            $journal->update();

            $journal = Loanrepayment::findOrFail($vehicle->loanrepayment_principal_id);
            $journal->void = 1;
            $journal->update();

            $journal = Loanrepayment::findOrFail($vehicle->loanrepayment_interest_id);
            $journal->void = 1;
            $journal->update();

            $journal = Loantransaction::findOrFail($vehicle->loantransaction_id);
            $journal->void = 1;
            $journal->update();


            Loanrepayment::vrepayLoan($loanamt,$loanaccount_id,$member,$vid);
        }


        if($request->input('shares') != 0.00 || $request->input('shares') != ''){

            $sharetransaction    = Sharetransaction::findOrFail($vehicle->sharetransaction_id);
            $sharetransaction->date = date("Y-m-d");
            $sharetransaction->amount = str_replace( ',', '', $request->input('shares'));
            $sharetransaction->update();

            $vehshare = Vehicleincome::where('id',$id)->first();
            $vehshare->sharetransaction_id = $sharetransaction->id;
            $vehshare->update();

        }

        //Contribution::nadd($dt, $request->input('asset_id'), $request->input('ainc_id') );

        if($request->input('offamt') > 0 ){


            $data = array(
                'date' => $data['date'],
                'debit_account' => $data['asset_id'],
                'credit_account' => $data['eq_id'],
                'description' => 'office contribution',
                'amount' => $request->input('offamt'),
                'initiated_by' => Auth::user()->username,
                'cid' => $cont->id
            );

            $journal = new Journal;

            $journal->journal_contentry($data);
        }else if($request->input('offamt') <= 0 ){

            $data = array(
                'date' => $data[ 'date'],
                'debit_account' => $data[ 'asset_id'],
                'credit_account' => $data['eq_id'],
                'description' => 'office contribution',
                'amount' => 200,
                'initiated_by' => Auth::user()->username,
                'cid' => $cont->id
            );

            $journal = new Journal;

            $journal->journal_contentry($data);

        }





        return Redirect::route('vehicleincomes.index');
    }

    /**
     * Remove the specified account from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $vehicle = Vehicleincome::findOrFail($id);
        $counter = Savingtransaction::where('id',$vehicle->savingtransaction_id)->count();
        if($counter >0){
            Savingtransaction::where('id',$vehicle->savingtransaction_id)->delete();
        }
        $count_shares= Sharetransaction::where('id','=',$vehicle->sharetransaction_id)->count();
        if($count_shares > 0){
            Sharetransaction::where('id','=',$vehicle->sharetransaction_id)->delete();
        }
        $cont_count = Contribution::where('vehicleincome_id',$id)->count();
        if($cont_count >0){
            $contribution = Contribution::where('vehicleincome_id',$id)->get()->first();
            if($contribution->debit_journal_id != null && $contribution->credit_journal_id != null){
                Journal::where('id','=',$contribution->credit_journal_id)->delete();
                Journal::where('id','=',$contribution->debit_journal_id)->delete();
            }
            Contribution::where('vehicleincome_id',$id)->delete();
        }

        if($vehicle->loantransaction_id != null || $vehicle->loantransaction_id != '' || $vehicle->loantransaction_id != 0){
            $saving_count = Loantransaction::where('id',$vehicle->loantransaction_id)->count();
            if($saving_count >0){
                Loantransaction::where('id',$vehicle->loantransaction_id)->delete();
            }

            $loan_count = Loanrepayment::where('id',$vehicle->loanrepayment_principal_id)->count();
            if($loan_count >0){
                Loanrepayment::where('id',$vehicle->loanrepayment_principal_id)->delete();
            }

            $repay_count =  Loanrepayment::where('id',$vehicle->loanrepayment_interest_id)->count();
            if($repay_count>0){
                Loanrepayment::where('id',$vehicle->loanrepayment_interest_id)->delete();
            }

            $journal_count = Journal::where('id',$vehicle->interest_credit_journal_id)->count();
            if($journal_count>0){
                Journal::where('id',$vehicle->interest_credit_journal_id)->delete();
            }

            $journal_count_1 = Journal::where('id',$vehicle->interest_debit_journal_id)->count();
            if($journal_count_1 > 0){
                Journal::where('id',$vehicle->interest_debit_journal_id)->delete();
            }

            $journal_count_2 = Journal::where('id',$vehicle->principal_credit_journal_id)->count();
            if($journal_count_2 > 0){
                Journal::where('id',$vehicle->principal_credit_journal_id)->delete();
            }

            $journal_count_3 = Journal::where('id',$vehicle->principal_debit_journal_id)->count();
            if($journal_count_3 > 0){
                Journal::where('id',$vehicle->principal_debit_journal_id)->delete();
            }

        }

        $journal_savings_count = Journal::where('id',$vehicle->savings_credit_journal_id)->count();
        if($journal_savings_count > 0){
            Journal::where('id',$vehicle->savings_credit_journal_id)->delete();
        }

        $journal_savings_debit_count = Journal::where('id',$vehicle->savings_debit_journal_id)->count();
        if($journal_savings_debit_count > 0){
            Journal::where('id',$vehicle->savings_debit_journal_id)->delete();
        }

        $journal_asset_count = Journal::where('id',$vehicle->asset_journal_id)->count();
        if($journal_asset_count > 0){
            Journal::where('id',$vehicle->asset_journal_id)->delete();
        }

        $journal_income_count = Journal::where('id',$vehicle->income_journal_id)->count();
        if($journal_income_count > 0){
            Journal::where('id',$vehicle->income_journal_id)->delete();
        }

        $journal_equity_credit_count = Journal::where('id',$vehicle->equity_credit_journal_id)->count();
        if($journal_equity_credit_count > 0){
            Journal::where('id',$vehicle->equity_credit_journal_id)->delete();
        }

        $journal_equity_debit_count = Journal::where('id',$vehicle->equity_debit_journal_id)->count();
        if($journal_equity_debit_count > 0){
            Journal::where('id',$vehicle->equity_debit_journal_id)->delete();
        }

        Vehicleincome::destroy($id);
        return Redirect::route('vehicleincomes.index')->withDeleted('The vehicle income has been deleted!!!!');
    }

}
