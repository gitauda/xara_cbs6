<?php namespace App\Http\Controllers;


use App\models\Member;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Laravel\Socialite\Facades\Socialite;
use Spatie\Permission\Models\Role;


/**
 * UsersController Class
 *
 * Implements actions regarding user management
 */
class UsersController extends Controller{
    /**
     * display a list of system users
     */
    public function index(){
        $users = User::all();
        return View::make('users.index')->with('users', $users);
    }

    /*
     * display the edit page
     */
    public function edit($user){

        $user = User::find($user);

        return View::make('users.edit')->with('user', $user);
    }


    /*
     * updates the user
     */
    public function update(Request $request,$user){

        $user = User::find($user);

        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->update();

        return Redirect::to('users/profile/'.$user->id);
    }
    /*
     * Displays the form for account creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return View::make('users.create', compact('roles'));
    }

    /*
     * Stores new account
     *
     * @return  Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $repo = App::make('App\Models\UserRepository');
        $user = $repo->signup($request->all());

        if ($user->id) {
            if (Config::get('Auth::signup_email')) {
                Mail::queueOn(
                    Config::get('Auth::email_queue'),
                    Config::get('Auth::email_account_confirmation'),
                    compact('user'),
                    function ($message) use ($user) {
                        $message
                            ->to($user->email, $user->username)
                            ->subject(Lang::get('Auth::Auth.email.account_confirmation.subject'));
                    }
                );
            }


            return Redirect::to('/');

        } else {
            $error = $user->errors()->all(':message');

            return Redirect::action('UsersController@create')
                ->withInput($request->except('password'))
                ->with('error', $error);
        }
    }

    /*
     * Displays the login form
     *
     * @return  Illuminate\Http\Response
     */
    public function login()
    {
        if (auth()->user()) {
            return Redirect::to('/dashboard');

        } else {
            return View::make('sign_in');
        }

        Member::dormantDeactivation();
    }

    /*
     * Attempt to do login
     *
     * @return  Illuminate\Http\Response
     */
    public function doLogin(Request $request)
    {
        $repo = App::make('App\models\UserRepository');
        $input = $request->all();

        #TODO: change this login process
        if ($repo->login($input)) {
            Session::put('xaraSession',$input['email']);
            return Redirect::intended('/dashboard');
        } else {
            if ($repo->isThrottled($input)) {
                $err_msg = Lang::get('auth.throttle');
            } elseif ($repo->existsButNotConfirmed($input)) {
                $err_msg = Lang::get('auth.not_confirmed');
            } else {
                $err_msg = Lang::get('auth.failed');
            }

            return Redirect::action('UsersController@login')
                ->withInput($request->except('password'))
                ->with('error', $err_msg);
        }
    }

    /*
     * Attempt to confirm account with code
     *
     * @param  string $code
     *
     * @return  Illuminate\Http\Response
     */
    public function confirm($code)
    {
        if (Auth::confirm($code)) {
            $notice_msg = Lang::get('auth.alerts.confirmation');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('auth.alerts.wrong_confirmation');
            return Redirect::action('UsersController@login')
                ->with('error', $error_msg);
        }
    }

    /*
     * Displays the forgot password form
     *
     * @return  Illuminate\Http\Response
     */
    public function forgotPassword()
    {
        return View::make(Config::get('Auth::forgot_password_form'));
    }

    /*
     * Attempt to send change password link to the given email
     *
     * @return  Illuminate\Http\Response
     */
    public function doForgotPassword(Request $request)
    {
        #TODO: change to use default laravel forgot password views
        if (Auth::forgotPassword($request->get('email'))) {
            $notice_msg = Lang::get('auth.alerts.password_forgot');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('auth.alerts.wrong_password_forgot');
            return Redirect::action('UsersController@doForgotPassword')
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /*
     * Shows the change password form with the given token
     *
     * @param  string $token
     *
     * @return  Illuminate\Http\Response
     */
    public function resetPassword($token)
    {
        return View::make(Config::get('Auth::reset_password_form'))
            ->with('token', $token);
    }

    /*
     * Attempt change password of the user
     *
     * @return  Illuminate\Http\Response
     */
    public function doResetPassword(Request $request)
    {
        //return $data = Input::all();
        $repo = App::make('App\Models\UserRepository');
        $input = array(
            'token'                 =>$request->input('token'),
            'password'              =>$request->input('password'),
            'password_confirmation' =>$request->input('password_confirmation'),
        );

        // By passing an array with the token, password and confirmation
        if ($repo->resetPassword($input)) {
            $notice_msg = Lang::get('auth.alerts.password_reset');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('auth.alerts.wrong_password_reset');
            return Redirect::action('UsersController@resetPassword',
             array('token'=>$input['token']))
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /*
     * Log the user out of the application.
     *
     * @return  Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }


    /*
     * Activate the user
     *
     */
    public function activate($user){

        $user = User::find($user);

        $user->confirmed = 1;
        $user->save();

        return Redirect::to('users');
    }


    /**
     * Deactivate the user
     *
     */
    public function deactivate($user){

        $user = User::find($user);

        $user->confirmed = 0;
        $user->save();

        return Redirect::to('users');
    }


    /**
     * Delete the user
     *
     */
    public function destroy($user){

        $user = User::find($user);


        $user->delete();

        return Redirect::to('users');
    }

    /**
     * change user password
     */
    public function changePassword(Request $request,$user){

        $user = User::find($user);

        $password_confirmation = $request->input('password_confirmation');
        $password = $request->input('password');

        if($password != $password_confirmation){

            return Redirect::to('users/password/'.$user->id)->with('error', 'passwords do not match');
        }
        else
        {

            $user->password = Hash::make($password);
            $user->update();

            return Redirect::to('users/profile/'.$user->id);
        }



    }


    public function password($user){

        $user = User::find($user);
        return View::make('users.password', compact('user'));

    }

    public function profile($user){

        $user = User::find($user);

        return View::make('users.profile', compact('user'));
    }

    public function signInGoogle(){
        return Socialite::driver('google')->redirect();

    }

    public function googleRedirect(User $user){
        $g_user = Socialite::driver('google')->user();

        if(User::where('email','=',$g_user->email)->first()){
            $checkUser = User::where('email','=',$g_user->email)->first();
            Auth::login($checkUser);
            return redirect('dashboard');
        }
//        $fb_user->token;
//        $fb_user->name;
        $user->provider_id = $g_user->getId();
        $user->username = $g_user->getName();
        $user->email = $g_user->getEmail();
        $user->save();

        return redirect('dashboard')->with('success','User logged in successfully');
    }

    public function signInFacebook(){
        return Socialite::driver('facebook')->redirect();

    }

    public function facebookRedirect(User $user){
        $fb_user = Socialite::driver('facebook')->user();
        #echo "<pre>"; print_r($fb_user); echo "</pre>"; die();
        if(User::where('email','=',$fb_user->email)->first()){
            $checkUser = User::where('email','=',$fb_user->email)->first();
            Auth::login($checkUser);
            return redirect('dashboard');
        }
//        $fb_user->token;
//        $fb_user->name;
        $user->provider_id = $fb_user->getId();
        $user->username = $fb_user->getName();
        $user->email = $fb_user->getEmail();
        $user->save();

        return redirect('dashboard')->with('success','Login Successful');

    }

    public function add(){


        $user = new User;

        $user->username = 'admin';
        $user->email = 'admin@rental.com';

        $user->password = Hash::make('password123');
        $user->confirmation_code = 'eoioweq982jwe';
        $user->remember_token = 'jsadksjd928323';
        $user->confirmed = '1';
        $user->save();


        echo "user created";
    }

    public function changePassword2(Request $request){

        $user_id = Auth::user()->id;


        $password_confirmation = $request->input('password_confirmation');
        $password = $request->input('password');

        if($password != $password_confirmation){

            return Redirect::back()->with('error', 'passwords do not match');
        }
        else
        {

            $pass = Hash::make($password);

            DB::table('users')->where('id', $user_id)->update(array('password' => $pass));

            return Redirect::to('users/logout');

        }
    }


    public function password2(){

        $user = Auth::user()->id;
        return View::make('css.password', compact('user'));

    }


    public function tellers(){

        $tellers = DB::table('users')->where('user_type', '=', 'teller')->get();

        return View::make('tellers.index', compact('tellers'));
    }

    public function createteller($id){

        $user = User::findorfail($id);

        $user->user_type = 'teller';
        $user->is_active = true;
        $user->update();

        return Redirect::to('tellers');
    }


    public function activateteller($id){

        $user = User::findorfail($id);


        $user->is_active = true;
        $user->update();

        return Redirect::to('tellers');
    }


    public function deactivateteller($id){

        $user = User::findorfail($id);


        $user->is_active = false;
        $user->update();

        return Redirect::to('tellers');
    }


    public function credits(){
        $credits = DB::table('users')->where('user_type', '=', 'credit')->get();
        return View::make('credits.index', compact('credits'));
    }

    public function createcredit($id){

        $user = User::findorfail($id);
        $user->user_type = 'credit';
        $user->is_active = true;
        $user->update();
        return Redirect::to('credits');
    }

    public function activatecredit($id){
        $user = User::findorfail($id);
        $user->is_active = true;
        $user->update();
        return Redirect::to('credits');
    }


    public function deactivacredit($id){
        $user = User::findorfail($id);
        $user->is_active = false;
        $user->update();
        return Redirect::to('credits');
    }


    public function newuser(Request $request){

        $input = $request->all();

        $roles = $request->input('role');

        $user = new User;

        $user->username = $input['username'];
        $user->email    = $input['email'];
        $user->password = $input['password'];
        $user->user_type = $input['user_type'];

        // The password confirmation will be removed from model
        // before saving. This field will be used in Ardent's
        // auto validation.
        $user->password_confirmation = $input['password_confirmation'];

        // Generate a random confirmation code
        $user->confirmation_code     = md5(uniqid(mt_rand(), true));

        // Save if valid. Password field will be hashed before save
        $user->save();



        foreach ($roles as $role) {

            $user->attachRole($role);
        }


        return Redirect::to('users');
    }



}
