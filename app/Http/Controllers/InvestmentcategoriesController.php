<?php namespace App\Http\Controllers;
use \App\Http\Controllers\Controller;

use App\models\Investmentcategory;
use App\models\Investmentclass;
use App\models\Investmenttype;
use App\models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class InvestmentcategoriesController extends Controller {
	/*
	*
	*Display Investments Categories
	* @return Response
	*/
	public function index(){
		$organization= Organization::find(1);
		$cats= Investmentcategory::all();
		$classes= Investmentclass::all();
		$types= Investmenttype::all();
		return view('investmentcats.index',compact('cats','classes','types','organization'));
	}
	/*
	*
	*Display Form to create investment category
	*/
	public function create($what){
		$categories=Investmentcategory::all(); $classes=Investmentclass::all();
		return view('investmentcats.create',compact('what','categories','classes'));
	}
	/*
	*
	*Create  investment category
	*/
	public function store(Request $request){
		$what= $request->get('what');
		if($what=='category'){
			$data=$request->all();
			$cat=new Investmentcategory;
			$cat->name=array_get($data, 'name');
			$cat->code=array_get($data, 'code');
			$cat->description=array_get($data, 'desc');
			$cat->save();
			$catdone='Investment Category successfully created!!!';
		}elseif($what=='class'){
			$data=$request->all();
			$cat=new Investmentclass;
			$cat->name=array_get($data, 'name');
			$cat->category=array_get($data, 'ccategory');
			$cat->code=array_get($data, 'code');
			$cat->description=array_get($data, 'desc');
			$cat->save();
			$catdone='Investment Class successfully created!!!';
		}elseif($what=='type'){
			$data=$request->all();
			$cat=new Investmenttype;
			$cat->name=array_get($data, 'name');
			$cat->category=array_get($data, 'tcategory');
			$cat->class=array_get($data, 'tclass');
			$cat->code=array_get($data, 'code');
			$cat->description=array_get($data, 'desc');
			$cat->save();

			$catdone='Investment type successfully created!!!';
		}

		$cats=Investmentcategory::all();
		$classes=Investmentclass::all();
		$types=Investmenttype::all();
		$organization=Organization::find(1);
		return view('investmentcats.index',compact('cats','classes','types','organization','catdone'));
	}
	/*
	*
	*Form to update investment category
	*/
	public function update($what,$id){
		if($what=='category'){
			$cats=Investmentcategory::where('id','=',$id)->get()->first();
			return view('investmentcats.edit',compact('cats','what'));
		}else if($what=='class'){
			$class=Investmentclass::where('id','=',$id)->get()->first();
			$mcategory=Investmentcategory::where('id','=',$class->category)->get()->first();
			$categories=Investmentcategory::all();
			return view('investmentcats.edit',compact('what','class','mcategory','categories'));
		}else if($what=='type'){
			$type=Investmenttype::where('id','=',$id)->get()->first();
			$mclass=Investmentclass::where('id','=',$type->class)->get()->first();
			$classes=Investmentclass::all();
			$mcategory=Investmentcategory::where('id','=',$mclass->category)->get()->first();
			$categories=Investmentcategory::all();
			return view('investmentcats.edit',compact('what','type','mclass','classes','mcategory','categories'));
		}
	}
	/*
	*
	*update investment category
	*/
	public function doupdate(Request $request, $id){
		$what=$request->get('what');
		if($what=='category'){
			$data=$request->all();
			$catinv= Investmentcategory::where('id','=',$id)->get()->first();
			$catinv->name= array_get($data,'name');
			$catinv->code= array_get($data, 'code');
			$catinv->description= array_get($data, 'desc');
			$catinv->update();
			$catupdated="The investment category successfully updated!!!";
		}else if($what=='class'){
			$data=$request->all();
			$class=Investmentclass::where('id','=',$id)->first();
			$class->name=array_get($data,'name');
			$class->category=array_get($data,'ccategory');
			$class->code=array_get($data, 'code');
			$class->description=array_get($data, 'desc');
			$class->update();
			$catupdated="The investment class successfully updated!!!";
		}else if($what=='type'){
			$data=$request->all();
			$type=Investmenttype::where('id','=',$id)->first();
			$type->name=array_get($data,'name');
			$type->class=array_get($data,'tclass');
			$type->category=array_get($data,'tcategory');
			$type->code=array_get($data, 'code');
			$type->description=array_get($data, 'desc');
			$type->update();
			$catupdated="The investment type successfully updated!!!";
		}
		$cats=Investmentcategory::all();
		$organization=Organization::find(1);
		$classes=Investmentclass::all(); $types=Investmenttype::all();
		return view('investmentcats.index',compact('cats','classes','types','catupdated','organization'));
	}
	/*
	*
	*deleting an investment category
	*/
	public function destroy($what,$id){
		if($what=='category'){
			Investmentcategory::find($id)->delete(); $mess='The investment category deleted permanently!!!';
		}else if($what=='class'){
			Investmentclass::find($id)->delete(); $mess='The investment class deleted permanently!!!';
		}else if($what=='type'){
			Investmenttype::find($id)->delete(); $mess='The investment type deleted permanently!!!';
		}
		return Redirect::back()->withPuff($mess);
	}
}
