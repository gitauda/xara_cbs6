<?php namespace App\Http\Controllers;



use App\models\Audit;
use App\models\FleetFuelConsumption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class FleetMgntController extends Controller
{


    /**
    * display a list of system roles
    */
    public function index(){

        $fuelConsumption= FleetFuelConsumption::all();
        return view('fleetMgnt.index',compact('fuelConsumption'));
    }


    /**
    * display the edit page
    */
    public function edit($id){

    }



     /*
    * updates the role
    */
    public function update(Request $request,$id){


    }




    /*
     * Displays the form for account creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {
    }

    /*
     * Stores new account
     *
     * @return  Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }





    /**
    * Delete the role
    *
    */

    public function destroy($id){

    }



    public function show($id){
    }


}
