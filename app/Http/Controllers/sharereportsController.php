<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\models\Member;
use App\models\Organization;
use App\models\Share;
use App\models\Sharetransaction;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;

class sharereportsController extends Controller{

    //The index page for share reports
    public function index(){
        return View::make('sharereports.index');
    }
    //Contribution Listing Report

    //Shares Listing Report

    //Individual Contribution Report
    public function show(){
        $members=Member::all();
        return View::make('sharereports.individualcontribution',compact('members'));
    }

    //View Individual Contribution
    public function individual(Request $request){
        //Obtain member ID selected
        $id= $request->get('memberid');

        if($request->get('format') == 'pdf'){
            if($id == 'All'){
                $members=Member::all();
                $arr = array();
                foreach($members as $member){
                    $contributions=Sharetransaction::where('shareaccount_id','=',$id)
                        ->where('type','=', 'credit')->sum('amount');
                    $value=Share::count();
                    if($value>0){
                        $var= share::where('id','=',1)->pluck('value');
                        if($var<= 0){
                            $sharevalue=0.00000009;
                        }else{
                            $sharevalue=$var;
                        }
                        $shares=$contributions/$sharevalue;
                        $transactions=Sharetransaction::where('shareaccount_id','=',$member->id)->get();
                        $amount = 0;
                        foreach ($transactions as $trans) {
                            // code...
                            $amount += $trans->amount;
                        }
                        $arr[$member->name] = array('amount' => $amount );
                    }
                }
                $organization= Organization::find(1);
                $pdf = PDF::loadView('sharereports.pdf.all', compact('organization','contributions','shares','arr'))->setPaper('a5')->setOrientation('potrait');
                return $pdf->stream('Individual Member Contribution Report.pdf');
            }else{
                $member=Member::where('id','=',$id)->get()->first();
                $contributions=Sharetransaction::where('shareaccount_id','=',$id)
                    ->where('type','=', 'credit')->sum('amount');
                $value=Share::count();
                if($value>0){
                    $var=share::where('id','=',1)->pluck('value');
                    if($var<=0){
                        $sharevalue=0.00000009;
                    }else{
                        $sharevalue=$var;
                    }
                    $shares=$contributions/$sharevalue;
                    $transactions=Sharetransaction::where('shareaccount_id','=',$id)->get();
                    $organization=Organization::find(1);
                    $pdf = PDF::loadView('sharereports.pdf.individualcontribution', compact('organization','member','contributions','shares','transactions'))->setPaper('a5')->setOrientation('potrait');
                    return $pdf->stream('Individual Member Contribution Report.pdf');
                }else if($value<=0){
                    return Redirect::back();
                }
            }

        }elseif($request->get('format') == 'excel'){
            if($id == 'All'){
                $members= Member::all();
                $arr = array();
                foreach($members as $member){
                    $contributions=Sharetransaction::where('shareaccount_id','=',$id)
                        ->where('type','=', 'credit')->sum('amount');
                    $value=Share::count();
                    if($value>0){
                        $var=share::where('id','=',1)->pluck('value');
                        if($var<=0){
                            $sharevalue=0.00000009;
                        }else{
                            $sharevalue=$var;
                        }
                        $shares=$contributions/$sharevalue;
                        $transactions=Sharetransaction::where('shareaccount_id','=',$member->id)->get();
                        $amount = 0;
                        foreach ($transactions as $trans) {
                            // code...
                            $amount += $trans->amount;
                        }
                        $arr[$member->name] = array('amount' => $amount,'name' => $member->name );
                    }
                }
                $organization=Organization::find(1);


                return Excel::create('Shares Report', function ($excel) use ($arr) {


                    $excel->sheet('Shares Report', function ($sheet) use ($arr) {

                        $sheet->setAllBorders('thin');

                        $sheet->setWidth(array(
                            'A' => 50,
                            'B' => 20,
                            'C' => 15,
                            'D' => 15
                        ));

                        $sheet->mergeCells('A1:D1');

                        $sheet->row(1, array("MOTOSACCO SHARES REPORTS"));

                        $sheet->cells('A1:D1', function ($cells) {
                            $cells->setAlignment('center');
                            $cells->setFont(array(
                                'family' => 'Calibri',
                                'bold' => true
                            ));
                        });

                        $sheet->mergeCells('A2:D2');

                        $sheet->row(3, array(
                            "Member", "Amount"
                        ));

                        $sheet->cells('A3:D3', function ($cells) {
                            $cells->setFont(array(
                                'bold' => true
                            ));
                        });

                        if (ob_get_level() > 0) {
                            ob_end_clean();
                        }

                        $row = 4;

                        $total = 0;
                        foreach ($arr as $k => $entry) {

                            $sheet->cell('A' . $row, function ($cell) use ($entry) {
                                $cell->setValue($entry['name']);
                            });
                            $sheet->cell('B' . $row, function ($cell) use ($entry) {
                                $cell->setValue($entry['amount']);
                            });
                            $total += $entry['amount'];
                            $row++;


                        }

                        $sheet->cell('A'.$row, function($cell){
                            $cell->setFont(array('bold'=>true));
                            $cell->setValue("Total:");
                        });

                        $sheet->cell('B'.$row, function($cell) use($total){
                            $cell->setFont(array('bold'=>true));
                            $cell->setValue(number_format($total, 2));
                        });


                    });

                })->export('xlsx');
            }else{
                $member=Member::where('id','=',$id)->get()->first();
                $contributions=Sharetransaction::where('shareaccount_id','=',$id)
                    ->where('type','=', 'credit')->sum('amount');
                $cont = array('cont' => $contributions);
                $value=Share::count();
                if($value>0){
                    $var=share::where('id','=',1)->pluck('value');
                    if($var[0]<= 0){
                        $sharevalue=0.00000009;
                    }else{
                        $sharevalue= $var[0];
                    }
                    $shares= $contributions/$sharevalue;
                    $transactions=Sharetransaction::where('shareaccount_id','=',$id)->get();
                    $organization=Organization::find(1);


                    return Excel::create($member->name.' Shares Report', function ($excel) use ($member,$cont) {

                        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
                        require_once(base_path() . "/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");


                        $excel->sheet('Shares Report', function ($sheet) use ($member, $cont) {

                            $sheet->setAllBorders('thin');

                            $sheet->setWidth(array(
                                'A' => 50,
                                'B' => 20,
                                'C' => 15,
                                'D' => 15
                            ));

                            $sheet->mergeCells('A1:D1');

                            $sheet->row(1, array("MOTOSACCO SHARES REPORTS"));

                            $sheet->cells('A1:D1', function ($cells) {
                                $cells->setAlignment('center');
                                // $cells->setBackground('#777777');
                                $cells->setFont(array(
                                    'family' => 'Calibri',
                                    'bold' => true
                                ));
                            });

                            $sheet->mergeCells('A2:D2');


                            $sheet->row(3, array(
                                "Member", "Amount"
                            ));

                            $sheet->cells('A3:D3', function ($cells) {
                                $cells->setFont(array(
                                    'bold' => true
                                ));
                            });

                            if (ob_get_level() > 0) {
                                ob_end_clean();
                            }

                            $row = 4;



                            $sheet->cell('A' . $row, function ($cell) use ($member) {
                                $cell->setValue($member->name);
                            });
                            $sheet->cell('B' . $row, function ($cell) use ($cont) {
                                $cell->setValue($cont['cont']);
                            });

                            $row++;

                            $sheet->cell('A'.$row, function($cell){
                                $cell->setFont(array('bold'=>true));
                                $cell->setValue("Total:");
                            });

                            $sheet->cell('B'.$row, function($cell) use($cont){
                                $cell->setFont(array('bold'=>true));
                                $cell->setValue(number_format($cont['cont'], 2));
                            });




                        });

                    })->export('xlsx');
                }else if($value<=0){
                    return Redirect::back();
                }
            }

        }


    }
}
