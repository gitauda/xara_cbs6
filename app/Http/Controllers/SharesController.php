<?php namespace App\Http\Controllers;

use App\models\Dividend;
use App\Http\Controllers\Controller;
use App\models\Member;
use App\models\Share;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class SharesController extends Controller {

	/*
	 * Display a listing of shares
	 *
	 * @return Response
	 */
	public function index()
	{
		$shares = Share::all();

		return view('shares.index', compact('shares'));
	}

	/*
	 * Show the form for creating a new share
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('shares.create');
	}

	/*
	 * Store a newly created share in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){

		$validator = Validator::make($data = $request->all(), Share::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$share = new Share;

		$share->value = $request->input('value');
		$share->transfer_charge =$request->input('transfer_charge');
		$share->charged_on = $request->input('charged_on');
		$share->save();

		return Redirect::route('shares.index');
	}

	/*
	 * Display the specified share.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$share = Share::findOrFail($id);

		return view('shares.show', compact('share'));
	}

	/*
	 * Show the form for editing the specified share.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$share = Share::find($id);

		return view('shares.edit', compact('share'));
	}

	/*
	 * Update the specified share in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$share = Share::findOrFail($id);

		$validator = Validator::make($data = $request->all(), Share::$rules);

		if ($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$share->value = $request->get('value');
		$share->transfer_charge = $request->get('transfer_charge');
		$share->charged_on = $request->get('charged_on');
		$share->update();

		return Redirect::to('shares/show/'.$share->id);
	}

	/*
	 * Share capital Contribution.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function contribution(){
		$members=Member::all();
		return view('sharecapital.contribution',compact('members'));
	}
	/*
	 * Share Capital Dividends.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function dividend(){
		$members=Member::all();
		return view('sharecapital.dividend',compact('members'));
	}
	/*
	 * Share capital report.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function parameters(){
		return view('sharecapital.parameters');
	}


	public function parameterize(Request $request){
		$divi=new Dividend;
		$divi->total=$request->input('sum_dividends');
		$divi->special=$request->input('special_dividends');
		$divi->outstanding=$request->input('outstanding_shares');
		$divi->save();

		return Redirect::action('SharesController@dividend');
	}
	/*
	 * Share capital report.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editparameters(){
		$div= Dividend::where('id','=',1)->get()->first();
		return view('sharecapital.editparameters',compact('div'));
	}

	public function doparameterize(Request $request){
		$divi=Dividend::where('id','=',1)->get()->first();;
		$divi->total=$request->input('sum_dividends');
		$divi->special=$request->input('special_dividends');
		$divi->outstanding=$request->input('outstanding_shares');
		$divi->update();

		return Redirect::action('SharesController@dividend');
	}
	/*
	 * Remove the specified share from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Share::destroy($id);
		return Redirect::route('shares.index');
	}

}
