<?php
namespace App\Http\Controllers;

use App\models\Account;
use App\models\Currency;
use App\Http\Controllers\Controller;
use App\models\Loanaccount;
use App\models\Loanproduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class LoanproductsController extends Controller{

	/*
	 * Display a listing of loanproducts
	 *
	 * @return Response
	 */
	public function index()
	{
		$loanproducts = Loanproduct::all();

		return view('loanproducts.index', compact('loanproducts'));
	}

	/*
	 * Show the form for creating a new loanproduct
	 *
	 * @return Response
	 */
	public function create()
	{

		$accounts = Account::all();
		$currencies = Currency::all();
		$charges = DB::table('charges')->where('category', '=', 'loan')->get();

		return view('loanproducts.create', compact('accounts', 'charges', 'currencies'));
	}

	/*
	 * Store a newly created loanproduct in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $data = $request->all();
		$validator = Validator::make($data, Loanproduct::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Loanproduct::submit($data);

		return Redirect::route('loanproducts.index')->withFlashMessage('Loan Product successfully created!');
	}

	/*
	 * Display the specified loanproduct.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$loanproduct = Loanproduct::findOrFail($id);

		return View::make('loanproducts.show', compact('loanproduct'));
	}

	public function memberloanshow($id)
	{
		/*$loanproduct = DB::table('loanaccounts')
		          ->join('loanproducts', 'loanaccounts.loanproduct_id', '=', 'loanaccounts.id')
		          ->join('members', 'loanaccounts.member_id', '=', 'member.id')
		          ->where('loanaccounts','=',$id)
		          ->select('members.name as mname','loanproducts.name as name','short_name','interest_rate','period','formula','amortization')
		          ->first();*/
		$loanaccount = Loanaccount::findOrFail($id);

		return View::make('css.memberloanshow', compact('loanaccount'));
	}

	/*
	 * Show the form for editing the specified loanproduct.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$loanproduct = Loanproduct::find($id);

		$currencies = Currency::all();

		return View::make('loanproducts.edit', compact('loanproduct', 'currencies'));
	}

	/*
	 * Update the specified loanproduct in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$loanproduct = Loanproduct::findOrFail($id);
        $data =$request->all();
		$validator = Validator::make($data, Loanproduct::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
        $membership_duration= $data['membershipduration'];
		if(empty($membership_duration)){$membership_duration=0;}

		$loanproduct->name = $request->input('name');
		$loanproduct->short_name = $request->input('short_name');
		$loanproduct->interest_rate = $request->input('interest_rate');
		$loanproduct->amortization = $request->input('amortization');
		$loanproduct->formula = $request->input('formula');
		$loanproduct->period = $request->input('period');
		$loanproduct->currency = array_get($data, 'currency');
		$loanproduct->auto_loan_limit = array_get($data, 'autoloanlimit');
		$loanproduct->max_multiplier=array_get($data, 'maxmultiplier');
		$loanproduct->membership_duration=$membership_duration;
		$loanproduct->update();

		return Redirect::route('loanproducts.index')->withFlashMessage('Loan Product successfully updated!');
	}

	/*
	 * Remove the specified loanproduct from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Loanproduct::destroy($id);

		return Redirect::route('loanproducts.index');
	}

}
