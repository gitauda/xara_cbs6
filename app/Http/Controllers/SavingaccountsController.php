<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\models\Member;
use App\models\Savingaccount;
use App\models\Savingproduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class SavingaccountsController extends Controller {

    /*
     * Display a listing of savingaccounts
     *
     * @return Response
     */
    public function index()
    {
        $savingaccounts = Savingaccount::all();

        return view('savingaccounts.index', compact('savingaccounts'));
    }

    /*
     * Show the form for creating a new savingaccount
     *
     * @return Response
     */
    public function create($id)
    {
        $member = Member::findOrFail($id);
        $savingproducts = Savingproduct::all();
        return view('savingaccounts.create', compact('member', 'savingproducts'));
    }


    /*
     * Store a newly created savingaccount in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Savingaccount::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }


        $member = Member::findOrFail($request->get('member_id'));
        $savingproduct = Savingproduct::findOrFail($request->get('savingproduct_id'));
        if(!empty($request->get('account_no')))
        {
            $acc_no = $request->get('account_no');
        }
        else{
            $acc_no = $savingproduct->shortname.'000000'.$member->membership_no;
        }
        $accnoavail=Savingaccount::where('account_number', '=', $acc_no)->count();
        $accavail=Savingaccount::where('savingproduct_id', '=', $savingproduct->id)->where('member_id', '=', $member->id)->count();

        if($accavail>0){
            return Redirect::back()->withErrors('Savings  Account for the member number already exists!');
        }
        if($accnoavail>0){
            return Redirect::back()->withErrors('Savings  Account number  number already exists!');
        }

        $savingaccount = new Savingaccount;
        $savingaccount->member()->associate($member);
        $savingaccount->savingproduct()->associate($savingproduct);
        $savingaccount->account_number = $acc_no;
        $savingaccount->save();

        //return Redirect::route('savingaccounts.index');
        //return Redirect::route('member/savingaccounts/{$member->id}');
        return view('savingaccounts.memberaccounts', compact('member'));
    }

    /*
     * Display the specified savingaccount.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $savingaccount = Savingaccount::findOrFail($id);

        return view('savingaccounts.show', compact('savingaccount'));
    }

    /*
     * Show the form for editing the specified savingaccount.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $savingaccount = Savingaccount::find($id);

        return view('savingaccounts.edit', compact('savingaccount'));
    }

    /*
     * Update the specified savingaccount in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $savingaccount = Savingaccount::findOrFail($id);

        $validator = Validator::make($data = $request->all(), Savingaccount::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $savingaccount->update($data);

        return Redirect::route('savingaccounts.index');
    }

    /*
     * Remove the specified savingaccount from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $savingaccount = Savingaccount::findOrFail($id);
        Savingaccount::destroy($id);
        $member=Member::findorFail($savingaccount->member_id);
        //return Redirect::route('savingaccounts.index');
        return view('savingaccounts.memberaccounts', compact('member'));
    }



    public function memberaccounts($id){
        $member = Member::findOrFail($id);

        return view('savingaccounts.memberaccounts', compact('member'));
    }

}
