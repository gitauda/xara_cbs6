<?php

namespace App\Http\Controllers;

use App\models\Plan;
use App\User;
use Illuminate\Http\Request;
use Stripe\StripeClient;
use Illuminate\Support\Str;

class SubscriptionController extends Controller
{
    protected $stripe;

   public function __construct(){
    $this->stripe = new StripeClient(env('STRIPE_SECRET'));
  }

    public function options(){
        $plans = Plan::all();
        return view('subscriptions.show_plan',compact('plans'));
    }

    public function createPlan(){

        return view('subscriptions.create_plan');

    }

    public function storePlan(Request  $request){
        $data = $request->except('_token');

        $data['slug'] = Str::slug(strtolower($data['name']),'_');
        $price = $data['cost'] * 100;
        $interval = $data['interval'];

        //create stripe product
        $stripeProduct = $this->stripe->products
        ->create(['name'=>$data['name']]);

        //stripe plan create
        $stripePlan = $this->stripe->plans->create([
            'amount' => $price,
            'currency' => 'kes',
            'interval' => $interval,
            'product' => $stripeProduct->id
        ]);

        $data['stripe_plan'] = $stripePlan->id;

        Plan::create($data);

        return redirect()->back();

    }

    public function payment(Request  $request){

        $plan = $request->plan;
        #echo '<pre>'; print_r($plan); echo '</pre>'; die();

        $data =[
            'intent' => auth()->user()->createSetupIntent(),
            'plan' => $plan
        ];
        return view('subscriptions.pay_form')->with($data);
    }

    public function paysubs(Request $request){
        $user = auth()->user();
        $plan = $request->get('plan');
        $pln = Plan::where('slug',$plan)->first();

        $paymentMethod = $request->paymentMethod;

        $user->createOrGetStripeCustomer();
        $user->updateDefaultPaymentMethod($paymentMethod);
        $user->newSubscription('default', $pln->stripe_plan)
            ->create($paymentMethod, [
                'email' => $user->email,
            ]);

        return redirect('dashboard')->with('success', 'Your plan subscribed successfully');


    }
}
