<?php namespace App\Http\Controllers;

use App\models\Audit;
use App\models\Currency;
use App\models\Emailgroup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class EmailGroupsController extends Controller {

    /*
     * Display a listing of currencies
     *
     * @return Response
     */
    public function index()
    {
        $groups = Emailgroup::where('organization_id',Auth::user()->organization_id)->get();
        return View::make('emailgroups.index', compact('groups'));
    }

    /*
     * Show the form for creating a new currency
     *
     * @return Response
     */
    public function create()
    {
        return View::make('emailgroups.create');
    }

    /*
     * Store a newly created currency in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Emailgroup::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $group = new Emailgroup;
        $group->name = $request->get('name');
        $group->email = $request->get('email');
        if($request->get('report_application') != null ){
            $group->report_applications = '1';
        }else{
            $group->report_applications = '0';
        }
        if($request->get('report_approved') != null ){
            $group->report_approved = '1';
        }else{
            $group->report_approved = '0';
        }
        if($request->get('report_rejected') != null ){
            $group->report_rejected = '1';
        }else{
            $group->report_rejected = '0';
        }
        if($request->get('report_balance') != null ){
            $group->report_balances = '1';
        }else{
            $group->report_balances = '0';
        }
        if($request->get('report_employee') != null ){
            $group->report_employee_on_leave = '1';
        }else{
            $group->report_employee_on_leave = '0';
        }
        if($request->get('report_individual') != null ){
            $group->report_individual = '1';
        }else{
            $group->report_individual = '0';
        }
        $group->organization_id = Auth::user()->organization_id;
        $group->save();
        Audit::logaudit('Email Group', 'create', 'created: '.$group->name);

        return Redirect::route('emailgroups.index')->withFlashMessage('Email Group successfully created!');
    }

    /*
     * Display the specified currency.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $currency = Currency::where('organization_id',Auth::user()->organization_id)->get();

        Audit::logaudit('Currency', 'view', 'viewed: '.$currency->name);

        return View::make('currencies.show', compact('currency'));

    }

    /*
     * Show the form for editing the specified currency.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $group = Emailgroup::find($id);

        return View::make('emailgroups.edit', compact('group'));
    }

    /*
     * Update the specified currency in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $group = Emailgroup::find($id);

        $validator = Validator::make($data = $request->all(), Emailgroup::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $group->name = $request->get('name');
        $group->email = $request->get('email');
        if($request->get('report_application') != null ){
            $group->report_applications = '1';
        }else{
            $group->report_applications = '0';
        }
        if($request->get('report_approved') != null ){
            $group->report_approved = '1';
        }else{
            $group->report_approved = '0';
        }
        if($request->get('report_rejected') != null ){
            $group->report_rejected = '1';
        }else{
            $group->report_rejected = '0';
        }
        if($request->get('report_balance') != null ){
            $group->report_balances = '1';
        }else{
            $group->report_balances = '0';
        }
        if($request->get('report_employee') != null ){
            $group->report_employee_on_leave = '1';
        }else{
            $group->report_employee_on_leave = '0';
        }
        if($request->get('report_individual') != null ){
            $group->report_individual = '1';
        }else{
            $group->report_individual = '0';
        }
        $group->organization_id = Auth::user()->organization_id;

        $group->update();

        Audit::logaudit('Email Group', 'update', 'updated: '.$group->name);

        return Redirect::route('emailgroups.index');
    }

    /*
     * Remove the specified currency from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $group = Emailgroup::findOrFail($id);
        Emailgroup::destroy($id);

        Audit::logaudit('Email Group', 'delete', 'deleted: '.$group->name);

        return Redirect::route('emailgroups.index');
    }

}
