<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\models\Member;
use App\models\Share;
use App\models\Shareaccount;
use App\models\Sharetransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class SharetransactionsController extends Controller {

    /*
     * Display a listing of sharetransactions
     *
     * @return Response
     */
    public function index()
    {
        $sharetransactions = Sharetransaction::all();

        return View::make('sharetransactions.index', compact('sharetransactions'));
    }

    /*
     * Show the form for creating a new sharetransaction
     *
     * @return Response
     */
    public function create($id)
    {


        $shareaccount = Shareaccount::findOrFail($id);

        $member = $shareaccount->member;

        return View::make('sharetransactions.create', compact('shareaccount', 'member'));


    }

    /*
     * Store a newly created sharetransaction in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($data = $request->all(), Sharetransaction::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $shareaccount = Shareaccount::findOrFail($request->get('account_id'));

        $sharetransaction = new Sharetransaction;

        $sharetransaction->date = $request->get('date');
        $sharetransaction->shareaccount()->associate($shareaccount);
        $sharetransaction->amount = $request->get('amount');
        $sharetransaction->type = $request->get('type');
        $sharetransaction->description = $request->get('description');
        $sharetransaction->save();

        return Redirect::to('sharetransactions/show/'.$shareaccount->id);

    }

    /*
     * Display the specified sharetransaction.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {



        $account = Shareaccount::findOrFail($id);

        $credit = DB::table('sharetransactions')->where('shareaccount_id', '=', $account->id)->where('type', '=', 'credit')->sum('amount');
        $debit = DB::table('sharetransactions')->where('shareaccount_id', '=', $account->id)->where('type', '=', 'debit')->sum('amount');

        $balance = $credit - $debit;

        $sh = Share::findOrFail(1);

        $sharevalue = $sh->value;

        if($sharevalue != 0){
            $shares = $balance/$sharevalue;
        } else {

            $shares = 0;
        }


        return View::make('sharetransactions.show', compact('account', 'shares'));


    }

    /*
     * Show the form for editing the specified sharetransaction.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $sharetransaction = Sharetransaction::find($id);

        return View::make('sharetransactions.edit', compact('sharetransaction'));
    }

    /*
     * Update the specified sharetransaction in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $sharetransaction = Sharetransaction::findOrFail($id);

        $validator = Validator::make($data = $request->all(), Sharetransaction::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $sharetransaction->update($data);

        return Redirect::route('sharetransactions.index');
    }

    /*
     * Remove the specified sharetransaction from storage.
     *
     * @param  int  $id
     * @return Response

     */
    public function sharetransfer(Request $request){
        $id1= $request->get('member1');
        $id2= $request->get('member2');
        $amountt= $request->get('amount');
        $memberfrom= Member::where('id','=',$id1)->first();

        $shareaccount1 = Shareaccount::findOrFail($id1);
        $shareaccount2 = Shareaccount::findOrFail($id2);


        $credit1 = DB::table('sharetransactions')->where('shareaccount_id', '=', $shareaccount1->id)->where('type', '=', 'credit')->sum('amount');
        $debit1 = DB::table('sharetransactions')->where('shareaccount_id', '=', $shareaccount1->id)->where('type', '=', 'debit')->sum('amount');

        $balance1 = $credit1 - $debit1;
        $credit2 = DB::table('sharetransactions')->where('shareaccount_id', '=', $shareaccount2->id)->where('type', '=', 'credit')->sum('amount');
        $debit2 = DB::table('sharetransactions')->where('shareaccount_id', '=', $shareaccount2->id)->where('type', '=', 'debit')->sum('amount');

        $balance2 = $credit2 - $debit2;


        if($amountt<=$balance1){
            $sharetransaction1 = new Sharetransaction;


            $sharetransaction1->date = $request->get('date');
            $sharetransaction1->shareaccount()->associate($shareaccount1);
            $sharetransaction1->amount = $request->get('amount');
            $sharetransaction1->type = 'debit';
            $sharetransaction1->description = $request->get('description');
            $sharetransaction1->save();


            $sharetransaction2 = new Sharetransaction;
            $sharetransaction2->date = $request->get('date');
            $sharetransaction2->shareaccount()->associate($shareaccount2);
            $sharetransaction2->amount = $request->get('amount');
            $sharetransaction2->type = 'credit';
            $sharetransaction2->description = $request->get('description');
            $sharetransaction2->save();
            return Redirect::to('sharetransactions/show/'.$shareaccount1->id);
        }else{

            return Redirect::back()->withInput()->withErrors('Insufficient balance to transfer---' .$amountt.'--to--'.$memberfrom->name);
        }

    }
    public function destroy($id)
    {
        Sharetransaction::destroy($id);

        return Redirect::route('sharetransactions.index');
    }

}
