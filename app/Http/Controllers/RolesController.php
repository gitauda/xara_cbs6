<?php namespace App\Http\Controllers;

use App\models\Audit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * rolesController Class
 *
 * Implements actions regarding role management
 */
class RolesController extends Controller
{


    /*
    * display a list of system roles
    */
    public function index(){

        $roles = Role::all();

        //Audit::logaudit('Roles', 'viewed user roles','viewed all roles',''.Confide::user()->name.'', 'viewed user roles in the system');
        Audit::logAuditwithfewargs('Roles', 'viewed user roles', '0');

        return View::make('roles.index')->with('roles', $roles);
    }


    /*
    * display the edit page
    */
    public function edit($id){

        $role = Role::find($id);
        $permissions = $role->permissions->toArray();
        #echo '<pre>'; print_r($permissions); echo '</pre>'; die;

        return View::make('roles.edit', compact('role', 'permissions'));
    }



    /*
   * updates the role
   */
    public function update(Request $request,$id){

        $perms = $request->get('permission');

        $role = Role::find($id);

        $role->name = $request->get('name');

        $role->update();

        $role->syncPermissions($perms);

        //Audit::logaudit('Roles', 'updated a user role', 'updated user role ',''.$request->get('name').'',' in the system');
        Audit::logAuditwithfewargs('Roles', 'updated a user role', '0');

        return Redirect::to('roles/show/'.$role->id)->with('success','Role successfully updated');

    }




    /*
     * Displays the form for account creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {

        $roles = Role::all();
        $permissions = Permission::with('roles')->get()->toArray();

        #echo '<pre>'; print_r($permissions); echo '</pre>'; die;



        return View::make('roles.create', compact('roles','permissions'));
    }

    /*
     * Stores new account
     *
     * @return  Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        #if()
        $role = Role::create(['name'=> $request->role ]);

        auth()->user()->assignRole($role);

        #$perms = $request->get('permission');



        #$role->syncPermissions($perms);

        Audit::logAuditwithfewargs('Roles', 'created a user role', '0');


        return redirect()->back()->with('flash_message','Role Created Successfully');


    }


    /*
    * Delete the role
    *
    */

    public function destroy($id){

        $role = Role::find($id);


        Role::destroy($id);

        //Audit::logaudit('Roles', 'deleted a user role', 'deleted user role ',''.$role->name.'',' in the system');
        Audit::logAuditwithfewargs('Roles', 'deleted a user role', '0');

        return redirect()->back();
    }



    public function show($id){

        $role = Role::find($id);
        $permissions = $role->permissions->toArray();

        #echo '<pre>'; print_r($permissions); echo '</pre>'; die;


        Audit::logAuditwithfewargs('Roles', 'viewed a user role', '0');
        return View::make('roles.show', compact('role', 'permissions'));
    }


























}
