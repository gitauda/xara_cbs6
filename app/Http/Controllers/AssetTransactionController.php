<?php namespace App\Http\Controllers;

use App\models\Asset;
use App\Http\Controllers\Controller;
use App\models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AssetTransactionController extends Controller {

    public function index()
	{
		//$accounts = DB::table('accounts')->orderBy('code', 'asc')->get();
		$assets=Asset::all();
		$members = Member::all();
		return View::make('assets.index', compact('assets','members'));
    }

    public function create()
	{
		// NEW ASSET PAGE
		$members = Member::all();
		$assetNum = 'AST_000'.(Asset::all()->count()+1);
		return View::make('assets.create', compact('assetNum','members'));
    }

    public function store(Request $request)
	{
		// STORE DATA IN DB
		$inputData=$request->all();
		$asset->quantity = $request->input('quantity');
 	    $station = $request->input('station');
		if(empty($request->input('rate'))){
			$inputData = array_merge($request->all(), array('rate'=>'0', 'method'=>'years'));
        }
        if(empty($request->input('lifeYears'))){
			$inputData = array_merge($request->all(), array('lifeYears'=>'0', 'method'=>'rate'));
		}
		 $inputData;
		Asset::registerAsset($inputData);
		$assets=Asset::all(); $members = Member::all();
        /*
		$assetAc = 'Accumulated Depreciation';
		$expAc = 'Depreciation Expense';
		$account1 = Account::where('name', $assetAc)->first();
		$account2 = Account::where('name', $expAc)->first();
		if(empty($account1)){
			Account::createAccount('ASSET', $assetAc, $inputData['purchasePrice']);
			if(empty($account2)){
				Account::createAccount('EXPENSE', $expAc);
			}
		} else{
			Account::where('name', $assetAc)->increment('balance', $inputData['purchasePrice']);
		}*/

		//return Redirect::action('assets/index');
		return View::make('assets.index', compact('assets','members'));
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// DISPLAY ASSET INFORMATION
		$asset = Asset::find($id);
		$member = Member::findorFail($asset->member_id);
		return View::make('assets.show', compact('asset','member'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// DISPLAY EDIT PAGE
		$asset = Asset::find($id);
		$member = Member::findorFail($asset->member_id);
		return View::make('assets.edit', compact('asset','member'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		// UPDATE DATA IN DB
		$asset->quantity=$request->input('quantity');
		$client->name = $request->input('name');
		if(!empty($request->input('lifeYears'))){
			$inputData = array_merge($request->all(), array('rate'=>'NULL', 'method'=>'years'));
		} elseif(!empty($request->input('rate'))){
			$inputData = array_merge($request->all(), array('lifeYears'=>'NULL', 'method'=>'rate'));
		}
		$assets=Asset::all(); $members = Member::all();

		// Reverse Existing Depreciation
		/*$item = Asset::find($id);
		$creditAc = Account::where('name', 'Accumulated Depreciation')->pluck('id');
		$debitAc = Account::where('name', 'Depreciation Expense')->pluck('id');
		$lastDepAmnt = round($item->purchase_price - $item->book_value, 2);
		$item->increment('book_value', $lastDepAmnt);

		Account::where('id', $creditAc)->increment('balance', $lastDepAmnt);
		Account::where('id', $debitAc)->decrement('balance', $lastDepAmnt);
		$data = array(
			'credit_account' =>$account['credit'] ,
			'debit_account' =>$account['debit'] ,
			'date' => $date,
			'amount' => $loanaccount->amount_disbursed,
			'initiated_by' => 'system',
			'description' => 'loan disbursement',
			'particulars_id' => '26',
			'narration' => $loanaccount->member->id
			);

		$journal = new Journal;

		$journal->journal_entry($data); */


		// Update Details
		Asset::updateAsset($inputData);
		//$this->depreciate($id);
		//return Redirect::action('assets@index');
		return View::make('assets.index', compact('assets','members'));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Asset::find($id)->delete();
		return Redirect::route('assets.index')->withDeleteMessage('successfully deleted!');
    }
}
?>
