<?php
namespace App\Http\Controllers;

use App\models\Disbursementoption;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class DisbursementController extends Controller {

	/*
	 * Display a listing of loan disbursementoptions
	 *
	 * @return Response
	 */
	public function index()
	{
		$options = Disbursementoption::all();

		return view('disbursements.index', compact('options'));
	}

	/*
	 * Show the form for creating a new loan disbursement option
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('disbursements.create');
	}
	/*
	 * Create the disbursement option
	 *
	 * @return Response
	 */
	public function docreate(Request $request)
	{
		$records= $request->all();
		$existence= Disbursementoption::where('name','=',$request->get('name'))->get();
		if(count($existence) <= 0){
			$disburse=new Disbursementoption;
			$disburse->name=$records['name'];
			$disburse->min=$records['min_amt'];
			$disburse->max=$records['max_amt'];
			$disburse->description=$records['desc'];
			$disburse->save();
			$options = Disbursementoption::all();
			$operation= "Disbursement option successfully created!";
			return View::make('disbursements.index', compact('options','operation'));
		}else if(count($existence)>0){
			return Redirect::back()->withWrath('The disbursement option already exists.');
		}
	}
	/*
	*
	*Form to update loan disbursement information
	*
	*/
	public function update($id){
		$disbursed=Disbursementoption::where('id','=',$id)->get()->first();
		return View::make('disbursements.update',compact('disbursed'));
	}

	/*
	*
	*updating loan disbursement information
	*
	*/
	public function doupdate(Request $request){
		$data=$request->all();
		$update=Disbursementoption::where('id','=',$data['id'])->get()->first();
		$update->name= $data['name'];
		$update->min= $data['min_amt'];
		$update->max= $data['max_amt'];
		$update->description= $data['desc'];
		$update->save();
		//Redirect user
		$options = Disbursementoption::all();
		$done="The disbursement option has been successfully updated!";
		return View::make('disbursements.index', compact('options','done'));
	}
	/*
	*
	*Deleting a disbursement option
	*
	*/
	public function destroy($id){
		Disbursementoption::destroy($id);
		$options = Disbursementoption::all();
		$shot="The disbursement option has been deleted!";
		return View::make('disbursements.index', compact('options','shot'));
	}
}
