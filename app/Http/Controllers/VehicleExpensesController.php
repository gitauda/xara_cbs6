<?php
namespace App\Http\Controllers;

use App\models\Account;
use App\Http\Controllers\Controller;
use App\models\Journal;
use App\models\Vehicle;
use App\models\Vehicleexpense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class VehicleExpensesController extends Controller {

    /*
     * Display a listing of accounts
     *
     * @return Response
     */
    public function index()
    {
        $vehicles = Vehicleexpense::all();

        return view('vehicleexpenses.index', compact('vehicles'));
    }

    /*
     * Show the form for creating a new account
     *
     * @return Response
     */
    public function create()
    {
        $vehicles = Vehicle::all();
        $aexp     = Account::where('category','EXPENSE')->get();
        $asset    = Account::where('category','ASSET')->get();
        return View::make('vehicleexpenses.create', compact('vehicles','aexp','asset'));
    }

    /*
     * Store a newly created account in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Vehicleexpense::$rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        // check if code exists
        $assign = Vehicle::where('id',$request->get('vehicle_id'))->first();
        $vehicle = new Vehicleexpense;
        $vehicle->vehicle_id = $request->get('vehicle_id');
        $vehicle->member_id = $assign->member_id;
        $vehicle->amount  = str_replace( ',', '', $request->get('amount'));
        $vehicle->expenseincurred = $request->get('incur');
        $vehicle->date = $request->get('date');
        /** TODO Reconcile member credit account with asset id */
        $vehicle->equity_member_credit_account = $request->get('asset_id');
        $vehicle->expense_account_id = $request->get('aexp_id');
        $vehicle->save();
        $data = array(
            'credit_account' => $request->get('asset_id'),
            'debit_account' => $request->get('aexp_id'),
            'date' => $request->get('date'),
            'amount' => str_replace( ',', '', $request->get('amount')),
            'initiated_by' => 'system',
            'description' => 'Vehicles Expenses',
            'expid'=>$vehicle->id
        );
        $journal = new Journal;
        $journal->journal_entry($data);
        return Redirect::route('vehicleexpenses.index');
    }

    /*
     * Display the specified account.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $vehicle = Vehicle::findOrFail($id);

        return View::make('vehicleincomes.show', compact('vehicle'));
    }

    /*
     * Show the form for editing the specified account.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $vehicle = Vehicleexpense::find($id);
        $aexp     = Account::where('category','EXPENSE')->get();
        $asset    = Account::where('category','ASSET')->get();
        $vehs = Vehicle::all();
        return View::make('vehicleexpenses.edit', compact('vehicle','vehs','aexp','asset'));
    }

    /*
     * Update the specified account in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $vehicle = Vehicleexpense::findOrFail($id);
        $data = $request->all();
        $validator = Validator::make($data, Vehicleexpense::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $vehicle->vehicle_id = $request->get('vehicle_id');
        $vehicle->amount  = str_replace( ',', '', $request->get('amount'));
        $vehicle->expenseincurred = $request->get('incur');
        $vehicle->date = $request->get('date');
        $vehicle->asset_account_id = $request->get('asset_id');
        $vehicle->expense_account_id = $request->get('aexp_id');
        $vehicle->update();
        $data = array(
            'credit_account' => $request->get('asset_id'),
            'debit_account' => $request->get('aexp_id'),
            'date' => $request->get('date'),
            'amount' => str_replace( ',', '', $request->get('amount')),
            'initiated_by' => 'system',
            'description' => 'Vehicles Expenses',
            'ajid' => $vehicle->asset_journal_id,
            'ejid' => $vehicle->expense_journal_id,
            'expid'=>$vehicle->id
        );
        return Redirect::route('vehicleexpenses.index');
    }

    /*
     * Remove the specified account from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Vehicleexpense::destroy($id);

        return Redirect::route('vehicleexpenses.index')->withDeleted('The expense record has been permanently deleted!!!!');
    }

}
