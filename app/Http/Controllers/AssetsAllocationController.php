<?php

use App\models\Account;
use App\models\AccountTransaction;
use App\models\Asset;
use App\models\AssetsAllocation;
use App\Http\Controllers\Controller;
use App\models\Journal;
use App\models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class AssetsAllocationController extends Controller {

    public function index()
	{
		$allocations=AssetsAllocation::where('submitted','!=',1)->get();
		$members = Member::all();
		return View::make('assets.allocation.index', compact('allocations','members'));
    }

    public function create()
	{
		// NEW ASSET PAGE
		$members = Member::all();
		$assets=Asset::where('status','<>','Disposed')
		->where('purchase_approved','=',1)->get();
		$allocations=AssetsAllocation::all();
		$assetNum = 'AST_000'.(Asset::all()->count()+1);
		return View::make('assets.allocation.create', compact('assetNum','members','assets','allocations'));
    }

    public function store(Request $request)
	{
		// STORE DATA IN DB
		$inputData=$request->all();
		$quantity=$request->get('quantity'); if($quantity<1){$quantity=0;}
 	    $station = $request->get('station');
		 $inputData;
		AssetsAllocation::registerAllocation($inputData);
		$allocations=AssetsAllocation::all(); $members = Member::all();
        /*
		$assetAc = 'Accumulated Depreciation';
		$expAc = 'Depreciation Expense';
		$account1 = Account::where('name', $assetAc)->first();
		$account2 = Account::where('name', $expAc)->first();
		if(empty($account1)){
			Account::createAccount('ASSET', $assetAc, $inputData['purchasePrice']);
			if(empty($account2)){
				Account::createAccount('EXPENSE', $expAc);
			}
		} else{
			Account::where('name', $assetAc)->increment('balance', $inputData['purchasePrice']);
		}*/

		//return Redirect::action('assets/index');
		return View::make('assets.allocation.index', compact('allocations','members'));
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// DISPLAY ASSET INFORMATION
		$asset = Asset::find($id);
		$member = Member::findorFail($asset->member_id);
		return View::make('assets.show', compact('asset','member'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// DISPLAY EDIT PAGE
		$asset = Asset::find($id);
		$member = Member::findorFail($asset->member_id);
		return View::make('assets.edit', compact('asset','member'));
	}
	public function disallocate($id)
	{
		// DISPLAY EDIT PAGE
		$allocation = AssetsAllocation::find($id);
		$allocation->submitted=1; $allocation->submitted_at=date('Y-m-d H:i:s');
		$allocation->update();
		//$member = Member::findorFail($asset->member_id);
		//return View::make('assets.edit', compact('asset','member'));
		return Redirect::action('AssetsAllocationController@index');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		// UPDATE DATA IN DB
		$asset->quantity=$request->input('quantity');
		$client->name = $request->input('name');
		if(!empty($request->input('lifeYears'))){
			$inputData = array_merge($request->all(), array('rate'=>'NULL', 'method'=>'years'));
		} elseif(!empty($request->input('rate'))){
			$inputData = array_merge($request->all(), array('lifeYears'=>'NULL', 'method'=>'rate'));
		}
		$assets=Asset::all(); $members = Member::all();

		// Reverse Existing Depreciation
		/*$item = Asset::find($id);
		$creditAc = Account::where('name', 'Accumulated Depreciation')->pluck('id');
		$debitAc = Account::where('name', 'Depreciation Expense')->pluck('id');
		$lastDepAmnt = round($item->purchase_price - $item->book_value, 2);
		$item->increment('book_value', $lastDepAmnt);

		Account::where('id', $creditAc)->increment('balance', $lastDepAmnt);
		Account::where('id', $debitAc)->decrement('balance', $lastDepAmnt);
		$data = array(
			'credit_account' =>$account['credit'] ,
			'debit_account' =>$account['debit'] ,
			'date' => $date,
			'amount' => $loanaccount->amount_disbursed,
			'initiated_by' => 'system',
			'description' => 'loan disbursement',
			'particulars_id' => '26',
			'narration' => $loanaccount->member->id
			);

		$journal = new Journal;

		$journal->journal_entry($data); */


		// Update Details
		Asset::updateAsset($inputData);
		//$this->depreciate($id);
		//return Redirect::action('assets@index');
		return View::make('assets.index', compact('assets','members'));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Asset::find($id)->delete();
		return Redirect::route('assets.index')->withDeleteMessage('successfully deleted!');
	}
	public function dispose($id,$action)
	{
		$asset = Asset::findorFail($id);
		if($action=='dispose'){$status='Disposed'; $disposed=1;}else if($action=='undispose'){$status='Used'; $disposed=0;}

		$asset->status =$status;
		$asset->disposed =$disposed;
		$asset->update();

		$assets=Asset::all(); $members = Member::all();
		return View::make('assets.index', compact('assets','members'));
	}

	/**
	 * Depreciation Method
	 */
	public function depreciate($id){
		$item = Asset::find($id);

		$creditAc = Account::where('name', 'Accumulated Depreciation')->pluck('id');
		$debitAc = Account::where('name', 'Depreciation Expense')->pluck('id');
		$depAmnt = Asset::calculateDepreciation($id);

		$depDiff = $item->book_value - $depAmnt;

		$dta1 = [
			'date' => date("Y-m-d"),
			'debit_account' => $debitAc,
			'credit_account' => $creditAc,
			'description' => "Depreciated $item->asset_name on ". date('jS M, Y'),
			'initiated_by' => Auth::user()->username,
		];

		if($depDiff < $item->salvage_value){
			$dta = array('amount'=>$item->book_value - $item->salvage_value);
			$data = array_merge($dta1, $dta);
		} else{
			$data = array_merge($dta1, array('amount' => $depAmnt));
		}

		// Store depreciation values in DB
		if($item->book_value == $item->purchase_price){
			// Record Depreciation
			$item->decrement('book_value', $depAmnt);
			if($item->book_value == $item->salvage_value){
				return Redirect::action('AssetMgmtController@index');
			}

			$item->last_depreciated = date('Y-m-d');
			$item->update();

			Account::where('id', $creditAc)->decrement('balance', $depAmnt);
			Account::where('id', $debitAc)->increment('balance', $depAmnt);

			$acTransaction = new AccountTransaction;
			$journal = new Journal;

			$acTransaction->createTransaction($data);
			$journal->journal_entry($data);

			return Redirect::action('AssetMgmtController@index');
		} else{
			// Reverse already existing depreciation & Re-run
			$lastDepAmnt = round($item->purchase_price - $item->book_value, 2);

			$reverseData = [
				'date' => date("Y-m-d"),
				'debit_account' => $creditAc,
				'credit_account' => $debitAc,
				'description' => "Reversed depreciation on $item->asset_name on ". date('jS M, Y'),
				'amount' => $lastDepAmnt,
				'initiated_by' => Auth::user()->username
			];

			// Reverse Existing Depreciation
			$item->increment('book_value', $lastDepAmnt);

			Account::where('id', $creditAc)->increment('balance', $lastDepAmnt);
			Account::where('id', $debitAc)->decrement('balance', $lastDepAmnt);

			$acTransaction = new AccountTransaction;
			$journal = new Journal;

			$acTransaction->createTransaction($reverseData);
			$journal->journal_entry($reverseData);

			// Re-record Depreciation
			$item->decrement('book_value', $depAmnt);
			if($item->book_value == $item->salvage_value){
				return Redirect::action('AssetMgmtController@index');
			}

			$item->last_depreciated = date('Y-m-d');
			$item->update();

			Account::where('id', $creditAc)->decrement('balance', $depAmnt);
			Account::where('id', $debitAc)->increment('balance', $depAmnt);

			$acTransaction->createTransaction($data);
			$journal->journal_entry($data);

			return Redirect::action('AssetMgmtController@index');
        }
    }

}
