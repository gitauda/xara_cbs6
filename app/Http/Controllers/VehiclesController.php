<?php
namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\models\Charge;
use App\models\Member;
use App\models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class VehiclesController extends Controller {

	/*
	 * Display a listing of accounts
	 *
	 * @return Response
	 */
	public function index()
	{
		$vehicles = Vehicle::all();

		return View::make('vehicles.index', compact('vehicles'));
	}

	/*
	 * Show the form for creating a new account
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('vehicles.create');
	}

    public function addVehicle($id){
        $member = Member::where('id','=',$id)->first();
        $charges = Charge::where('category', 'member')->get();
        return View::make('vehicles.add', compact('member','charges'));
    }

    public function addedVehicle(Request $request){
        $validator = Validator::make($data = $request->all(), Vehicle::$rules);

		if ($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}
		// check if code exists
		$vehicle = new Vehicle;

        $vehicle->make = $request->input('make');
		$vehicle->regno = $request->input('regno');
		$vehicle->member_id = $request->input('member');
        $vehicle->registration_fee = $request->input('registration');
		$vehicle->save();
        $member = $request->input('member');
        return Redirect::route('members.show',compact('member'));
    }
	/*
	 * Store a newly created account in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($data = $request->all(), Vehicle::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}


		// check if code exists


		$vehicle = new Vehicle;

        $vehicle->make = $request->input('make');
		$vehicle->regno = $request->input('regno');

		$vehicle->save();


		return Redirect::route('vehicles.index');
	}

	/*
	 * Display the specified account.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$vehicle = Vehicle::findOrFail($id);

		return View::make('vehicles.show', compact('vehicle'));
	}

	/*
	 * Show the form for editing the specified account.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$vehicle = Vehicle::find($id);

		return View::make('vehicles.edit', compact('vehicle'));
	}

	/*
	 * Update the specified account in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$vehicle = Vehicle::findOrFail($id);

		$validator = Validator::make($data = $request->all(), Vehicle::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
        $vehicle->make = $request->input('make');
		$vehicle->regno = $request->input('regno');

		$vehicle->update();



		return Redirect::route('vehicles.index');
	}

	/*
	 * Remove the specified account from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Vehicle::destroy($id);

		return Redirect::route('vehicles.index');
	}

}
