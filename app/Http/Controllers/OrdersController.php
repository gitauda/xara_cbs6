<?php
namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Application;
use App\models\Order;

use http\Env\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class OrdersController extends Controller {

	/*
	 * Display a listing of orders
	 *
	 * @return Response
	 */
	public function index()
	{
		$orders = Order::all();

		return view('orders.index', compact('orders'));
	}

	/*
	 * Show the form for creating a new order
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('orders.create');
	}

	/*
	 * Store a newly created order in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($data = $request->all(), Order::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$order = new Order;

		$order->product_id = $request->get('product_id');
		$order->order_date = date('d-m-Y');
		// $order->customer_name = $member->member_name;
		// $order->customer_number = $member->member_account;
		$order->save();

		$application = new Application;

		// $application->product = $loan_product;
		// $application->member_account = $member->member_account;
		// $application->member = $member->member_name;
		// $application->amount = $request->get('product_price');
		$application->save();

		return Redirect::route('orders.index');
	}

	/*
	 * Display the specified order.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$order = Order::findOrFail($id);

		return view('orders.show', compact('order'));
	}

	/*
	 * Show the form for editing the specified order.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$order = Order::find($id);

		return view('orders.edit', compact('order'));
	}

	/*
	 * Update the specified order in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$order = Order::findOrFail($id);

		$validator = Validator::make($data = $request->all(), Order::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$order->update($data);

		return Redirect::route('orders.index');
	}

	/*
	 * Remove the specified order from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Order::destroy($id);

		return Redirect::route('orders.index');
	}

}
