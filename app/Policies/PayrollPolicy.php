<?php

namespace App\Policies;

use App\Models\Payroll;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PayrollPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(?User $user, Payroll $payroll){
        if($payroll){
            return true;
        }

        if($user->can('manager_payroll')){
            return true;
        }

        return $user->user_type == 'MD';
    }

    public function create(User $user){
        if($user->can('manager_payroll')){
            return true;
        }
    }

    public function update(User $user, Payroll $payroll){
        if($user->can('manager_payroll')){
            return true;
        }
    }

    public function delete(User $user, Payroll $payroll){
        if($user->can('manager_payroll')){
            return true;
        }
    }
}
