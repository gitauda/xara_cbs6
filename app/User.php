<?php namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{

    use Notifiable,Billable;
    use HasRoles;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	* validation rules
	*
	*/
	public static $rules = [
        #'password' => 'required|min:4',
        'create' => [
            'username' => 'required|alpha_dash',
            'email'    => 'required|email',
        ]
    ];


}
