<?php

namespace App\Exports;

use App\models\LoanAccount;
use App\Models\Loantransaction;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class DisbursedLoanListingExport implements FromQuery,WithHeadings,WithMapping,WithColumnWidths,WithEvents,WithColumnFormatting
{
    use Exportable;

    private $data;
    private $loans;
    private $period;
    private $from;
    private $to;

    public function __construct($data,$period){

        #$this->loans =$loans;
        $this->data =$data;
        $this->period = $period;
//        $this->from = $from;
//        $this->to = $to;
    }

    /*
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        if($this->data['period'] == 'As at date'){
            $loans = Loanaccount::query()->where('is_disbursed', true)->where('date_disbursed', '<=', $this->data['date']);
        }else{
            $loans = Loanaccount::query()->where('is_disbursed', true)->whereBetween('date_disbursed', array($this->data['from'], $this->data['to']));
        }
        return $loans;

    }

    public function headings(): array
    {
        return [
            ["Loan Listing Report ". $this->period],
            ["Membership No","Member name","Loan product","Loan Number", "Loan Amount", "Loan Balance"],
            ["Total:"]
        ];
    }

    public function map($loan): array
    {

        if(Loantransaction::getLoanBalance($loan) > 5)
            return [
                [
                    $loan->member->membership_no,
                $loan->member->name,
                $loan->loanproduct->name,
                $loan->account_number,
                Loanaccount::getActualAmount($loan),
                Loantransaction::getLoanBalanceAt($loan, $this->data['to'])
                ],
                [
                   # $loan->lines->first()->total +=Loanaccount::getActualAmount($loan),
                   # $loan->lines->first()->totalBal += Loantransaction::getLoanBalanceAt($loan, $this->data['to'])
                ]
            ];



    }

    public function columnWidths(): array
    {
        return [
            "A"=>30, "B"=>50 , "C"=>25, "D"=>20, "E"=>20, "F"=>20
        ];
    }

    public function registerEvents(): array
    {
        $row = 3;
        $total = 0;
        $totalBal = 0;
        return [
            AfterSheet::class => function(AfterSheet $event) use ($row,$total,$totalBal) {
                $event->sheet->mergeCells('A1:E1');
                $event->sheet->getStyle('A1:F1')->applyFromArray([
                    'font' =>[
                        'bold' => true,
                        'family' => 'calibri'
                    ],
                    'alignment' =>[
                        'horizontal'=> Alignment::HORIZONTAL_CENTER
                    ]
                ]);



                $event->sheet->getStyle('A2:F2')->applyFromArray([
                    'font' =>[
                        'bold' => true,
                    ],
                ]);
                $event->sheet->getStyle('E'.$row)->applyFromArray([
                    'font' =>[
                        'bold' => true,
                    ],
                ]);
                $event->sheet->getStyle('F'.$row)->applyFromArray([
                    'font' =>[
                        'bold' => true,
                    ],
                ]);

                $event->sheet->mergeCells('A'.$row.':D'.$row);
            }
        ];
    }

    public function columnFormats(): array
    {
//        $total += Loanaccount::getActualAmount($loan);
//        $totalBal += Loantransaction::getLoanBalanceAt($loan, $this->to);
        return [
            'B3' => NumberFormat::FORMAT_NUMBER_00,
            'F3' => NumberFormat::FORMAT_NUMBER_00
        ];
    }
}
