<?php

namespace App\Exports;

use App\models\Member;
use Maatwebsite\Excel\Concerns\FromCollection;

class MemberListing implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Member::all();
    }
}
