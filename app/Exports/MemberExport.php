<?php

namespace App\Exports;

use App\models\Member;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class MemberExport implements FromQuery, Responsable,WithHeadings, WithMapping, ShouldAutoSize,WithEvents
{
    use Exportable;

    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    /*
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {

        return Member::query();
    }

    public function map($member): array
    {
        #dd($member->group->name);
       return [
           $member->membership_no,
           $member->name,
           $member->group['name'],
           $member->branch->name,
       ];
    }

    public function headings(): array
    {
        return [
            ["MOTOSACCO SAVINGS REPORTS"],
            ["Member Number", "Member Name", "Member Group", "Member Branch"]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event){
                $event->sheet->mergeCells('A1:D1');
                $event->sheet->getStyle('A1:D1')->applyFromArray([
                    'font' =>[
                        'bold' => true,
                        'family' => 'calibri'
                    ],
                    'alignment' =>[
                        'horizontal'=> Alignment::HORIZONTAL_CENTER
                    ]
                ]);



                $event->sheet->getStyle('A2:D2')->applyFromArray([
                    'font' =>[
                        'bold' => true,
                    ],
                    'borders' => [
                        'borderStyle' => Border::BORDER_THIN
                    ]
                ]);
            }
        ];
    }
}
