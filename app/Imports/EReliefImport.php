<?php

namespace App\Imports;

use App\ERelief;
use Maatwebsite\Excel\Concerns\ToModel;

class EReliefImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ERelief([
            //
        ]);
    }
}
