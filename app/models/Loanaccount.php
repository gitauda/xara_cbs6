<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Loanaccount extends Model {

    // Add your validation rules here
    public static $rules = [
        'loanproduct_id' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = [];



    public function loanproduct(){

        return $this->belongsTo('App\models\Loanproduct');
    }


    public function member(){
        return $this->belongsTo('App\models\Member');
    }


    public function loanrepayments(){

        return $this->hasMany('App\models\Loanrepayment');
    }

    public static function Loans($id){

        return Loanaccount::where('member_id',$id)->get();
    }


    public function loantransactions(){

        return $this->hasMany('App\models\Loantransaction');
    }

    public function guarantors(){

        return $this->hasMany('App\models\Loanguarantor');
    }

    public static function refinance($member,$loanproduct){
        #TODO: changeo
        $product=$loanproduct->id;
        $memid=$member->id;
        $loans=Loanaccount::where('member_id',$memid)
            ->where('is_approved','=',1)
            ->where('loanproduct_id','=',$product)
            ->get();
        $total_balance=0;
        $date=date('Y-m-d');
        if(count($loans)>0){
            foreach($loans as $loan){
                $balance=Loantransaction::getLoanBalance($loan);
                //Repay all loans
                $principal_due = $balance/2;
                $interest_due = $balance/2;
                if($balance>0){
                    Loanrepayment::payInterest($loan, $date, $interest_due);
                    Loanrepayment::payPrincipal($loan, $date, $principal_due);
                    Loantransaction::repayLoan($loan, $balance, $date,0,0);
                    //Close all loans
                    Loanaccount::closeLoan($loan);
                    //Get total loan balances
                }
                $total_balance+=$balance;
            }
        }
        if($total_balance<1){$total_balance=0;} #changeo
        return $total_balance;
    }
    public static function amountGuarantee($member_id){
        $guaranteed = DB::table('loanguarantors')->where('member_id','=',$member_id)->sum('amount');
        return $guaranteed;
    }
    //Close a loan account
    public static function closeLoan($loanaccount){
        $loanaccount->loan_status='closed';
        $loanaccount->update();
    }
    public static function submitApplication($data){

        $member_id = array_get($data, 'member_id');
        $loanproduct_id = array_get($data, 'loanproduct_id');
        $member = Member::findorfail($member_id);
        $loanproduct = Loanproduct::findorfail($loanproduct_id);
        //refinance the loan
        $refinance=Loanaccount::refinance($member,$loanproduct);
        $amount_applied=array_get($data,'amount_applied')- $refinance;
        //Submit Application
        $application = new Loanaccount;
        $application->member()->associate($member);
        $application->loanproduct()->associate($loanproduct);
        $application->application_date = array_get($data, 'application_date');
        if($amount_applied>0){
            $application->amount_applied = $amount_applied;
        }else{
            $application->amount_applied = 0.00;
        }
        $application->interest_rate = $loanproduct->interest_rate;
        $application->rate_type="monthly"; $application->frequency="monthly";
        $application->period = $loanproduct->period;
        $application->repayment_duration = array_get($data, 'repayment_duration');
        $application->account_number = Loanaccount::loanAccountNumber($application);
        $application->save();

        $loanaccount = Loanaccount::findorfail($application->id);
        $loanaccount->account_number = Loanaccount::loanAccountNumber($loanaccount);
        $loanaccount->update();
        if(!empty(array_get($data, 'purposes'))){
            for ($i=0; $i <count(array_get($data, 'purposes')) ; $i++) {
                # code...

                if((array_get($data, 'purposes')[$i] != '' || array_get($data, 'purposes')[$i] != null)){
                    $purpose = new Loanpurpose;
                    $purpose->loanaccount_id=$application->id;
                    $purpose->purpose = array_get($data, 'purposes')[$i];
                    $purpose->amount = array_get($data, 'amount')[$i];
                    $purpose->save();
                }
            }
        }
        if(!empty(array_get($data, 'securities'))){
            for ($i=0; $i <count(array_get($data, 'securities')) ; $i++) {
                # code...

                if((array_get($data, 'securities')[$i] != '' || array_get($data, 'securities')[$i] != null)){
                    $security = new Loansecurity;
                    $security->loanaccount_id=$application->id;
                    $security->name = array_get($data, 'securities')[$i];
                    $security->save();

                }
            }
        }

        include(app_path() . '/views/AfricasTalkingGateway.php');
        for ($i=0; $i <count(array_get($data, 'guarantor_id')) ; $i++) {
            #code...
            if((array_get($data, 'guarantor_id')[$i] != '' || array_get($data, 'guarantor_id')[$i] != null)){
                $guarantor = new Loanguarantor;
                $guarantor->loanaccount_id=$application->id;
                $guarantor->member_id = array_get($data, 'guarantor_id')[$i];
                $guarantor->amount=array_get($data, 'guarantoramount')[$i]; #changeo
                $guarantor->date = array_get($data, 'application_date');
                $guarantor->save();
                $member1 = Member::findOrFail(array_get($data, 'guarantor_id')[$i]);
                /**if($member1->email != null){
                Mail::send( 'emails.guarantor', array('name'=>$member1->name,'mname'=>$member->name, 'id'=>$member->id_number, 'amount_applied'=>array_get($data, 'amount_applied'),'product'=>$loanproduct->name,'application_date'=>array_get($data, 'application_date')), function( $message ) use ($member1)
                {
                $message->to($member1->email )->subject( 'Guarantor Approval' );
                });
                }*/

                // Be sure to include the file you've just downloaded

                // Specify your authentication credentials
                $username   = "lixnet";
                $apikey     = "a8d19ab5cfe8409bf737a4ef53852ab515560e31fcac077c3e6bb579cc2681e6";
                // Specify the numbers that you want to send to in a comma-separated list
                // Please ensure you include the country code (+254 for Kenya in this case)
                $recipients = $member1->phone;
                // And of course we want our recipients to know what we really do
                $message    = $member->name." ID ".$member->id_number." has borrowed a loan of ksh. ".array_get($data, 'amount_applied')." for loan product ".$loanproduct->name." on ".array_get($data, 'application_date')." and has selected you as his/her guarantor.\nPlease login and approve or reject\nRegards,\n Motosacco.";
                // Create a new instance of our awesome gateway class
                $gateway    = new AfricasTalkingGateway($username, $apikey);
                /*************************************************************************************
                NOTE: If connecting to the sandbox:
                1. Use "sandbox" as the username
                2. Use the apiKey generated from your sandbox application
                https://account.africastalking.com/apps/sandbox/settings/key
                3. Add the "sandbox" flag to the constructor
                $gateway  = new AfricasTalkingGateway($username, $apiKey, "sandbox");
                 **************************************************************************************/
                // Any gateway error will be captured by our custom Exception class below,
                // so wrap the call in a try-catch block
                /* try
                 {
                     // Thats it, hit send and we'll take care of the rest.
                     $results = $gateway->sendMessage($recipients, $message);

                     $thisMonth=date('Y-m',time());
                     $smsLogs=Smslog::where('date','like',$thisMonth.'%')
                             ->where('user',$member_id)->first();
                     if(sizeof($smsLogs) >=1){
                         //update sms logs
                         $smsLogs->monthlySmsCount +=1;
                         $smsLogs->update();
                     }else{
                         //insert to sms logs
                         $newSms=new Smslog();
                         $newSms->user=$member_id;
                         $newSms->monthlySmsCount=1;
                         $newSms->date=date('Y-m-d',time());
                         $newSms->charged=0;
                         $newSms->save();
                     }

                     //foreach($results as $result) {
                         // status is either "Success" or "error message"
                         //echo " Number: " .$result->number;
                         //echo " Status: " .$result->status;
                         //echo " StatusCode: " .$result->statusCode;
                         //echo " MessageId: " .$result->messageId;
                         //echo " Cost: "   .$result->cost."\n";
                     //}
                 }
                 catch ( AfricasTalkingGatewayException $e )
                 {
                     echo "Encountered an error while sending: ".$e->getMessage();
                 }*/
            }
        }

        /***********************if(array_get($data, 'amount_applied')<=$loanproduct->auto_loan_limit){
        $loanaccount = Loanaccount::findorfail($application->id);
        $loanaccount->date_approved = array_get($data, 'application_date');
        $loanaccount->amount_approved = array_get($data, 'amount_applied');
        //$loanaccount->amount_to_pay = (array_get($data, 'amount_approved')*array_get($data, 'interest_rate')/100)+array_get($data, 'amount_approved');
        $loanaccount->interest_rate = $loanproduct->interest_rate;
        $loanaccount->period = $loanproduct->period;
        $loanaccount->is_approved = TRUE;
        $loanaccount->is_new_application = FALSE;
        $amount = array_get($data, 'amount_applied');
        $date = array_get($data, 'application_date');
        $loanaccount->date_disbursed = $date;
        $loanaccount->amount_disbursed = $amount;
        $loanaccount->repayment_start_date = array_get($data, 'application_date');
        $loanaccount->account_number = Loanaccount::loanAccountNumber($loanaccount);
        $loanaccount->is_disbursed = TRUE;
        $loanaccount->update();
        $loanamount = $amount + Loanaccount::getInterestAmount($loanaccount);
        Loantransaction::disburseLoan($loanaccount, $loanamount, $date);
        }

        if(array_get($data, 'guarantor_id1') != null || array_get($data, 'guarantor_id1') != ''){
        $mem_id = array_get($data, 'guarantor_id1');
        $member1 = Member::findOrFail($mem_id);
        $loanaccount = Loanaccount::findOrFail($application->id);
        $guarantor = new Loanguarantor;
        $guarantor->member()->associate($member1);
        $guarantor->loanaccount()->associate($loanaccount);
        $guarantor->save();
        if($member1->email != null){
        Mail::send( 'emails.guarantor', array('name'=>$member1->name,'mname'=>$member->name, 'id'=>$member->id_number, 'amount_applied'=>array_get($data, 'amount_applied'),'product'=>$loanproduct->name,'application_date'=>array_get($data, 'application_date')), function( $message ) use ($member1)
        {
        $message->to($member1->email )->subject( 'Guarantor Approval' );
        });
        }
        // Be sure to include the file you've just downloaded

        // Specify your authentication credentials
        $username   = "lixnet";
        $apikey     = "a8d19ab5cfe8409bf737a4ef53852ab515560e31fcac077c3e6bb579cc2681e6";
        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        $recipients = $member1->phone;
        // And of course we want our recipients to know what we really do
        $message    = $member->name." ID ".$member->id_number." has borrowed a loan of ksh. ".array_get($data, 'amount_applied')." for loan product ".$loanproduct->name." on ".array_get($data, 'application_date')." and has selected you as his/her guarantor.
        Please login and approve or reject
        Thank you!";
        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);

        // Any gateway error will be captured by our custom Exception class below,
        // so wrap the call in a try-catch block
        try
        {
        // Thats it, hit send and we'll take care of the rest.
        $results = $gateway->sendMessage($recipients, $message);

        foreach($results as $result) {
        // status is either "Success" or "error message"
        echo " Number: " .$result->number;
        echo " Status: " .$result->status;
        //echo " StatusCode: " .$result->statusCode;
        echo " MessageId: " .$result->messageId;
        echo " Cost: "   .$result->cost."\n";
        }
        }
        catch ( AfricasTalkingGatewayException $e )
        {
        echo "Encountered an error while sending: ".$e->getMessage();
        }
        }
         ***********/
        Audit::logAudit(date('Y-m-d'), Confide::user()->username, 'loan application', 'Loans', array_get($data, 'amount_applied'));
    }



    public static function submitShopApplication($data){

        $mem = array_get($data, 'member');

        $member_id = DB::table('members')->where('membership_no', '=', $mem)->pluck('id');
        $loanproduct_id = array_get($data, 'loanproduct');


        $member = Member::findorfail($member_id);

        $product = Product::findorfail(array_get($data, 'product'));

        $loanproduct = Loanproduct::findorfail($loanproduct_id);


        $application = new Loanaccount;


        $application->member()->associate($member);
        $application->loanproduct()->associate($loanproduct);
        $application->application_date = date('Y-m-d');

        $application->amount_applied = array_get($data, 'amount');
        $application->interest_rate = $loanproduct->interest_rate;
        $application->period = array_get($data, 'repayment');
        $application->repayment_duration = array_get($data, 'repayment');
        $application->loan_purpose = array_get($data, 'purpose');
        $application->save();


        Order::submitOrder($product, $member);
    }
    public static function loanAccountNumber($loanaccount){

        $member = Member::find($loanaccount->member->id);

        $count = count($member->loanaccounts);
        $count = $count + 1;

        //$count = DB::table('loanproducts')->where('member_id', '=', $loanaccount->member->id)->count();

        $loanno = $loanaccount->loanproduct->short_name."-".$loanaccount->member->membership_no."-".$count;

        return $loanno;

    }

    public static function intBalOffset($loanaccount){
        $principal = Loanaccount::getPrincipalBal($loanaccount);
        $rate = $loanaccount->interest_rate/100;
        $interest_amount = $principal * $rate;
        return $interest_amount;
    }

    public static function getEMPTacsix($loanaccount){
        $principal = $loanaccount->amount_disbursed;
        $rate = $loanaccount->interest_rate/100;
        $time = $loanaccount->repayment_duration;
        $interest = $principal * $rate;
        $amount = $principal + $interest;
        $amt = $amount/$time;
        return $amt;
    }


    public static function getInterestAmount($loanaccount){
        /*$principal =Loanaccount::getPrincipalBal($loanaccount);
        $rate = $loanaccount->interest_rate/100;
        $interest_amount= $principal * $rate;*/
        $period=$loanaccount->period;
        $period2=Loantransaction::getInstallment($loanaccount,'period');
        $period2=round($period2);  $rate=Loantransaction::getrate($loanaccount);
        $loan_balance=(float)$loanaccount->amount_disbursed+(float)$loanaccount->top_up_amount; $total_interest=0;
        $loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
        $formula=$loanproduct->formula;
        if($formula=="SL"){
            $total_interest=$loan_balance*$rate*$period;
        }else{
            for($i=1; $i<=$period; $i++){
                $installment=Loantransaction::getInstallment($loanaccount);
                $loan_balance-=$installment;
                /*if($loan_balance<0){
                    $total_interest+=$installment;
                }*/
            }
            $total_interest=$loan_balance*-1;
        }

        return $total_interest;
    }


    public static function hasAccount($member, $loanproduct){
        foreach ($member->loanaccounts as $loanaccount) {
            if($loanaccount->loanproduct->name == $loanproduct->name){
                return true;
            }
            else {
                return false;
            }
        }
    }
    /*Loan Insurance*/
    public static function getInsurance($repayment_period, $amount_applied){
        $insurance = (((5.03*$repayment_period) + 3.03)* $amount_applied)/6000;
        return $insurance;
    }


    public static function getTotalDue($loanaccount){
        $balance = Loantransaction::getLoanBalance($loanaccount);
        if($balance > 1 ){
            $principal = Loantransaction::getPrincipalDue($loanaccount);
            $interest = Loantransaction::getInterestDue($loanaccount);
            $total = $principal + $interest;
            return $total;
        }else {
            return 0;
        }
    }

    public static function getDurationAmount($loanaccount){
        $interest = Loanaccount::getInterestAmount($loanaccount);
        $principal = $loanaccount->amount_disbursed;
        $total =$principal + $interest;
        if($loanaccount->repayment_duration != null){
            $amount = $total/$loanaccount->repayment_duration;
        } else {
            $amount = $total/$loanaccount->period;
        }
        return $amount;
    }


    public static function getLoanAmount($loanaccount){
        $interest_amount = Loanaccount::getInterestAmount($loanaccount);
        $principal = $loanaccount->amount_disbursed;
        $topup = $loanaccount->top_up_amount;
        $overcharge=Loanaccount::getOverchargeAmount($loanaccount);
        $amount = $principal + $interest_amount + $topup;
        #$amount=$amount-$overcharge; if((float)$amount<1){$amount=0;}
        return $amount;
    }

    public static function getActualAmount($loanaccount){
        $principal = $loanaccount->amount_disbursed;
        $topup = $loanaccount->top_up_amount;
        $amount = $principal + $topup;
        return $amount;
    }
    public static function getOverchargeAmount($loanaccount){
        $overcharge=DB::table('loanaccounts')->where('member_id', '=', $loanaccount->member_id)->where('loanproduct_id', '=', $loanaccount->loanproduct_id)->sum('amount_overpaid');
        if(empty($overcharge) || (float)$overcharge<1){$overcharge=0;}
        return $overcharge;
    }
    public static function xovercharge($loanaccount,$amount){ #changeo
        $ocaccounts=DB::table('loanaccounts')->where('member_id', '=', $loanaccount->member_id)->where('loanproduct_id', '=', $loanaccount->loanproduct_id)->get();/*->where('amount_overpaid','!=','')*/
        $xamount=(float)$amount;
        foreach($ocaccounts as $ocaccount){ $overpaid=$ocaccount->amount_overpaid;
            if($overpaid>=$xamount){$xamount=(float)$overpaid-$xamount; $newOverpaid=$xamount;}else{
                if($xamount>0){$xamount-=(float)$overpaid;}else{$xamount=0;}
                $newOverpaid=0;
            }
            if($newOverpaid>0){$is_overpaid=1;}else{$is_overpaid=0;}
            Loanaccount::where('id', '=', $ocaccount->id)
                ->update(['is_overpaid' => $is_overpaid]);
            Loanaccount::where('id', '=', $ocaccount->id)
                ->update(['amount_overpaid' => $newOverpaid]);
        }
    }
    public static function guaranteedAmount($loanaccount){ #changeo
        $g_amount=DB::table('loanguarantors')->where('loanaccount_id', '=', $loanaccount->id)->sum('amount');
        return (float)$g_amount;
    }
    public static function getEMP($loanaccount){
        $loanamount = Loanaccount::getLoanAmount($loanaccount);
        if($loanaccount->repayment_duration > 0){
            $period = $loanaccount->repayment_duration;
        }else {
            $period = $loanaccount->period;
        }
        if($loanaccount->loanproduct->amortization == 'EP'){
            if($loanaccount->loanproduct->formula == 'RB'){
                $principal = $loanaccount->amount_disbursed + $loanaccount->top_up_amount;
                $principal = $principal/$period;
                $interest = Loantransaction::getLoanBalance($loanaccount) * ($loanaccount->loanproduct->rate/100);
                $mp = $principal + $interest;
            }
            if($loanaccount->loanproduct->formula == 'SL'){
                $mp = $loanamount/$period;
            }
        }
        if($loanaccount->loanproduct->amortization == 'EI'){
            $mp = $loanamount / $loanaccount->repayment_duration;
        }
        return $mp;
    }

    public static function getInterestBal($loanaccount,$date=null){
        $interest_amount = Loanaccount::getInterestAmount($loanaccount);
        $interest_paid = Loanrepayment::getInterestPaid($loanaccount,$date);
        $interest_bal = (float)$interest_amount - (float)$interest_paid;
        return $interest_bal;
    }

    public static function getPrincipalBal($loanaccount,$date=null){
        $date_disbursed=$loanaccount->date_disbursed;
        $amount_disbursed=$loanaccount->amount_disbursed;
        if(!isset($date_disbursed)){$date_disbursed=0000-00-00;}
        if(!isset($amount_disbursed)){$amount_disbursed=00;}
        $arrears = Loantransaction::where('loanaccount_id', $loanaccount->id)
            ->where('type', 'debit')
            ->where('date', '>', $date_disbursed)
            ->sum('amount');
        $principal_amount = $loanaccount->amount_disbursed + $loanaccount->top_up_amount;// + $arrears;
        $principal_paid =Loanrepayment::getPrincipalPaid($loanaccount,$date);
        $principal_bal = (float)$principal_amount - (float)$principal_paid;
        return $principal_bal;
    }
    public static function getLoanBalNoInterest($loanaccount,$date=null){
        $date_disbursed=$loanaccount->date_disbursed;
        $amount_disbursed=$loanaccount->amount_disbursed;
        if(!isset($date_disbursed)){$date_disbursed=0000-00-00;}
        if(!isset($amount_disbursed)){$amount_disbursed=00;}
        $principal_amount = $loanaccount->amount_disbursed + $loanaccount->top_up_amount;// + $arrears;
        $amount_paid =Loanrepayment::getAmountPaid($loanaccount,$date);
        $loan_bal = $principal_amount - $amount_paid;
        return $loan_bal;
    }
    public static function getLoanB($loanaccount){
        $paid = DB::table('loanrepayments')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>', $loanaccount->date_disbursed)->sum('principal_paid');
        return $paid;
    }
    public static function getPrincipalPaid($loanaccount, $date = null){
        if($date == null){
            $date = date('Y-m-d');
        }
        $principal_paid = Loanrepayment::getPrincipalPaidAt($loanaccount, $date);
        return $principal_paid;
    }

    public static function getDeductionAmount($loanaccount, $date){
        $transactions = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->get();
        $amount = 0;
        foreach ($transactions as $transaction) {
            $period = date('m-Y', strtotime($transaction->date));
            if($date == $period){
                if($transaction->type == 'credit'){
                    $amount = $transaction->amount;
                }
            }
        }
        return $amount;
    }

    public static function getRepaymentAccount($account, $date){

        $transactions = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->get();
        $amount = '';
        foreach ($transactions as $transaction) {

            $period = date('m-Y', strtotime($transaction->date));

            if($date == $period){

                if($transaction->type == 'credit'){
                    $amount = $transaction->amount;
                }

            }
        }
        return $loanaccount;
    }

}
