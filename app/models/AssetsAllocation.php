<?php  namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class AssetsAllocation extends Model{
	public $table = 'assetsallocations';

	public static $rules = [

    ];

    public function assets()
    {
        return $this->belongsTo('Asset');
    }
    public static function registerAllocation($data){
		$allocation = new AssetsAllocation;

		$allocation->asset = $data['asset'];
		$allocation->member_id = $data['member'];
		$allocation->amount = $data['quantity'];
		$allocation->date_allocated = date('Y-m-d');
		$allocation->submission_date = $data['sub_date'];
		$allocation->save();
	}
}
?>
