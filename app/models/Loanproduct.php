<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Loanproduct extends Model {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function loanpostings(){

		return $this->hasMany('App\Loanposting');
	}


	public function loanaccounts(){

		return $this->hasMany('App\Loanaccount');
	}


	public function charges(){

		return $this->belongsToMany('App\Charge');
	}


	public static function submit($data){

		//$charges = Input::get('charge');

		$loanproduct = new Loanproduct;
		$membership_duration=$data['membershipduration'];
		if(empty($membership_duration)){
            $membership_duration=0;
        }

		$loanproduct->name = $data['name'];
		$loanproduct->short_name = $data['short_name'];
		$loanproduct->interest_rate = $data['interest_rate'];
		$loanproduct->formula = $data['formula'];
		$loanproduct->amortization = $data['amortization'];
		$loanproduct->currency = $data['currency'];
		$loanproduct->period = $data[ 'period'];
		$loanproduct->auto_loan_limit = $data[ 'autoloanlimit'];
		$loanproduct->application_form = $data[ 'appform'];
		$loanproduct->membership_duration=$membership_duration;
		$loanproduct->max_multiplier=3;
		$loanproduct->save();

		Audit::logAudit(date('Y-m-d'), Auth::user()->username, 'loan product creation', 'Loans', '0');


		$loan_id = $loanproduct->id;

		Loanposting::submit($loan_id, $data);


		/*
		foreach($charges as $charge){

			$loanproduct->charges()->attach($charge);
		}

		*/

	}

}
