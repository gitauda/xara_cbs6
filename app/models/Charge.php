<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Charge extends Model {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];


	public function loanproducts(){

		return $this->belongsToMany('Loanproduct');
	}


	public function savingproducts(){

		return $this->belongsToMany('Savingproduct');
	}

}
