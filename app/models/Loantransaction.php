<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Zizaco\Confide\Confide;

class Loantransaction extends Model {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];
	// Don't forget to fill this array
	protected $fillable = [];
	public function loanaccount(){

		return $this->belongsTo('Loanaccount');
	}
	/*public static function getLoanBalance($loanaccount){
			$principal_bal = Loanaccount::getPrincipalBal($loanaccount);
			if(!empty($principal_bal)){
				$rate = $loanaccount->interest_rate/100;
				$interest_due = $principal_bal * $rate;
				$balance = $principal_bal + $interest_due;
				return $balance;
			}else{
				return 0;
			}
	}*/
	#BIGCHANGE
	/*public static function getLoanBalance($loanaccount){
		$amountpaid = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>', $loanaccount->date_disbursed)->where('type', '=', 'credit')->sum('amount');
		$period=Loantransaction::getInstallment($loanaccount,'period');
		$eachpay=Loantransaction::getInstallment($loanaccount,'l');
		$amountowing=(int)$eachpay*(int)$period;
		$loanbalance=$amountowing-$amountpaid;
		return $loanbalance;
	}*/
	public static function getLoanBalance($loanaccount){
		/*
		$period=Loantransaction::getInstallment($loanaccount,'period');
		$eachpay=Loantransaction::getInstallment($loanaccount,'l');
		$amountowing=(int)$eachpay*(int)$period;
		$loanbalance=$amountowing-$amountpaid;*/
		$amountpaid = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('type', '=', 'credit')->sum('amount');
		$amountgiven=(float)$loanaccount->amount_disbursed+(float)$loanaccount->top_up_amount;
		$total_interest=Loanaccount::getInterestAmount($loanaccount);
		$totaltobepaid=(float)$amountgiven + (float)$total_interest;
		$loanBal=(float)$totaltobepaid-(float)$amountpaid; if($loanBal<1){$loanBal=0;}  $loanBal=round($loanBal,2);
		return $loanBal;
	}

	public static function getLoanBalanceAt($loanaccount,$date){
		$amountpaid = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->whereBetween('date', array($loanaccount->date_disbursed, $date))->where('type', '=', 'credit')->sum('amount');
		$amountgiven=(float)$loanaccount->amount_disbursed+(float)$loanaccount->top_up_amount;
		$total_interest=Loanaccount::getInterestAmount($loanaccount);
		$totaltobepaid=(float)$amountgiven+(float)$total_interest;
		return (float)$totaltobepaid-(float)$amountpaid;
	}
	public static function getMemberLoanBalance($member_id,$what=NULL){
		$loanaccounts = DB::table('loanaccounts')->where('member_id', '=', $member_id)->where('is_disbursed','=',1)->get();
		$loanBalances=0; $withGuaranteeLoanBal=0;
		foreach($loanaccounts as $loanaccount){
			$loanBalance=Loantransaction::getLoanBalance($loanaccount);
			$guaranteed=Loanaccount::guaranteedAmount($loanaccount);
			if($loanBalance<1){$loanBalance=0;}
			$withGuaranteeBal=(float)$loanBalance-(float)$guaranteed; if($withGuaranteeBal<1){$withGuaranteeBal=0;}
			$loanBalances+=$loanBalance; $withGuaranteeLoanBal+=$withGuaranteeBal;
		}
		if($what=='withGuaranteeBal'){return $withGuaranteeLoanBal;}else{return $loanBalances;}
	}
	public static function getLoanBalanceNoInterest($loanaccount){
		$amountpaid = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('type', '=', 'credit')->sum('amount');
		$amountgiven=(float)$loanaccount->amount_disbursed+(float)$loanaccount->top_up_amount;
		$totaltobepaid=(float)$amountgiven;
		return (float)$totaltobepaid-(float)$amountpaid;
	}
	#end BIGCHANGE
	/*public static function getLoanBalanceAt($loanaccount, $date){
		$loanAmount = $loanaccount->amount_disbursed + $loanaccount->top_up_amount;
		$principal_paid = Loanrepayment::getPrincipalPaidAt($loanaccount, $date);
		$rate = $loanaccount->interest_rate/100;
		$principal_bal = $loanAmount - $principal_paid;
		$interest_due = $principal_bal * $rate;
		$balance = $principal_bal + $interest_due;

		$arrearDate = date('Y-m-d', strtotime('+1 day', strtotime($loanaccount->date_disbursed)));
		$arrears = Loantransaction::where('loanaccount_id', $loanaccount->id)
		->where('type', 'debit')
		->whereBetween('date', array($arrearDate, $date))
		->sum('amount');

		return $principal_bal;//+$arrears;
	}*/


	public static function getRemainingPeriod($loanaccount){

		$paid_periods = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)
			->where('description', '=', 'loan repayment')
			->where('date', '>', $loanaccount->date_disbursed)
			->count();

		$remaining_period = $loanaccount->repayment_duration - $paid_periods;

		return $remaining_period;
	}
	#BIGCHANGE
	/*public static function getPrincipalDue($loanaccount){
		$remaining_period = Loantransaction::getRemainingPeriod($loanaccount);
		$principal_paid = Loanrepayment::getPrincipalPaid($loanaccount);
		$principal_balance = $loanaccount->amount_disbursed - $principal_paid;
		if($principal_balance > 0 && $remaining_period > 0){
			$principal_due = $principal_balance/$remaining_period;
		}else{
			$principal_due = Loanaccount::getPrincipalBal($loanaccount);
		}
		return $principal_due;
	}

	public static function getInterestDue($loanaccount){
		$principal_bal = Loanaccount::getPrincipalBal($loanaccount);
		$rate = $loanaccount->interest_rate/100;
		$interest_due = $principal_bal * $rate;
		return $interest_due;
	}*/

	public static function tomonthlyRate($rate){
        $rate=$rate/100; $rate=$rate/12;
        return $rate;
    }
    public static function toannualRate($rate){
        $rate=$rate/100; $rate=$rate*12;
        return $rate;
    }
    public static function justRate($rate){
        $rate=$rate/100;
        return $rate;
    }
//match rate with frequency..frequency=the duration after which an installment is paid
	public static function getrate($loanaccount){
        //getaccountdetails
		$frequency=$loanaccount->frequency; $rate=$loanaccount->interest_rate;
        $rate_type=$loanaccount->rate_type;
        $balance =Loanaccount::getPrincipalBal($loanaccount);
		//endgetaccountdetails
        if($frequency=='annually' && $rate_type!=='annually'){
            $rate=Loantransaction::toannualRate($rate);
        }else if($frequency=='monthly' && $rate_type!=='monthly'){
            $rate=Loantransaction::tomonthlyRate($rate);
        }else{$rate=Loantransaction::justRate($rate);}
        return (float)$rate;
	}

	public static function getInstallment($loanaccount,$what=null){
		//getaccountdetails
		$loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
		$formula=$loanproduct->formula;
		$months=$loanaccount->period;
		$rate=Loantransaction::getrate($loanaccount);
		$balance =Loanaccount::getLoanBalNoInterest($loanaccount);
		$amount=$loanaccount->amount_disbursed+$loanaccount->top_up_amount; $interest=$balance*$rate*(int)$months;
		$amortization=$loanproduct->amortization; $total_amount=$amount+$interest;
		//endgetaccountdetails
		if($amortization=="EI" && $formula=="SL"){
			$period=$months; #no. of installments
			$eachpay=(float)$total_amount/(float)$months;
		}else if($amortization=="EP" && $formula=="SL"){
			$principal=Loantransaction::getPrincipalDue($loanaccount);
			$period=$months; #no. of installments
			$eachpay=(float)$principal/(float)$interest;
		}else{
			$rate=Loantransaction::getrate($loanaccount);
			$r1=$rate+1; $r2=pow($r1,-$months); $r3=1-$r2;
			$r4=$r3/$rate; $period=$r4; #no. of installments
			$eachpay=$amount/$r4; #installment amount
		}
		if($what=='period'){return $period;}else{return $eachpay;}
	}
	public static function getInterestDue($loanaccount){
		//getaccountdetails
		$loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
		$formula=$loanproduct->formula; $months=$loanaccount->period;
		$rate=Loantransaction::getrate($loanaccount);
		$balance =Loanaccount::getLoanBalNoInterest($loanaccount); //$int_bal=Loanaccount::getInterestBal($loanaccount);
		//endgetaccountdetails
		//if($int_bal>0){$interest=$balance*$rate;}else{
		//	$interest=0;
		//}
		$interest=$balance*$rate;
        return $interest;
	}



	public static function getPrincipalDue($loanaccount){
		$loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
		$formula=$loanproduct->formula; $months=(int)$loanaccount->period;
		$rate=Loantransaction::getrate($loanaccount);
		$amortization=$loanproduct->amortization;
		$amount_given=(float)$loanaccount->amount_disbursed+(float)$loanaccount->top_up_amount;
		if($amortization=="EP" && $formula=="SL"){
			$principal=$amount_given/$months;
		}else{
			$installment=Loantransaction::getInstallment($loanaccount,'0');
        	$interest=Loantransaction::getInterestDue($loanaccount);
			//check if a loan is fully paid
			if($interest>0){$principal=$installment-$interest;}else{
				$principal=0;
			}
		}
        return $principal;
    }

	public static function getInterestDueFor($loanaccount,$amount){#changeo
		//getaccountdetails
		$balance =Loanaccount::getLoanBalNoInterest($loanaccount); $initial_balance =$loanaccount->amount_disbursed+$loanaccount->top_up_amount;
		//endgetaccountdetails
		$installment=Loantransaction::getInstallment($loanaccount); $rate=Loantransaction::getrate($loanaccount);
		$loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
		$formula=$loanproduct->formula;
		if($amount>=$installment){
			$diff=(float)$amount/(float)$installment; $interest=0;
			if($formula=="SL"){
				$interest=$initial_balance*$rate*round($diff);
			}else{
				for ($i=1; $i<=$diff; $i++){
					$interests=$balance*$rate;
					$balance-=$installment;
					$interest+=$interests;
				}
			}
		}else{$interest=$amount;}
        return $interest;
	}
	public static function getAmountDueAt($loanaccount,$ldate,$what){#changeo
		$fdate=$loanaccount->date_disbursed;
		$months=Loantransaction::monthsDiff($fdate,$ldate); $interest=0;
		$balance =$loanaccount->amount_disbursed+$loanaccount->top_up_amount; #Loanaccount::getLoanBalNoInterest($loanaccount);
		$installment=Loantransaction::getInstallment($loanaccount); $rate=Loantransaction::getrate($loanaccount);
		$loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
		$formula=$loanproduct->formula;
		if($formula=="SL"){
			$interest=$balance*$rate*round($months);
		}else{
			for ($i=1; $i<=3; $i++){
				$interests=$balance*$rate;
				$balance-=$installment;
				$interest+=$interests;
			}
		}

		$installments=(float)$installment*(float)$months;
		if($what=='installments'){return $installments;}else if($what=='interest'){return $interest;}
	}
	public static function getAmountUnpaid($loanaccount){
		$supposed=Loantransaction::getAmountDueAt($loanaccount,date('Y-m-d'),'installments');
		$amountpaid = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('type', '=', 'credit')->sum('amount');
		$amountUnpaid=(float)$supposed-(float)$amountpaid; if($amountUnpaid<1){$amountUnpaid=0;}
		return $amountUnpaid;
	}
	public static function getMemberAmountUnpaid($member_id){
		$loanaccounts = DB::table('loanaccounts')->where('member_id', '=', $member_id)->get(); $allUnpaid=0;
		foreach($loanaccounts as $loanaccount){
			$unpaid=Loantransaction::getAmountUnpaid($loanaccount);
			$allUnpaid+=$unpaid;
		}
		return $allUnpaid;
	}
	public static function monthsDiff($fdate,$ldate){

		$fdate_split=array_pad(explode('-',$fdate,3), 3, null); $ldate_split=array_pad(explode('-',$ldate,3), 3, null);
		$fdate_year=(int)$fdate_split[0]; $fdate_month=(int)$fdate_split[1]; $fdate_days=(int)$fdate_split[2];
		$ldate_year=(int)$ldate_split[0]; $ldate_month=(int)$ldate_split[1]; $ldate_days=(int)$ldate_split[2];
		if($fdate_year==$ldate_year){
			$months=$ldate_month-$fdate_month;
		}else{$previous_months=12-$fdate_month; $months=$previous_months+$ldate_month; }
		$days=$ldate_days-$fdate_days;
		if($days<0){$months_diff=$months-1;}else{$months_diff=$months;}
		return (int)$months_diff;
	}
	public static function dateDue($newDate,$dueDate){ //checks if the dueDate was reached on the newDate
		$cd=explode("-",$newDate); $cdyear=(int)$cd[0]; $cdmonth=(int)$cd[1]; $cdday=(int)$cd[2];
		$dd=explode("-",$dueDate); $ddyear=(int)$dd[0]; $ddmonth=(int)$dd[1]; $ddday=(int)$dd[2];
		if($cdyear==$ddyear && $cdmonth==$ddmonth && $cdday>=$ddday){
			return 1;
		}else if($cdyear==$ddyear  && $cdmonth>$ddmonth){return 1;}
		else if($cdyear>$ddyear){return 1;}else{return 0;}
	}
	#BIGCHANGE
	public static function getExtraAmount($loanaccount,$what,$date=null){#changeo
		if($date!=null){
			$alloverpayments = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('date', '<=', $date)->where('description','=','loan repayment')->where('extra_amount_desc','=','overpayment')->sum('extra_amount');
			$allarrears = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('date', '<=', $date)->where('description','=','loan repayment')->where('extra_amount_desc','=','arrears')->sum('extra_amount');
		}else{
			$alloverpayments = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('description','=','loan repayment')->where('extra_amount_desc','=','overpayment')->sum('extra_amount');
			$allarrears = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('description','=','loan repayment')->where('extra_amount_desc','=','arrears')->sum('extra_amount');
		}
		$extra_amount=(float)$alloverpayments-(float)$allarrears;
		if($extra_amount<0){$arrears=(float)$allarrears-(float)$alloverpayments; $overpayments=0;}else{
			$overpayments=$extra_amount; $arrears=0;
		}
		if($what=='arrears'){return $arrears;}else if($what=='overpayments'){return $overpayments;}
	}
	public static function getExtraAmountFor($loanaccount,$what,$amount){#changeo
		$installment=Loantransaction::getInstallment($loanaccount,'0');
		$extra_amount=(float)$amount-(float)$installment;
		if($extra_amount<0){$arrears=(float)$installment-(float)$amount; $overpayments=0;}else{
			$overpayments=$extra_amount; $arrears=0;
		}
		if($what=='arrears'){return $arrears;}else if($what=='overpayments'){return $overpayments;}
	}
	public static function getLoanExtra($loanaccount,$date=null){#changeo
		if($date!=null){
			$alloverpayments = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('date', '<=', $date)->where('description','=','loan repayment')->where('extra_amount_desc','=','overpayment')->sum('extra_amount');
			$allarrears = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('date', '<=', $date)->where('description','=','loan repayment')->where('extra_amount_desc','=','arrears')->sum('extra_amount');
		}else{
			$alloverpayments = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('description','=','loan repayment')->where('extra_amount_desc','=','overpayment')->sum('extra_amount');
			$allarrears = DB::table('loantransactions')->where('loanaccount_id', '=', $loanaccount->id)->where('date', '>=', $loanaccount->date_disbursed)->where('description','=','loan repayment')->where('extra_amount_desc','=','arrears')->sum('extra_amount');
		}
		$extra_amount=(float)$alloverpayments-(float)$allarrears;
		if($extra_amount<0){$extra='arrears';}else if($extra_amount>0){$extra='over_payment';}else{$extra='none';}
		return $extra;
	}
	public static function getDueAmount($loanaccount){#changeo
		$install=Loantransaction::getInstallment($loanaccount,'0'); //$principal_due + $interest_due;
		$ext=Loantransaction::getLoanExtra($loanaccount); $tdclass="";
		$arrears=Loantransaction::getExtraAmount($loanaccount,'arrears');
		$overpayments=Loantransaction::getExtraAmount($loanaccount,'overpayments');
		$amount_unpaid=Loantransaction::getAmountUnpaid($loanaccount); $unpaid_hide='hide';
		if($ext=='arrears'){
		  $amount_due=(float)$install+(float)$arrears+(float)$amount_unpaid; $extra_amount=$arrears; $extra_name='arrears';
		}else if($ext='over_payment'){
		  if($overpayments>=$amount_unpaid){$overpayments=$overpayments-$amount_unpaid;
			$amount_due=(float)$install-(float)$overpayments; $extra_amount=$overpayments; $extra_name='over_payment';
		  }else{
			$arrears=(float)$amount_unpaid-(float)$overpayments; $overpayments=0;
			$amount_due=(float)$install+$arrears; $extra_amount=$arrears; $extra_name='arrears';
		  }
		}else{
		  $amount_due=$install+(float)$amount_unpaid; $unpaid_hide=''; $tdclass='hide'; $extra_name='extra amount'; $extra_amount=0;
		}  if($amount_due<1){$amount_due=0;}
		return $amount_due;
	}
	public static function repayLoan($loanaccount, $amount, $date,$overpayment,$arrears){
		if($overpayment!=0){$extra_amount=$overpayment; $extra_amount_desc='overpayment';}else if($arrears!=0){
			$extra_amount=$arrears; $extra_amount_desc='arrears';
		}else{$extra_amount=0; $extra_amount_desc='none';}
		$transaction = new Loantransaction;
		$transaction->loanaccount()->associate($loanaccount);
		$transaction->date = $date;
		$transaction->description = 'loan repayment';
		$transaction->amount =$amount;
		$transaction->extra_amount = $extra_amount;
		$transaction->extra_amount_desc = $extra_amount_desc;
		$transaction->type = 'credit';
		$transaction->save();
		Audit::logAudit($date, Confide::user()->username, 'loan repayment', 'Loans', $amount);
	}

	public static function disburseLoan($loanaccount, $amount, $date){ #changeo
		$transaction = new Loantransaction;
		$transaction->loanaccount()->associate($loanaccount);
		$transaction->date = $date;
		$transaction->description = 'loan disbursement';
		$transaction->amount = $amount;
		$transaction->type = 'debit';
		$transaction->save(); $amount_approved=$loanaccount->amount_disbursed;
		$overcharge=Loanaccount::getOverchargeAmount($loanaccount);
		if($overcharge>0){#changeo
			$principal_bal =Loanaccount::getPrincipalBal($loanaccount);
			$interest_bal=Loanaccount::getInterestBal($loanaccount);
			if($overcharge>=$amount_approved){
				Loanaccount::xovercharge($loanaccount,$amount_approved); $overcharge_refund=$amount_approved;
				$principal_due=$principal_bal; $interest_due=$interest_bal;
			}else{
				Loanaccount::xovercharge($loanaccount,$overcharge); $overcharge_refund=$overcharge;
				$interest_due=Loantransaction::getInterestDueFor($loanaccount,$overcharge); $principal_due=(float)$overcharge-(float)$interest_due;
			}
			$arrears=Loantransaction::getExtraAmountFor($loanaccount,'arrears',$overcharge_refund);
			$overpayment=Loantransaction::getExtraAmountFor($loanaccount,'overpayments',$overcharge_refund);
			Loanrepayment::payInterest($loanaccount, $date, $interest_due);
			Loanrepayment::payPrincipal($loanaccount,$date,$principal_due);
			Loantransaction::repayLoan($loanaccount, $overcharge_refund, $date,$overpayment,$arrears);
		}
		$account = Loanposting::getPostingAccount($loanaccount->loanproduct, 'disbursal');

		$data = array(
			'credit_account' =>$account['credit'] ,
			'debit_account' =>$account['debit'] ,
			'date' => $date,
			'amount' => $loanaccount->amount_disbursed,
			'initiated_by' => 'system',
			'description' => 'loan disbursement',
			'particulars_id' => '26',
			'narration' => $loanaccount->member->id
			);

		$journal = new Journal;

		$journal->journal_entry($data);

		if($overcharge>0){#changeo
			$data2 = array(
				'credit_account' =>'6',
				'debit_account' =>'99',
				'date' => $date,
				'amount' => $overcharge_refund,
				'initiated_by' => 'system',
				'description' => 'loanovercharge',
				'particulars_id' => '75',
				'narration' => $loanaccount->member->id
				);

			$journal = new Journal;
			$journal->journal_entry($data2);
		}
		Audit::logAudit($date, Confide::user()->username, 'loan disbursement', 'Loans', $amount);

	}

	public static function refinanceLoan($loanaccount, $amount, $date) {
		$transaction = new Loantransaction;
		$transaction->loanaccount()->associate($loanaccount);
		$transaction->date = $date;
		$transaction->description = 'loan refinance';
		$transaction->amount = $amount;
		$transaction->type = 'debit';
		$transaction->save();


		$account = Loanposting::getPostingAccount($loanaccount->loanproduct, 'disbursal');

		$data = array(
			'credit_account' =>$account['credit'] ,
			'debit_account' =>$account['debit'] ,
			'date' => $date,
			'amount' => $loanaccount->amount_disbursed,
			'initiated_by' => 'system',
			'description' => 'loan refinance',
			'particulars_id' => '26',
			'narration' => $loanaccount->member->id
		);


		$journal = new Journal;


		$journal->journal_entry($data);

		Audit::logAudit($date, Confide::user()->username, 'loan refinance', 'Loans', $amount);
	}

	public static function topupLoan($loanaccount, $amount, $date){

		$transaction = new Loantransaction;

		$transaction->loanaccount()->associate($loanaccount);
		$transaction->date = $date;
		$transaction->description = 'loan top up';
		$transaction->amount = $amount;
		$transaction->type = 'debit';
		$transaction->save();


		$account = Loanposting::getPostingAccount($loanaccount->loanproduct, 'disbursal');

		$data = array(
			'credit_account' =>$account['credit'] ,
			'debit_account' =>$account['debit'] ,
			'date' => $date,
			'amount' => $amount,
			'initiated_by' => 'system',
			'description' => 'loan top up',
			'particulars_id' => 18,
			'narration' => $loanaccount->member->id
			);


		$journal = new Journal;


		$journal->journal_entry($data);

		Audit::logAudit($date, Confide::user()->username, 'loan to up', 'Loans', $amount);



	}




	}
