<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PettycashItem extends Model {

	protected $table = 'pettycash_items';

	// Add your validation rules here
	public static $rules = [

	];

	// Link with AccountTransaction controller
	public function accountTransaction(){
		return $this->belongsTo('AccountTransaction');
	}

}
