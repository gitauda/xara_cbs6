<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Tlbpayment extends Model {

	// Add your validation rules here
	public static $rules = [
		 'amount' => 'required',
		 'eq_id' => 'required',
		 'date' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function account(){

		return $this->belongsTo('App\models\Account');
	}

	public function vehicle(){

		return $this->belongsTo('App\models\Vehicle');
	}

}
