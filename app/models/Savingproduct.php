<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Savingproduct extends Model {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function savingproductcoa(){

		return $this->hasMany('App\Account');
	}


	public function savingaccounts(){

		return $this->hasMany('App\Savingaccount');
	}


	public function savingpostings(){

		return $this->hasMany('App\Savingposting');
	}


	public function charges(){

		return $this->belongsToMany('App\Charge');
	}


}
