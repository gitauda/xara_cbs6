<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tbopeningbalance extends Model{

	protected $table = 'openingbalances';

	/**
	 * Link with accounts
	 */
	public function account(){
		return $this->belongsTo('accounts');
	}

	public static function getTrialBalance($account, $openingbalance, $from, $to)
    {

        $balance = 0;

        $credit = DB::table('journals')->where('account_id', '=', $account->id)->where('type', '=', 'credit')->whereBetween('date', array($from, $to))->sum('amount');
        $debit = DB::table('journals')->where('account_id', '=', $account->id)->where('type', '=', 'debit')->whereBetween('date',  array($from, $to))->sum('amount');

        if ($account->category == 'ASSET') {

            $balance = $debit - $credit;
            $total=$openingbalance+$balance;


        }

        if ($account->category == 'INCOME') {

            $balance = $credit - $debit;
             $total=$openingbalance+$balance;

        }

        if ($account->category == 'LIABILITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EQUITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EXPENSE') {

            $balance = $debit - $credit;
           $total=$openingbalance+$balance;

        }


        return $total;


    }
public static function getTrialBalanceAsAt($account, $openingbalance,$date)
    {

        $balance = 0;
        $opendate=date('Y-m-d',strtotime('2019-01-01'));

        $credit = DB::table('journals')->where('account_id', '=', $account->id)->where('date', '>=',$opendate )->where('type', '=', 'credit')->where('date','<=',$date)->sum('amount');
        $debit = DB::table('journals')->where('account_id', '=', $account->id)->where('type', '=', 'debit')->where('date', '>=', $opendate)->where('date','<=',$date)->sum('amount');

        if ($account->category == 'ASSET') {

            $balance = $debit - $credit;
            $total=$openingbalance+$balance;


        }

        if ($account->category == 'INCOME') {

            $balance = $credit - $debit;
             $total=$openingbalance+$balance;

        }

        if ($account->category == 'LIABILITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EQUITY') {

            $balance = $credit - $debit;
            $total=$openingbalance+$balance;

        }

        if ($account->category == 'EXPENSE') {

            $balance = $debit - $credit;
           $total=$openingbalance+$balance;

        }


        return $total;


    }
}
