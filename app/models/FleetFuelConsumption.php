<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;

class FleetFuelConsumption extends Model {
    /*
    use \Traits\Encryptable;


    protected $encryptable = [

        'relief_amount',

    ];
    */

    public $table = "fleetFuelConsumption";

    public static $rules = [
        'employee' => 'required',
        'relief' => 'required',
        'amount' => 'required|regex:/^(\$?(?(?=\()(\())\d+(?:,\d+)?(?:\.\d+)?(?(2)\)))$/',
    ];

    public static $messages = array(
        'employee.required'=>'Please select employee!',
        'relief.required'=>'Please select relief type!',
        'amount.required'=>'Please insert amount!',
        'amount.regex'=>'Please insert a valid amount!',
    );

    // Don't forget to fill this array
    protected $fillable = [];


    public function vehicle(){

        return $this->belongsTo('Vehicle');
    }
    public function relief(){

        return $this->belongsTo('Relief');
    }
    public function emails()
    {
        return $this->belongsToMany('Email', 'users_email', 'Uid', 'email_id');
    }
    public function createCollection($acc_name, $product)
    {

        //create savings control
        $account = new Account;
        if($account->category = 'LIABILITY'){
            $category = $account->category;


            $account->code = getaccountcode($category);
            $account->name = $acc_name . ' control';
            $account->active = TRUE;
            $account->savingproduct()->associate($product);

            $account->save();

        }else if($category = 'INCOME'){

            $account->category = 'INCOME';
            $account->code = getaccountcode($category);
            $account->name = $acc_name . ' fee income';
            $account->active = TRUE;
            $account->savingproduct()->associate($product);

            $account->save();
        }



    }

}
