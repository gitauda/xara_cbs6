<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Savingtransaction extends Model {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $guarded = [];


	public function savingaccount(){

		return $this->belongsTo('App\Savingaccount');
	}


	public static function getWithdrawalCharge($savingaccount){
		$chargeamount = 0;

		foreach ($savingaccount->savingproduct->charges as $charge) {

			if($charge->payment_method == 'withdrawal'){

				$chargeamount = $chargeamount + $charge->amount;

			}

		}

		return $chargeamount;
	}



	public static function withdrawalCharges($savingaccount, $date, $transAmount){

		foreach($savingaccount->savingproduct->charges as $charge){

			if($charge->payment_method == 'withdrawal'){


					if($charge->calculation_method == 'percent'){
						$amount = ($charge->amount/ 100) * $transAmount;
					}

					if($charge->calculation_method == 'flat'){
						$amount = $charge->amount;
					}



					$savingtransaction = new Savingtransaction;

					$savingtransaction->date = $date;
					$savingtransaction->savingaccount()->associate($savingaccount);
					$savingtransaction->saving_amount = $amount;
					$savingtransaction->management_fee = 0;
					$savingtransaction->type = 'debit';
					$savingtransaction->description = 'withdrawal charge';
					$savingtransaction->save();


				foreach($savingaccount->savingproduct->savingpostings as $posting){

					if($posting->transaction == 'charge'){

						$debit_account = $posting->debit_account;
						$credit_account = $posting->credit_account;

						$data = array(
						'credit_account' => $credit_account,
						'debit_account' => $debit_account,
						'date' => $date,
						'amount' => $amount,
						'initiated_by' => 'system',
						'description' => 'cash withdrawal'
					);


					$journal = new Journal;

					$journal->journal_entry($data);
					}

				}

			}
		}
	}



	public static function importSavings($member, $date, $savingaccount,  $saving_amount, $management_fee, $description,$bank,$method,$vehicle){

		/**$member = Member::find($member[0]->id);
		$savingaccount = Savingaccount::find($savingaccount[0]->id);**/

       $member = Member::find($member->id);
		$savingaccount = Savingaccount::find($savingaccount->id);
		$vehicle = Vehicle::find($vehicle->id);

		//check if account and member exists
		$savingtransaction = new Savingtransaction;

		$savingtransaction->date = $date;
		$savingtransaction->savingaccount()->associate($savingaccount);
		$savingtransaction->saving_amount = $saving_amount;
		$savingtransaction->management_fee = $management_fee;
		$savingtransaction->type = 'credit';
		$savingtransaction->description = $description;
		$savingtransaction->payment_method =$method;
		$savingtransaction->bank_sadetails = $bank;

		$savingtransaction->vehicle_id = $vehicle->id;

		$savingtransaction->transacted_by = $member->name;
		$savingtransaction->save();


		foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'deposit'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}
			}
        $debit_account2 = Account::where('name', 'like', '%'.'Bank Account'.'%')->pluck('id');
		$credit_account2 = Account::where('name', 'like', '%'.'Management Fee'.'%')->where('category', 'INCOME')->pluck('id');
		$particulars = Particular::where('name', 'like', '%'.'saving transactions'.'%')->first();

		if(empty($particulars)){
			$particulars = new Particular;
			$particulars->name = 'saving transactions';
			$particulars->creditaccount_id = $credit_account2;
			$particulars->debitaccount_id = $debit_account2;
			$particulars->save();
		}


			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => $date,
				'amount' => $saving_amount,
				'initiated_by' => 'system',
				'description' => $description,
                'particulars_id' => '8',
                'narration' => $member->id
			);
			$data2 = array(
				'credit_account' => $credit_account2,
				'debit_account' => $debit_account2,
				'date' => $date,
				'amount' => $management_fee,
				'initiated_by' => 'system',
				'description' => $description,
                'particulars_id' => '8',
                'narration' => $member->id
			);


			$journal = new Journal;


			$journal->journal_entry($data);

			$journal2 = new Journal;


			$journal2->journal_entry($data2);

			$message = "Confirmed. you have deposited ksh ".$saving_amount." to saving account ".$savingaccount->account_number." on ".$date." The Management fee charged is Ksh " .$management_fee."Thank you! \n Regards, Mwamba Sacco. ";
			$phone=$member->phone;
			if($phone != null){
			 //include(app_path() . '/views/AfricasTalkingGateway.php');
		// Specify your authentication credentials
		#TODO: update AFRICAS TALKING KEYS
        $username   = "Mwambasacco";
        $apikey     = "58e74c598e3e9cca0e6d67ecc2e1a50baf4a0b73307a2a52b6f6e125ccb69e2e";

        /**$username   = "sandbox";
        $apikey     = "caf42c7e94b0ded87448a0e10700b26bbbde736aa4ae30be3c2b7109c1cc4950";**/
        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        $recipients = $member->phone;
        // And of course we want our recipients to know what we really do
        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);
        // Any gateway error will be captured by our custom Exception class below,
        // so wrap the call in a try-catch block
        try
        {
            // Thats it, hit send and we'll take care of the rest.
            $results = $gateway->sendMessage($recipients, $message);
            $thisMonth=date('Y-m',time());
            $smsLogs=Smslog::where('date','like',$thisMonth.'%')
            		->where('user',$member->id)->first();
            if(sizeof($smsLogs) >=1){
            	//update sms logs
            	$smsLogs->monthlySmsCount +=1;
            	$smsLogs->update();
            }else{
            	//insert to sms logs
            	$newSms=new Smslog();
            	$newSms->user=$member->id;
            	$newSms->monthlySmsCount=1;
            	$newSms->date=date('Y-m-d',time());
            	$newSms->charged=0;
            	$newSms->save();
            }
            /*foreach($results as $result) {
                // status is either "Success" or "error message"
                echo " Number: " .$result->number;
                echo " Status: " .$result->status;
                //echo " StatusCode: " .$result->statusCode;
                echo " MessageId: " .$result->messageId;
                echo " Cost: "   .$result->cost."\n";
            }*/
        }
        catch ( AfricasTalkingGatewayException $e )
        {
            echo "Encountered an error while sending: ".$e->getMessage();
        }
           }
			Audit::logAudit(date('Y-m-d'), Auth::user()->username, 'Savings imported', 'Savings', $saving_amount);
	}



	public static function creditAccounts($data){
		$savingaccount = Savingaccount::findOrFail($data['account_id']);

		$savingtransaction = new Savingtransaction;

		$savingtransaction->date = $data['date'];
		$savingtransaction->savingaccount()->associate($savingaccount);
		$savingtransaction->saving_amount = $data['saving_amount'];
		$savingtransaction->type = $data['type'];
		$savingtransaction->description = 'savings deposit';
        $savingtransaction->payment_method = $data['pay_method'];
        $savingtransaction->bank_sadetails = $data['bank_details'];

		$savingtransaction->save();

		// deposit
		if((array_get($data,'type') == 'credit')&&(array_get($data,'pay_method') == 'cash')){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'deposit_cash'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}
			}
                      }
                      elseif((array_get($data,'type') == 'credit')&&(array_get($data,'pay_method') == 'bank')){
                              foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'deposit'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}

                               }
                               }

			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => array_get($data, 'date'),
				'amount' => array_get($data,'saving_amount'),
				'initiated_by' => 'system',
				'description' => 'cash deposit'
				);


			$journal = new Journal;


			$journal->journal_entry($data);

			Audit::logAudit(date('Y-m-d'), Auth::user()->username, 'savings deposit', 'Savings', array_get($data,'saving_amount'));

			}


	public static function transact($date, $savingaccount, $saving_amount, $management_fee, $type, $description, $transacted_by, $member,$bank,$method,$vehicle){

		$savingtransaction = new Savingtransaction;

		$savingtransaction->date = $date;
		$savingtransaction->savingaccount()->associate($savingaccount);
		$savingtransaction->saving_amount = $saving_amount;
		$savingtransaction->management_fee = $management_fee;
		$savingtransaction->type = $type;
		$savingtransaction->description = $description;
		$savingtransaction->vehicle_id = $vehicle;
		$savingtransaction->payment_method =$method;
		$savingtransaction->bank_sadetails = $bank;
		$savingtransaction->transacted_by = $transacted_by;
		$savingtransaction->save();

       // bank withdrawal

		if($type == 'debit' && $method=='bank'){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'withdrawal'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}


			}



			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => $date,
				'amount' => $saving_amount,
				'initiated_by' => 'system',
				'description' => $description,
                'bank_details' => $bank,
                'particulars_id' => '8',
                'narration' => $member->id
				);


			$journal = new Journal;


			$journal->journal_entry($data);

            $message = "Confirmed. you have withdrawn ksh ".$saving_amount." from saving account ".$savingaccount->account_number." on ".$date." Thank you! \n Regards, motosacco. ";

			#TODO: confirm if the param is $saving_amount
			Savingtransaction::withdrawalCharges($savingaccount, $date, $saving_amount);

			Audit::logAudit(date('Y-m-d'), Auth::user()->username, $description, 'Savings', $saving_amount);

		}
		//cash withdrawal

		if($type == 'debit' && $method=='cash'){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'withdrawal_cash'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}


			}



			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => $date,
				'amount' => $saving_amount,
				'initiated_by' => 'system',
				'description' => $description,
				'bank_details' => $bank,
                'particulars_id' => '8',
                'narration' => $member->id
			);



			$journal = new Journal;


			$journal->journal_entry($data);

            $message = "Confirmed. you have withdrawn ksh ".$saving_amount." from saving account ".$savingaccount->account_number." on ".$date." Thank you! \n Regards, motosacco. ";


			Savingtransaction::withdrawalCharges($savingaccount, $date, $saving_amount);

			Audit::logAudit(date('Y-m-d'), Auth::user()->username, $description, 'Savings', $saving_amount);

		}


		// bank deposit
		if($type == 'credit' && $method=='bank'){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'deposit'){

					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}
			}
        $debit_account2 = Account::where('name', 'like', '%'.'Bank Account'.'%')->pluck('id');
		$credit_account2 = Account::where('name', 'like', '%'.'Management Fee'.'%')->where('category', 'INCOME')->pluck('id');
		$particulars = Particular::where('name', 'like', '%'.'saving transactions'.'%')->first();

		if(empty($particulars)){
			$particulars = new Particular;
			$particulars->name = 'saving transactions';
			$particulars->creditaccount_id = $credit_account2;
			$particulars->debitaccount_id = $debit_account2;
			$particulars->save();
        }


			$data = array(
				'credit_account2' => $credit_account,
				'debit_account2' => $debit_account,
				'date' => $date,
				'amount' => $saving_amount,
				'initiated_by' => 'system',
				'description' => $description,
                'bank_details' => $bank,
                'particulars_id' => '8',
                'narration' => $member->id
			);


            $data2 = array(
				'credit_account2' => $credit_account2,
				'debit_account2' => $debit_account2,
				'date' => $date,
				'amount' => $management_fee,
				'initiated_by' => 'system',
				'description' => $description,
                'particulars_id' => '8',
                'narration' => $member->id
			);


			$journal = new Journal;


			$journal->journal_entry($data);
                 $journal1 = new Journal;


			$journal1->journal_entry($data2);



           $message = "Confirmed. you have deposited ksh ".$saving_amount." to saving account ".$savingaccount->account_number." on ".$date." The Management fee charged is ksh ".$management_fee." Thank you! \n Regards, Mwamba Sacco. ";

			Audit::logAudit(date('Y-m-d'), Auth::user()->username, $description, 'Savings', $saving_amount);

		}

       // cash deposit
		if($type == 'credit' && $method=='cash' ){


			foreach($savingaccount->savingproduct->savingpostings as $posting){

				if($posting->transaction == 'deposit_cash'){
					$debit_account = $posting->debit_account;
					$credit_account = $posting->credit_account;
				}
			}

                       $debit_account2 = Account::where('name', 'like', '%'.'Cash Account'.'%')->pluck('id');
		$credit_account2 = Account::where('name', 'like', '%'.'Management Fee'.'%')->where('category', 'INCOME')->pluck('id');
		$particulars = Particular::where('name', 'like', '%'.'saving transactions'.'%')->first();

		if(empty($particulars)){
			$particulars = new Particular;
			$particulars->name = 'saving transactions';
			$particulars->creditaccount_id = $credit_account2;
			$particulars->debitaccount_id = $debit_account2;
			$particulars->save();
                        }

			$data = array(
				'credit_account' => $credit_account,
				'debit_account' => $debit_account,
				'date' => $date,
				'amount' => $saving_amount,
				'initiated_by' => 'system',
				'description' => $description,
                'bank_details' => $bank,
                'particulars_id' => '8',
                'narration' => $member->id
			);

            $data2 = array(
				'credit_account2' => $credit_account2,
				'debit_account2' => $debit_account2,
				'date' => $date,
				'amount' => $management_fee,
				'initiated_by' => 'system',
				'description' => $description,
                'particulars_id' => '8',
                'narration' => $member->id
			);


			$journal = new Journal;


			$journal->journal_entry($data);
            $journal1 = new Journal;


			$journal1->journal_entry($data2);



            $message = "Confirmed. you have deposited ksh ".$saving_amount." to saving account ".$savingaccount->account_number." on ".$date." The Management fee charged is ksh ".$management_fee." Thank you! \n Regards, Mwamba Sacco. ";

			Audit::logAudit(date('Y-m-d'), Auth::user()->username, $description, 'Savings', $saving_amount);

		}



       include(base_path() . '/resources/views/AfricasTalkingGateway.php');
        // Specify your authentication credentials
        $username   = "Mwambasacco";
        $apikey     = "58e74c598e3e9cca0e6d67ecc2e1a50baf4a0b73307a2a52b6f6e125ccb69e2e";
        /**$username   = "sandbox";
        $apikey     = "caf42c7e94b0ded87448a0e10700b26bbbde736aa4ae30be3c2b7109c1cc4950";**/
        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        $recipients = $member->phone;
        // And of course we want our recipients to know what we really do
        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);
        // Any gateway error will be captured by our custom Exception class below,
        // so wrap the call in a try-catch block
        try
        {
            // Thats it, hit send and we'll take care of the rest.
            $results = $gateway->sendMessage($recipients, $message);
            $thisMonth=date('Y-m',time());
            $smsLogs=Smslog::where('date','like',$thisMonth.'%')
            		->where('user',$member->id)->first();
            if(sizeof(array($smsLogs)) >=1){
            	//update sms logs
            	$smsLogs->monthlySmsCount +=1;
            	$smsLogs->update();
            }else{
            	//insert to sms logs
            	$newSms=new Smslog();
            	$newSms->user=$member->id;
            	$newSms->monthlySmsCount=1;
            	$newSms->date=date('Y-m-d',time());
            	$newSms->charged=0;
            	$newSms->save();
            }
            /*foreach($results as $result) {
                // status is either "Success" or "error message"
                echo " Number: " .$result->number;
                echo " Status: " .$result->status;
                //echo " StatusCode: " .$result->statusCode;
                echo " MessageId: " .$result->messageId;
                echo " Cost: "   .$result->cost."\n";
            }*/
        }
        catch ( AfricasTalkingGatewayException $e )
        {
            echo "Encountered an error while sending: ".$e->getMessage();
        }

	}




	public static function trasactionExists($date, $savingaccount){

		$count = DB::table('savingtransactions')->where('date', '=', $date)->where('savingaccount_id', '=', $savingaccount->id)->count();

		if($count >= 1){

			return true;
		} else {

			return false;
		}
	}





}
