<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Matrix extends Model {

	protected $table='matrices';
	// Add your validation rules here
	public static $rules = [
		 'name' => 'required',
		 'maximum'=>'required'
	];


}
