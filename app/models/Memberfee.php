<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Memberfee extends Model
{
    // Add your validation rules here
	public static $rules = [
        'member_registration_fee' => 'required',
        'eq_id' => 'required',
        'asset_id' => 'required',
        // 'member_type'=>'required'
   ];
}
