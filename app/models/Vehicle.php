<?php namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model {

	// Add your validation rules here
	public static $rules = [
		'regno' => 'required',
		'make' => 'required',
	];

	public static $messages = array(
		'make.required'=>'Please insert vehicle make!',
        'regno.required'=>'Please insert vehicle registration number!',
    );

	// Don't forget to fill this array
	protected $fillable = [];

	public function collections(){
		return $this->hasMany('App\models\FleetCashCollection');
	}
	public function collector(){
		return $this->belongsTo('App\models\FleetCashCollector','cash_collector');
	}
	public function maintRecords(){
		return $this->hasMany('App\models\FleetMaintRecord');
	}

	public static function unicode($lastRec){

		if(count($lastRec)>0){$recId=(int)$lastRec->id+1;}else{$recId=1;}
        $unicode=$recId*3*($recId+2);
		return $unicode;
	}

	public static function assignedvehicle($id){

        $vehicle = Vehicle::find($id);

		return $vehicle;
	}

}
