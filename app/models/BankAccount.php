<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

Class BankAccount extends Model{

	protected $table = 'bank_accounts';

	/**
	 * Validation Rules
	 */
	public static $rules = [
		'bnkName' => 'required',
		'acName' => 'required',
		'acNumber' => 'required',
	];

	/**
	 * Link with BankStatement
	 */
	public function bankStatement(){
		return $this->hasMany('BankStatement');
	}

	/**
	 * Get Bank Account Statement
	 */
	 public static function getStatement($id){
 		return DB::table('bank_statements')
 					->where('bank_account_id',$id)
 					->select('bank_statements.bal_bd as bal_bd','bank_statements.stmt_month as stmt_month',
 							'bank_statements.created_at as stmt_date','bank_statements.is_reconciled')
 					->get();
 	}

	public static function getLastReconciliation($id){
		return DB::table('bank_statements')
						->where('bank_account_id',$id)
						->where('is_reconciled', 1)
						->select('stmt_month')
						->orderBy('stmt_month', 'DESC')
						->first();
	}

	public static function bankAccBal($id){
		$deposit=AccountTransaction::where("is_bank",1)->where("bank_account_id",$id)->where("type","deposit")->sum("transaction_amount");
		$wdraw=AccountTransaction::where("is_bank",1)->where("bank_account_id",$id)->where("type","withdraw")->sum("transaction_amount");
		$bal=$deposit-$wdraw; return $bal;
	}

}
