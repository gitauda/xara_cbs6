<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Shareaccount extends Model {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];


	public function member(){

		return $this->belongsTo('App\models\Member');
	}


	public function sharetransactions(){

		return $this->hasMany('App\models\Sharetransaction');
	}



	public static function createAccount($id){

		$member = Member::find($id);


		$share = new Share;


		$acc = 'SH-'.$member->membership_no;

		$shareaccount = new Shareaccount;


		$shareaccount->member()->associate($member);

		$shareaccount->account_number = $acc;

		$shareaccount->opening_date = date('Y-m-d');

		$shareaccount->save();
	}

	public static function getShares($shareaccount) {

        $credit = DB::table('sharetransactions')->where('shareaccount_id', '=', $shareaccount->id)->where('type', '=', 'credit')->sum('amount');
        $debit = DB::table('sharetransactions')->where('shareaccount_id', '=', $shareaccount->id)->where('type', '=', 'debit')->sum('amount');

        $balance = $credit - $debit;

        $sh = Share::findOrFail(1);

        $sharevalue = $sh->value;

        if($sharevalue != 0){
            $shares = $balance/$sharevalue;
        } else {

            $shares = 0;
        }

        return $shares;
    }
}
