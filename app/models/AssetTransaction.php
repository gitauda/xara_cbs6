<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Facades\Input;


class AssetTransaction extends Model{
	public $table = 'asset_transactions';

	public static $rules = [

	];
	public function allocations()
    {
        return $this->hasMany('AssetAllocation','asset');
    }

    public static function transact($data){
        $asset_trans = new AssetTransaction;

		$asset_trans->asset = $data['asset'];
		$asset_trans->type = "asset_disposal";
		$asset_trans->amount = $data['price'];
		$asset_trans->quantity = $data['quantity'];
		$asset_trans->updated_at = date('Y-m-d H:i:s');
		$asset_trans->created_at = date('Y-m-d H:i:s');
		$asset_trans->transacted_by=auth()->user()->id;
		$asset_trans->save();
	}

	public static function dispose_journals($id,$price){
		//all journals to be affected
		$asset=Asset::findorFail($id);
		$price=Input::get('price'); $depAmnt = Asset::calculateDepreciation($id);
		$creditAc = Account::where('name', $asset->asset_name)->pluck('id');
		$debitAc = Account::where('name', 'Disposal')->pluck('id');
		$data1 = [
			'date' => date("Y-m-d"),
			'debit_account' => $debitAc,
			'credit_account' => $creditAc,
			'description' => "Disposal of $asset->asset_name on ". date('jS M, Y'),
			'amount' => $asset->purchase_price,
			'initiated_by' => auth()->user()->username,
			'particulars_id' => 0,
			'narration'=>auth()->user()->id
		];

		$acTransaction = new AccountTransaction;
		$journal = new Journal;
		$acTransaction->createTransaction($data1);
		$journal->journal_entry($data1);

		$creditAc2 = Account::where('name', 'Disposal')->pluck('id');
		$debitAc2 = Account::where('name', 'Depreciation Expense')->pluck('id');
		$data2 = [
			'date' => date("Y-m-d"),
			'debit_account' => $debitAc2,
			'credit_account' => $creditAc2,
			'description' => "Disposal of $asset->asset_name on ". date('jS M, Y'),
			'amount' => $depAmnt,
			'initiated_by' => auth()->user()->username,
			'particulars_id' => 0,
			'narration'=>auth()->user()->id
		];

		$acTransaction2 = new AccountTransaction;
		$journal2 = new Journal;
		$acTransaction2->createTransaction($data2);
		$journal2->journal_entry($data2);

		$creditAc3 = Account::where('name', 'Disposal')->pluck('id');
		$debitAc3 = Account::where('name', 'Cash Account')->pluck('id');
		$data3 = [
			'date' => date("Y-m-d"),
			'debit_account' => $debitAc3,
			'credit_account' => $creditAc3,
			'description' => "Disposal of $asset->asset_name on ". date('jS M, Y'),
			'amount' => $price,
			'initiated_by' => auth()->user()->username,
			'particulars_id' => 0,
			'narration'=>auth()->user()->id
		];

		$acTransaction3 = new AccountTransaction;
		$journal3 = new Journal;
		$acTransaction3->createTransaction($data3);
		$journal3->journal_entry($data3);

		$tprice=(int)$price+(int)$depAmnt;
		if((int)$tprice!=(int)$asset->purchase_price){
			$diff=(int)$asset->purchase_price-(int)$tprice;
			if($diff<0){
				$profit=$diff*1; $loss='none'; $profloss=$profit;
			}else{$loss=$diff; $profit='none'; $profloss=$loss;}

			if($loss='none'){
				$creditAc4 = Account::where('name', 'Disposal')->pluck('id');
				$debitAc4 = Account::where('name', 'Profit/loss')->pluck('id');
			}else{
				$creditAc4 = Account::where('name', 'Profit/loss')->pluck('id');
				$debitAc4 = Account::where('name', 'Disposal')->pluck('id');
			}
			$data4 = [
				'date' => date("Y-m-d"),
				'debit_account' => $debitAc4,
				'credit_account' => $creditAc4,
				'description' => "Disposal of $asset->asset_name on ". date('jS M, Y'),
				'amount' => $profloss,
				'initiated_by' => auth()->user()->username,
				'particulars_id' => 0,
				'narration'=>auth()->user()->id
			];

			$acTransaction4 = new AccountTransaction;
			$journal4 = new Journal;
			$acTransaction4->createTransaction($data4);
			$journal4->journal_entry($data4);
		}
		//endAll journals to be affected

	}
}
