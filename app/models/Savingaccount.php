<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Savingaccount extends Model
{

    // Add your validation rules here
    public static $rules = [
        'account_number' => 'unique:account_number'
    ];

    // Don't forget to fill this array
    protected $fillable = [];


    public function member()
    {

        return $this->belongsTo('App\models\Member');
    }


    public function savingproduct()
    {

        return $this->belongsTo('App\models\Savingproduct');
    }


    public function transactions()
    {

        return $this->hasMany('App\models\Savingtransaction');
    }


    public static function getLastAmount($savingaccount)
    {

        $saving = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)
            ->where('type', '=', 'credit')->OrderBy('date', 'desc')
            ->pluck('amount');


        if ($saving) {
            return $saving;
        } else {
            return 0;
        }


    }


    public static function getAccountBalance($savingaccount)
    {

        $deposits = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)
            ->where('type', '=', 'credit')->sum('saving_amount');
        $withdrawals = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)
            ->where('type', '=', 'debit')->sum('saving_amount');

        $balance = $deposits - $withdrawals; if($balance<1){$balance=0;}

        return $balance;
    }
    public static function getFinalAccountBalance($savingaccount)
    {
        $balance =Savingaccount::getAccountBalance($savingaccount);
        if($savingaccount->savingproduct_id==4){
            $member_id=$savingaccount->member_id;
            $guaranteeamount=Loanaccount::amountGuarantee($member_id);
            $loanBalance=Loantransaction::getMemberLoanBalance($member_id);
            $finalamount=(float)$balance-((float)$guaranteeamount+(float)$loanBalance);
            if($finalamount<0){$finalamount=0;} $balance=$finalamount;
        }
        return $balance;
    }
    public static function getUserSavingsBalance($member)
    {
        $savingaccounts=DB::table('savingaccounts')->where('member_id', '=', $member)->get();
        $scount=DB::table('savingaccounts')->where('member_id', '=', $member)->count();
        if($scount>0){
            $samount=0;
            foreach($savingaccounts as $savingaccount){
                $accountbal=Savingaccount::getAccountBalance($savingaccount);
                $samount+=(int)$accountbal;
            }
            return $samount;
        }else{return 0;}
    }
    public static function withdrawSavings($member_id,$amount)
    {   $date=date('Y-m-d');
        $savingaccounts=DB::table('savingaccounts')->where('member_id', '=', $member_id)->get();
        $member=Member::findOrFail($member_id);
        $samount=0; $type="debit"; $description="Loan offset"; $transacted_by="xara"; $remamount=$amount;
        $scount=DB::table('savingaccounts')->where('member_id', '=', $member_id)->count();
        if($scount>0){

            foreach($savingaccounts as $savingaccount){
                $savinaccount=Savingaccount::findOrFail($savingaccount->id);
                $accountbal=Savingaccount::getAccountBalance($savingaccount);
                if($savingaccount->savingproduct_id==4 & $accountbal>0){
                    if($accountbal>=$amount){$wdamount=$amount; $remamount=0;}else{$wdamount=$accountbal; $remamount-=$accountbal;}
                    Savingtransaction::transact($date, $savinaccount, $wdamount, 0,$type, $description, $transacted_by, $member,'','','');
                }
                if($remamount!=0){
                    if($accountbal>=$amount){$wdamount=$remamount; $remamount=0;}else{$wdamount=$accountbal; $remamount-=$accountbal;}
                    Savingtransaction::transact($date, $savinaccount, $wdamount,0, $type, $description, $transacted_by, $member,'','','');
                }
            }
        }
    }
    public static function getFinalSavingsBalance($member)
    {	$scount=DB::table('savingaccounts')->where('member_id', '=', $member)->count();
        if($scount>0){
            $rawsamount= Savingaccount::getUserSavingsBalance($member);
            $guaranteeamount=Loanaccount::amountGuarantee($member);
            $loanBalance=Loantransaction::getMemberLoanBalance($member);
            $finalsamount=(float)$rawsamount-(float)$guaranteeamount; $finalsamount=$finalsamount-(float)$loanBalance;
            if($finalsamount<0){$finalamount=0;}
        }else{$finalsamount=0;}
        return $finalsamount;
    }
    public static function getDepositSavingsBalance($member)
    {
        $scount=DB::table('savingaccounts')->where('member_id', '=', $member)->where('savingproduct_id' ,'=', 4)->count();
        if($scount>0){
            $savingaccount=DB::table('savingaccounts')->where('member_id', '=', $member)->where('savingproduct_id' ,'=', 4)->first();
            $accountbal=Savingaccount::getAccountBalance($savingaccount);
        }else{$accountbal=0;}
        return (float)$accountbal;
    }
    public static function getFinalDepositBalance($member_id)
    {
        $scount=DB::table('savingaccounts')->where('member_id', '=', $member_id)->where('savingproduct_id' ,'=', 4)->count();
        if($scount>0){
            $rawsamount=Savingaccount::getDepositSavingsBalance($member_id);
            $guaranteeamount=Loanaccount::amountGuarantee($member_id);
            $loanBalance=Loantransaction::getMemberLoanBalance($member_id);
            $finalamount=(float)$rawsamount-(float)$guaranteeamount; $finalamount=$finalamount-(float)$loanBalance;
            if($finalamount<1){$finalamount=0;}
        }else{$finalamount=0;}
        return  $finalamount;
    }
    public static function getSavingsBetween($savingaccount, $from, $to)
    {

        $deposits = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'credit')->whereBetween('date', array($from, $to))->sum('saving_amount');
        $withdrawals = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'debit')->whereBetween('date', array($from, $to))->sum('saving_amount');

        $balance = $deposits - $withdrawals;

        return $balance;
    }

    public static function getSavingsAsAt($savingaccount, $date)
    {

        $deposits = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'credit')->where('date', '<=', $date)->sum('saving_amount');
        $withdrawals = DB::table('savingtransactions')->where('savingaccount_id', '=', $savingaccount->id)->where('type', '=', 'debit')->where('date', '<=', $date)->sum('saving_amount');

        $balance = $deposits - $withdrawals;

        return $balance;
    }


    public static function getDeductionAmount($account, $date)
    {

        $transactions = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)->get();

        $amount = 0;
        foreach ($transactions as $transaction) {

            $period = date('m-Y', strtotime($transaction->date));

            if ($date == $period) {

                if ($transaction->type == 'credit') {
                    $amount = $transaction->saving_amount;
                }

            }

        }


        return $amount;
    }


    public static function getAccountNumbersString()
    {
        $savingsAccounts = DB::table('savingaccounts')->limit(10)->get();
        $savingsAccountsArray = array();
        foreach ($savingsAccounts as $savingsAccount) {
            array_push($savingsAccountsArray, $savingsAccount->account_number);
        }
        return strtolower(trim(implode(",", $savingsAccountsArray)));
    }


}
