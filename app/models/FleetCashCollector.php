<?php namespace App\models;


use Illuminate\Database\Eloquent\Model;

class FleetCashCollector extends Model {
    /*
    use \Traits\Encryptable;


    protected $encryptable = [

        'relief_amount',

    ];
    */

    public $table = "fleetCashCollectors";

    public static $rules = [
        'employee' => 'required',
        'relief' => 'required',
        'amount' => 'required|regex:/^(\$?(?(?=\()(\())\d+(?:,\d+)?(?:\.\d+)?(?(2)\)))$/',
    ];

    public static $messages = array(
        'employee.required'=>'Please select employee!',
        'relief.required'=>'Please select relief type!',
        'amount.required'=>'Please insert amount!',
        'amount.regex'=>'Please insert a valid amount!',
    );

    // Don't forget to fill this array
    protected $fillable = [];

    public function vehicle(){
        return $this->hasOne('Vehicle','cash_collector');
    }

    public function collections(){
        return $this->hasMany('FleetCashCollection','collector_id');
    }

    public function createCollection($acc_name, $product)
    {

        //create savings control

        $category = 'LIABILITY';

        $account = new Account;

        $account->category = 'LIABILITY';
        $account->code = getaccountcode($category);
        $account->name = $acc_name . ' control';
        $account->active = TRUE;
        $account->savingproduct()->associate($product);

        $account->save();


        $category = 'INCOME';

        $account = new Account;

        $account->category = 'INCOME';
        $account->code = getaccountcode($category);
        $account->name = $acc_name . ' fee income';
        $account->active = TRUE;
        $account->savingproduct()->associate($product);

        $account->save();
    }

}
