<?php
namespace App\models;

use App\User;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


/**
 * Class UserRepository
 *
 * This service abstracts some interactions that occurs between Confide and
 * the Database.
 */
class UserRepository
{
    use ThrottlesLogins;
    /*
     * Signup a new account with the given parameters
     *
     * @param  array $input Array containing 'username', 'email' and 'password'.
     *
     * @return  User User object that may or may not be saved successfully. Check the id to make sure.
     */
    public function signup($input)
    {

        $organization = Organization::find(1);
        $name = $input['organization'];

        $org = new Organization;
        $lcode = $org->encode($name);

        $organization->name = $name;
        $organization->licensed = 100;
        $organization->license_code = $lcode;
        $organization->update();


        $user = new User;

        $user->username = $input['username'];
        $user->email    = $input['email'];
        $user->password = $input['password'];
        $user->user_type = $input['user_type'];

        // The password confirmation will be removed from model
        // before saving. This field will be used in Ardent's
        // auto validation.
        $user->password_confirmation = $input['password_confirmation'];

        // Generate a random confirmation code
        $user->confirmation_code     = md5(uniqid(mt_rand(), true));

        // Save if valid. Password field will be hashed before save
        $this->save($user);



        return $user;
    }

    /**
     * Attempts to login with the given credentials.
     *
     * @param  array $input Array containing the credentials (email/username and password)
     *
     * @return  boolean Success?
     */
    public function login($input)
    {
 //        if($request->isMethod('post')){
//            $data = $request->input();
//            $adminCount = Admin::where(['username' => $data['username'],'password'=>md5($data['password']),'status'=>1])->count();
//            if($adminCount > 0){
//                //echo "Success"; die;
//                Session::put('adminSession', $data['username']);
//                return redirect('/admin/dashboard');
//            }else{
//                //echo "failed"; die;
//                return redirect('/admin')->with('flash_message_error','Invalid Username or Password');
//            }
//        }
//        return view('admin.admin_login');
       return  Auth::attempt(['username' => $input['email'], 'password' => $input['password']]);

    }

    /**
     * Checks if the credentials has been throttled by too
     * much failed login attempts
     *
     * @param  array $credentials Array containing the credentials (email/username and password)
     *
     * @return  boolean Is throttled
     */
    // public function isThrottled($input)
    // {
    //     return Confide::isThrottled($input);
    // }

    /**
     * Checks if the given credentials correponds to a user that exists but
     * is not confirmed
     *
     * @param  array $credentials Array containing the credentials (email/username and password)
     *
     * @return  boolean Exists and is not confirmed?
     */
    public function existsButNotConfirmed($input)
    {
        //$user = Confide::getUserByEmailOrUsername($input);

        $user = Auth::check($input);

        if ($user) {
            $correctPassword = Hash::check(
                isset($input['password']) ? $input['password'] : false,
                $input->password
            );

            return (! $input->confirmed && $correctPassword);
        }
    }

    /**
     * Resets a password of a user. The $input['token'] will tell which user.
     *
     * @param  array $input Array containing 'token', 'password' and 'password_confirmation' keys.
     *
     * @return  boolean Success
     */
    public function resetPassword($input)
    {
        $result = false;
        $email = DB::table('password_reminders')->where('token' ,$input['token'])->pluck('email');
        $user = User::where('email', $email)->first();

        if ($user) {
            $user->password = $input['password'];
            $user->password_confirmation = $input['password_confirmation'];
            $result = $this->save($user);
        }

        // If result is positive, destroy token
        if ($result) {
            'test';
           // Confide::destroyForgotPasswordToken($input['token']);
        }

        return $result;
    }

    /**
     * Simply saves the given instance
     *
     * @param  User $instance
     *
     * @return  boolean Success
     */
    public function save(User $instance)
    {
        return $instance->save();
    }
}
