<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class Member extends Model {

    // Add your validation rules here
    public static $rules = [
        'mname' => 'required',
        'membership_no' => 'required',
        'branch_id' => 'required',
        'phone' => 'required',
        // 'member_type'=>'required'
    ];

    // Don't forget to fill this array
    protected $fillable = [];


    public function branch(){

        return $this->belongsTo('App\models\Branch');
    }

    public static function getBank($id){
        if($id == 0){
            return '';
        }else{
            return Bank::find($id)->pluck('name');
        }
    }

    public function bank(){

        return $this->belongsTo('App\models\Bank');
    }

    public function group(){

        return $this->belongsTo('App\models\Group');
    }

    public function kin(){

        return $this->hasMany('App\models\Kin');
    }


    public function savingaccounts(){

        return $this->hasMany('App\models\Savingaccount');
    }


    public function shareaccount(){

        return $this->hasOne('App\models\Shareaccount');
    }



    public function loanaccounts(){

        return $this->hasMany('App\models\Loanaccount');
    }

    public function documents(){

        return $this->hasMany('App\models\Document');
    }


    public function guarantors(){

        return $this->hasMany('App\models\Loanguarantor');
    }



    public static function getMemberAccount($id){

        $account_id = DB::table('savingaccounts')->where('member_id', '=', $id)->pluck('id');

        $account = Savingaccount::find($account_id);

        return $account;
    }
    public static function getMemberName($id){

        $member = Member::find($id);

        return $member->name;
    }
    public static function getMemberNo($id){

        $member = Member::find($id);

        return $member->membership_no;
    }

    public static function deactivateportal($id){
        $member = Member::find($id);

        DB::table('users')->where('username', '=', $member->membership_no)->delete();

        $member->is_css_active = false;
        $member->update();

        $email=$member->email;
        $password=strtoupper(Str::random(6));
        $name=$member->name;

        Mail::send( 'emails.deactivate', array('name'=>$name), function( $message ) use ($member)
        {
            //$message->from ('kevohm.km@gmail.com','Kelvin Gikonyo');

            $message->to($member->email )->subject( 'Self Service Portal Deactivation' );
        });

    }
    public static function dormantDeactivation(){
        $members = Member::all(); $m=0;
        foreach($members as $member){$m++;
            $savings=DB::table('savingaccounts')->where('member_id','=',$member->id)
                ->orderBy('id','DESC')->pluck('created_at');
            $loans=DB::table('loanaccounts')->where('member_id','=',$member->id)
                ->orderBy('id','DESC')->pluck('application_date');
            $loantransactions= DB::table('loantransactions')->orderBy('id','DESC')->get();
            foreach($loantransactions as $ltransaction){
                $loanaccount= Loanaccount::findorfail($ltransaction->loanaccount_id);
                if($loanaccount->member->id == $member->id){
                    $llast_transact= $ltransaction->date;
                    break;
                }
            }
            $savetransactions= DB::table('savingtransactions')->orderBy('id','DESC')->get();
            foreach($savetransactions as $stransaction){
                $savingaccount= Savingaccount::findorfail($stransaction->savingaccount_id);
                if($savingaccount->member->id == $member->id){
                    $slast_transact= $stransaction->date;
                    break;
                }
            }

            $inactivity_duration = DB::table('configuration')->where('id', '=', 1)->pluck('value');
            $lastSave= explode('',$savings);
            $lastSaving= $lastSave[0];
            $lastLoan=$loans; //'2020-4-20';
            if(!empty($savings)){
                $lastSave=explode(' ',$savings);
                $lastSaving=$lastSave[0];
            }
            else{
                $lastSaving=$member->lastactivated_at;
            }
            if(!empty($loans)){
                $lastLoan=$loans;
            }else{
                $lastLoan=$member->lastactivated_at;
            }
            if(empty($llast_transact)){
                $llast_transact= $member->lastactivated_at;
            }
            if(empty($slast_transact)){
                $slast_transact=$member->lastactivated_at;
            }
            $savings_diff= Loantransaction::monthsDiff($lastSaving,date('Y-m-d'));
            $loans_diff = Loantransaction::monthsDiff($lastLoan,date('Y-m-d'));
            $stransact_diff= Loantransaction::monthsDiff($slast_transact,date('Y-m-d'));
            $ltransact_diff= Loantransaction::monthsDiff($llast_transact,date('Y-m-d'));
            if($savings_diff>(int)$inactivity_duration && $loans_diff>(int)$inactivity_duration && $stransact_diff>(int)$inactivity_duration && $ltransact_diff>(int)$inactivity_duration){
                Member::deactivateportal($member->id);
            }
        }
    }

}
