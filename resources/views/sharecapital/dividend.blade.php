@extends('layouts.css')
@section('xara_cbs')

    <?php
    $counter= App\models\Dividend::count();
    ?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Member Dividends</h3>

                            <div class="card-header-right">
                                @if($counter==0)
                                    <a class="dt-button btn-sm" href="{{url('sharecapital/parameters')}}">  Set Dividend Settings</a>
                                @endif
                                @if($counter==1)
                                    <a class="dt-button btn-sm" href="{{url('sharecapital/editparameters')}}">  Update Dividend Settings</a>
                                @endif
                            </div>
                            @if (Session::get('notice'))
                                <div class="alert alert-info">{{ Session::get('notice') }}</div>
                            @endif

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member #</th>
                                        <th>Member Name</th>
                                        <th>Shares</th>
                                        <th>Acc. Dividends</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 1;
                                    function asMoney($value){
                                        return number_format($value,2);
                                    }
                                    $sharecount= App\models\Share::count();
                                    if($sharecount>0){
                                        $sharevalue= App\models\Share::where('id','=',1)->pluck('value');
                                        switch($sharevalue[0]){
                                            case $sharevalue[0] ==0:
                                                $sharevalue[0] =0.00000009;
                                                $count= App\models\Dividend::count();
                                                if($count>0){
                                                    $pars= App\models\Dividend::where('id','=',1)->get()->first();
                                                    $top=$pars->total- $pars->special;
                                                    $multiplier= $top/$pars->outstanding;
                                                }else if($count<=0){
                                                    $multiplier= 0.0000000009;
                                                }
                                                break;

                                            case $sharevalue[0] > 0:
                                                $count= App\models\Dividend::count();
                                                if($count>0){
                                                    $pars= App\models\Dividend::where('id','=',1)->get()->first();
                                                    $top=$pars->total- $pars->special;
                                                    $multiplier=$top/$pars->outstanding;
                                                }else if($count<=0){
                                                    $multiplier=0.0000000009;
                                                }
                                                break;
                                        }
                                    }else if($sharecount<=0){
                                        $sharevalue=0.000000009;
                                        $count= App\models\Dividend::count();
                                        if($count>0){
                                            $pars= App\models\Dividend::where('id','=',1)->get()->first();
                                            $top=$pars->total- $pars->special;
                                            $multiplier=$top/$pars->outstanding;
                                        }else if($count<=0){
                                            $multiplier=0.0000000009;
                                        }
                                    }
                                    ?>
                                    @foreach($members as $member)
                                        <tr>
                                            <td> {{ $i }}</td>
                                            <td>{{ $member->membership_no }}</td>
                                            <td>{{ $member->name }}</td>
                                            <?php $credit= App\models\Sharetransaction::where('shareaccount_id','=',$member->id)->where('type','=','credit')->sum('amount');
                                            $debit= App\models\Sharetransaction::where('shareaccount_id','=',$member->id)->where('type','=','debit')->sum('amount');
                                            $contributions=$credit-$debit; ?>
                                            <td>{{asMoney($contributions/$sharevalue[0])}}</td>
                                            <td>{{asMoney($multiplier * $contributions)}}</td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
