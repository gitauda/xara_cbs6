@extends('layouts.css')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Update Dividend Parameters</h3>
                            @if (count($errors)> 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ url('sharecapital/editparameters') }}}">@csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="username">Sum of Dividends</label>
                                        <input class="form-control numbers" type="text" name="sum_dividends"
                                               value="{{$div->total}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Special Dividends</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="special_dividends"
                                               value="{{$div->special}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Outstanding Shares </label>
                                        <input class="form-control numbers" type="text" name="outstanding_shares"
                                               value="{{$div->outstanding}}">
                                    </div>
                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">Update Parameters</button>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
