@extends('layouts.css')
@section('xara_cbs')
    <br/>
    <?php
    function asMoney($value){
        return number_format($value,2);
    }
    ?>


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Share Capital Contributions</h3>
                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{url('transfershares')}}"> Transfer Shares</a>
                            </div>
                            @if (Session::get('notice'))
                                <div class="alert alert-info">{{ Session::get('notice') }}</div>
                            @endif
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member #</th>
                                        <th>Member Name</th>
                                        <th>Contributions</th>
                                        <th>Shares</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 1;
                                    $sharevalue= App\models\Share::where('id','=',1)->pluck('value');
                                    ?>
                                    @foreach($members as $member)
                                        <tr>
                                            <td> {{ $i }}</td>
                                            <td>{{ $member->membership_no }}</td>
                                            <td>{{ $member->name }}</td>
                                            <?php $credit= App\models\Sharetransaction::where('shareaccount_id','=',$member->id)->where('type','=','credit')->sum('amount');
                                            $debit= App\models\Sharetransaction::where('shareaccount_id','=',$member->id)->where('type','=','debit')->sum('amount');
                                            $contributions=$credit-$debit; ?>
                                            <td>{{asMoney($contributions)}}</td>
                                            <td>{{asMoney($contributions/$sharevalue[0])}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li>
                                                            <a href="{{url('sharetransactions/show/'.$member->id)}}">
                                                                Contribute
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@stop
