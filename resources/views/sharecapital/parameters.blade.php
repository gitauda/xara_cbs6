@extends('layouts.css')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Dividend Computation Parameters</h3>
                            @if (count($errors)> 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ URL::to('sharecapital/parameters') }}}" >@csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="sum_dividends">Sum of Dividends</label>
                                        <input class="form-control numbers" placeholder="" type="text"
                                               name="sum_dividends" id="sum_dividends" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="amount_disbursed">Special Dividends</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="special_dividends"
                                               id="amount_disbursed" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="outstanding_shares">Outstanding Shares </label>
                                        <input class="form-control numbers" type="text" name="outstanding_shares"  id="outstanding_shares" value="">
                                    </div>
                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">Set Parameters</button>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
