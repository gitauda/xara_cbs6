@extends('layouts.savings')
@section('xara_cbs')
    <br/>
    <?php
    function asMoney($value) {
        return number_format($value, 2);
    }
    ?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors)> 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>New Saving Product</h3>


                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{url('savingproducts') }}" accept-charset="UTF-8">@csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="username">Product Name</label>
                                        <input class="form-control" placeholder="" type="text" name="name" id="name" value="{{{ old('name') }}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Product Short Name</label>
                                        <input class="form-control" placeholder="" type="text" name="shortname" id="shortname" value="{{{ old('shortname') }}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Currency</label>
                                        <select class="form-control" name="currency" required>
                                            @foreach($currencies as $currency)
                                                <option value="{{ $currency->shortname }}"> {{ $currency->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Account opening balance</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="opening_balance" id="opening_balance" value="{{{ old('opening_balance') }}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Cash Account</label>
                                        <select class="form-control" name="cash_account" required>
                                            <option></option>
                                            @foreach($accounts as $account)
                                                @if($account->category == 'ASSET')
                                                    <option value="{{ $account->id }}">{{ $account->name."(".$account->code.")" }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Savings Control Account</label>
                                        <select class="form-control" name="saving_control_acc" required>
                                            <option></option>
                                            @foreach($accounts as $account)
                                                @if($account->category == 'LIABILITY')
                                                    <option value="{{ $account->id }}">
                                                        {{ $account->name."(".$account->code.")" }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Fee Income Account</label>
                                        <select class="form-control" name="fee_income_acc" required>
                                            <option></option>
                                            @foreach($accounts as $account)
                                                @if($account->category == 'INCOME')
                                                    <option value="{{ $account->id }}">{{ $account->name."(".$account->code.")" }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Interest Rate</label>
                                        <input class="form-control" placeholder="input preferred rate" type="text" name="interestrate" id="interestrate" value="{{{ old('interestrate') }}}" required>

                                    </div>

                                    <div class="form-group">
                                        <label for="username">Minimum Allowable Amount</label>
                                        <input class="form-control" placeholder="OPTIONAL FIELD" type="text" name="minamount" id="minamount" value="{{{ old('min_amount') }}}" >

                                    </div>

                                    <div class="form-group">
                                        <label for="username">Product Type</label>
                                        <select class="form-control" name="type" required>
                                            <option></option>
                                            <option value="BOSA">BOSA</option>
                                            <option value="FOSA">FOSA</option>

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <input type='checkbox' name='is_special'>
                                        <label for="username">special product</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="special_date">special_date</label>
                                        <input class="form-control" placeholder="" type="date" name="special_date" id="special_date" value="{{{ date('Y-d-m')}}}" required>
                                    </div>

                                    <div class="dt-responsive table-responsive">
                                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">

                                            <thead>
                                            <th></th>
                                            <th>Charge </th>
                                            <th>Amount</th>
                                            </thead>

                                            <tbody>
                                            @foreach($charges as $charge)

                                                @if($charge->category == 'saving')
                                                    <tr>
                                                        <td><input type="checkbox" value="{{$charge->id}}" name="charge_id[]"></td>
                                                        <td>{{$charge->name}}</td>
                                                        <td>{{asMoney($charge->amount)}}</td>

                                                    </tr>
                                                @endif
                                            @endforeach

                                            </tbody>

                                        </table>
                                    </div>
                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Create Product</button>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
