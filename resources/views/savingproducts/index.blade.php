@extends('layouts.savings')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            @if(isset($surface))
                                <div class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{$surface }}</strong>
                                </div>
                            @endif
                                @if(isset($smash))
                                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <strong>{{ $smash }}</strong>
                                    </div>
                                @endif

                                <h3>Saving Products</h3>>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ url('savingproducts/create')}}">new Saving product</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>#</th>
                                    <th>Product Name</th>
                                    <th>Short name</th>
                                    <th>Opening Balance</th>
                                    <th>Interest Rate</th>
                                    <th>Currency</th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($savingproducts as $product)
                                        <tr>
                                            <td> {{ $i }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->shortname }}</td>
                                            <td>{{ $product->opening_balance }}</td>
                                            <td>{{ $product->Interest_Rate }}</td>
                                            <th>{{$product->currency}}</th>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{url('savingproducts/show/'.$product->id)}}">View</a></li>
                                                        <li><a href="{{url('savingproducts/update/'.$product->id)}}">Update</a></li>
                                                        <li><a href="{{url('savingproducts/delete/'.$product->id)}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
