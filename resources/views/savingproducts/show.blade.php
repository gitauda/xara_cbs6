@extends('layouts.savings')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>{{$savingproduct->name}}</h3>
                        </div>
                        <div class="row">
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                        <tr>

                                            <td> Name</td><td>{{ $savingproduct->name}}</td>
                                        </tr>

                                        <tr>

                                            <td> Short name</td><td>{{ $savingproduct->shortname}}</td>
                                        </tr>

                                        <tr>

                                            <td> Opening Balance</td><td>{{ $savingproduct->opening_balance}}</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>

                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                        <tr>
                                            <td> Transaction</td>
                                            <td>Debit Account</td>
                                            <td>Credit Account</td>
                                        </tr>

                                        @foreach($savingproduct->savingpostings as $posting)
                                            <tr>

                                                <td> {{$posting->transaction }}</td>
                                                <td>

                                                    <?php

                                                    $account = App\models\Account::findorfail($posting->debit_account);


                                                    ?>
                                                    {{ $account->name.'('.$account->code.')'}}</td>
                                                <td>

                                                    <?php

                                                    $account = App\models\Account::findorfail($posting->credit_account);


                                                    ?>
                                                    {{ $account->name.'('.$account->code.')'}}</td>
                                            </tr>

                                        @endforeach
                                    </table>
                                </div>

                            </div>
                        </div>

                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>Charge</th>
                                    <th>Amount</th>
                                    <th>Calculation</th>
                                    </thead>
                                    <tbody>
                                    @foreach($savingproduct->charges as $charge)
                                        <tr>
                                            <td>{{$charge->name }}</td>
                                            <td>{{$charge->amount }}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
