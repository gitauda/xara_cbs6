@extends('layouts.css')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">

                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{url('products/create')}}">New Product</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>Image </th>
                                        <th>Vendor </th>

                                        <th>Name </th>
                                        <th>Description</th>

                                        <th>Price</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>

                                            <td><img src=" {{ asset('public/uploads/images/'.$product->image)}}" width="15%"/></td>
                                            <td>{{ $product->vendor->name  }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->description }}</td>
                                            <td>{{ $product->price }}</td>
                                            <td>{{ $product->status }}</td>


                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">


                                                        <li><a href="{{url('products/edit/'.$product->id)}}">Edit</a> </li>


                                                        <li><a href="{{url('products/delete/'.$product->id)}}"> Delete</a></li>
                                                    </ul>
                                                </div>

                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
