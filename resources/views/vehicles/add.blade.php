@extends('layouts.member')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Create Vehicles</h3>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ url('vehicles/add') }}}" >@csrf
                                <fieldset>
                                    <input type="hidden" name="member" value="{{$member->id}}" />
                                    <div class="form-group">
                                        <label for="username">Vehicle Make:</label>
                                        <input class="form-control" placeholder="" type="text" name="make" id="make" value="{{{ old('make') }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Vehicle Registration Number:</label>
                                        <input class="form-control" placeholder="" type="text" name="regno" id="regno" value="{{{ old('regno') }}}">
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control selectable"  name="registration">
                                            @foreach($charges as $charge)
                                                <option value="{{$charge->amount}}">{{$charge->name. '-'.$charge->amount}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">Create Vehicle</button>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
