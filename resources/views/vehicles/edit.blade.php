@extends('layouts.member')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Update Vehicles</h3>


                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{ url('vehicles/update/'.$vehicle->id) }}}" accept-charset="UTF-8">@csrf

                                <fieldset>

                                    <div class="form-group">
                                        <label for="make">Vehicle Make:</label>
                                        <input class="form-control" placeholder="" type="text" name="make" id="make" value="{{$vehicle->make}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="regno">Vehicle Registration Number:</label>
                                        <input class="form-control" placeholder="" type="text" name="regno" id="regno" value="{{$vehicle->regno}}">
                                    </div>

                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Update Vehicle</button>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
