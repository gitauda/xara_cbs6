@extends('layouts.css')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if(isset($created))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ $created}}}</strong>
                            </div>
                        @endif
                        @if(isset($updated))
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ $updated }}}</strong>
                            </div>
                        @endif
                        @if(Session::has('deleted'))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ Session::get('deleted') }}}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>
                                Investment Projects
                            </h3>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ URL::to('projects/create') }}">New Investment Project</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Project Name</th>
                                        <th>Investment Name</th>
                                        <th>Project Units</th>
                                        <th>Price per Unit</th>
                                        <th>Description</th>
                                        <th></th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    <?php $i=1;?>
                                    @if(isset($projects) && $i<=count($projects))
                                        @foreach($projects as $invest)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$invest->name}}</td>
                                                <td>{{$project= App\models\Investment::where('id','=',$invest->investment_id)->pluck('name')}}</td>
                                                <td>{{$invest->units}}</td>
                                                <td>{{$invest->unit_price}}</td>
                                                <td>{{$invest->description}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="{{url('projects/update/'.$invest->id)}}">Edit</a> </li>
                                                            <li><a href="{{url('projects/delete/'.$invest->id)}}" onclick="return (confirm('Are you sure you want to delete this investment project?'))"> Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $i++;?>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
