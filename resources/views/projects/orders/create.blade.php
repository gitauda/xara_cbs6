@extends('layouts.membercss')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if(Session::has('none'))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ Session::get('none') }}}</strong>
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>{{$project->name }} Project</h3>

                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ url('projects/orders/create') }}}" >@csrf
                                <fieldset>
                                    <!--BEGIN VERBOTEN-->
                                    <input type="hidden" name="project" value="{{$project->id}}">
                                    <input type="hidden" name="member"
                                           value="{{$member= App\models\Member::where('email','=',Auth::user()->email)->pluck('id')}}">
                                    <!--END VERBOTEN-->
                                    <div class="form-group">
                                        <label for="username" id="amt">Project Name
                                            <span style="color:red">*</span></label>
                                        <input class="form-control" placeholder="" type="text" name="name"
                                               value="{{{$project->name }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username" id="amt">Units Investing
                                            <span style="color:red">*</span></label>
                                        <input class="form-control numbers" placeholder="" type="text" name="units"
                                               value="{{{ old('units') }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Payment Mode<span style="color:red">*</span></label>
                                        <select class="form-control" name="payment_mode" >
                                            <option value="">Select Payment Option</option>
                                            <option value="">--------------------------</option>
                                            <option value="Loan Transfer">Loan Application</option>
                                            <option value="Savings Transfer">Savings Transfer </option>
                                            <option value="Mpesa Payment">Mpesa Payment</option>
                                            <option value="Cash Payment">Cash Payment</option>
                                        </select>
                                    </div>
                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            Submit Investment Order
                                        </button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
