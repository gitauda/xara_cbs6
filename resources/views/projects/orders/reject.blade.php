@extends('layouts.css')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if(Session::has('none'))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ Session::get('none') }}}</strong>
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3> Reject Investment Project</h3>

                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ url('projects/orders/reject') }}}" >@csrf
                                <fieldset>
                                    <!--BEGIN VERBOTEN-->
                                    <input type="hidden" name="project" value="{{$project->id}}">
                                    <input type="hidden" name="member"
                                           value="{{$member= App\models\Member::where('email','=',Auth::user()->email)->pluck('id')}}">
                                    <!--END VERBOTEN-->
                                    <div class="form-group">
                                        <label for="username" id="amt">Member Name
                                            <span style="color:red">*</span></label>
                                        <input class="form-control" placeholder="" type="text" name="membername"
                                               value="{{{$membername= App\models\Member::where('email','=',Auth::user()->email)->pluck('name') }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username" id="amt">Project Name
                                            <span style="color:red">*</span></label>
                                        <input class="form-control" placeholder="" type="text" name="name"
                                               value="{{{$project12= App\models\Project::where('id','=',$project->project_id)->pluck('name') }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username" id="amt">Units Investing
                                            <span style="color:red">*</span></label>
                                        <input class="form-control numbers" placeholder="" type="text" name="units"
                                               value="{{{$project->units}}}">
                                    </div>
                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            Reject Investment Order
                                        </button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
