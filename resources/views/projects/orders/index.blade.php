@extends('layouts.css')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if(isset($created))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ $created}}}</strong>
                            </div>
                        @endif
                        @if(isset($updated))
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ $updated }}}</strong>
                            </div>
                        @endif
                        @if(isset($approved))
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ $approved }}}</strong>
                            </div>
                        @endif
                        @if(isset($rejected))
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ $rejected }}}</strong>
                            </div>
                        @endif
                        @if(Session::has('deleted'))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ Session::get('deleted') }}}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <strong>
                                INVESTMENT PROJECTS ORDERS
                            </strong>

{{--                            <div class="card-header-right">--}}
{{--                                <a class="dt-button btn-sm" href="{{ URL::to('projects/create') }}">New Investment Project</a>--}}
{{--                            </div>--}}

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member Name</th>
                                        <th>Project Name</th>
                                        <th>Units Ordered</th>
                                        <th>Date Ordered</th>
                                        <th></th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    <?php $i=1;?>
                                    @if(isset($projects) && $i <=count($projects))
                                        @foreach($projects as $invest)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$member= App\models\Member::where('id','=',$invest->member_id)->pluck('name')}}</td>
                                                <td>{{$project= App\models\Project::where('id','=',$invest->project_id)->pluck('name')}}</td>
                                                <td>{{$invest->units}}</td>
                                                <td>{{$invest->date}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="{{url('projects/orders/update/'.$invest->id)}}" onclick="return (confirm('Are you sure you want to update this project order?'))"> Edit</a></li>
                                                            <li><a href="{{url('projects/orders/approve/'.$invest->id)}}"  onclick="return (confirm('Are you sure you want to approve this project order?'))">Approve</a> </li>
                                                            <li><a href="{{url('projects/orders/reject/'.$invest->id)}}" onclick="return (confirm('Are you sure you want to reject this project order?'))"> Reject</a></li>
                                                            <li><a href="{{url('projects/orders/delete/'.$invest->id)}}" onclick="return (confirm('Are you sure you want to delete this project order?'))"> Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $i++;?>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
