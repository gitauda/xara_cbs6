@extends('layouts.css')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if(Session::has('none'))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ Session::get('none') }}}</strong>
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>New Investment Project</h3>

                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ url('projects') }}}" >@csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="username" id="amt">Project Name <span style="color:red">*</span></label>
                                        <input class="form-control" placeholder="" type="text" name="name"
                                               value="{{{ old('name') }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Investment Type<span style="color:red">*</span></label>
                                        <select class="form-control" name="investment" >
                                            <option value="">Select Investment Type</option>
                                            <option value="">--------------------------</option>
                                            @foreach($investment as $loanproduct)
                                                <option value="{{$loanproduct->id}}">{{ $loanproduct->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="username" id="amt">Units (Total Number) <span style="color:red">*</span></label>
                                        <input class="form-control numbers" placeholder="" type="text" name="units"
                                               value="{{{ old('units') }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username" id="amt">Price per Unit
                                            <span style="color:red">*</span>
                                        </label>
                                        <input class="form-control numbers" placeholder="" type="text" name="unit_price"
                                               value="{{{ old('unit_price') }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="desc" id="amt">
                                            Project Description
                                            <span style="color:red">*</span>
                                        </label>
                                        <textarea name="desc" id="desc" class="form-control"> {{{old('desc')}}} </textarea>
                                    </div>

                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            Create Investment Project
                                        </button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
