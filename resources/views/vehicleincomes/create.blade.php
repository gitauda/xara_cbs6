@extends('layouts.member')
@section('xara_cbs')

    <script type="text/javascript">
        $(document).ready(function(){
            $("#inc").hide();
            $('#type').change(function(){
                if($(this).val() == "Expense"){
                    $("#inc").show();
                }else{
                    $("#inc").hide();
                    $("#incur").val('');
                }

            });

            $('#vehicle_id').change(function(){
                $.get("{{ url('api/loanaccount')}}",
                    { option: $(this).val() },
                    function(data) {
                        $('#loanproduct').empty();
                        $('#loanproduct').append("<option value=''>----------------select Member Loan Product--------------------</option>");
                        $.each(data, function(key, element) {
                            $('#loanproduct').append("<option value='" + key +"'>" + element + "</option>");
                        });
                    });
            });

            $('#amount').keyup(function(){
                var savings = $('#savings').val('0.00');
                var shares  = $('#shares').val('0.00');
                var offamt  = $('#offamt').val('0.00');
                var loans   = $('#loans').val('0.00');
            });


            $('#shares').keyup(function(){
                var amount = $('#amount').val().replace(/,/g, '');
                var savings = $('#savings').val().replace(/,/g, '');
                var shares = $('#shares').val().replace(/,/g, '');
                var loans = $('#loans').val().replace(/,/g, '');
                var offamt = $('#offamt').val().replace(/,/g, '');
                var balance = 0;
                balance = amount - savings - loans - offamt;
                if(balance<shares || amount==''){
                    alert('The value can`t exceed '+balance);
                    shares = $('#shares').val('0.00');
                }
            });

            $('#savings').keyup(function(){
                var amount = $('#amount').val().replace(/,/g, '');
                var savings = $('#savings').val().replace(/,/g, '');
                var shares = $('#shares').val().replace(/,/g, '');
                var loans = $('#loans').val().replace(/,/g, '');
                var offamt = $('#offamt').val().replace(/,/g, '');
                var balance = 0;
                balance = amount - shares - loans - offamt;
                if(balance<savings || amount==''){
                    alert('The value can`t exceed '+balance);
                    savings = $('#savings').val('0.00');
                }
            });

            $('#offamt').keyup(function(){
                var amount = $('#amount').val().replace(/,/g, '');
                var savings = $('#savings').val().replace(/,/g, '');
                var shares = $('#shares').val().replace(/,/g, '');
                var loans = $('#loans').val().replace(/,/g, '');
                var offamt = $('#offamt').val().replace(/,/g, '');
                var balance = 0;
                balance = amount - shares - loans - savings;
                if(balance<offamt || amount==''){
                    alert('The value can`t exceed '+balance);
                    offamt = $('#offamt').val('0.00');
                }
            });

            $('#loans').keyup(function(){
                var amount = $('#amount').val().replace(/,/g, '');
                var savings = $('#savings').val().replace(/,/g, '');
                var shares = $('#shares').val().replace(/,/g, '');
                var loans = $('#loans').val().replace(/,/g, '');
                var offamt = $('#offamt').val().replace(/,/g, '');
                var balance = 0;
                balance = amount - shares - savings - offamt;
                if(balance<loans || amount==''){
                    alert('The value can`t exceed '+balance);
                    loans = $('#loans').val('0.00');
                }
            });

        });
    </script>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors)> 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Vehicle Incomes</h3>
                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ url('vehicleincomes') }}}" accept-charset="UTF-8">{{csrf_field()}}

                                <fieldset>

                                    <div class="form-group">
                                        <label for="username">Vehicle <span style="color:red">*</span></label>
                                        <select name="vehicle_id" id="vehicle_id" class="form-control" data-live-search="true" required>
                                            <option></option>
                                            @foreach($vehicles as $vehicle)
                                                <option value="{{ $vehicle->id }}"> {{ $vehicle->make.' - '.$vehicle->regno }}</option>
                                            @endforeach
                                        </select>

                                    </div>


                                    <div class="form-group">
                                        <label for="dropper_default">Date <span style="color:red">*</span></label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input required class="form-control datepicker" readonly="readonly" placeholder="" type="text" name="date" id="dropper_default" value="{{ old('date') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="amount">Amount <span style="color:red">*</span></label>
                                        <input required class="form-control" placeholder="" type="text" name="amount" id="amount" value="{{ old('amount') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="loanproduct">Loan Product</label>
                                        <select name="loanproduct_id" id="loanproduct" class="form-control">
                                            @foreach($loanproducts as $loanproduct)
                                                <option value="{{$loanproduct->id}}">{{$loanproduct->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="loans">Loan Repayment</label>
                                        <input required class="form-control" placeholder="" type="text" name="loans" id="loans" value="{{ old('loans') }}">
                                    </div>



                                    <div class="form-group">
                                        <label for="offamt">Office Contribution
                                        </label>
                                        <input required class="form-control" placeholder="" type="text" name="offamt" id="offamt" value="{{old('offamt') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="reasons">Missed Contribution Reasons (If any)</label>
                                        <textarea name="reasons" id="reasons" class="form-control"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="savings">Commissions/Savings</label>
                                        <input required class="form-control" placeholder="" type="text" name="savings" id="savings" value="{{ old('savings') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="shares">Shares
                                        </label>
                                        <input required class="form-control" placeholder="" type="text" name="shares" id="shares" value="{{old('shares')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="account">Asset Account <span style="color:red">*</span> </label>
                                        <select  required class="form-control" name="asset_id" id="account">
                                            <option> select account</option>
                                            @foreach($asset as $account)
                                                <option value="{{$account->id}}">{{$account->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="account">Liability Account <span style="color:red">*</span> </label>
                                        <select  required class="form-control" name="ainc_id" id="account">
                                            <option> select account</option>
                                            @foreach($ainc as $account)
                                                <option value="{{$account->id}}">{{$account->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="eq_id">Equity Account <span style="color:red">*</span> </label>
                                        <select  required class="form-control" name="eq_id" id="eq_id">
                                            <option> select account</option>
                                            @foreach($equities as $account)
                                                <option value="{{$account->id}}">{{$account->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Create Income</button>
                                    </div>

                                </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
