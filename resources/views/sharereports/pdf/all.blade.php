
<html><head>
    <style>
      @page { margin:  30px; }
      .header { position: fixed; left: 0px; top: -150px; right: 0px; height: 150px;  text-align: center; }
      .footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 50px;  }
      .footer .page:after { content: counter(page, upper-roman); }
      .content { margin-top: 0px;  }

    </style><body style="font-size:10px">
    <?php
    function asMoney($value) {
      return number_format($value, 2);
    }
    ?>

   <div class="footer">
     <p class="page">Page <?php $PAGE_NUM ?></p>
   </div>
   <div class="content">
     <table >
      <tr>
          <td style="width:150px">

              <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="{{ $organization->logo }}" style="height: 100px; margin: 10px 0;"/>

          </td>
        <td>
        <strong>
          {{ strtoupper($organization->name)}}<br>
          </strong>
          {{ $organization->phone}} |
          {{ $organization->email}} |
          {{ $organization->website}}<br>
          {{ $organization->address}}
        </td>
        <td>
          <strong><h3>ALL MEMBERS CONTRIBUTION </h3></strong>
        </td>
      </tr>
      <tr>
      <hr>
      </tr>
    </table>

      <br><br>
      <table class="table table-bordered" border="1" cellpadding = '0' cellspacing= "0" style="width:100%">
          <tr style="font-weight:bold">
              <td>Member name</td>
              <td>Amount</td>

          </tr>
          <tbody>
          <?php
            $total=0;
          ?>
          @foreach($arr as $key => $value)
             <tr>
                <td>{{$key}} </td>
                <td>{{$value['amount']}}</td>

              </tr>
              <?php
                $total+=$value['amount'];
              ?>

            @endforeach
          </tbody>

          <tr>
            <td> <strong>Total Contributions: &emsp;  </strong></td>
            <td> <strong> {{ asMoney($total)}}</strong></td>
          </tr>
      </table>

   </div>
 </body></html>
