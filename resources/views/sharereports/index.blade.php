@extends('layouts.ports')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3> Share Reports</h3>
                        </div>
                        <div class="card-block">

                            <ul>
                                <li>
                                    <a href="{{ url('sharereports/contributionlisting') }}" target="_blank"> Contributions Listing Report</a>
                                </li>
                                <li>
                                    <a href="{{url('sharereports/sharelisting')}}" target="_blank">Shares Listing Report</a>
                                </li>
                                <li>
                                    <a href="{{url('sharereports/individualcontribution')}}"> Individual Contribution Report</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
