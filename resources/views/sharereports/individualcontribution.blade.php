@extends('layouts.ports')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3> Individual Contributions Report</h3>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="card-block">

                            <form target="_blank" method="POST" action="{{url('sharereports/individualcontribution')}}" >@csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="username">Select Member:</label>
                                        <select name="memberid" id="memberid" class="form-control" required>
                                            <option value="All">All Members</option>
                                            @foreach($members as $member)
                                                <option value="{{$member->id }}">
                                                    {{ $member->membership_no.' : '.$member->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Format</label>
                                        <select class="form-control" name="format" id="format" required>
                                            <option value="">Select format</option>
                                            <option value="pdf">PDF</option>
                                            <option value="excel">Excel</option>
                                        </select>
                                    </div>
                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            View Member Contributions
                                        </button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
