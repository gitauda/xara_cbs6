@extends('layouts.member')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>TLB Payments</h3>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ url('tlbpayments/create')}}">New Member Fee</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>

                                    <th>#</th>
                                    <th>Vehicle</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Account</th>
                                    <th></th>

                                    </thead>
                                    <tbody>

                                    <?php $i = 1; ?>
                                    @foreach($tlbs as $tlb)

                                        <tr>

                                            <td> {{ $i }}</td>
                                            <td>{{ $tlb->vehicle->make.' '.$tlb->vehicle->regno }}</td>
                                            <td>{{ $tlb->amount }}</td>
                                            <td>{{ $tlb->date }}</td>
                                            <td>{{ $tlb->account->name }}</td>
                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{URL::to('tlbpayments/edit/'.$tlb->id)}}">Update</a></li>

                                                        <li><a href="{{URL::to('tlbpayments/delete/'.$tlb->id)}}">Delete</a></li>

                                                    </ul>
                                                </div>

                                            </td>



                                        </tr>

                                        <?php $i++; ?>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
