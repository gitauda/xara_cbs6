@extends('layouts.member')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Tlb Payment</h3>


                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{ url('tlbpayments') }}" accept-charset="UTF-8">@csrf

                                <fieldset>

                                    <div class="form-group">
                                        <label for="username">Vehicle <span style="color:red">*</span></label>
                                        <select name="vehicle_id" id="vehicle_id" class="form-control" data-live-search="true" required>
                                            <option></option>
                                            @foreach($vehicles as $vehicle)
                                                <option value="{{ $vehicle->id }}"> {{ $vehicle->make.' - '.$vehicle->regno }}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="username">Amount</label>
                                        <input class="form-control" placeholder="" type="text" name="amount" id="amount" value="{{ old('amount') }}">
                                    </div>


                                    <div class="form-group">
                                        <label for="username">Date <span style="color:red">*</span></label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input required class="form-control datepicker" readonly="readonly" placeholder="" type="text" name="date" id="date" value="{{ old('date') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Equity Account <span style="color:red">*</span> </label>
                                        <select  required class="form-control" name="eq_id" id="eq_id">
                                            <option> select account</option>
                                            @foreach($equities as $account)
                                                <option value="{{$account->id}}">{{$account->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="username">Asset Account <span style="color:red">*</span> </label>
                                        <select  required class="form-control" name="asset_id" id="asset_id">
                                            <option> select account</option>
                                            @foreach($asset as $account)
                                                <option value="{{$account->id}}">{{$account->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Create TLB Payment</button>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
