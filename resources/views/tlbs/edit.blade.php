@extends('layouts.member')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if(Session::get('notice'))
                            <div class="alert alert-success">{{ Session::get('notice') }}</div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Tlb Payment</h3>


                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{ url('tlbpayments/update/'.$tlb->id) }}" accept-charset="UTF-8">{{csrf_field()}}

                                <fieldset>



                                    <div class="form-group">
                                        <label for="amount">Amount</label>
                                        <input class="form-control" placeholder="" type="text" name="amount" id="amount" value="{{$tlb->amount}}">
                                    </div>


                                    <div class="form-group">
                                        <label for="dropper_default">Date <span style="color:red">*</span></label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input required class="form-control" readonly="readonly" placeholder="Select Date" type="text" name="date" id="dropper_default" value="{{$tlb->date}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Equity Account <span style="color:red">*</span> </label>
                                        <select  required class="form-control" name="eq_id" id="account">
                                            <option> select account</option>
                                            @foreach($equities as $account)
                                                <option value="{{$account->id}}"<?= ($tlb->account_id == $account->id)?'selected="selected"':''; ?>>{{$account->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Asset Account <span style="color:red">*</span> </label>
                                        <select  required class="form-control" name="asset_id" id="account">
                                            <option> select account</option>
                                            @foreach($asset as $account)
                                                <option value="{{$account->id}}"<?= ($tlb->asset_id==$account->id)?'selected="selected"':''; ?>>{{$account->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Create TLB Payment</button>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@stop
