@extends('layouts.new_main')
@section('xara_cbs')
    <div class="pcoded-content">
    {{-- TODO: move breadcrumbs to partials   --}}
    <!-- [ breadcrumb ] start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="feather icon-home bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Dashboard</h5>
                            <span>Cbs Home Page</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/')}}"><i class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Dashboard</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <!-- [ page content ] start -->
                        <div class="card">
                            @if(Session::get('notice'))
                            <div class="alert alert-success">{{ Session::get('notice') }}</div>
                             @endif
                            <div class="card-header">
                                <h5>Members</h5>
                                <div class="card-header-right">
                                    <ul class="list-unstyled card-option">
                                        <li class="first-opt"><i class="feather icon-chevron-left open-card-option"></i></li>
                                        <li><i class="feather icon-maximize full-card"></i></li>
                                        <li><i class="feather icon-minus minimize-card"></i></li>
                                        <li><i class="feather icon-refresh-cw reload-card"></i></li>
                                        <li><i class="feather icon-trash close-card"></i></li>
                                        <li><i class="feather icon-chevron-left open-card-option"></i></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Member Number</th>
                                            <th>Member Name</th>
                                            <th>Member Branch</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($members as $member)
                                            <tr>
                                                <td> {{ $i }}</td>
                                                <td>{{ $member->membership_no }}</td>
                                                <td>{{ $member->name }}</td>
                                                <td>{{ $member->branch->name }}</td>
                                                <td>
                                                    <a href="{{ url('member/savingaccounts/'.$member->id) }}" class="dt-button btn-sm">Transact Savings</a>

                                                </td>
                                                <td>
                                                    <a href="{{ url('sharetransactions/show/'.$member->shareaccount->id) }}" class="dt-button btn-sm">Transact Shares</a>

                                                </td>

                                            </tr>

                                            <?php $i++; ?>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <!-- [ page content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
