@extends('layouts.savings')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Share Management</h3>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <tr>
                                        <td>Unit Value</td><td>{{ $share->value }}</td>
                                    </tr>
                                    <tr>
                                        <td>Transfer Charge</td><td>{{$share->transfer_charge}}</td>
                                    </tr>
                                    <tr>
                                        <td>Charged on </td><td>{{$share->charged_on}}</td>
                                    </tr>
                                    <tr>

                                        <td><a class="dt-button btn-sm" href="{{url('shares/edit/'.$share->id)}}">Update</a></td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
