@extends('layouts.savings')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Share Management</h3>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{ url('shares/update/'.$share->id) }}">@csrf

                                <fieldset>

                                    <div class="form-group">
                                        <label for="value">Unit value</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="value" id="value" value="{{ $share->value }}" required>
                                    </div>


                                    <div class="form-group">
                                        <label for="transfer_charge">Transfer Charge</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="transfer_charge" id="transfer_charge" value="{{ $share->transfer_charge }}" required>
                                    </div>


                                    <div class="form-group">
                                        <label for="charged_on">Charged on</label>
                                        <select class="form-control" name="charged_on" required>
                                            <option value="{{$share->charged_on }}">{{$share->charged_on }}</option>

                                            <option>---------------------------</option>

                                            <option value="donor">Donor</option>
                                            <option value="recepient">Recepient</option>

                                        </select>
                                    </div>

                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">update</button>
                                    </div>

                                </fieldset>
                            </form>


                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
