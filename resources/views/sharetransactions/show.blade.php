@extends('layouts.member')
@section('xara_cbs')


    <?php


    function asMoney($value) {
        return number_format($value, 2);
    }

    ?>


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->

                    <div class="card">
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-condensed table-bordered table-hover">
                                    <tr>
                                        <td>Member Name:</td><td>{{ $account->member->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Member No:</td><td>{{ $account->member->membership_no}}</td>
                                    </tr>
                                    <tr>
                                        <td>Account No:</td><td>{{ $account->account_number}}</td>
                                    </tr>

                                    <tr>
                                        <td>Total Shares:</td><td>{{ $shares }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h5>Members</h5>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm"   href="{{ url('sharetransactions/create/'.$account->id)}}"> New Transaction</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Debit (DR)</th>
                                    <th>Credit (CR)</th>
                                    </thead>
                                    <tbody>
                                    @foreach($account->sharetransactions as $transaction)

                                        <tr>
                                            <td>{{ $transaction->date }}</td>
                                            <td>{{ $transaction->description }}</td>

                                            @if( $transaction->type == 'debit')
                                                <td >{{ asMoney($transaction->amount)}}</td>
                                                <td>0.00</td>
                                            @endif

                                            @if( $transaction->type == 'credit')
                                                <td>0.00</td>
                                                <td>{{ asMoney($transaction->amount) }}</td>
                                            @endif
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
