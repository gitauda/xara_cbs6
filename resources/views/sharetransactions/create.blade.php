@extends('layouts.main')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors)> 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <strong>Member: {{ $member->name }}</strong><br>
                            <strong>Member #: {{ $member->membership_no }}</strong><br>
                            <strong>Share Account #: {{ $shareaccount->account_number }}</strong><br>

                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{ url('sharetransactions') }}}" accept-charset="UTF-8">@csrf

                                <fieldset>
                                    <div class="form-group">
                                        <label for="username">Transaction </label>
                                        <select name="type" class="form-control" required>
                                            <option></option>
                                            <option value="credit"> Purchase</option>

                                        </select>
                                    </div>

                                    <input type="hidden" name="account_id" value="{{ $shareaccount->id}}">


                                    <div class="form-group">
                                        <label for="dropper-default"> Date</label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input class="form-control" placeholder="" readonly type="text" name="date" id="dropper-default" value="{{{ old('date') }}}" required>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="amount"> Amount</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount" value="{{{ old('amount') }}}" required>
                                    </div>


                                    <div class="form-group">
                                        <label for="description"> Description</label>
                                        <textarea class="form-control" name="description" id="description">{{{ old('description') }}}</textarea>

                                    </div>
                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>

@stop
