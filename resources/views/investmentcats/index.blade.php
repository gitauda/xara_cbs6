@extends('layouts.css')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->

                    <div class="card">

                        <div class="card-header">
                            <h3>Investment Categories</h3>
                            @if(isset($catdone))
                                <div class="alert alert-info alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{{ $catdone}}}</strong>
                                </div>
                            @endif
                            @if(isset($catupdated))
                                <div class="alert alert-info alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{{ $catupdated}}}</strong>
                                </div>
                            @endif
                            @if(Session::has('puff'))
                                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{{Session::get('puff')}}}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="card-block">


                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs  tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#categories" role="tab">Categories</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#classes" role="tab">Classes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#types" role="tab">Types</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content tabs card-block">

                                <div class="tab-pane active" id="categories" role="tabpanel">
                                    <div class="panel-heading">
                                        <p>
                                            <a href="{{ url('investmentscats/create/category') }}" class="btn btn-success">
                                                New Investment category
                                            </a>
                                        </p>
                                    </div>
                                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Category Name</th>
                                            <th>Category Code</th>
                                            <th>Description</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i=1;?>
                                        @if(isset($cats) && $i<=count($cats))
                                            @foreach($cats as $invest)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$invest->name}}</td>
                                                    <td>{{$invest->code}}</td>
                                                    <td>{{$invest->description}}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                Action <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <li><a href="{{url('investmentscats/update/category/'.$invest->id)}}">Edit</a> </li>
                                                                <li><a href="{{url('investmentscats/delete/category/'.$invest->id)}}" onclick="return (confirm('Are you sure you want to delete this sacco investment?'))">
                                                                    Delete
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php $i++;?>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="classes" role="tabpanel">
                                    <div class="panel-heading">
                                        <p>
                                            <a href="{{ url('investmentscats/create/class') }}" class="btn btn-success">
                                                New Investment Class
                                            </a>
                                        </p>
                                    </div>
                                    <div class="dt-responsive table-responsive">
                                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Class Name</th>
                                                <th>Class Category</th>
                                                <th>Class Code</th>
                                                <th>Description</th>
                                                <th></th>
                                            </tr>

                                            </thead>
                                            <tbody>
                                            <?php $i=1;?>
                                            @if(isset($classes) && $i<=count($classes))
                                                @foreach($classes as $class)
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$class->name}}</td>
                                                        <td>{{$class->category}}</td>
                                                        <td>{{$class->code}}</td>
                                                        <td>{{$class->description}}</td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                    Action <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="{{url('investmentscats/update/class/'.$class->id)}}">Edit</a> </li>
                                                                    <li><a href="{{url('investmentscats/delete/class/'.$class->id)}}" onclick="return (confirm('Are you sure you want to delete this investment category?'))">
                                                                        Delete
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $i++;?>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="types" role="tabpanel">
                                    <div class="panel-heading">
                                        <p>
                                            <a href="{{ url('investmentscats/create/type') }}" class="btn btn-success">
                                                New Investment Type
                                            </a>
                                        </p>
                                    </div>
                                    <div class="dt-responsive table-responsive">
                                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Type Name</th>
                                                <th>Type Category</th>
                                                <th>Type Class</th>
                                                <th>Type Code</th>
                                                <th>Description</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $i=1;?>
                                            @if(isset($types) && $i<=count($types))
                                                @foreach($types as $type)
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$type->name}}</td>
                                                        <td>{{$type->category}}</td>
                                                        <td>{{$type->class}}</td>
                                                        <td>{{$type->code}}</td>
                                                        <td>{{$type->description}}</td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                    Action <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="{{url('investmentscats/update/type/'.$type->id)}}">Edit</a> </li>
                                                                    <li><a href="{{url('investmentscats/delete/type/'.$type->id)}}" onclick="return (confirm('Are you sure you want to delete this investment category?'))">
                                                                            Delete
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $i++;?>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- [ page content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
