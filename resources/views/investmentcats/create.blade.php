@extends('layouts.css')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->

                    <div class="card">
                        @if(Session::has('wrath'))
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{ Session::get('wrath')}}</strong>
                            </div>
                        @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        <div class="card-header">
                            <h3>New Investment {{$what}}</h3>

                        </div>
                        <div class="card-block">

                            @if($what==='category')
                                <form method="POST" action="{{ url('investmentscats') }}" >@csrf
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="username">Category Name</label><br>
                                            <input class="form-control" placeholder="" type="text" name="name"
                                                   value="{{{ old('name') }}}" required>
                                            <input type='hidden' name='what' value='category'>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Category Code</label><br>
                                            <input class="form-control" placeholder="" type="text" name="code"
                                                   value="{{{ old('code') }}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Category Description</label><br>
                                            <textarea name="desc" class="form-control"> </textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="form-actions form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm pull-centre">
                                                        Create Investment Category
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            @elseif($what==='class')
                                <form method="POST" action="{{ url('investmentscats') }}">@csrf
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="username">Class Name</label><br>
                                            <input class="form-control" placeholder="" type="text" name="name"
                                                   value="{{{ old('name') }}}" required>
                                            <input type='hidden' name='what' value='class'>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Class Category</label><br>
                                            <select name="ccategory" class='form-control' required >
                                                <option></option>
                                                @foreach($categories as $cat)
                                                    <option value='{{$cat->id}}'>{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Class Code</label><br>
                                            <input class="form-control" placeholder="" type="text" name="code"
                                                   value="{{{ old('code') }}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Class Description</label><br>
                                            <textarea name="desc" class="form-control"> </textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="form-actions form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm pull-centre">
                                                        Create Category class
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            @elseif($what==='type')
                                <form method="POST" action="{{ url('investmentscats') }}" >@csrf
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="username">Type Name</label><br>
                                            <input class="form-control" placeholder="" type="text" name="name"
                                                   value="{{{ old('name') }}}" required>
                                            <input type='hidden' name='what' value='type'>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Type Category</label><br>
                                            <select name="tcategory" class="tcategory form-control" required>
                                                <option></option>
                                                @foreach($categories as $cat)
                                                    <option value='{{$cat->id}}'>{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Type Class</label><br>
                                            <select name="tclass" class="tclass form-control">
                                                <option></option>
                                                @foreach($classes as $class)
                                                    <option value='{{$class->id}}'>{{$class->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Type Code</label><br>
                                            <input class="form-control" placeholder="" type="text" name="code"
                                                   value="{{{ old('code') }}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Type Description</label><br>
                                            <textarea name="desc" class="form-control"></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="form-actions form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm pull-centre">
                                                        Create type
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            @endif
                        </div>
                        <!-- [ page content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>





<script type="text/javascript">
	$(document).ready(function(){
        $('.tcategory').change(function(){
            var lpvalue=$(this).val(); var hii=$(this);
            $.get("{{ url('ajaxinvestmentclasses')}}",
                {lpvalue:lpvalue},
                function(data){
                    $('.tclass').val(data);
                });
        });

    });
</script>
@stop
