<!DOCTYPE html>
<html class="no-js">

<!-- Mirrored from  by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 22 May 2016 23:38:24 GMT -->
@include('includes.sitehead')

<body class="pattern-4">

<!-- Preloader -->
<div id="preloader">
    <div id="status"></div>
</div>

<!--  START STYLE SWITCHER (preview only)  -->



<!--  END STYLE SWITCHER (preview only)  -->


<div class="wrapper">

    <div class="row">

        <div class="col-lg-12">
            <?php
            $organization = App\models\Organization::find(Auth::user()->organization_id);
            $pdate = (strtotime($organization->payroll_support_period)-strtotime(date("Y-m-d"))) / 86400;
            $pfinancial = (strtotime($organization->erp_support_period)-strtotime(date("Y-m-d"))) / 86400;
            $pcbs = (strtotime($organization->cbs_support_period)-strtotime(date("Y-m-d"))) / 86400;

            if(($pdate<0 && $organization->payroll_license_key ==1) && ($pfinancial<0 && $organization->erp_license_key ==1) && ($pcbs<0 && $organization->cbs_license_key ==1)){?>
            <h4 style="color:red">
                Your annual support licenses for all xara financial products have expired!!!....
                Please upgrade your licenses by clicking on the link below.
            </h4>
            <a href="{{ url('activatedproducts') }}">Upgrade license</a>
            <hr>
            <?php }?>

        </div>
    </div>



@include('includes.hrheader')
@include('includes.navpayroll')

<!--Revolution Slider begin-->



    <!--end Revolution Slider-->

    @yield('content')

    <footer id="footer" class="content">


        <div class="footer-bottom container-center cf">

            <div class="bottom-left">
                <p class="copyright">Copyright &copy; {{date('Y')}} Lixnet Technologies. All rights reserved</p>
            </div><!-- end bottom-left -->

            <div class="bottom-right">
                <nav>
                    <ul id="footer-nav">

                        <li><a href="#">About</a></li>

                        <li><a href="#">Support</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </nav>
            </div><!-- end bottom-right -->

        </div><!-- end footer-bottom -->
    </footer>

</div><!-- end wrapper -->

<div class="scroll-top"><a href="#"><i class="fa fa-angle-up"></i></a></div>

<!--*************************
*							*
*      JAVASCRIPT FILES	 	*
*							*
************************* -->

<!--imports jquery-->
<script type="text/javascript"  src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
<script type="text/javascript"  src="{{asset('js/jquery-migrate-1.1.0.min.js')}}"></script>
<!-- used for the contact form -->
<script type="text/javascript"  src="{{asset('js/jquery.form.js')}}"></script>
<!-- used for the the fun facts counter -->
<script type="text/javascript"  src="{{asset('js/jquery.countTo.js')}}"></script>
<!-- for displaying flickr images -->
<script type="text/javascript"  src="{{asset('js/jflickrfeed.min.js')}}"></script>
<!-- used to trigger the animations on elements -->
<script type="text/javascript"  src="{{asset('js/waypoints.min.js')}}"></script>
<!-- used to stick the navigation menu to top of the screen on smaller displays -->
<script type="text/javascript"  src="{{asset('js/waypoints-sticky.min.js')}}"></script>
<!-- used for rotating through tweets -->
<script type="text/javascript"  src="{{asset('js/vTicker.js')}}"></script>
<!-- imports jQuery UI, used for toggles, accordions, tabs and tooltips -->
<script type="text/javascript"  src="{{asset('js/jquery-ui-1.10.3.custom.min.js')}}"></script>
<!-- used for smooth scrolling on local links -->
<script type="text/javascript"  src="{{asset('js/jquery.scrollTo-1.4.3.1-min.js')}}"></script>
<!-- used for opening images in a Lightbox gallery -->
<script type="text/javascript"  src="{{asset('js/nivo-lightbox.min.js')}}"></script>
<!-- used for displaying tweets -->
<script type="text/javascript"  src="{{asset('js/jquery.tweet.js')}}"></script>
<!-- flexslider plugin, used for image galleries (blog post preview, portfolio single page) and carousels -->
<script type="text/javascript"  src="{{asset('js/jquery.flexslider-min.js')}}"></script>
<!-- used for sorting portfolio items -->
<script type="text/javascript"  src="{{asset('js/jquery.isotope.js')}}"></script>
<!-- for dropdown menus -->
<script type="text/javascript"  src="{{asset('js/superfish.js')}}"></script>
<!-- for detecting Retina displays and loading images accordingly -->
<script type="text/javascript"  src="{{asset('js/retina-1.1.0.min.js')}}"></script>
<script type="text/javascript"  src="{{asset('js/jquery.sharrre.min.js')}}"></script>
<!-- easing plugin for easing animation effects -->
<script type="text/javascript"  src="{{asset('js/jquery-easing-1.3.js')}}"></script>
<!-- Slider Revolution plugin -->
<script type="text/javascript"  src="{{asset('plugins/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
<script type="text/javascript"  src="{{asset('plugins/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
<!--imports custom javascript code-->
<script type="text/javascript"  src="{{asset('js/custom.js')}}"></script>
<script type="text/javascript"  src="{{asset('js/options.js')}}"></script>


</body>
</html>
