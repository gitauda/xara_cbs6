@include('includes.head')
<body>
<div class="loader-bg">
    <div class="loader-bar"></div>
</div>
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        @include('includes.top_nav')
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                @include('includes.nav_rep')
                <div class="pcoded-content">
                    @yield('xara_cbs')
                </div>
                <div id="styleSelector">
                </div>
            </div>
        </div>
    </div>
@include('includes.foot')
</body>

{{--<div class="main_wrapper">--}}
{{--@include('includes.head')--}}
{{--@include('includes.nav')--}}
{{--@include('includes.nav_rep')--}}

{{--<div id="page-wrapper">--}}
{{--            <div class="row" style="margin-top: -8%;">--}}
{{--                <div class="col-lg-12">--}}
{{--                    @yield('content')--}}
{{--                </div>--}}
{{--                <!-- /.col-lg-12 -->--}}
{{--            </div>--}}
{{--            <!-- /.row -->--}}
{{--        </div>--}}
{{--        <!-- /#page-wrapper -->--}}
{{--@include('includes.footer')--}}
{{--</div>--}}
