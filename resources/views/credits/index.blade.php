@extends('layouts.system')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Credit Committee Officers</h3>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>Officer</th>
                                    <th>status</th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    @foreach($credits as $user)
                                        <tr>

                                            <td>{{ $user->username }}</td>


                                            @if($user->is_active)
                                                <td> activated </td>
                                            @else
                                                <td> deactivated </td>
                                            @endif
                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        @if($user->is_active)
                                                            <li><a href="{{url('credits/deactivate/'.$user->id)}}">Deactivate</a></li>
                                                        @else
                                                            <li><a href="{{url('credits/activate/'.$user->id)}}">Activate</a></li>
                                                        @endif

                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
