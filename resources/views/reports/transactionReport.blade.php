
<?php


function asMoney($value) {
    return number_format($value, 2);
}

?>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><style type="text/css">

        table {
            max-width: 100%;
            background-color: transparent;
        }
        th {
            text-align: left;
        }
        .table {
            width: 100%;
            margin-bottom: 2px;
        }
        hr {
            margin-top: 1px;
            margin-bottom: 2px;
            border: 0;
            border-top: 2px dotted #eee;
        }

        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 12px;
            line-height: 1.428571429;
            color: #333;
            background-color: #fff;


        @page { margin: 50px 30px; }
        .header { position: top; left: 0px; top: -150px; right: 0px; height: 100px;  text-align: center; }
        .content {margin-top: -100px; margin-bottom: -150px}
        .footer { position: fixed; left: 0px; bottom: -60px; right: 0px; height: 50px;  }
        .footer .page:after { content: counter(page, upper-roman); }
    </style></head><body>

<div class="header">
    <table >

        <tr>



            <td>
                <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="{{ $organization->logo }}"
                     style="height: 50px;"/>
            </td>

            <td>
                <div align="center"> <strong>
                        {{ strtoupper($organization->name)}}
                    </strong><br><p> </div>
                <div align="center"> {{ $organization->phone}}<br><p> </div>
                <div align="center"> {{ $organization->email}}<br><p> </div>
                <div align="center"> {{ $organization->website}}<br><p> </div>
                <div align="center"> {{ $organization->address}} </div>


            </td>


        </tr>


        <tr>

            <hr>
        </tr>



    </table>
</div>



<div class="footer">
    <p class="page">Page <?php $PAGE_NUM ?></p>
</div>


<div class="content" style='margin-top:0px;'>
<!-- <div align="center"><strong>Expenditure Report as at {{date('d-M-Y')}}</strong></div><br> -->
    <div align="center"><strong>Transactions Report as from:  {{$from}} To:  {{$to}}</strong></div><br>



    <table class="table table-bordered" border='1' cellspacing='0' cellpadding='0'>


        <caption>Deposit</caption>
        <tr>

            <th width='20'><strong># </strong></th>
            <th><strong>Date</strong></th>
            <th><strong>Description </strong></th>
            <th><strong>Amount</strong></th>

            <!-- <th><strong>account </strong></th> -->

        </tr>
        <?php $i =1; $total=0;?>

        @foreach($transaction as $transact)
            <tr>


                <td td width='20' valign="top">{{$i}}</td>
                <td> {{ $transact->transaction_date }}</td>
                <td> {{ $transact->description }}</td>
                <td> {{ asMoney($transact->transaction_amount) }}</td>


            </tr>
            <?php $i++; $total=$total + $transact['transaction_amount'];?>
        @endforeach


        <tr>
            <td >  </td>
            <td >  </td>
            <td >  </td>
            <td>   </td>
        <tr>
            <td colspan="2"></td>


            <td><b>Total transact: </td><td></b><b> {{asMoney($total)}}</b></td></tr>

    </table>

    <table class="table table-bordered" border='1' cellspacing='0' cellpadding='0'>


        <caption>Withdraw</caption>
        <tr>

            <th width='20'><strong># </strong></th>
            <th><strong>Date</strong></th>
            <th><strong>Description </strong></th>
            <th><strong>Amount</strong></th>

            <!-- <th><strong>account </strong></th> -->

        </tr>
        <?php $i =1; $total=0;?>

        @foreach($transaction1 as $transact1)
            <tr>


                <td td width='20' valign="top">{{$i}}</td>
                <td> {{ $transact1->transaction_date }}</td>
                <td> {{ $transact1->description }}</td>
                <td> {{ asMoney($transact1->transaction_amount) }}</td>


            </tr>
            <?php $i++; $total=$total + $transact1['transaction_amount'];?>


        @endforeach
        <tr>
            <td >  </td>
            <td >  </td>
            <td >  </td>
            <td>   </td>

        </tr>
        <tr>
            <td colspan="2"></td>


            <td><b>Total transact: </td><td></b><b> {{asMoney($total)}}</b></td></tr>

    </table>



    <br><br>


</div></body></html>
