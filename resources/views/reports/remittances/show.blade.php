@extends('layouts.remittance')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3>Monthly Remittance Management</h3>
                        </div>
                        <div class="card-block">

                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <tr>
                                        <td>Amount </td><td>{{$remittance->monthly_remittance_amount}}</td>
                                    </tr>
                                    <tr>
                                        <td><a href="{{url('monthlyremittances/edit/'.$remittance->id)}}">Update</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
