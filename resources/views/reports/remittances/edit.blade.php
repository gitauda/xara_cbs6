@extends('layouts.remittance')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3>Monthly Remittance Management</h3>

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{ url('monthlyremittances/update/'.$remittance->id) }}">@csrf

                                <fieldset>
                                    <div class="form-group">
                                        <label for="username">Amount</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount" value="{{ $remittance->amount }}" required>
                                    </div>


                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">update</button>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
