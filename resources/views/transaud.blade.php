@extends('layouts.main')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">

                        </div>
                        <div class="card-block">

                            <form method="post" action="{{URL::to('transaudits')}}">@csrf
                                <div class="col-md-4">
                                    <input type="date" class="form-control datepicker" placeholder="Transaction date" name="date" id="date" readonly>
                                </div>


                                <div class="col-md-4">

                                    <select name="type" class="form-control" required>

                                        <option value="loan">Loan Transactions</option>
                                        <option value="savings">Savings Transactions</option>
                                    </select>



                                </div>

                                <div class="col-md-4">
                                    <button class="btn btn-primary">View Transactions</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
