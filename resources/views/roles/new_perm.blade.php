@extends('layouts.system')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3>New Permission</h3>
                            @if (Session::get('error'))
                                <div class="alert alert-error alert-danger">
                                    @if (is_array(Session::get('error')))
                                        {{ head(Session::get('error')) }}
                                    @endif
                                </div>
                            @endif

                            @if (Session::get('notice'))
                                <div class="alert">{{ Session::get('notice') }}</div>
                            @endif

                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ url('permissions') }}}" >@csrf


                                <fieldset>
                                    <div class="form-group">
                                        <label for="name">Permission</label>
                                        <input class="form-control" placeholder="new  Permission" type="text" name="permission" id="permission" value="{{{ old('name') }}}">
                                    </div>


                                            <ul class="scroll-list cards">
                                                @foreach($roles as $role)
                                                <li>

                                                    <input type="checkbox" name="roles[]" value="{{ $role->id }}" > {{$role->name}}

                                                </li>
                                                @endforeach
                                            </ul>



                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Create</button>
                                    </div>


                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@stop
