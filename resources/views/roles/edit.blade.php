@extends('layouts.system')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3>Update Role</h3>
                            @if (Session::get('error'))
                                <div class="alert alert-error alert-danger">
                                    @if (is_array(Session::get('error')))
                                        {{ head(Session::get('error')) }}
                                    @endif
                                </div>
                            @endif

                            @if (Session::get('notice'))
                                <div class="alert">{{ Session::get('notice') }}</div>
                            @endif

                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{ url('roles/update',$role->id) }}}" accept-charset="UTF-8">@csrf


                                <fieldset>
                                    <div class="form-group">
                                        <label for="role">Role Name</label>
                                        <input class="form-control" placeholder="role name" type="text" name="name" id="role" value="{{$role->name}}" required autofocus>
                                    </div>



                                    <div class="accordion-block">
                                        <br>


                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                            <?php $i = 1; ?>

                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="{{$i}}">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                {{$role->name}} Role
                                                            </a>
                                                        </h4>
                                                    </div>



                                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body mb-5">

                                                            <ul>
                                                                @foreach($permissions as $perm)
                                                                    <li>
                                                                        <input type="checkbox" name="permission[]" value="{{ $perm['id'] }}" @if(in_array($role->role_id,$perm)) checked @endif> {{$perm['name']}}

                                                                    </li>
                                                                @endforeach
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                        </div>

                                        <div class="form-actions form-group">

                                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                        </div>
                                    </div>


                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
