@extends('layouts.system')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3>New Role</h3>
                            @if (Session::get('error'))
                                <div class="alert alert-error alert-danger">
                                    @if (is_array(Session::get('error')))
                                        {{ head(Session::get('error')) }}
                                    @endif
                                </div>
                            @endif

                            @if (Session::get('notice'))
                                <div class="alert">{{ Session::get('notice') }}</div>
                            @endif

                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{ url('roles') }}}" accept-charset="UTF-8">@csrf


                                <fieldset>
                                    <div class="form-group">
                                        <label for="name">Role Name</label>
                                        <input class="form-control" placeholder="role name" type="text" name="role" id="role" value="{{{ old('name') }}}">
                                    </div>


                                    <div class="col-lg-7">
                                        <br>
                                        <ul class="scroll-list cards">
                                            @foreach($permissions as $perm)
                                                <li>

                                                    <input type="checkbox" name="permission[]" value="{{ $perm['id'] }}" > {{$perm['name']}}

                                                </li>
                                            @endforeach
                                        </ul>

                                        <div class="form-actions form-group">

                                            <button type="submit" class="btn btn-primary btn-sm">Create</button>
                                        </div>
                                    </div>


                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
