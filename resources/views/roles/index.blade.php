@extends('layouts.system')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Roles</h3>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ url('roles/create')}}">New Role</a>
                                <a class="dt-button btn-sm" href="{{ url('permissions/create')}}">New Permissions</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>

                                    <tr>
                                        <th>Role</th>
                                        <th></th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    @foreach($roles as $role)
                                        <tr>

                                            <td>{{ $role->name }}</td>

                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{url('roles/show/'.$role->id)}}">Permissions</a></li>
                                                        <li><a href="{{url('roles/edit/'.$role->id)}}">Edit</a></li>


                                                        <li><a href="{{url('roles/destroy/'.$role->id)}}">Delete</a></li>
                                                    </ul>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
