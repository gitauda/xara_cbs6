@extends('layouts.system')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">

                            <h3>{{$role->name}} Roles And Permissions</h3>

                            @if (Session::has('flash_message'))

                                <div class="alert alert-success">
                                    {{ Session::get('flash_message') }}
                                </div>
                            @endif

                            @if (Session::has('delete_message'))

                                <div class="alert alert-danger">
                                    {{ Session::get('delete_message') }}
                                </div>
                            @endif

                        </div>
                        <div class="card-block">
                            <div class="col-md-12 col-lg-6">

                                <ul class="scroll-list cards">
                                    @foreach($permissions as $perm)
                                        <li>

                                            <input type="checkbox" name="permission[]" value="{{ $perm['id'] }}"  @if(in_array($role['role_id'],$perm)) checked @endif> {{$perm['name']}}

                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
