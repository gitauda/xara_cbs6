<?php
	function asMoney($value) {
	  return number_format($value, 2);
	}
?>

@extends('layouts.accounting')
@section('xara_cbs')

<style>
	.top-header{
		background: #E1F5FE !important;
		color: #777;
		vertical-align: middle !important;
		padding: 10px 5px !important;
	}

	input[type='text']{
		width: 210px !important;
	}

	table{
		table-layout: fixed;
	}

	td.total{
		border-bottom: 3px double #777 !important;
		//text-decoration: underline;
		font-weight: bold !important;
	}

</style>

<div class="row">
	<div class="col-lg-12">
		<h4>
			New Transaction
		</h4>

		@if(isset($notice))
		<div class="alert alert-info">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			{{$notice}}
		</div>
		@endif
		@if(isset($warn))
		<div class="alert alert-warning">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
			{{$warn}}
		</div>
		@endif
		<hr>
	</div>
</div>

<!-- INPUT FORM -->
<div class="row">
	<div class="col-lg-12">
		<div class="top-header">
			<form class="form-inline" action="{{ URL::to('newpettycash') }}" method="GET" data-parsley-validate>
			<h5>Add Receipt Items</h5>

				<!--<div class="form-group">
					<label>Item <span style="color:red">*</span>:</label><br>
					<input type="text" minlength="2" data-parsley-trigger="change focusout"class="form-control input-sm" name="item" placeholder="Item Name" required>
				</div>&emsp;&nbsp;-->

				<div class="form-group">
						<label for="particulars">Particulars/Item</label><br>
						<select class="form-control selectable" name="item" id="particulars" required>
								@foreach($particulars as $particular)
										<option value="{{ $particular->id }}">{{ $particular->name }}</option>
								@endforeach
						</select>
				</div>&emsp;&nbsp;

				<div class="form-group">
					<label>Description:</label><br>
					<input type="text" class="form-control input-sm" name="desc" placeholder="Trans Description" value="">
				</div>&emsp;&nbsp;

				<input type="hidden"  class="form-control input-sm" name="qty"  value="1">


				<div class="form-group">
					<label>Date:</label><br>
					<input type="text" class="form-control input-sm datepicker" name="date"  value="{{date('Y-m-d')}}" readonly required>
				</div>&emsp;&nbsp;

				<div class="form-group">
					<label>Amount <span style="color:red">*</span>:</label><br>
					<input data-parsley-trigger="change focusout" data-parsley-type="number" class="form-control input-sm" name="unit_price" placeholder="Amount" required>
				</div>&emsp;&nbsp;

				<div class="form-group">
					<label>Receipt <span style="color:red">*</span>:</label><br>
					<input data-parsley-trigger="change focusout" type="text" class="form-control input-sm" name="receipt" placeholder="Receipt no if it exists">
				</div>&emsp;&nbsp;

				<div class="form-group">
					<!-- <label>Submit</label><br> --><br>
					<input type="submit" class="btn btn-success btn-sm" name="itemSubmit" value="Add Item">
				</div>
			</form>
		</div>
		<hr>
	</div>

	<div class="col-lg-12">
		<div>
			<form action="{{URL::to('petty_cash/commitTransaction')}}" method="POST">
				<h4><font color="#40AEED">Receipt Items</font></h4>
				<table class="table table-condensed table-bordered table-responsive table-hover">
					<thead>
						<th>#</th>
						<th>Particular/Item</th>
						<th>Description</th>
						<th>Date</th>
						<th>Receipt</th>
						<th>Total Amount</th>
						<th>Action</th>
					</thead>
					<tbody>
						@if($trItems != null)
						<?php $count = 0; $itemTotal = 0; $grandTotal = 0; ?>
						@foreach($trItems as $trItem)
							<?php
								$itemTotal = $trItem['quantity'] * $trItem['unit_price'];
								$grandTotal += $itemTotal;
							?>
							<tr>
								<td>{{ $count+1 }}</td>
								<td>{{ $trItem['item_name'] }}</td>
								<td>{{ $trItem['description'] }}</td>
								<td>{{ $trItem['date'] }}</td>
								<td>{{ $trItem['receipt'] }}</td>
								<td>{{ $itemTotal }}</td>
								<td>
				               <div class="btn-group">
				                  <a href="{{URL::to('petty_cash/newTransaction/remove/'.$count)}}" class="btn btn-danger btn-circle btn-sm"><i class="fa fa-times"></i></a>
				               </div>
								</td>
							</tr>
							<?php $count++; ?>
						@endforeach
						<tr>
							<td colspan="3"></td>
							<td class="total">Grand Total</td>
							<td class="total">{{ $grandTotal }}</td>
							<td></td>
						</tr>
						@endif
					</tbody>
				</table>
				<hr>
				<div class="form-group">
					<a href="{{ URL::to('petty_cash') }}" class="btn btn-danger btn-sm">Cancel</a>
					<div class="form-group pull-right">
						<input type="submit" class="btn btn-primary btn-sm" name="btnProcess" value="Process">
					</div>
				</div>
				<hr>
			</form>
		</div>
	</div>
</div>
<script>
    window.ParsleyConfig = {
        errorsWrapper: '<div></div>',
        errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
        errorClass: 'has-error',
        successClass: 'has-success'
    };
</script>
@stop
