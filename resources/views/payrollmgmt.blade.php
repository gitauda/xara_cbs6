@extends('layouts.payroll')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h4>Employees</h4>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Personal File Number</th>
                                        <th>Employee Name</th>
                                        <th>Employee Branch</th>
                                        <th>Employee Department</th>
                                        <th>Action</th>
                                    </tr>


                                    </thead>
                                    <tbody>

                                    <?php $i = 1; ?>
                                    @foreach($employees as $employee)

                                        <tr>

                                            <td> {{ $i }}</td>
                                            <td>{{ $employee->personal_file_number }}</td>
                                            <td>{{ $employee->first_name.' '.$employee->last_name}}</td>
                                            @if( $employee->branch_id!=0)
                                                <td>{{ App\models\Branch::getName($employee->branch_id) }}</td>
                                            @else{
                                            <td></td>
                                            @endif
                                            @if( $employee->department_id!= 0)
                                                <td>{{ App\models\Department::getName($employee->department_id) }}</td>
                                            @else
                                                <td></td>
                                            @endif

                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">

                                                        <li><a href="{{url('employees/edit/'.$employee->id)}}">Update</a></li>

                                                        <li><a href="{{url('employees/delete/'.$employee->id)}}">Delete</a></li>

                                                    </ul>
                                                </div>

                                            </td>

                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
