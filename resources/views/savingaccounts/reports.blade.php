@extends('layouts.ports')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3> Saving Reports</h3>
                        </div>
                        <div class="card-block">

                            <ul>
                                <li>
                                    <a href="{{ url('reports/savinglisting') }}" target="_blank"> Savings Listing report</a>
                                </li>
                                @foreach($savingproducts as $savingproduct)
                                <li>
                                    <a href="{{ url('reports/savingproduct/'.$savingproduct->id)}}" target="_blank">{{ $savingproduct->name}} report</a>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
