@extends('layouts.savings')
@section('xara_cbs')
    <script src="{{asset('media/jquery-1.8.0.min.js')}}" ></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#acct').hide();
            $('#manual').click(function(){
                $('#acct').toggle();

            });

        });
    </script>
    <script>
        $(document).ready(function() {
            $('#savingproduct_id').change(function(){
                $.get("{{ url('api/getaccountno')}}",
                    { product: $(this).val(),
                        member: $('#member').val(),

                    },
                    function(data) {

                        $("#previews").html(data);

                    });
            });
        });
    </script>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Saving Account</h3>


                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{ url('savingaccounts') }}" accept-charset="UTF-8">@csrf

                                <fieldset>


                                    <input type="hidden" name="member_id" id="member" value="{{ $member->id }}" />


                                    <div class="form-group">
                                        <label for="username">Saving Product</label>
                                        <select class="form-control" name="savingproduct_id" id="savingproduct_id" required>

                                            <option></option>
                                            @foreach($savingproducts as $product)

                                                <option value="{{ $product->id }}">{{ $product->name }}</option>

                                            @endforeach


                                        </select>
                                    </div>
                                    <div class="form-group" id="previews">
                                        <label for="username">Account Number Preview Field</label>
                                        <!--<input type="text" placeholder="account no." name="preview" id="preview" value="" class="form-control">-->


                                    </div>

                                    <div>
                                        <p >Manually Add Account Number? click<a href="#" id="manual"> Here.</a></p>

                                    </div>
                                    <div class="form-group" id="acct">
                                        <label for="username">Account Number</label>
                                        <input class="form-control" placeholder="account no." type="text" name="account_no" id="account_no" value="{{{ old('account_no') }}}">
                                    </div>

                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Create Account</button>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
