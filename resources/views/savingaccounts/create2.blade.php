@extends('layouts.savings')

<script src="{{asset('media/jquery-1.8.0.min.js')}}"/>
@section('content')
    <br/>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#acct').hide();
            $('#manual').click(function(){
                $('#acct').toggle();

            });

        });
    </script>
    <script>
        $(document).ready(function() {
            $('#savingproduct_id').change(function(){
                $.get("{{ url('api/getaccountno')}}",
                    { product: $(this).val(),
                        member: $('#member').val(),

                    },
                    function(data) {

                        $("#previews").html(data);

                    });
            });
        });
    </script>

    <div class="row">
        <div class="col-lg-12">
            <h3>Saving Product</h3>

            <hr>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-5">



            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif

            <form method="POST" action="{{ URL::to('savingaccounts') }}" accept-charset="UTF-8">{{csrf_field()}}

                <fieldset>


                    <div class="form-group">
                        <label for="member">Member</label>
                        <select class="form-control" name="member_id" id="member" required>
                            <option></option>
                            @foreach($members as $member)
                                <option value="{{ $member->id }}">{{ $member->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="savingproduct_id">Saving Product</label>
                        <select class="form-control" name="savingproduct_id" id="savingproduct_id" required>
                            <option></option>
                            @foreach($savingproducts as $product)
                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" id="previews">
                        <label for="username">Account Number Preview Field</label>
                        <!--<input class="form-control" placeholder="account no." type="text" name="preview" id="preview" value="" readonly>-->

                    </div>

                    <div>
                        <p >Manually Add Account Number? click<a href="#" id="manual"> Here.</a></p>

                    </div>
                    <div class="form-group" id="acct">
                        <label for="username">Account Number</label>
                        <input class="form-control" placeholder="account no." type="text" name="account_no" id="account_no" value="{{{ Input::old('account_no') }}}">
                    </div>

                    <div class="form-actions form-group">

                        <button type="submit" class="btn btn-primary btn-sm">Create Product</button>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
@stop
