@extends('layouts.savings')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Saving Accounts</h3>
                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ url('savingaccounts/create2')}}">new Saving
                                    Account</a>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>#</th>
                                    <th>Member Name</th>
                                    <th>Savings Product</th>
                                    <th>Account Number</th>
                                    <th></th>

                                    </thead>
                                    <tbody>

                                    <?php $i = 1; ?>
                                    @foreach($savingaccounts as $saving)

                                        <tr>

                                            <td> {{ $i }}</td>
                                            <td>{{ $saving->member->name }}</td>
                                            <td>{{ $saving->savingproduct->name }}</td>
                                            <td>{{ $saving->account_number }}</td>

                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                            data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">
                                                        <?php #<li><a href="{{URL::to('savingaccounts/edit/'.$saving->id)}}">Update</a></li>  ?>

                                                        <li><a href="{{url('savingaccounts/destroy/'.$saving->id)}}">Delete</a>
                                                        </li>

                                                    </ul>
                                                </div>

                                            </td>
                                        </tr>

                                        <?php $i++; ?>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
