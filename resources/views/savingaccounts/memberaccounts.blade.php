@extends('layouts.main')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>{{$member->name}}</h3>
                            <p>Saving Accounts</p>

                            <div class="card-header-right">
                                @if(Auth::user()->user_type != 'teller')
                                    <a class="dt-button btn-sm" href="{{ url('savingaccounts/create/'.$member->id)}}">new Saving Account</a>
                                @endif
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>

                                    <th>#</th>
                                    <th>Member Nam</th>
                                    <th>Savings Product</th>
                                    <th>Account Number</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    </thead>
                                    <tbody>

                                    <?php $i = 1; ?>
                                    @foreach($member->savingaccounts as $saving)
                                        <?php $accountBal=App\models\Savingaccount::getAccountBalance($saving); ?>
                                        <tr>
                                            <td> {{ $i }}</td>
                                            <td>{{ $saving->member->name }}</td>
                                            <td>{{ $saving->savingproduct->name }}</td>
                                            <td>{{ $saving->account_number }}</td>
                                            <td> <a href="{{ url('savingtransactions/show/'.$saving->id)}}" class="btn btn-primary btn-sm">Transactions </a></td>
                                            <td> <a href="{{ url('savingtransactions/create/'.$saving->id)}}" class="btn btn-primary btn-sm">Transact </a></td>
                                            <?php if($accountBal<=0){?>
                                            <td> <a href="{{ url('savingaccounts/destroy/'.$saving->id)}}" class="btn btn-primary btn-sm">Delete</a></td>
                                            <?php } ?>
                                        </tr>

                                        <?php $i++; ?>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
