@extends('layouts.main_hr')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    @include('partials.system_cards')
                    <div class="card">
                        @if (Session::has('flash_message'))

                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}
                            </div>
                        @endif

                        @if (Session::has('delete_message'))

                            <div class="alert alert-danger">
                                {{ Session::get('delete_message') }}
                            </div>
                        @endif
                        <div class="card-header">
                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ url('employees/create')}}">New Employee</a>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="font-size:11px;">PFN</th>
                                        <th style="font-size:11px;">Employee Name</th>
                                        <th style="font-size:11px;">ID</th>
                                        <th style="font-size:11px;">KRA PIN</th>
                                        <th style="font-size:11px;">NSSF NO.</th>
                                        <th style="font-size:11px;">NHIF NO.</th>
                                        <th style="font-size:11px;">Branch</th>
                                        <th style="font-size:11px;">Department</th>

                                        <th style="font-size:11px;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php $i = 1; ?>
                                    @foreach($employees as $employee)

                                        <tr>

                                            <td> {{ $i }}</td>
                                            <td>{{ $employee->personal_file_number }}</td>
                                            @if($employee->middle_name == null || $employee->middle_name == '')
                                                <td>{{ $employee->first_name.' '.$employee->last_name}}</td>
                                            @else
                                                <td>{{ $employee->first_name.' '.$employee->middle_name.' '.$employee->last_name}}</td>
                                            @endif
                                            <td>{{ $employee->identity_number }}</td>
                                            <td>{{ $employee->pin }}</td>
                                            <td>{{ $employee->social_security_number }}</td>
                                            <td>{{ $employee->hospital_insurance_number }}</td>
                                            <?php if( $employee->branch_id!=0){ ?>
                                            <td>{{ App\models\Branch::getName($employee->branch_id) }}</td>
                                            <?php }else{?>
                                            <td></td>
                                            <?php } ?>
                                            <?php if( $employee->department_id!= 0){ ?>
                                            <td>{{ App\models\Department::getName($employee->department_id).' ('.App\models\Department::getCode($employee->department_id).')' }}</td>
                                            <?php }else{?>
                                            <td></td>
                                            <?php } ?>
                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">

                                                        <li><a href="{{url('employees/view/'.$employee->id)}}">View</a></li>

                                                        <li><a href="{{url('employees/edit/'.$employee->id)}}">Update</a></li>

                                                        <li><a href="{{url('employees/deactivate/'.$employee->id)}}" onclick="return (confirm('Are you sure you want to deactivate this employee?'))">Deactivate</a></li>

                                                    </ul>
                                                </div>

                                            </td>
                                        </tr>

                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
