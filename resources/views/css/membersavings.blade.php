@extends('layouts.membercss')
@section('content')
<br/>

<?php


function asMoney($value) {
  return number_format($value, 2);
}

?>

<div class="row">
	<div class="col-lg-12">
    <strong>Member: {{ $member->name }}</strong><br> 
    <strong>Member no.: {{ $member->membership_no }}</strong><br> 
    <strong>Savings Account no.: {{ $savingaccount->account_number }}</strong><br>
    <strong>Account Balance: {{ asMoney($balance) }}</strong><br>
    @if($balance > Savingtransaction::getWithdrawalCharge($savingaccount)) 
    <?php //<strong>Available Balance: {{ asMoney($balance - Savingtransaction::getWithdrawalCharge($savingaccount)) }}</strong><br>?>
    @endif 
    <strong>Interest:</strong> {{asMoney($interest)}}<br>
    @if($savingaccount->savingproduct_id == 4)
      <strong>Loan balance: {{asMoney($loanBalance)}}</strong><br>
      <strong>Guarantee amount: {{asMoney($guaranteed)}}</strong><br>
    @endif
    <strong>Available balance: {{asMoney($total_balance)}}</strong>
    <hr>
</div>	
</div>


<div class="row">
	<div class="col-lg-4">

		
		 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif

		 <form method="POST" action="{{{ URL::to('membersavingtransactions/'.$savingaccount->id) }}}" accept-charset="UTF-8">



   
    <fieldset>
        <div class="form-group">
            <label for="username">Transaction </label>
           <select name="type" class="form-control" required>
            <option></option>
            <option value="credit"> Deposit</option>
            <option value="debit"> Withdraw</option>
           </select>
        </div>
        
        
        
         <input type="hidden" name="account_id" value="{{ $savingaccount->id}}">
        

        <div class="form-group">
            <label for="username"> Date</label>
            <input class="form-control datepicker" readonly placeholder="" type="text" name="date" id="date" value="{{date('Y-m-d')}}" required>
        </div>


        <div class="form-group">
            <label for="username"> Amount</label>
            <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount" value="{{{ Input::old('amount') }}}" required>
        </div>


         <div class="form-group">
            <label for="username"> Description</label>
            <textarea class="form-control" name="description">{{{ Input::old('description') }}}</textarea>
            
        </div>

            <input class="form-control" placeholder="" type="hidden" name="transacted_by" id="transacted_by" value="{{$member->name}}" required>
            <input class="form-control" placeholder="" type="hidden" name="mid" id="mid" value="{{$member->id}}" required>
            <input class="form-control" placeholder="" type="hidden" name="phone" id="phone" value="{{$member->phone}}" required>


        <div class="form-actions form-group">
        
          <button type="submit" class="btn btn-primary btn-sm">Submit</button>
        </div>

    </fieldset>
</form>
		

  </div>

</div>
























@stop