@extends('layouts.membercss')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Saving Accounts</h3>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>

                                    <th>#</th>

                                    <th>Savings Product</th>
                                    <th>Account Number</th>

                                    <th></th>

                                    </thead>
                                    <tbody>

                                    <?php $i = 1; ?>
                                    @foreach($member->savingaccounts as $saving)

                                        <tr>

                                            <td> {{ $i }}</td>

                                            <td>{{ $saving->savingproduct->name }}</td>
                                            <td>{{ $saving->account_number }}</td>

                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{URL::to('memtransactions/'.$saving->id)}}">View</a></li>
                                                        {{--<li><a href="{{ URL::to('membersavingtransactions/create/'.$saving->id)}}" >Transact </a></li> --}}
                                                    </ul>
                                                </div>

                                            </td>



                                        </tr>

                                        <?php $i++; ?>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
