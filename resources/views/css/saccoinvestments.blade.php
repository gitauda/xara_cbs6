@extends('layouts.css')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->

                    <div class="card">
                        @if(isset($out))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ $out }}}</strong>
                            </div>
                        @endif
                        @if(isset($exit))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ $exit }}}</strong>
                            </div>
                        @endif
                        @if(isset($grab))
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{{ $grab }}}</strong>
                            </div>
                        @endif
                        <div class="card-header">
                            <h3>
                                Investments
                            </h3>
                            <div class="panel-heading">
                                <p>
                                    <a href="{{ url('saccoinvestments/create') }}" class="btn btn-success">
                                        New Sacco Investment
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="card-block">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Investment Name</th>
                                    <th>Vendor Name</th>
                                    <th>Investment Value</th>
                                    <th>Growth Type</th>
                                    <th>Growth Rate</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;
                                use App\models\Vendor;
                                ?>
                                @if(isset($investment) && $i <=count($investment))
                                    @foreach($investment as $invest)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$invest->name}}</td>
                                            <td>{{$vendor= App\models\Vendor::where('id','=',$invest->vendor_id)->pluck('name')}}</td>
                                            <td>{{$invest->valuation}}</td>
                                            <td>{{$invest->growth_type}}</td>
                                            <td>{{$invest->growth_rate}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{url('saccoinvestments/edit/'.$invest->id)}}">Edit</a> </li>
                                                        <li><a href="{{url('saccoinvestments/delete/'.$invest->id)}}" onclick="return (confirm('Are you sure you want to delete this sacco investment?'))"> Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++;?>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- [ page content ] end -->
            </div>
        </div>
    </div>


@stop
