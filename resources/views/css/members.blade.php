@extends('layouts.member')
@section('xara_cbs')
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <!-- [ page content ] start -->

                <div class="card">
                    @if(Session::get('notice'))
                        <div class="alert alert-success">{{ Session::get('notice') }}</div>
                    @endif
                    <div class="card-header">
                        <h5>Members</h5>

                     <div class="card-header-right">
                        <a class="dt-button btn-sm" href="{{ URL::to('members/create')}}">New Member</a>
                     </div>

                    </div>
                    <div class="card-block">


                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs  tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#active-members" role="tab">Active Members</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#inactive-members" role="tab">InActive Members</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tabs card-block">
                            <div class="tab-pane active" id="active-members" role="tabpanel">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member Number</th>
                                        <th>Member Name</th>
                                        <th></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($members as $member)
                                            @if($member->is_active)
                                                <tr>
                                                    <td> {{ $i }}</td>
                                                    <td>{{ $member->membership_no }}</td>
                                                    <td>{{ $member->name }}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button"
                                                                    class="btn btn-info btn-sm dropdown-toggle"
                                                                    data-toggle="dropdown" aria-expanded="false">
                                                                Action <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                @if($member->is_css_active == false)
                                                                    <li>
                                                                        <a href="{{url('portal/activate/'.$member->id)}}">Activate</a>
                                                                    </li>
                                                                @endif
                                                                @if($member->is_css_active == true)
                                                                    <li>
                                                                        <a href="{{url('portal/deactivate/'.$member->id)}}">Deactivate</a>
                                                                    </li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endif
                                        @endforeach
                                        </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="inactive-members" role="tabpanel">
                                <div class="dt-responsive table-responsive">
                                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Member Number</th>
                                            <th>Member Name</th>
                                            <th>Member Branch</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            @foreach($members as $member)
                                                @if(!$member->is_active)
                                                    <tr>
                                                        <td> {{ $i }}</td>
                                                        <td>{{ $member->membership_no }}</td>
                                                        <td>{{ $member->name }}</td>
                                                        <td>{{ $member->branch->name }}</td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button"
                                                                        class="btn btn-info btn-sm dropdown-toggle"
                                                                        data-toggle="dropdown" aria-expanded="false">
                                                                    Action <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    @if($member->is_css_active == false)
                                                                        <li>
                                                                            <a href="{{url('portal/activate/'.$member->id)}}">Activate</a>
                                                                        </li>
                                                                    @endif
                                                                    @if($member->is_css_active == true)
                                                                        <li>
                                                                            <a href="{{url('portal/deactivate/'.$member->id)}}">Deactivate</a>
                                                                        </li>
                                                                    @endif
                                                                    <li><a href="{{url('css/reset/'.$member->id)}}">Reset
                                                                        Password</a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                @endif
                                            @endforeach
                                            </tbody>
                                    </table>
                                </div>
                             </div>






                    </div>
                </div>
                <!-- [ page content ] end -->
            </div>
        </div>
    </div>
</div>
</div>
@endsection
