<!DOCTYPE html>
<html lang="en">
<head>
    <title>CBS | XARA CBS</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="XARA CBS| Core banking system"/>
    <meta name="keywords" content="" />
    <meta name="author" content="xara_cbs" />
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('assets/assets/images/favicon.ico')}}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"><link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- waves.css -->
    <link rel="stylesheet" href="{{ asset('assets/assets/pages/waves/css/waves.min.css')}}" type="text/css" media="all"><!-- feather icon --> <link rel="stylesheet" type="text/css" href="../files/assets/icon/feather/css/feather.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/assets/icon/themify-icons/themify-icons.css')}}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/assets/icon/icofont/css/icofont.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/assets/icon/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/assets/icon/feather/css/feather.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/assets/css/widget.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/assets/css/pages.css')}}">
</head>

<body themebg-pattern="theme1">
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="loader-track">
        <div class="preloader-wrapper">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="page-wrapper">
            <div class="">
                <div class="row">

                    <div class="text-center">
                        <h2 >Please select a subscription service</h2>
                    </div>
                    <div class="col-xl-4 col-md-6 text-center"></div>
                    
                    <!-- lettest acivity and statustic card start -->
                    @foreach($plans as $plan)
                    
                    <div class="col-xl-4 col-md-12">
                        <div class="card sale-card hover">
                            <div class="card-header text-center">
                                <h5>{{$plan->name}}</h5>
                                <p class="text-muted m-b-2">{{number_format($plan->cost, 2)}}</p>
                            </div>
                            <div class="card-block text-center">
                                <div class="align-middle m-b-25">
                                    <div class="d-inline-block m-r-15">
                                        <h6>Ad Free</h6>
                                        <span class="status active"></span>
                                    </div>
                                </div>
                                <div class="align-middle m-b-25">
                                    <div class="d-inline-block m-r-15">
                                        <h6>100 Daily Transactions</h6>
                                    </div>
                                </div>
                                <div class="align-middle m-b-25">
                                    <div class="d-inline-block m-r-15">
                                        <h6>5 Users</h6>
                                    </div>
                                </div>
                                <div class="align-middle m-b-25">
                                    <div class="d-inline-block m-r-15">
                                        <h6>100 Emails</h6>
                                    </div>
                                </div>
                                <div class="right-icon-control m-t-15">
                                    <a href="{{url('payment',$plan->slug)}}"  class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">UPGRADE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                   
                </div>

            </div>
        </div>

    </div>
</div>



<script type="text/javascript" src="{{asset('assets/bower_components/jquery/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bower_components/popper.js/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- waves js -->
<script src="{{asset('assets/assets/pages/waves/js/waves.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{asset('assets/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{asset('assets/bower_components/modernizr/js/modernizr.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bower_components/modernizr/js/css-scrollbars.js')}}"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="{{ asset('assets/bower_components/i18next/js/i18next.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>

<script src="{{ asset('assets/assets/js/pcoded.min.js')}}"></script>
<script src="{{ asset('assets/assets/js/vertical/vertical-layout.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/pages/widget/widget-data.js')}}"></script>
<!--Color Script Common-->
<script type="text/javascript" src="{{asset('assets/assets/js/common-pages.js')}}"></script>


</body>


<!-- Mirrored from demo.dashboardpack.com/admindek-html/default/auth-lock-screen.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 06 Nov 2020 19:58:46 GMT -->
</html>
