@extends('layouts.ports')
@section('xara_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3> {{ucfirst($type)}} Report Period</h3>
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}
                                    @endforeach
                                    <a href="#"  class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                </div>
                            @endif
                        </div>
                        <div class="card-block">

                            <form target="_blank" method="GET" action="{{url('reports/redirect')}}">@csrf

                                <div class="form-group">
                                    <label for="username">Period options</label>
                                    <select class="form-control" name="period" id="period" required>
                                        <option value="">select period</option>
                                        <option value="custom">Custom range</option>
                                        <option value="As at date">As at Date</option>
                                        <option value="year">Year</option>
                                        <option value="month">Month</option>
                                    </select>
                                </div>
                                <!--  <div class="form-group">
                                      <label for="username">Format</label>
                                      <select class="form-control" name="format" id="format">
                                          <option value="pdf">Pdf</option>
                                          <option value="excel">Excel</option>
                                      </select>
                                  </div>-->
                                <input type="hidden" name="loanproduct" value="{{$id}}">
                                <div class="form-group" id="year">

                                    <label for="username">Select Year <span style="color:red">*</span></label>
                                    <div class="right-inner-addon ">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        <input class="form-control datepicker42" readonly="readonly" placeholder="" type="text"
                                               name="year" id="dropper_default" value="{{date('Y')}}">
                                    </div>

                                </div>

                                <div id="custom">


                                    <div class="form-group">
                                        <label for="username">From <span style="color:red">*</span></label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input required class="form-control datepicker" readonly="readonly" placeholder="" type="text" name="from" id="from" value="{{{ old('from') }}}">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="username">To <span style="color:red">*</span></label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input required class="form-control datepicker" readonly="readonly" placeholder="" type="text" name="to" id="to" value="{{{ old('to') }}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group" id="select_date">
                                    <label for="username">Date <span style="color:red">*</span></label>
                                    <div class="right-inner-addon ">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        <input class="form-control datepicker" readonly="readonly" placeholder="" type="text"
                                               name="date" id="date" value="{{date('Y-m-d')}}">
                                    </div>
                                </div>
                                <div class="form-group" id="month">
                                    <label for="username">Select month <span style="color:red">*</span></label>
                                    <div class="right-inner-addon ">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        <input class="form-control datepicker2" readonly="readonly" placeholder="" type="text"
                                               name="month" id="date" value="{{date('m-Y')}}">
                                    </div>
                                </div>

                                <input type="hidden" name="loanproduct_id" value="{{$id}}">
                                <input type="hidden" name="type" value="{{$type}}">

                                @if($type == 'savings')
                                    <input type="hidden" name="savingaccount_id" value="{{$id}}">
                            @endif

                            <!--  <div class="form-group">
            <label for="username">As at Date </label>
            <input class="form-control" placeholder="" type="date" name="date" id="date" value="{{date('Y-m-d')}}">
        </div> -->
                                <div class="form-group">
                                    <label for="username">Format</label>
                                    <select class="form-control" name="format" id="format" required>
                                        <option value="">Select format</option>
                                        <option value="pdf">PDF</option>
                                        <option value="excel">Excel</option>
                                    </select>
                                </div>



                                <div class="form-actions form-group">


                                    <button type="submit" class="btn btn-primary btn-sm">Generate Report</button>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#year').hide();
            $('#select_date').hide();
            $('#month').hide();
            $('#custom').hide();


            $('#period').change(function () {
                if ($(this).val() == "As at date" || $(this).val() == "day") {
                    $('#year').hide();
                    $('#select_date').show();
                    $('#month').hide();
                    $('#custom').hide();
                }else if ($(this).val() == "year") {
                    $('#year').show();
                    $('#select_date').hide();
                    $('#month').hide();
                    $('#custom').hide();
                } else if ($(this).val() == "month") {
                    $('#year').hide();
                    $('#select_date').hide();
                    $('#month').show();
                    $('#custom').hide();

                } else if ($(this).val() == "custom") {
                    $('#year').hide();
                    $('#select_date').hide();
                    $('#month').hide();
                    $('#custom').show();

                }
                else {
                    $('#year').hide();
                    $('#select_date').hide();
                    $('#month').hide();
                    $('#custom').hide();
                }

            });
        });

    </script>

@stop
