<html><head>
    <style>
        @page {
            margin: 170px 30px;
        }

        .header {
            position: fixed;
            left: 0px;
            top: -150px;
            right: 0px;
            height: 150px;
            text-align: center;
        }

        .footer {
            position: fixed;
            left: 0px;
            bottom: -165px;
            right: 0px;
            height: 50px;
        }

        .footer .page:after {
            content: counter(page, upper-roman);
        }

        .content {
            margin-top: -70px;
        }

    </style>
<body style="font-size:10px">
<?php


function asMoney($value)
{
    return number_format($value, 2);
}

?>
<div class="header">
    <table>
        <tr>
            <td>
                <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="{{ $organization->logo }}"
                     style="height: 50px;"/>
            </td>
            <td>
                <strong>
                    {{ strtoupper($organization->name)}}<br>
                </strong>
                {{ $organization->phone}} <br>
                {{ $organization->email}}<br>
                {{ $organization->website}}<br>
                {{ $organization->address}}


            </td>

            <?php
            $range = '';
            if ($period == 'As at date') {
                $newDate = date("d-M-Y", strtotime($date));
                $range = 'As at ' . $newDate;
            } else if ($period == 'custom') {
                $newFrom = date("d-M-Y", strtotime($from));
                $newTo = date("d-M-Y", strtotime($to));
                $range = $newFrom . ' to ' . $newTo;
            }
            ?>
            <td>
                <strong><h3>EXPENSE REPORT <br> {{$range}} </h3></strong>

            </td>
        </tr>


        <tr>

            <hr>
        </tr>


    </table>
</div>
<div class="footer">
    <p class="page">Page <?php $PAGE_NUM ?></p>
    <p><strong>Generated at: {{date('D M j, Y')}} by {{Confide::user()->username}}.</strong></p>
</div>

<div class="content">

    <table class="table table-bordered" style="width:100%">
        <tr>
            <td style="border-bottom:1px solid black;"><strong>Date</strong></td>
            <td style="border-bottom:1px solid black;"><strong>Type</strong></td>
            <td style="border-bottom:1px solid black;"><strong>Description</strong></td>
            <td style="border-bottom:1px solid black;"><strong>Amount</strong></td>
        </tr>
        <?php $total_expenses = 0; ?>
        @foreach($expenses as  $expense)
            <tr>
                <td style="border-bottom:0.5px solid gray;">{{ $expense->date }}</td>
                <td style="border-bottom:0.5px solid gray;">{{$expense->particular->name}}</td>
                <td style="border-bottom:0.5px solid gray;">{{$expense->description}}</td>
                <td style="border-bottom:0.5px solid gray;">{{asMoney( $expense->amount) }}</td>
                <?php $total_expenses += $expense['amount'];?>
            </tr>
        @endforeach
        <tr>
            <td style="border-top:1px solid black; border-bottom:1px solid black;"><strong>TOTAL EXPENSES</strong></td>
            <td style="border-top:1px solid black; border-bottom:1px solid black;"></td>
            <td style="border-top:1px solid black; border-bottom:1px solid black;"></td>
            <td style="border-top:1px solid black; border-bottom:1px solid black;">
                <strong>{{asMoney($total_expenses)}}</strong>
            </td>
        </tr>
    </table>

    <h3>Summary</h3>
    <table style="width:100%">
        <tbody>
        @foreach($expe as $expense)
            <tr>
                <td style="border-bottom:0.5px solid gray;">{{$expense['expense']->particular->name}}</td>
                <td style="border-bottom:0.5px solid gray;"><strong>{{asMoney($expense['amount'])}}</strong></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body></html>