@extends('layouts.ports')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3> Financial Reports</h3>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="card-block">

                            <form target="_blank" method="post" action="{{url('reports/financials')}}">@csrf


                                <div class="form-group">
                                    <label for="username">Report</label>
                                    <select class="form-control" name="report_type" id="report_type">
                                        <option value="">select report</option>
                                        <option>--------------------------</option>
                                        <option value="balancesheet">Balance Sheet</option>
                                        <option value="income">Profit & Loss</option>
                                        <option value="trialbalance">Trial Balance</option>
                                        <option value="cashbook">Cash book</option>
                                        <option value="budget">Budget Report</option>
                                        <option value="income_reports">Income Report</option>
                                        <option value="expenses_reports">Expenses Report</option>
                                    </select>
                                </div>

                                <script type="text/javascript">
                                    var reportTypeSelect = document.getElementById("report_type");
                                    reportTypeSelect.onchange = function (event) {
                                        var selected = this.options[this.selectedIndex].value;
                                        var incomeParticularsGroup = document.getElementById("income-particulars-group");
                                        var expenseParticularsGroup = document.getElementById("expense-particulars-group");

                                        if (selected === 'income_reports') {
                                            incomeParticularsGroup.style.display = 'block';
                                            expenseParticularsGroup.style.display = 'none';
                                        } else if (selected === 'expenses_reports') {
                                            incomeParticularsGroup.style.display = 'none';
                                            expenseParticularsGroup.style.display = 'block';
                                        } else {
                                            incomeParticularsGroup.style.display = 'none';
                                            expenseParticularsGroup.style.display = 'none';
                                        }
                                    };
                                </script>

                                <div class="form-group" id="income-particulars-group" style="display: none;">
                                    <label for="income-particulars">Income Particulars</label>
                                    <select class="form-control" name="income-particulars" id="income-particulars">
                                        <option value="0">All</option>
                                        @foreach($incomeParticulars as $incomeParticular)
                                            <option value="{{ $incomeParticular->id }}">{{ $incomeParticular->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group" id="expense-particulars-group" style="display: none;">
                                    <label for="expense-particulars">Expense Particulars</label>
                                    <select class="form-control" name="expense-particulars" id="expense-particulars">
                                        <option value="0">All</option>
                                        @foreach($expenseParticulars as $expenseParticular)
                                            <option value="{{ $expenseParticular->id }}">{{ $expenseParticular->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="username">Period</label>
                                    <select class="form-control" name="period" id="period">
                                        <option value="">select period</option>
                                        <option>--------------------------</option>
                                        <option value="As at date">As at Date</option>
                                        <option value="custom">Custom</option>
                                    </select>
                                </div>
                                <div id="customperiod">

                                    <div class="form-group">
                                        <label for="username">From <span style="color:red">*</span></label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input class="form-control datepicker" readonly="readonly" placeholder="" type="text"
                                                   name="from" id="from" value="{{{ old('from') }}}">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="username">To <span style="color:red">*</span></label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input class="form-control datepicker" readonly="readonly" placeholder="" type="text"
                                                   name="to" id="to" value="{{{ old('to') }}}">
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group" id="date">
                                    <label for="username">Date <span style="color:red">*</span></label>
                                    <div class="right-inner-addon ">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        <input class="form-control datepicker" readonly="readonly" placeholder="" type="text"
                                               name="date" id="date" value="{{date('Y-m-d')}}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="username">Format</label>
                                    <select class="form-control" name="format" id="format" required>
                                        <option value="">Select format</option>
                                        <option value="pdf">PDF</option>
                                        <option value="excel">Excel</option>
                                    </select>
                                </div>


                            <!--  <div class="form-group">
            <label for="username">As at Date </label>
            <input class="form-control" placeholder="" type="date" name="date" id="date" value="{{date('Y-m-d')}}">
        </div> -->


                                <div class="form-actions form-group">


                                    <button type="submit" class="btn btn-primary btn-sm">View Report</button>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#customperiod').hide();
            $('#date').hide();

            $('#period').change(function () {
                if ($(this).val() == "As at date") {
                    $('#customperiod').hide();
                    $('#date').show();
                } else if ($(this).val() == "custom") {
                    $('#customperiod').show();
                    $('#date').hide();
                } else {
                    $('#customperiod').hide();
                    $('#date').hide();
                }

            });
        });

    </script>

@stop
