<html><head>
   <style>
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px;}
   </style></head><body>
        <div class='onerow topdivrow'>
            <div>
                <strong>
                    {{ strtoupper($organization->name)}}<br>
                </strong>
                {{ $organization->phone}}<br>
                {{ $organization->email}}<br>
                {{ $organization->website}}<br>
                {{ $organization->address}}
            </div>
        </div><br>
        <div class='onerow headerrow'>
            <span>STATEMENT OF FINANCIAL POSITION</span>
        </div>
        <div class='onerow headerrow'>
            <table>
                <tr><td>Sacco Society</td><td></td><td>C S _</td></tr>
                <tr><td>Financial Year:</td><td></td><td></td></tr>
                <tr><td>Start Date:</td><td></td><td></td></tr>
                <tr><td>End Date:</td><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td>Ref NO.</td><td>ASSETS</td><td></td><td></td></tr>
                <tr><td>1</td><td>Cash and Cash Equivalent</td><td>0</td><td>0</td></tr>
                <tr><td>1.1</td><td>Cash in Hand (Both Local and Foreign Notes and Coins)</td><td>0</td><td>0</td></tr>
                <tr><td>1.2</td><td>Cash at Bank: (Placement with Financial Institutions)</td><td>0</td><td>0</td></tr>
                <tr><td></td><td></td><td></td><td></td></tr>
                <tr><td>2</td><td>Prepayments and Sundry receivables</td><td>0</td><td>0</td></tr>
                <tr><td></td><td></td><td></td><td></td></tr>
                <tr><td>3</td><td>Financial Investments</td><td>0</td><td>0</td></tr>
                <tr><td>3.1</td><td>Government Securities</td><td>0</td><td>0</td></tr>
                <tr><td>3.2</td><td>Other Securities</td><td></td><td></td></tr>
                <tr><td>3.3</td><td>Other Investments</td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td></tr>
                <tr><td>4</td><td>Net Loan Portfolio</td><td>0</td><td>0</td></tr>
                <tr><td>4.1</td><td>Gross Loan Portfolio</td><td></td><td></td></tr>
                <tr><td>4.2</td><td>Allowance for loan losss</td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td></tr>
                <tr><td>5</td><td>Accounts Receivables</td><td>0</td><td>0</td></tr>
                <tr><td>5.1</td><td>Tax Recoverable</td><td></td><td></td></tr>
                <tr><td>5.2</td><td>Deferred Tax Assets</td><td></td><td></td></tr>
                <tr><td>5.3</td><td>Retirement Benefit Assets</td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td></tr>
                <tr><td>6</td><td>Property and equipment</td><td>0</td><td>0</td></tr>
                <tr><td>6.1</td><td>Investment Properties</td><td></td><td></td></tr>
                <tr><td>6.2</td><td>Property and Equipment</td><td></td><td></td></tr>
                <tr><td>6.3</td><td>Prepaid Lease Rentals</td><td></td><td></td></tr>
                <tr><td>6.4</td><td>Intangible Assets</td><td></td><td></td></tr>
                <tr><td>6.5</td><td>Other Assets</td><td></td><td></td></tr>
                <tr><td></td><td>Total Assets</td><td>0</td><td>0</td></tr>
                <tr><td></td><td>LIABILITIES</td><td></td><td></td></tr>
                <tr><td>7</td><td>Savings Deposits</td><td>0</td><td>0</td></tr>
                <tr><td>8</td><td>Short Term Deposits</td><td>0</td><td>0</td></tr>
                <tr><td>9</td><td>Non withdraw-able deposits</td><td>0</td><td>0</td></tr>
                <tr><td>10</td><td>Accounts Payables & Other Liabilities</td><td>0</td><td>0</td></tr>

            </table> 
        </div>
    </body>
</html>