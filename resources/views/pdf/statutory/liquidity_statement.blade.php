<html><head>
   <style>
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 5px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px;}
   </style></head><body>

    <div class='onerow topdivrow'>
        <div>
            <strong>
                {{ strtoupper($organization->name)}}<br>
            </strong>
            {{ $organization->phone}}<br>
            {{ $organization->email}}<br>
            {{ $organization->website}}<br>
            {{ $organization->address}}
        </div>
    </div><br>
    <div class='onerow headerrow'>
        <span><u>LIQUIDITY STATEMENT</u></span>
    </div>
    <div class='onerow'>
    <table class="table table-bordered onerowtable">
              <tr>
                <td></td>
                <td>Sacco Society</td>
                <td></td>
                <td>CS No.......</td>
              </tr> 
              <tr>
                <td></td><td>Financial Year</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td>Start  Date</td><td></td><td></td>
              </tr> 
              <tr>
                <td></td><td>End  Date</td><td></td><td></td>
              </tr>
              <tr><td colspan='4'></td></tr>
              <tr>
                <td>1</td><td colspan='2'>Notes and Coins</td><td></td>
              </tr>  
              <tr>
                <td>1.1</td><td colspan='2'>Local Notes and Coins</td><td></td>
              </tr>
              <tr>
                <td>1.2</td><td colspan='2'>Foreign Notes and Coins</td><td></td>
              </tr>
              <tr>
                <td>2</td><td colspan='2'>Bank  balances</td><td></td>
              </tr>
              <tr>
                <td>2.1</td><td colspan='2'>Balances with banks</td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'>Less</td><td></td>
              </tr>
              <tr>
                <td>2.2</td><td colspan='2'>Time deposits with banks more than 90 days</td><td></td>
              </tr>
              <tr>
                <td>2.3</td><td colspan='2'>Overdrafts and matured loans/ Advances from banks</td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td>
              </tr>
              <tr>
                <td>3</td><td colspan='2'>Balances with Other Financial Institutions</td><td></td>
              </tr>
              <tr>
                <td>3.1</td><td colspan='2'>Balances with Other Sacco societies</td><td></td>
              </tr>
              <tr>
                <td>3.2</td><td colspan='2'>Balances with Other Financial Institutions other than Banks
                        and Sacco societies</td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'>Less:</td><td></td>
              </tr>
              <tr>
                <td>3.3</td><td colspan='2'>Balances due to other Sacco societies</td><td></td>
              </tr>
              <tr>
                <td>3.4</td><td colspan='2'>Balances due to Financial Institutions</td><td></td>
              </tr>
              <tr>
                <td>3.5</td><td colspan='2'>Matured Loans/Advances from Financial Institutions</td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td>
              </tr>
              <tr>
                <td>4</td><td colspan='2'>GOVERNMENT SECURITIES</td><td></td>
              </tr>
              <tr>
                <td>4.1</td><td colspan='2'>Treasury Bills</td><td></td>
              </tr>
              <tr>
                <td>4.2</td><td colspan='2'>Treasury Bonds</td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td>
              </tr>
              <tr>
                <td>5</td><td colspan='2'>NET LIQUID ASSETS (1-4)</td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td>
              </tr>
              <tr>
                <td>6</td><td colspan='2'>DEPOSIT BALANCES</td><td></td>
              </tr>
              <tr>
                <td>6.1</td><td colspan='2'>Deposits from members including interest</td><td></td>
              </tr>
              <tr>
                <td>6.2</td><td colspan='2'>Deposits from all other sources including accrued interest</td><td></td>
              </tr>
              <tr>
                <td>6.3</td><td colspan='2'>Total Deposits</td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'>Less</td><td></td>
              </tr>
              <tr>
                <td>6.4</td><td colspan='2'>Balances due to Sacco societies</td><td></td>
              </tr>
              <tr>
                <td>6.5</td><td colspan='2'>Balances due to banks</td><td></td>
              </tr>
              <tr>
                <td>6.6</td><td colspan='2'>Balances due to other Financial Institutions</td><td></td>
              </tr>
              <tr>
                <td>6.7</td><td colspan='2'>Total deductions</td><td></td>
              </tr>
              <tr>
                <td>6.8</td><td colspan='2'>Net Deposit Liabilities</td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td>
              </tr>
              <tr>
                <td>7</td><td colspan='2'>OTHER LIABILITIES</td><td></td>
              </tr>  
              <tr>
                <td>7.1</td><td colspan='2'>Matured</td><td></td>
              </tr>
              <tr>
                <td>7.2</td><td colspan='2'>Maturing within 91 days</td><td></td>
              </tr>
              <tr>
                <td>7.3</td><td colspan='2'>Total Other Liabilities</td><td></td>
              </tr>
              <tr>
                <td></td><td colspan='2'></td><td></td>
              </tr>
              <tr>
                <td>8</td><td colspan='2'>LIQUIDITY RATIO</td><td></td>
              </tr>
              <tr>
                <td>8.1</td><td colspan='2'>Net Liquid Assets (5)</td><td></td>
              </tr>
              <tr>
                <td>8.2</td><td colspan='2'>Total Short term Liabilities 6.3+7.3</td><td></td>
              </tr> 
              <tr>
                <td>8.3</td><td colspan='2'>Ratio of(8.1/8.2)%</td><td></td>
              </tr>
              <tr>
                <td>8.4</td><td colspan='2'>Minimum Holding of Liquid Assets Requirement</td><td></td>
              </tr>
              <tr>
                <td>8.5</td><td colspan='2'>Excess/Deficit (8.3-8.4)</td><td></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        </table><br>
        <div style='text-align:center;'>
            <span>* A reconciliation for the difference to be attached on a separate sheet.</span><br>
            <span>Note: Monthly return to be received on or before the 15th day of the following month</span>
        </div>
    </div>
    <div class='onerow'>
        <span>AUTHORIZATION</span><br><br>
        <table>
          <tr style=''>
              <td>
                <span>We declare that this return, to the best of our knowledge and belief is correct.</span><br><br>
                <span>Sign.....................................................................................Date:...............................................Name of Authorizing Officer...............................................................</span><br><br>
                <span>Sign.....................................................................................Date:...............................................Name of Authorizing Officer...............................................................</span>
              </td>
           </tr> 
        </table>
    </div><br><br>
    <div class='onerow'>
        <span>COMPLETION INSTRUCTIONS FOR LIQUIDITY STATEMENT</span><br><br>

        <span>1. Notes and Coins</span><br>
        <span>1.1</span> <div class='spandiv'><span> Local</span><br>
        <span>Enter all notes and coins on the Sacco Society‘s premises (including mobile units) which
                are legal tender in Kenya.</span></div><br><br>
        <span>1.2</span> <div class='spandiv'><span>Foreign</span><br>
        <span>Enter the Kenyan shillings equivalent of all convertible foreign currencies held by the
                Sacco Society. The CBK mean rates as on the reporting dates should be applied in
                converting foreign currencies into Kenya shillings.</span></div><br><br>
        <span>2. Balances with Banks</span><br>
        <span>2.1</span> <div class='spandiv'><span>Balances with banks</span><br>
        <span>Enter the total of all balances (overnight, call and time) held at domestic commercial
                banks excluding un-cleared effects. These balances should include accrued interest.</span></div><br>
        <span>2.2</span> <div class='spandiv'><span>Time Deposits with Banks</span><br>
        <span>Enter the amount of time deposits including accrued interest entered in 4(a) above whose
                maturities exceed 91 days.</span></div><br>
        <span>2.3</span> <div class='spandiv'><span> Balances Due to Banks</span><br>
        <span>Enter the total of balances due to commercial banks including accrued interest.</span></div><br>
        <span>2.4</span> <div class='spandiv'><span> Overdrafts and Matured loans and advances from domestic banks</span><br>
        <span>Enter the total of all overdrafts and any other debit balances on matured loans and
                advances including guarantees and bonds issued by commercial banks.</span></div><br><br>
        <span>3. Balances with Financial Institutions</span><br>
        <span>3.1</span> <div class='spandiv'><span>Balances with Financial Institutions</span><br>
        <span>Enter  the  total  of  all  balances  (overnight,  call  and  time)  placed  with  the  Sacco  Society  by financial  institutions  (mortgage  companies,  building  societies  etc),  excluding  un-cleared effects but including accrued interest.</span></div><br>
        <span>3.2</span> <div class='spandiv'><span>Time Deposits with Financial Institutions</span><br>
        <span>Enter the amount of time deposits including accrued interest entered in 6(a) above whose maturities exceed 91 days</span></div><br>
        <span>3.3</span> <div class='spandiv'><span>Balances due to Financial Institutions</span><br>
        <span>Enter the total of balances received from financial institutions including accrued interest. This balance should agree  with the  total  analysed in  the  table attached and should exclude balances with maturity period exceeding 91 days.</span></div><br>
        <span>3.4</span> <div class='spandiv'><span>Matured Loans and Advances received from Financial Institutions</span><br>
        <span>Enter  the  total  of  matured  loans  and  advances  including  guarantee  s,  bills  discounted, promissory  notes  and  performance  bonds  received  from  financial  institutions.  All deposits/placements  with  institutions/building  societies  under  liquidation  should  not  be reported as part of liquid assets.</span></div><br><br>
        <span>4. Government Securities</span> <br>
        <span>4.1</span> <div class='spandiv'><span>Treasury Bills</span><br>
        <span>Enter  the  amortized  cost  of  all  Kenya  Government  Treasury  Bills  investments  by  the reporting Sacco Society, net of encumbered Treasury Bills. Encumbered Treasury Bills are  those  pledged  to  secure  any  form  of  credit  facility  granted  to  the  r  eporting  Sacco Society.</span></div><br>
        <span>4.2</span> <div class='spandiv'><span>Treasury Bonds/ Bearer Bonds</span><br>
        <span>Enter  the  amortized  cost  or  fair  value  of  all  treasury  bonds/bearer  bonds  traded  in  the Nairobi  Stock  Exchange  acquired  by  the  reporting  Sacco  Society  directly  from  the government and its issuing agents and those discounted from third parties</span></div><br><br>
        <span>5. Net Liquid Assets.  Enter the sum of items 1 to 4 above</span> <br><br>
        <span>6. Deposit Balances</span> <br>
        <span>6.1</span> <div class='spandiv'><span>Total Deposit</span><br>
        <span>Enter total deposits (6.1 –6.2) from the members and all other sources, including accrued interest, but excluding un-cleared effects.</span></div><br>
        <span>Less:</span><br>
        <span>6.2</span> <div class='spandiv'><span>Balances Due to Sacco societies.</span><br>
        <span>Enter the total amount of balances due to other Sacco societies including accrued interest</span></div><br>
        <span>6.3</span> <div class='spandiv'><span>Balances Due to banks.</span><br>
        <span>Enter the total of balances due to domestic and foreign commercial banks including accrued interest</span></div><br>
        <span>6.4</span> <div class='spandiv'><span>Balances due to Financial Institutions</span><br>
        <span>Enter the total amount of balances due to financial institutions including accrued interest</span></div><br>
        <span>6.5</span> <div class='spandiv'><span>Total Deductions</span><br>
        <span>Enter the total of items 6.4 –6.6</span></div><br>
        <span>6.6</span> <div class='spandiv'><span> Net Deposit Liabilities</span><br>
        <span>Enter the net amount of item 6.3 less 6.7</span></div><br><br>
        <span>7. Other liabilities</span> <br>
        <span>7.1</span> <div class='spandiv'><span> Matured</span><br>
        <span>Enter the sum of all matured liabilities (Including crystallized off-balance sheet commitments) that have cash flow implications and are due for payment.</span></div><br>
        <span>7.2</span> <div class='spandiv'><span> Maturing within 91 days</span><br>
        <span>Enter the sum of all liabilities that will mature within 90 days from the date of the return.</span></div><br>
        <span>7.3</span> <div class='spandiv'><span> Total other liabilities</span><br>
        <span>Enter the total of items 7.1 –7.2</span></div><br>
        <span>8.</span> <div class='spandiv'><span> Liquidity Ratio  Calculate the ratios using the formula provided</span><br>
        <span>The liquidity statement should be completed as per the instructions contained in this regulation, and should be submitted on the 15th of the following month.</span></div><br>

    </div>

</body></html>