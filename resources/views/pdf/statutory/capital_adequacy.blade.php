<html><head>
   <style>
    table,th,td { border: 1px solid black; border-collapse: collapse;}
    th,td {padding: 15px;}
    .topdivrow{width:100%; }
    .topdivrow div{text-align:center; margin:0 auto;}
    .headerrow{text-align:center; }
    .onerow{padding:12px 2px;} .subspans{text-align:center;} 
    .subspans span{} .onerowtable{margin:0 auto;} .spandiv{display:inline-block;} .bottomdiv{text-align:center;}
    .spandiv2{padding-left:18px; padding-top:5px;} 
   </style></head><body>

    <div class='onerow topdivrow'>
        <div>
            <strong>
                {{ strtoupper($organization->name)}}<br>
            </strong>
            {{ $organization->phone}}<br>
            {{ $organization->email}}<br>
            {{ $organization->website}}<br>
            {{ $organization->address}}
        </div>
    </div><br>
    <div class='onerow headerrow'>
        <span><u>CAPITAL ADEQUACY RETURN</u></span>
    </div><br> 
    <div class='onerow'>
    <table class="onerowtable">
              <tr>
                <td>Name of Sacco Society</td>
                <td></td>
                <td>CS No............</td>
              </tr> 
              <tr>
                <td>Financial Year:</td> <td></td> <td></td>
              </tr> 
              <tr>
                <td>Start Date:</td> <td></td> <td></td>
              </tr> 
              <tr>
                <td>End Date:</td> <td></td> <td></td>
              </tr> 
              <tr>
                <td>1</td> <td>CAPITAL COMPONENTS</td> <td></td>
              </tr> 
              <tr>
                <td colspan='2'></td>  <td></td>
              </tr> 
              <tr>
                <td>1.1</td> <td>CORE CAPITAL</td> <td></td>
              </tr> 
              <tr>
                <td>1.1.1</td> <td>Share capital</td> <td></td>
              </tr> 
              <tr>5
                <td>1.1.2</td> <td>Statutory reserves</td> <td></td>
              </tr>    
              <tr>
                <td>1.1.3</td> <td>Retained earnings/Accumulated losses</td> <td></td>
              </tr>  
              <tr>
                <td>1.1.4</td> <td>Net Surplus after Tax,current year to-date 50%</td> <td></td>
              </tr>
              <tr>
                <td>1.1.5</td> <td>Capital Grants</td> <td></td>
              </tr>  
              <tr>
                <td>1.1.6</td> <td>General Reserves</td> <td></td>
              </tr>  
              <tr>
                <td>1.1.7</td> <td>Other Reserves</td> <td></td>
              </tr>   
              <tr>
                <td>1.1.8</td> <td>Sub-Total (1.1 .1 to 1.1.7)</td> <td></td>
              </tr>
              <tr rowspan='2'>
                <td colspan='2'>LESS DEDUCTIONS</td> <td></td>
              </tr>  
              <tr>
                <td>1.1.9</td> <td>Investments in Subsidiary and Equity instruments
                            of Other Institutions</td> <td></td>
              </tr>  
              <tr>
                <td>1.1.10</td> <td>Other deductions</td> <td></td>
              </tr>
              <tr>
                <td>1.1.11</td> <td>TotalDeductions (1.1.9 to 1.1.10)</td> <td></td>
              </tr>
              <tr>
                <td>1.1.12</td> <td>CORE CAPITAL (1.1.8 Less 1.1.11)</td> <td></td>
              </tr>  
              <tr>
                <td>1.1.13</td> <td>INSTITUTIONAL CAPITAL (1.1.12 Less 1.1.1)</td> <td></td>
              </tr>
              <tr>
                <td colspan='3'></td>
              </tr> 
              <tr>
                <td>2.</td> <td>ON - BALANCE SHEET ASSETS</td> <td></td>
              </tr>   
              <tr>
                <td>2.1</td> <td>Cash (Local + Foreign Currency)</td> <td></td>
              </tr>  
              <tr>
                <td>2.2</td> <td>Government Securities</td> <td></td>
              </tr> 
              <tr>
                <td>2.3</td> <td>Deposits and Balances at Other Institutions.</td> <td></td>
              </tr>   
              <tr>
                <td>2.4</td> <td>Loans and Advances</td> <td></td>
              </tr>    
              <tr>
                <td>2.5</td> <td>Investments</td> <td></td>
              </tr> 
              <tr>
                <td>2.6</td> <td>Property and Equipment (net of depreciation)</td> <td></td>
              </tr>
              <tr>
                <td>2.7</td> <td>Other Assets</td> <td></td>
              </tr>    
              <tr>
                <td>2.8</td> <td>TOTAL(2.1 to 2.7)</td> <td></td>
              </tr> 
              <tr>
                <td>2.9</td> <td>Total Assets (As per Balance Sheet)</td> <td></td>
              </tr>  
              <tr>
                <td>2.10</td> <td>Difference</td> <td></td>
              </tr> 
              <tr>
                <td colspan='3'></td>
              </tr>
              <tr>
                <td>3</td> <td>OFF-BALANCE SHEET ASSETS</td> <td></td>
              </tr>  
              <tr>
                <td>4</td> <td>CAPITAL RATIO CALCULATIONS</td> <td></td>
              </tr>  
              <tr>
                <td>4.1</td> <td>Total Asset value of on-balance sheet items as per 2.8 above</td> <td></td>
              </tr>    
              <tr>
                <td>4.2</td> <td>Total Asset value of off-balance sheet items as per 3 above</td> <td></td>
              </tr>  
              <tr>
                <td>4.3</td> <td>Total Assets (4.1 + 4.2)</td> <td></td>
              </tr>  
              <tr>
                <td>4.4</td> <td>Total Deposits Liabilities (As per Balance Sheet)</td> <td></td>
              </tr>
              <tr>
                <td>4.5</td> <td>Core Capital to Assets Ratio (1.1.12/4.3)%</td> <td></td>
              </tr> 
              <tr>
                <td>4.6</td> <td>Minimum Core Capital to Assets Ratio requirement</td> <td></td>
              </tr>   
              <tr>
                <td>4.7</td> <td>Excess (deficiency) (4.5 less 4.6)</td> <td></td>
              </tr>  
              <tr>
                <td>4.8</td> <td>Institutional Capital to Assets Ratio (1.1.13/4.3)%</td> <td></td>
              </tr> 
              <tr>
                <td>4.9</td> <td>Minimum Institutional to Asset Ratio requirement</td> <td></td>
              </tr>
              <tr>
                <td>4.10</td> <td>Excess (deficiency) (4.8 less 4.9)</td> <td> </td>
              </tr>
              <tr>
                <td>4.11</td> <td>Core capital to Deposits Ratio (1.1.12/4.4)%</td> <td></td>
              </tr>   
              <tr>
                <td>4.12</td> <td>Minimum Core Capital to Deposits requirement</td> <td></td>
              </tr>  
              <tr>
                <td>4.13</td> <td>Excess/(Deficiency) (4.0 less 5.0)</td> <td></td>
              </tr>   
              <tr>
                <td colspan='3'>* A reconciliation for the difference to be attached on a separate sheet.</td> 
              </tr> 
              <tr>
                <td colspan='3'>Note: Monthly return to be received on or before the 15th day of the following month</td> 
              </tr>         
              <tr>
                <td colspan='3'>AUTHORIZATION:</td> 
              </tr> 
              <tr>
                <td colspan='3'>We declare that this return, to the best of our knowledge and belief is correct.</td> 
              </tr>
              <tr>
                <td colspan='3'><span>............................................................Sign..................................................Date:..........................................</span><br><br> 
                <span>Name of Authorizing Officer..........................................................................</span></td>
              </tr>
              <tr>
                <td colspan='3'><span>............................................................Sign..................................................Date:..........................................</span><br><br> 
                <span>Name of Countersigning Officer..........................................................................</span></td>
              </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
        </table><br><br>
    </div>
    <div class='onerow'>
        <span><u>COMPLETION INSTRUCTIONS ON CAPITAL ADEQUACY RETURN</u></span><br><br>
        <div class='spandiv2'>
            <span>1. CAPITAL COMPONENTS</span><br>
            <span>1.1 CORE CAPITAL</span>
        </div><br><br>
        <span>1.1.1 Share Capital</span><br><span>This is the value issued and fully paid members shares.</span><br><br>
        <span>1.1.2 Statutory Reserve</span><br>
        <span>These are accumulated reserves that have been appropriated from net surplus (revenue
                reserves) over the years. This is normally 20% of retained earnings after tax..</span><br><br>
        <span>1.1.3 Retained Earnings/Accumulated Lossesl</span><br>
        <span>These are reserves retained from earnings or accumulated losses from the profits/losses of
                prior years. They should however exclude reserves arising from revaluation of investment
                properties and cumulative unrealised gains and losses on financial instruments.</span><br><br>
        <span>1.1.4 Net Surplus after Tax Current Year to Date 50%</span><br>
        <span>This is 50% of current year to date un-audited after tax profits. The Sacco Society must
                have made adequate provisions for loan losses, depreciation, amortization and other
                expenses. In arriving at the applicable figure, any proposed dividends would have been
                taken into account. This should however exclude reserves arising from revaluation of
                investment properties and cumulative unrealised gains and losses on financial
                instruments. In case of a loss, full amount should be included.</span><br><br>
        <span>1.1.5 Donations and Grants</span><br>
        <span>These are donations to the Sacco Society that are irredeemable or non payable</span><br><br>
        <span>1.1.6 General Reserves</span><br><span>These are reserve provisions to cover other non performing receivable accounts.</span><br><br>
        <span>1.1.7 Other Reserves</span><br>
        <span>These are all other reserves, which have not been included above. Such reserves should
                be permanent, unencumbered, uncollectible and thus able to absorb losses. Further, the
                reserves should exclude cumulative unrealised gains and losses on available-for- sale-
                instruments.</span><br><br>
        <span>1.1.8 Sub-Totals</span><br><span>Enter in this line the sub-total of all the items from 1.1.1 to 1.1.5.</span><br><br>

        <span>DEDUCTIONS FROM CORE CAPITAL</span><br><br>

        <span>1.1.9 Investment in Subsidiary and Equity Instruments of Other Institutions.</span><br>
        <span>This is investments made by a Sacco Society in its subsidiary institutions and equity instruments of other institutions.</span><br><br>
        <span>1.1.10 Other Deductions</span><br><span>In this line, enter any other deductible items that have not been dealt with in 1.1.9.</span><br><br>
        <span>1.1.11 Total Deductions</span><br><span>This is the total of all the items from 1.1.9 to 1.1.10.</span><br><br>
        <span>1.1.12 Core Capital</span><br><span>Core Capital is the deduction of line 1.1.8 from line 1.1.11.</span><br><br>
        <span>1.1.13 Institutional Capital</span><br><span>Institutional Capital comprises of all items in the Core Capital less Share Capital i.e.
                (1.1.12 less 1.1.1)</span><br><br>
        
        <span>2. ON-BALANCE SHEET ASSETS</span><br><br>
        <div class='spandiv2'>
            <span>2.1 Cash</span><br><span>Enter in this line cash at hand (Local + Foreign notes and coins).</span><br><br>
            <span>2.2 Government Securities</span><br><span>These are Treasury bills and treasury issued by the Government of Kenya.</span><br><br>
            <span>2.3 Deposits and Balances Due from Lending Institutions</span><br>
            <span>These are deposits and balances held with banks, other financial institutions, and other
                    Sacco societies including overnight balances.</span><br><br>
            <span>2.4 Loans and Advances</span><br>
            <span>These refer to are facilities advanced to members whether secured or not. These be
                    reported net of provisions which must be computed in accordance with Classification of
                    Assets and Provisioning Return. However, provisions appropriated from retained earning
                    should not be netted off from loans and advances.</span><br><br>
            <span>2.5 Investments</span><br>
            <span>These are investments in a Sacco Society‘s subsidiary institutions and other financial
                institutions.</span><br><br>
            <span>2.6 Property and Equipment</span><br>
            <span>These are assets acquired for use in the operation of the business or for investment
                    purposes, e.g. furniture, computers, freehold and leasehold land and buildings. They
                    should be shown net of accumulated depreciation, amortized cost, or at fair value.</span><br><br>
            <span>2.7 Other Assets</span><br><span>These are other assets, which have not been dealt with above.</span><br><br>
            <span>2.8 Total On-Balance Sheet Assets</span><br><span>Enter in this line total on-balance sheet asset i.e. total of line 2.1 to 2.7.</span><br><br>
            <span>2.9 Total Assets (As per Balance Sheet)</span><br>
            <span>Total asset figure as reported in the Balance Sheet for the period should be indicated in this line..</span><br><br>
            <span>2.10 Difference</span><br><span>Enter in this line cash at hand (Local + Foreign notes and coins).</span><br><br>
            <span>2.1 Cash</span><br>
            <span>This is the difference between total on-balance sheet assets and total assets as reported in
                    the un-Audited Monthly Balance Sheet. The difference should be explained in the form
                    of reconciliation.</span><br><br>

            <span>3 OFF-BALANCE SHEET ITEMS</span><br>
            <span>In this line, indicate computed off-balance sheet assets such as existing guarantees by the
                    Sacco Society</span><br><br>
            
            <span>4.0 CAPITAL RATIO CALCULATIONS</span><br>
            <span>Compute as per the formulae provided in the form.</span><br><br>
        
            <span>4.5 Total Deposits</span><br>
            <span>Total deposit figure as reported in the Balance Sheet for the period should be indicated in
                    this line.</span><br><br>
        </div><br><br>

        <span>GENERAL: All reported items should agree with or capable of being derived from the
                figures reported in the Balance Sheet for the period. This is a monthly return and should
                be submitted by the 15th day of the following month.</span>
    </div>
</body></html>