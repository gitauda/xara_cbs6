<?php
function asMoney($value)
{
    return number_format($value, 2);
}
?>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><style type="text/css">
        table {
            max-width: 100%;
            background-color: transparent;
        }

        th {
            text-align: left;
        }

        .table {
            width: 100%;
            margin-bottom: 2px;

        }

        hr {
            margin-top: 1px;
            margin-bottom: 2px;
            border: 0;
            border-top: 2px dotted #eee;
        }

        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 12px;
            line-height: 1.428571429;
            color: #333;
            background-color: #fff;
        }
    </style></head><div class="row">
    <div class="col-lg-8">
        <table class="table table-bordered">
            <tr>
                <td>

                    <img src="{{asset('public/uploads/logo/'.$organization->logo)}}" alt="{{ $organization->logo }}" style="height: 50px;"/>

                </td>
                <td>
                    <strong>
                        {{ strtoupper($organization->name)}}<br>
                    </strong>
                    {{ $organization->phone}}<br>
                    {{ $organization->email}}<br>
                    {{ $organization->website}}<br>
                    {{ $organization->address}}
                </td>
            </tr>
            <tr>
                <hr>
            </tr>
        </table>
        <table class="table table-bordered">
            <tr>
                <td>Member:</td>
                <td> {{ $member->name}}</td>
            </tr>
            <tr>
                <td>Member #:</td>
                <td> {{ $member->membership_no}}</td>
            </tr>
            <tr>
                <td>Branch :</td>
                <td> {{ $member->branch->name}}</td>
            </tr>
            <tr>
                <hr>
            </tr>
        </table>
        <br><br>
        <h3 style="color:green">Loan Summary</h3>
        <hr>
        <table class="table table-bordered">
            <tr>
                <td><strong>Loan Product</strong></td>
                <td><strong>Loan Number</strong></td>
                <td><strong>Loan Amount</strong></td>
                <td><strong>Loan Balance</strong></td>
            </tr>
            @foreach($loans as $loan)
                @if(Loantransaction::getLoanBalance($loan) > 0)
                    <tr>
                        <td>{{$loan->loanproduct->name}}</td>
                        <td>{{$loan->account_number}}</td>
                        <td>{{asMoney(Loanaccount::getLoanAmount($loan))}}</td>
                        <td>{{asMoney(Loantransaction::getLoanBalance($loan))}}</td>
                    </tr>
                @endif
            @endforeach
        </table>
         <br><br>
        <h3 style="color:green">Guarantor Details</h3>
        <hr>
        <table class="table table-bordered">
            <tr>
                <td><strong>Guarantor Name</strong></td>
                <td><strong>Loan Number</strong></td>
                <td><strong>Amount Guaranteed</strong></td>
                            </tr>
            @foreach($loans as $loan)
                <?php $guarantorrs=Loanguarantor::where('loanaccount_id', '=',$loan->id)->get();     ?>
                @if(!empty($guarantorrs) && Loantransaction::getLoanBalance($loan) > 0)
                    @foreach($guarantorrs as $guarantorr)

                    <tr>
                        <td>{{$guarantorr->member->name}}</td>
                        <td>{{$loan->account_number}}</td>
                        <td>{{asMoney($guarantorr->amount)}}</td>
                        
                    </tr>
                  @endforeach

                @endif
            @endforeach
        </table>
         
        <br><br>
       @if(!empty($guarantors))
        <h3 style="color:green">Loans Guaranteed by {{strtoupper($member->name)}} </h3>
        <hr>
         
         	<table class="table table-bordered">
                 <tr>
                            <td><strong>Member</strong></td>
                <td><strong>Loan Number</strong></td>
                <td><strong>Amount Guaranteed</strong></td>
               <td><strong>Date</strong></td>
                <td><strong>Loan Balance</strong></td>
                

            </tr>
           @foreach($guarantors as $guarantor)
               <?php $loann= Loanaccount::where('id', '=', $guarantor->loanaccount_id)->where('is_disbursed','=',1)->first();?>
                @if(!empty($loann) && Loantransaction::getLoanBalance($loann) > 0)
                    <tr>
                        <td>{{$loann->member->name}}</td>
                        <td>{{$guarantor->loanaccount->account_number}}</td>
                        <td>{{asMoney($guarantor->amount)}}</td>
                         <td>{{$guarantor->date}}</td>

                        <td>{{asMoney(Loantransaction::getLoanBalance($loann))}}</td>
                    </tr>
                @endif

            @endforeach

                   </table>
            @endif

        <br><br>
            
        @if(count($account)>0)
            <h3 style="color:green">Saving Transaction Summary:
                Account # &emsp;
                <strong style="color:blue;">
                    {{ $account->account_number}}
                </strong>
            </h3>
            <hr>
            <table class="table table-bordered">
                <tr>
                    <td><strong> Date </strong></td>
                    <td><strong> Description </strong></td>
                    <td><strong> Dr </strong></td>
                    <td><strong> Cr </strong></td>
                </tr>
                @foreach($transactions as $transaction)
                    <tr>
                        <td>
                            <?php
                            $date = date("d-M-Y", strtotime($transaction->date));
                            ?>
                            {{ $date}}</td>
                        <td>{{ $transaction->description }}</td>
                        @if( $transaction->type == 'debit')
                            <td>{{ asMoney($transaction->amount)}}</td>
                            <td>0.00</td>
                        @endif
                        @if( $transaction->type == 'credit')
                            <td>0.00</td>
                            <td>{{ asMoney($transaction->amount) }}</td>
                        @endif
                    </tr>
                @endforeach
                <tr>
                    <td>
                        <strong>
                            Savings Balance: {{ asMoney($balance) }}
                        </strong>
                    </td>
                </tr>
                <tr>
                    <hr>
                </tr>
            </table>
        @endif
        <br><br>
 <h4 style="color:green"> SUMMARY FOR OTHER SAVING PRODUCTS</h4>
     <hr>
        <table class="table table-bordered">
         
   <tr>
                <td><strong>Member #</strong></td>
                 <td><strong>Member Name</strong></td>
                   <td><strong>Saving product</strong></td>
               <td><strong>Account Number</strong></td>
  
              <td><strong>Account Balance</strong></td>
  
 
           </tr>
           
           <?php $date=date("Y-m-d"); ?>
            @foreach($savingaccounts as $savingaccount)
            <tr>
            <td>{{$member->membership_no}}</td>
                 <td>{{$member->name}}</td>
                   <td>{{$savingaccount->savingproduct->name}}</td>
               <td>{{$savingaccount->account_number}}</td>
  
              <td>{{asMoney(Savingaccount::getSavingsAsAt($savingaccount, $date))}}</td>
                </tr>
              @endforeach
           </table>
  <br><br>
   @foreach($savingaccounts as $savingaccount)
  @if($savingaccount->savingproduct->Interest_Rate>0)
<h4> {{$savingaccount->savingproduct->Interest_Rate}}% Interest Earned on {{$savingaccount->savingproduct->name}} SAVINGS</h4>
     <hr>
   <table class="table table-bordered">
       <tr>
       <?php 
        $amount= Savingaccount::getSavingsAsAt($savingaccount, $date);
       $interestamount=($savingaccount->savingproduct->Interest_Rate/100)*$amount;
       
               ?>
        <td> Amount:</td>
 

               <td> {{ asMoney($interestamount)}}</td>



            </tr>
            </table>

   <br><br>
   @endif
    @endforeach
<h4> Loanovercharge</h4>
     <hr>
   <table class="table table-bordered">
       <tr>
       <?php 
       $id=$member->id;
       $amount=0.0;
       $amount=Loanrepayment::loanOverchargemem($id);
        ?>
        <td> Amount:</td>
 

               <td> {{ asMoney($amount)}}</td>



            </tr>
            </table>
             <br><br>
        <i style="color:red">Thank you for being a cooperative and active member. Asante Member!</i>
    </div>
</div>
</html>
