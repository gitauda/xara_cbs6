@extends('layouts.teller')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h4>Members</h4>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Member Number</th>
                                        <th>Member Name</th>
                                        <th>Member Branch</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($members as $member)
                                        <tr>
                                            <td> {{ $i }}</td>
                                            <td>{{ $member->membership_no }}</td>
                                            <td>{{ $member->name }}</td>
                                            <td>{{ $member->branch->name }}</td>

                                            <td>
                                                <a href="{{ url('member/savingaccounts/'.$member->id) }}" class="btn btn-info btn-sm">Transact Savings</a>

                                            </td>

                                            <td>
                                                <a href="{{ url('sharetransactions/show/'.$member->shareaccount->id) }}" class="btn btn-info btn-sm">Transact Shares</a>

                                            </td>

                                        </tr>

                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
