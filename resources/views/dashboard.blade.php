@extends('layouts.main')
@section('xara_cbs')
    <?php use App\models\Organization;
    //use App\Branch;
    $organization = Organization::find(1);
    $installationdate=date('Y-m-d',strtotime($organization->installation_date));

    $splitdate = explode('-', $installationdate);
    //split to obtain month and day from the installation date
    $day=$splitdate[2];
    $month=$splitdate[1];
    $year=date('Y');
    //get the due date for annual subscription fee.
    $date =date('d-F-Y',strtotime($day.'-'.$month.'-'.$year));
    //$date =date('d-F-Y',strtotime('21-01-2020'));
    $todaydate=date('d-F-Y');
    ?>


    @include('partials.breadcrumbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    @include('partials.system_cards')
                    <div class="card">
                        @if(Session::get('notice'))
                            <div class="alert alert-info">{{{ Session::get('notice') }}}
                                <span class="pull-right closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            </div>
                        @endif
                        <div class="card-header">
                            {{-- <h5>Members</h5>--}}
                            @if($todaydate==$date && Auth::user()->user_type == 'admin')
                                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                                <h4>Please Pay your Annual Subscription Fee whch is due on <b>{{$date}}</b> to continue enjoying the support services<br><b>IGNORE IF ALREADY PAID</b></h4>
                            @endif
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ Lang::get('messages.table.number') }}</th>
                                        <th>{{ Lang::get('messages.table.name') }}</th>
                                        <th>{{ Lang::get('messages.table.branch') }}</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($members as $member)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ $member->membership_no }}</td>
                                            <td>{{ $member->name }}</td>
                                            <td>{{ $member->branch->name }}</td>
                                            <td>
                                                <a href="{{ url('members/summary/'.$member->id) }}"
                                                   class="btn btn-info btn-sm">Summary</a>
                                            </td>
                                            <td>
                                                <div class="btn-group pull-right">
                                                    <button type="button"
                                                            class="btn btn-info btn-sm dropdown-toggle dropdown-menu-left"
                                                            data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li>
                                                            <a href="{{ url('member/savingaccounts/'.$member->id) }}">
                                                                {{{ Lang::get('messages.savings') }}}
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{  url('members/loanaccounts/'.$member->id) }}">
                                                                {{{ Lang::get('messages.loans') }}}
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ url('sharetransactions/show/'.$member->shareaccount->id) }}">
                                                                {{{ Lang::get('messages.shares') }}}
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ url('members/show/'.$member->id) }}">
                                                                {{{ Lang::get('messages.manage') }}}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@stop
