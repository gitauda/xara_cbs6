@extends('layouts.loans')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}
                            </div>
                        @endif
                        @if (Session::has('delete_message'))
                            <div class="alert alert-danger">
                                {{ Session::get('delete_message') }}
                            </div>
                        @endif
                        <div class="card-header">
                            <h3>Guarantor Matrices</h3>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ url('matrices/create')}}">New Guarantor Matrix</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <?php
                            function asMoney($value) {
                                return number_format($value, 2);
                            }
                            ?>
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>#</th>
                                    <th>Matrix Name</th>
                                    <th>Maximum Amount</th>
                                    <th>Description</th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($matrices as $product)
                                        <tr>
                                            <td> {{ $i }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ asMoney($product->maximum) }}</td>
                                            <td>{{ $product->description }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{url('matrices/update/'.$product->id)}}">Update</a></li>
                                                        <li><a href="{{url('matrices/delete/'.$product->id)}}" onclick="return (confirm('Are you sure you want to delete this guarantor matrix?'))">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@endsection
