@extends('layouts.member')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Assign Vehicles</h3>


                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ url('assignvehicles/update/'.$vehicle->id) }}}" accept-charset="UTF-8">@csrf

                                <fieldset>

                                    <div class="form-group">
                                        <label for="member_id">Member <span style="color:red">*</span></label>
                                        <select name="member_id" id="member_id" class="form-control" data-live-search="true">
                                            <option></option>
                                            @foreach($members as $member)
                                                <option value="{{ $member->id }}"<?= ($vehicle->member_id==$member->id)?'selected="selected"':''; ?>> {{ $member->name }}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="vehicle_id">Vehicle <span style="color:red">*</span></label>
                                        <select name="vehicle_id"  id="vehicle_id" class="form-control" data-live-search="true">
                                            <option></option>
                                            @foreach($vehs as $veh)
                                                <option value="{{ $vehicle->id }}"<?= ($vehicle->vehicle_id==$veh->id)?'selected="selected"':''; ?>> {{ $veh->make.' - '.$veh->regno }}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                    </div>

                                </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
