@extends('layouts.member')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Assign Vehicles</h5>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ url('assignvehicles/create')}}">Assign</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>

                                    <th>#</th>
                                    <th>Vehicle Make</th>
                                    <th>Registration Number</th>
                                    <th>Member</th>
                                    <th></th>

                                    </thead>
                                    <tbody>

                                    <?php $i = 1; ?>
                                    @foreach($vehicles as $vehicle)

                                        <tr>

                                            <td> {{ $i }}</td>
                                            <td>{{ App\models\Vehicle::assignedvehicle($vehicle->vehicle_id)->make }}</td>
                                            <td>{{ App\models\Vehicle::assignedvehicle($vehicle->vehicle_id)->regno }}</td>
                                            <td>{{ App\models\Member::getMemberName($vehicle->member_id) }}</td>
                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                            data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{url('assignvehicles/edit/'.$vehicle->id)}}">Update</a>
                                                        </li>

                                                        <li><a href="{{url('assignvehicles/delete/'.$vehicle->id)}}">Delete</a>
                                                        </li>

                                                    </ul>
                                                </div>

                                            </td>

                                        </tr>

                                        <?php $i++; ?>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
