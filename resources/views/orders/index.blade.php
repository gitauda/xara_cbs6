@extends('layouts.css')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">

                    </div>
                    <div class="card">
                        <div class="card-header">
                            <strong>
                                Orders
                            </strong>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>Member Name </th>
                                        <th>Member # </th>
                                        <th>Product </th>
                                        <th>Supplier</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>

                                            <td>{{ $order->customer_name  }}</td>
                                            <td>{{ $order->customer_number  }}</td>
                                            <td>{{ $order->product->name  }}</td>
                                            <td>{{ $order->product->vendor->name  }}</td>
                                            <td>{{ $order->status }}</td>


                                            <td>

                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">

                                                        <li><a href="#"> Delivered</a></li>
                                                    </ul>
                                                </div>

                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
