@extends('layouts.loans')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if(Session::has('wrath'))
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{ Session::get('wrath')}}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>New Loan Disbursement Option</h3>
                            @if (count($errors) >0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{ url('disbursements/create') }}" accept-charset="UTF-8">{{ csrf_field() }}
                                <fieldset>
                                    <div class="form-group">
                                        <label for="name">Disbursement Name</label>
                                        <input class="form-control" placeholder="" type="text" name="name"
                                               value="{{{ old('name') }}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="min_amt">Minimum Amount</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="min_amt" id="min_amt"
                                               value="{{{ old('min_amt') }}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="max_amt">Maximum Amount</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="max_amt" id="max_amt"
                                               value="{{{ old('max_amt') }}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="desc">Disbursement Description</label>
                                        <textarea name="desc" id="desc" class="form-control"> </textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <div class="form-actions form-group">
                                                <button type="submit" class="btn btn-primary btn-sm pull-centre">
                                                    Create Disbursement Option
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
