@extends('layouts.loans')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        @if(isset($done))
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{$done}}</strong>
                            </div>
                        @endif
                        @if(isset($operation))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{$operation}}</strong>
                            </div>
                        @endif
                        @if(isset($shot))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{$shot}}</strong>
                            </div>
                        @endif
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}
                            </div>
                        @endif
                        @if (Session::has('delete_message'))
                            <div class="alert alert-danger">
                                {{ Session::get('delete_message') }}
                            </div>
                        @endif
                        <div class="card-header">
                            <h3>Disbursement Options</h3>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ url('disbursements/create')}}">New Disbursement Option</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <?php
                            function asMoney($value) {
                                return number_format($value, 2);
                            }
                            ?>
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Minimum Amount</th>
                                    <th>Maximum Amount</th>
                                    <th>Description</th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($options as $option)
                                        <tr>
                                            <td> {{ $i }}</td>
                                            <td>{{ $option->name }}</td>
                                            <td>{{ asMoney($option->min)}}</td>
                                            <td>{{ asMoney($option->max)}}</td>
                                            <td>{{ $option->description }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{url('disbursements/update/'.$option->id)}}">Update</a></li>
                                                        <li><a href="{{url('disbursements/delete/'.$option->id)}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
