@extends('layouts.ports')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors)> 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Members Listing Report</h3>

                        </div>
                        <div class="card-block">

                            <form target="blank" method="POST" action="{{{ url('reports/listing') }}}" accept-charset="UTF-8">@csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="type">Select Members <span style="color:red">*</span></label>
                                        <select class="form-control" id="type" name="type" required>
                                            <option value="all">All</option>
                                            <option value="active">Active</option>
                                            <option value="inactive">Inactive</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="format">Format</label>
                                        <select class="form-control" name="format" id="format" required>
                                            <option value="">Select format</option>
                                            <option value="pdf">PDF</option>
                                            <option value="excel">Excel</option>
                                        </select>
                                    </div>
                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">Select</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
