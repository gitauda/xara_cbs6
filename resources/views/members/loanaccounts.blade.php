@extends('layouts.member')
@section('xara_cbs')
<?php
function asMoney($value){
    return number_format($value, 2);
}
?>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <!-- [ page content ] start -->
                <div class="card">
                    <div class="card-header">
                        <h3>{{$member->name}} Loan Accounts</h3>

                     <div class="card-header-right">
                        <a class="dt-button btn-sm" href="{{ url('loans/apply/'.$member->id)}}">New Loan</a>
                     </div>

                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Loan Type</th>
                                    <th>Loan Number</th>
                                    <th>Loan Amount</th>
                                    <th>Disbursed On</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($member->loanaccounts as $loan)
                                        @if($loan->is_disbursed == TRUE)
                                            <tr>
                                                <td> {{ $i }}</td>
                                                <td>{{ $loan->loanproduct->name }}
                                                    @if(1> App\models\Loantransaction::getLoanBalance($loan))
                                                        <span class="label label-success">Cleared</span>
                                                    @endif
                                                </td>
                                                <td>{{ $loan->account_number }}</td>
                                                <td>{{ asMoney(App\models\Loanaccount::getPrincipalBal($loan) + App\models\Loanrepayment::getPrincipalPaid($loan)) }}</td>
                                                <td>{{ $loan->date_disbursed }}</td>
                                                <td>
                                                    <a href="{{ url('loans/show/'.$loan->id) }}" class="btn btn-info btn-sm">Manage</a>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        @endif
                                    @endforeach


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- [ page content ] end -->
            </div>
        </div>
    </div>
</div>
@stop
