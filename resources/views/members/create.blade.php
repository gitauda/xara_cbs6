@extends('layouts.member')
@section('xara_cbs')



    <script type="text/javascript">
        $(document).ready(function(){

            $('#bank_id').change(function(){
                $.get("{{ url('api/dropdown')}}",
                    { option: $(this).val() },
                    function(data) {
                        $('#bbranch_id').empty();
                        $('#bbranch_id').append("<option>----------------select Bank Branch--------------------</option>");
                        $.each(data, function(key, element) {
                            $('#bbranch_id').append("<option value='" + key +"'>" + element + "</option>");
                        });
                    });
            });
        });
    </script>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}<br>
                                        @endforeach
                                    </div>
                                @endif
                                <div class="card-header">
                                    <h3>New Member</h3>
                                    <?php use App\models\Organization;
                                    $organization = Organization::find(1);

                                    $string = $organization->name;

                                    function initials($str) {
                                        $ret = '';
                                        foreach (explode(' ', $str) as $word){
                                            if($word == null){
                                                $ret .= strtoupper($str[0]);
                                            }else{
                                                $ret .= strtoupper($word[0]);
                                            }
                                        }
                                        return $ret;
                                    }

                                    //echo initials($string);?>

                                </div>


                                <div class="card-block">

                                <form method="POST" action="{{{ url('members/update/'.$member->id) }}}" accept-charset="UTF-8" enctype="multipart/form-data">@csrf
                                    <div class="row">
                                        <div class="col-lg-4">

                                            <fieldset>
                                                <div class="form-group">
                                                    <label for="username">Member Branch <span style="color:red">*</span></label>
                                                    <select name="branch_id" class="form-control">
                                                        <option></option>
                                                        @foreach($branches as $branch)
                                                            <option value="{{ $branch->id }}"> {{ $branch->name }}</option>
                                                        @endforeach

                                                    </select>

                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Member Groups</label>
                                                    <select name="group_id" class="form-control">
                                                        <option></option>
                                                        @foreach($groups as $group)
                                                            <option value="{{$group->id }}"> {{ $group->name }}</option>
                                                        @endforeach

                                                    </select>

                                                </div>
                                            </fieldset>

                                        </div>
                                        <div class="col-lg-3">

                                            <fieldset>
                                                <div class="form-group">
                                                    <label for="username">Member Photo</label>
                                                    <input  type="file" name="photo" id="name">
                                                </div>


                                                <div class="form-group">
                                                    <label for="username">Member Signature</label>
                                                    <input  type="file" name="signature" id="signature" >
                                                </div>
                                            </fieldset>

                                        </div>
                                        <div class="col-lg-4">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label for="username">Membership Number <span style="color:red">*</span></label>
                                                    <input class="form-control" placeholder="" type="text" name="membership_no" id="membership_no" value="{{initials($string).'.'.$mno}}" >
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-lg-12"><hr></div></div>
                                    <div class="row">


                                        <div class="col-lg-4">

                                            <fieldset>
                                                <div class="form-group">
                                                    <label for="username">Member Names <span style="color:red">*</span></label>
                                                    <input class="form-control" placeholder="" type="text" name="mname" id="mname" value="{{{ old('mname') }}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="username">ID Number</label>
                                                    <input class="form-control numberInput" maxlength="8" placeholder="" type="text" name="mid_number" id="mid_number" value="{{{ old('mid_number') }}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="username">Gender</label><br>
                                                    <input class=""  type="radio" name="gender" id="gender" value="M"> Male
                                                    <input class=""  type="radio" name="gender" id="gender" value="F"> Female
                                                </div>
                                            </fieldset>
                                        </div>


                                        <div class="col-lg-4">

                                            <fieldset>
                                                <div class="form-group">
                                                    <label for="username">Phone Number <span style="color:red">*</span></label>
                                                    <input class="form-control numberInput" maxlength="10" placeholder="" type="text" name="phone" id="phone" value="{{{ old('phone') }}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="username">Email Address</label>
                                                    <input class="form-control" placeholder="" type="email" name="email" id="email" value="{{{ old('email') }}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="username">Address</label>
                                                    <textarea class="form-control"  name="address" id="address">{{{ old('email') }}}</textarea>
                                                </div>
                                            </fieldset>


                                        </div>


                                        <div class="col-lg-4">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label for="username">Monthly Remmitance Amount</label>
                                                    <input class="form-control numberInput" placeholder="" type="text" name="monthly_remittance_amount" id="monthly_remittance_amount" value="{{{ old('monthly_remittance_amount') }}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="username">Bank</label>
                                                    <select name="bank_id" id="bank_id" class="form-control">
                                                        <option></option>
                                                        @foreach($banks as $bank)
                                                            <option value="{{ $bank->id }}"> {{ $bank->bank_name }}</option>
                                                        @endforeach

                                                    </select>

                                                </div>

                                                <div class="form-group">
                                                    <label for="username">Bank Branch</label>
                                                    <select name="bbranch_id" id="bbranch_id" class="form-control">
                                                        <option></option>
                                                    </select>

                                                </div>

                                                <div class="form-group">
                                                    <label for="username">Bank Account Number</label>
                                                    <input class="form-control numberInput" placeholder="" type="text" name="bank_acc" id="bank_acc" value="{{{ old('bank_acc') }}}">
                                                </div>


                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="1" name="share_account" checked>
                                                        Open Share Account
                                                    </label>
                                                </div>


                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="1" name="active" checked>
                                                        Active
                                                    </label>
                                                </div>


{{--                     <div class="form-group">--}}
{{--                        <label for="username">Open Savings Account (<i>press ctrl and click to select multiple</i>)</label>--}}

{{--                        <select multiple name="saving_account" class="form-control">--}}

{{--                            @foreach($savingproducts as $sacc)--}}
{{--                                                <option value="{{ $sacc->id}}"> {{ $sacc->name}}</option>--}}
{{--                            @endforeach--}}

{{--                                                </select>--}}

{{--                                             </div>--}}



                                            </fieldset>


                                        </div>
                                    </div>


                                    <div class="row">


                                    <div class="col-lg-12"><hr></div></div>

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <a name="docs" href="#docs" style="background-color:green;border-radius:3px;color:white;padding:10px;text-decoration:none;" id="add">Add Member Documents</a><br><br>
                                            <div id="items"></div>
                                        </div>

                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $("body").on("click", "#add", function (e) {
                                                    //Append a new row of code to the "#items" div
                                                    $("#items").append('<div><input name="path[]" type="file" /><button type="button" style="background-color:green;color:white" id="add">Add </button>&nbsp;&nbsp;|&nbsp;&nbsp;<button class="delete btn-danger">Remove</button></div>');
                                                });

                                                $("body").on("click", ".delete", function (e) {
                                                    $(this).parent("div").remove();
                                                });

                                            });
                                        </script>

                                    </div>


                                    <div class="row">
                                        <div class="col-lg-12"><hr></div></div>

                                    <div class="col-lg-12">
                                        <h4 style="align-content: center"><strong>Next of Kin</strong></h4>
                                        <div class="table-responsive dt-responsive" >
                                            <table class="table table-striped table-bordered" id="nextkin">
                                                <tr>
                                                    <th><input class='ncheck_all' type='checkbox' onclick="select_all()"/></th>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Goodwill (%)</th>
                                                    <th>ID Number</th>
                                                    <th>Relationship</th>
                                                    <th>Contact</th>
                                                </tr>


                                                <tr>
                                                    <td><input type='checkbox' class='tabledit-input form-control input-sm ncase'/></td>
                                                    <td><span id='nsnum'>1.</span></td>
                                                    <td class="tabledit-view-mode"><span class="tabledit-span">{{{ old('kin_first_name[0]') }}}</span>
                                                        <input class="tabledit-input form-control input-sm" type="text" id='name' name='name[0]' value="{{{ old('kin_first_name[0]') }}}" placeholder="Enter Kin Name"/></td>
                                                    <td class="tabledit-view-mode"><span class="tabledit-span">{{{ old('goodwill[0]') }}}</span>
                                                        <input class="tabledit-input form-control input-sm" type='text' id='goodwill' name='goodwill[0]' value="{{{ old('goodwill[0]') }}}" placeholder="Enter % Goodwill"/></td>
                                                    <td class="tabledit-view-mode"><span class="tabledit-span">{{{ old('id_number[0]') }}}</span>
                                                        <input class="tabledit-input form-control input-sm" type='text' id='id_number' name='id_number[0]' value="{{{ old('id_number[0]') }}}" placeholder="Enter Kin Id Number"/> </td>
                                                    <td class="tabledit-view-mode"><span class="tabledit-span">{{{ old('relationship[0]') }}}</span>
                                                        <input class="tabledit-input form-control input-sm" type='text' id='relationship' name='relationship[0]' value="{{{ old('relationship[0]') }}}" placeholder="Enter Relationship With Kin"/></td>
                                                    <td class="tabledit-view-mode"><span class="tabledit-span">{{{ old('contact[0]') }}}</span>
                                                        <textarea class="tabledit-input form-control input-sm" name="contact[0]" id="contact">{{{ old('contact[0]') }}}</textarea></td>
                                                </tr>

                                            </table>
                                            <button type="button" class="btn btn-primary waves-effect waves-light add" onclick="add_nk_row();" >Add Row</button>
                                            <button type="button" class="btn btn-danger waves-effect waves-light ndelete" >Delete Row</button>

                                        </div>
                                        <script>
                                            $(".ndelete").on('click', function() {
                                                if($('.ncase:checkbox:checked').length > 0){
                                                    if (window.confirm("Are you sure you want to delete this kin detail(s)?"))
                                                    {
                                                        $('.ncase:checkbox:checked').parents("#nextkin tr").remove();
                                                        $('.ncheck_all').prop("checked", false);
                                                        check();
                                                    }else{
                                                        $('.ncheck_all').prop("checked", false);
                                                        $('.ncase').prop("checked", false);
                                                    }
                                                }
                                            });
                                            var i=2;
                                            $(".naddmore").on('click',function(){
                                                count=$('#nextkin tr').length;
                                                var data="<tr><td><input type='checkbox' class='ncase'/></td><td><span id='nsnum"+i+"'>"+count+".</span></td>";
                                                data +="<td><input class='tabledit-input form-control input-sm' type='text' id='name"+i+"' name='name["+(i-1)+"]' value='{{{ old('name["+(i-1)+"]') }}}'/></td><td><input class='tabledit-input form-control input-sm' type='text' id='goodwill"+i+"' name='goodwill["+(i-1)+"]' value='{{{ old('goodwill["+(i-1)+"]') }}}'/></td><td><input class='tabledit-input form-control input-sm' type='text' id='id_number"+i+"' name='id_number["+(i-1)+"]' value='{{{ old('id_number["+(i-1)+"]') }}}'/></td><td><input class='tabledit-input form-control input-sm' type='text' id='relationship"+i+"' name='relationship["+(i-1)+"]' value='{{{ old('relationship["+(i-1)+"]') }}}'/></td><td><textarea class='tabledit-input form-control input-sm' name='contact["+(i-1)+"]' id='contact"+i+"'>{{{ old('contact["+(i-1)+"]') }}}</textarea></td>";
                                                $('#nextkin').append(data);
                                                i++;
                                            });



                                            function select_all() {
                                                $('input[class=ncase]:checkbox').each(function(){
                                                    if($('input[class=ncheck_all]:checkbox:checked').length == 0){
                                                        $(this).prop("checked", false);
                                                    } else {
                                                        $(this).prop("checked", true);
                                                    }
                                                });
                                            }

                                            function check(){
                                                obj=$('#nextkin tr').find('span');
                                                $.each( obj, function( key, value ) {
                                                    id=value.id;
                                                    $('#'+id).html(key+1);
                                                });
                                            }

                                        </script>

                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12"><hr></div></div>

                                    <div class="col-lg-12">
                                        <h4 style="align-content: center;"><strong>Assign Vehicle</strong></h4>
                                        <div class="table-responsive" >
                                            <table class="table table-striped table-bordered" id="vehicle" >
                                                <tr>
                                                    <th><input class='vcheck_all' type='checkbox' onclick="select_all()"/></th>
                                                    <th>#</th>
                                                    <th>Registration number</th>
                                                    <th>Make</th>
                                                    <th>Fee</th>
                                                </tr>


                                                <tr>
                                                    <td><input type='checkbox' class='vcase'/></td>
                                                    <td><span id='vsnum'>1.</span></td>
                                                    <td class="tabledit-view-mode"><input class="tabledit-input form-control input-sm vehdata"  type='text' id='regno' name='regno[0]' value="{{{ old('regno[0]') }}}"/></td>
                                                    <td class="tabledit-view-mode"><input class="tabledit-input form-control input-sm vehdata" type='text' id='make' name='make[0]' value="{{{ old('make[0]') }}}"/></td>
                                                    <td class="tabledit-view-mode">
                                                        <select class="tabledit-input form-control input-sm vehdata"  id='fee' name='fee[0]'>
                                                            @foreach($charges as $charge)
                                                                <option value="{{$charge->id}}">{{$charge->name. '-'.$charge->amount}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>

                                                </tr>

                                            </table>
                                            <button type="button" class="btn btn-primary waves-effect waves-light add vaddmore" onclick="add_ve_row();" >Add Row</button>
                                            <button type="button" class="btn btn-danger waves-effect waves-light vdelete" >Delete Row</button>

                                            {{-- <button type="button" class='vdelete'>- Delete</button>
                                            <button type="button" class='vaddmore'>+ Add More</button> --}}
                                        </div>
                                        <script>
                                            $(".vdelete").on('click', function() {
                                                if($('.vcase:checkbox:checked').length > 0){
                                                    if (window.confirm("Are you sure you want to delete this vehicle(s)?"))
                                                    {
                                                        $('.vcase:checkbox:checked').parents("#vehicle tr").remove();
                                                        $('.vcheck_all').prop("checked", false);
                                                        check();
                                                    }else{
                                                        $('.vcheck_all').prop("checked", false);
                                                        $('.vcase').prop("checked", false);
                                                    }
                                                }
                                            });
                                            var i=2;
                                            $(".vaddmore").on('click',function(){
                                                count=$('#vehicle tr').length;
                                                var data="<tr><td><input type='checkbox' class='vcase'/></td><td><span id='vsnum"+i+"'>"+count+".</span></td>";
                                                data +="<td><input class='vehdata' type='text' id='regno"+i+"' name='regno["+(i-1)+"]' value='{{{ old('regno["+(i-1)+"]') }}}'/></td><td><input class='vehdata' type='text' id='make"+i+"' name='make["+(i-1)+"]' value='{{{ old('make["+(i-1)+"]') }}}'/></td>";
                                                data +="<td><select class='vehdata' id='fee"+i+"' name='fee["+(i-1)+"]'>";
                                                @foreach($charges as $charge)
                                                    data +="<option value='{{$charge->id}}'>{{$charge->name. '-'.$charge->amount}}</option>";
                                                @endforeach
                                                    data +="</select></td>";
                                                $('#vehicle').append(data);
                                                i++;
                                            });

                                            function select_all() {
                                                $('input[class=vcase]:checkbox').each(function(){
                                                    if($('input[class=vcheck_all]:checkbox:checked').length == 0){
                                                        $(this).prop("checked", false);
                                                    } else {
                                                        $(this).prop("checked", true);
                                                    }
                                                });
                                            }

                                            function check(){
                                                obj=$('#vehicle tr').find('span');
                                                $.each( obj, function( key, value ) {
                                                    id=value.id;
                                                    $('#'+id).html(key+1);
                                                });
                                            }

                                        </script>

                                    </div>

                                    <div class="row">

                                        <div class="col-lg-12"><hr>
                                        </div>
                                    </div>


                                    <div class="row">


                                        <div class="col-lg-4 pull-right">

                                            <fieldset>



                                                <div class="form-actions form-group">

                                                    <button type="submit" class="btn btn-primary btn-sm">Create Member</button>
                                                </div>

                                            </fieldset>
                                        </div>

                                    </div>
                                </form>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
@stop
