@extends('layouts.ports')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3> Member Reports</h3>
                        </div>
                        <div class="card-block">

                            <ul>
                                <li>
                                    <a href="{{ url('reports/listing') }}"> Members Listing report</a>
                                </li>
                                <li>
                                    <a href="{{url('reports/remittance')}}" target="_blank"> Members monthly Remittance Schedule report</a>
                                </li>
                                <li>
                                    <a href="{{url('reports/combined')}}"> Members Comprehensive Statement</a>
                                </li>
                                <li>
                                    <a href="{{url('reports/projects')}}" target="_blank">Project Statements</a>
                                </li>
                                <li>
                                    <a href="{{url('reports/allotment')}}" target="_blank">Allotment Certificates</a>
                                </li>
                                <li>
                                    <a href="{{url('reports/blank')}}" target="_blank">Blank report template</a>
                                </li>
                                <li>

                                    <a href="{{url('reports/members')}}"> Members` Vehicle statements report</a>

                                </li>
                                <li>

                                    <a href="{{url('reports/member/payroll')}}"> Members statements report</a>

                                </li>
                                <li>

                                    <a href="{{url('reports/deduction')}}">Monthly Deduction Report</a>

                                </li>
                                <li>

                                    <!--

                                          <li>

                                            <a href="reports/combined" target="_blank">Combined Member Statement</a>

                                          </li>
                                    -->

                                <li>
                                    <a href="{{url('reports/deduction')}}">Monthly Deduction Report</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
