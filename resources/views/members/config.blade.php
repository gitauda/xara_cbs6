@extends('layouts.member')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if(Session::get('notice'))
                            <div class="alert alert-success">{{ Session::get('notice') }}</div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Member configuration</h3>


                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{ url('member_config') }}" accept-charset="UTF-8" enctype="multipart/form-data">@csrf
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="username">Inactivity duration in months </label>
                                        <input class="form-control numbers"  placeholder="months" type="text" name="inactivity_duration" id="" value="{{$inactivity_duration[0]}}">
                                        <br/>
                                        <input type='submit' class='btn btn-primary waves-effect waves-light' name='subiduration' value='update'>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
