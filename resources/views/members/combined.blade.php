@extends('layouts.ports')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if ($errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3> Combined Member Reports</h3>
                        </div>
                        <div class="card-block">

                            <form method="post" action="{{url('reports/combinedstatement')}}">@csrf
                                <div class="form-group">
                                    <label for="username">Member
                                        <select class="form-control" name="member" required>
                                            <option value="">-----select member-------</option>
                                            <!--<option>-----------------------------------------</option>-->
                                            @foreach($members as $member)
                                                <option value="{{$member->id}}">{{$member->membership_no}}
                                                    &nbsp;&nbsp; {{ ucwords($member->name)}}</option>
                                            @endforeach
                                        </select>
                                    </label>
                                </div>
                                <div class="form-actions form-group">
                                    <button type="submit" class="btn btn-primary btn-sm">View Report</button>
                                </div>
                            </form>            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
