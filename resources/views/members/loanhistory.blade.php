@extends('layouts.member')
@section('xara_cbs')
    <?php
    function asMoney($value)
    {
        return number_format($value, 2);
    }
    ?>


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>{{ $member->name}}</h3>
                            <small>Loan History</small><br><br>
                            <div class="card-header-right">
                                <a class="dt-button btn-sm" target="_blank" href="{{ url('members/loanhistory/'.$member->id.'/report') }}">Report</a>
                            </div>

                        </div>


                        <div class="card-block col-lg-10 center-block">
                        @foreach($member->loanaccounts as $loanaccount)
                            <h4>{{ $loanaccount->account_number }}</h4>

                                <div class="dt-responsive table-responsive">
                                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                        <tr>
                                            <td>Loan Type</td>
                                            <td>{{ $loanaccount->loanproduct->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Date Disbursed</td>
                                            <td>{{ $loanaccount->date_disbursed}}</td>
                                        </tr>
                                        <tr>
                                            <td>Amount Disbursed</td>
                                            <td>{{ asMoney($loanaccount->amount_disbursed)}}</td>
                                        </tr>
                                        @if($loanaccount->is_top_up)
                                            <tr>
                                                <td>Top Up Amount</td>
                                                <td>{{ asMoney($loanaccount->top_up_amount)}}</td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>

                                <div class="dt-responsive table-responsive">
                                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Description</th>
                                            <th>Cr</th>
                                            <th>Dr</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($loanaccount->loantransactions as $loantransaction)
                                            <tr>
                                                <td>{{ $loantransaction->date }}</td>
                                                <td>{{ $loantransaction->description }}</td>
                                                @if($loantransaction->type == 'credit')
                                                    <td>{{ asMoney($loantransaction->amount) }}</td>
                                                    <td>0.00</td>
                                                @else
                                                    <td>0.00</td>
                                                    <td>{{ asMoney($loantransaction->amount) }}</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endforeach
                            </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>


@endsection
