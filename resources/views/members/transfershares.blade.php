@extends('layouts.main')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3> Transfer shares  of a member</h3>


                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{ url('shares/transfer') }}}" accept-charset="UTF-8">@csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="username">FROM
                                            <select class="form-control" name="member1" required>
                                                <option value="">select member from</option>
                                                <option>-----------------------------------------</option>
                                                @foreach($members as $memberr)
                                                    <option value="{{$memberr->id}}">{{$memberr->membership_no}}
                                                        &nbsp;&nbsp; {{ ucwords($memberr->name)}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">TO:
                                            <select class="form-control" name="member2" required>
                                                <option value="">select member to </option>
                                                <option>-----------------------------------------</option>
                                                @foreach($members as $memberr)
                                                    <option value="{{$memberr->id}}">{{$memberr->membership_no}}
                                                        &nbsp;&nbsp; {{ ucwords($memberr->name)}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label for="dropper-default"> Date</label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input class="form-control" placeholder="" readonly type="text" name="date" id="dropper-default" value="{{{ old('date') }}}" required>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="amount"> Amount</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount" value="{{{ old('amount') }}}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="username"> Description</label>
                                        <textarea class="form-control" name="description">{{{ old('description') }}}</textarea>

                                    </div>


                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                                    </div>


                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
