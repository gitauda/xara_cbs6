@extends('layouts.member')
@section('xara_cbs')
    <br/>
    <?php
    function asMoney($value)
    {
        return number_format($value, 2);
    }
    ?>


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                            @if(Session::get('notice'))
                                <div class="alert alert-success">{{ Session::get('notice') }}</div>
                            @endif
                            <div class="card-header">
                                <h3>Summary</h3>

                                <div class="card-header-right">
                                    <a class="dt-button btn-sm" href="{{ url('members/edit/'.$member->id)}}">Update Details</a>
                                    <a class="dt-button btn-sm" href="{{ url('members/summary/'.$member->id)}}">Summary</a>
                                </div>

                            </div>
                            <div class="row">
                                <div class="card-block">
                                    <img src="{{  asset('public/uploads/photos/'.$member->photo)}}" width="150px" height="130px" alt="no photo">
                                    <img src="{{  asset('public/uploads/photos/'.$member->signature)}}" width="120px" height="50px" alt="no signature">
                                </div>

                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table class="table table-condensed table-bordered table-hover">
                                            <tr>
                                                <td>Member Name</td>
                                                <td>{{ $member->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Membership Number</td>
                                                <td>{{ $member->membership_no}}</td>
                                            </tr>
                                            @if($member->branch != null)
                                                <tr>
                                                    <td>Branch</td>
                                                    <td>{{ $member->branch->name}}</td>
                                                </tr>
                                            @endif
                                            @if($member->group != null)
                                                <tr>
                                                    <td>Group</td>
                                                    <td>{{ $member->group->name}}</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td>ID Number</td>
                                                <td>{{ $member->id_number}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                            </div>

                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table class="table table-condensed table-bordered table-hover">

                                        <tr>
                                            <td>Deposits</td>
                                            <td>
                                                @foreach($member->savingaccounts as $savingaccount)
                                                    <strong>{{ $savingaccount->savingproduct->name }}</strong>
                                                    : {{ asMoney(App\models\Savingaccount::getAccountBalance($savingaccount)) }}<br>
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Loans</td>
                                            <td>
                                                @foreach($member->loanaccounts as $loanaccount)
                                                    @if($loanaccount->is_disbursed == 1)
                                                        <?php $amountgiven= $loanaccount->amount_disbursed; $dategiven= $loanaccount->date_disbursed;
                                                        $lastpaid= App\models\Loanrepayment::lastRepayment($loanaccount);
                                                        ?>
                                                        <div class='oneloanproduct' lang='{{$loanaccount->id}}' src='{{$amountgiven}}' for='{{$dategiven}}' href='{{$lastpaid}}' data-toggle="modal" data-target="#myModal">
                                                            <strong>
                                                                {{ $loanaccount->account_number }} - {{ $loanaccount->loanproduct->name }}
                                                            </strong>
                                                            : {{ asMoney(App\models\Loanaccount::getPrincipalBal($loanaccount)) }}
                                                        </div><br>
                                                    @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Shares</td>
                                            <td>{{ App\models\Shareaccount::getShares($member->shareaccount) }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <?php
    /*<div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Loan payment progress</h4>
            </div>
            <div class="modal-body">
                <span>A loan of <u>Ksh <span class='popamountgivenspan'></span></u> was given on <u><span class='popdategivenspan'></span></u></span><br>
                <span><u>Ksh <span class='amountpaid'></span></u> has been paid as of <u><?php echo date('m/d/Y'); ?></u></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>*/ ?>
    <script type="text/javascript">
        /*$(document).ready(function(){
                $('.oneloanproduct').on('click',function(){
                    var loanaccount=$(this).attr('lang');
                    var amountgiven=$(this).attr('src');
                    var dategiven=$(this).attr('for');
                    var lastpaid=$(this).attr('href');
                    $('.popamountgivenspan').html(amountgiven);
                    $('.popdategivenspan').html(dategiven);
                    $.get("{{ url('loanfetch')}}",{loanaccount:loanaccount},function(data){
                        $('.amountpaid').html(data);
                });

            });
    });*/
    </script>
@stop
