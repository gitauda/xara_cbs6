@extends('layouts.member')
@section('xara_cbs')

    <?php
    function asMoney($value) {
        return number_format($value, 2);
    }

    ?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">


                        <div class="card">
                            @if(Session::get('notice'))
                                <div class="alert alert-success">{{ Session::get('notice') }}</div>
                            @endif
                            <div class="card-header">
                                <h5>Members</h5>

                                <div class="card-header-right">
                                    <a class="dt-button btn-sm" href="{{ url('members/edit/'.$member->id)}}">Update Details</a>
                                    <a class="dt-button btn-sm" href="{{ url('members/summary/'.$member->id)}}">Summary</a>
                                </div>

                            </div>
                            <div class="row">
                                <div class="card-block">
                                    <img src="{{  asset('public/uploads/photos/'.$member->photo)}}" width="150px" height="130px" alt="no photo">
                                    <img src="{{  asset('public/uploads/photos/'.$member->signature)}}" width="120px" height="50px" alt="no signature">
                                </div>
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table class="table table-condensed table-bordered table-hover">
                                            <tr>

                                                <td>Member Name</td><td>{{ $member->name}}</td>


                                            </tr>
                                            <tr>

                                                <td>Membership Number</td><td>{{ $member->membership_no}}</td>


                                            </tr>
                                            @if($member->member_type != null)
                                                <tr>
                                                    <td>Member Type</td><td>{{ $member->member_type}}</td>
                                                </tr>
                                            @endif
                                            @if($member->branch != null)
                                                <tr>
                                                    <td>Branch</td><td>{{ $member->branch->name}}</td>
                                                </tr>
                                            @endif

                                            @if($member->group != null)
                                                <tr>

                                                    <td>Group</td><td>{{ $member->group->name}}</td>


                                                </tr>
                                            @endif

                                            <tr>

                                                <td>ID Number</td><td>{{ $member->id_number}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table class="table table-condensed table-bordered table-hover">
                                            <tr>
                                                @if($member->gender == 'M')
                                                    <td>Gender</td><td>Male</td>
                                                @else

                                                @endif

                                                @if($member->gender == 'F')
                                                    <td>Gender</td><td>Male</td>
                                                @else

                                                @endif

                                                @if($member->gender == '')
                                                    <td>Gender</td><td></td>
                                                @else

                                                @endif

                                            </tr>

                                            <tr>

                                                <td>Phone Number</td><td>{{ $member->phone}}</td>


                                            </tr>
                                            <tr>

                                                <td>Email Address</td><td>{{ $member->email}}</td>


                                            </tr>
                                            <tr>

                                                <td>Address</td><td>{{ $member->address}}</td>


                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="card-block">


                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs  tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#remittance" role="tab">Remittance</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#kin" role="tab">Next Of Kin</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#documents" role="tab">Documents</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#loans" role="tab">Loan Accounts</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#savings" role="tab">Saving Accounts</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#shares" role="tab">Share Accounts</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content tabs card-block">
                                    <div class="tab-pane active" id="remittance" role="tabpanel">
                                        <div class="dt-responsive table-responsive">

                                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                                <tr>
                                                    <td>Monthly Saving Remittance </td><td>{{ asMoney($member->monthly_remittance_amount )}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="kin" role="tabpanel">
                                        <div class="panel-heading">
                                            <a class="btn btn-info btn-sm" href="{{ url('kins/create/'.$member->id)}}">new Kin</a>
                                        </div>
                                        <div class="dt-responsive table-responsive">
                                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                                <thead>

                                                <th>#</th>
                                                <th>Kin Name</th>
                                                <th>ID Number</th>
                                                <th>Relationship</th>
                                                <th>Goodwill</th>
                                                <th></th>

                                                </thead>
                                                <tbody>

                                                {{--                                        @foreach($member->kin as $kin)--}}

                                                {{--                                            <tr>--}}

                                                {{--                                                <td> {{ $i ?? '' }}</td>--}}
                                                {{--                                                <td>{{ $kin->name }}</td>--}}
                                                {{--                                                <td>{{ $kin->id_number }}</td>--}}
                                                {{--                                                <td>{{ $kin->rship }}</td>--}}
                                                {{--                                                <td>{{ $kin->goodwill.' %' }}</td>--}}
                                                {{--                                                <td>--}}

                                                {{--                                                    <div class="btn-group">--}}
                                                {{--                                                        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">--}}
                                                {{--                                                            Action <span class="caret"></span>--}}
                                                {{--                                                        </button>--}}

                                                {{--                                                        <ul class="dropdown-menu" role="menu">--}}
                                                {{--                                                            <li><a href="{{url('kins/edit/'.$kin->id)}}">Update</a></li>--}}

                                                {{--                                                            <li><a href="{{url('kins/delete/'.$kin->id)}}">Delete</a></li>--}}

                                                {{--                                                        </ul>--}}
                                                {{--                                                    </div>--}}

                                                {{--                                                </td>--}}



                                                {{--                                            </tr>--}}


                                                {{--                                        @endforeach--}}


                                                </tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="documents" role="tabpanel">
                                        <div class="dt-responsive table-responsive">
                                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                <th>#</th>
                                                <th>Document</th>
                                                <th>Action</th>
                                                </thead>
                                                <tbody>
                                                <?php $j=1;?>
                                                @foreach($documents as $document)
                                                    <tr class="del<?php echo $document->id; ?>">
                                                        <td>{{$j}}</td>
                                                        <td>{{$document->path}}</td>
                                                        <td><a class="btn btn-danger delbtn" id="<?php echo $document->id; ?>">Delete</a></td>
                                                    </tr>
                                                    <?php $j++;?>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="loans" role="tabpanel">
                                        <div class="panel-heading">
                                            <a class="btn btn-info btn-sm" href="{{ url('loans/apply/'.$member->id)}}">New Loan</a>
                                            <a class="btn btn-success btn-sm" href="{{ url('members/loanhistory/'.$member->id)}}">Loan History</a>
                                        </div>
                                        <div class="dt-responsive table-responsive">
                                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                                <thead>

                                                <th>#</th>
                                                <th>Loan Product</th>

                                                <th>Account Number</th>
                                                <th>Loan Amount</th>

                                                <th>Loan Balance</th>
                                                <th></th>

                                                </thead>
                                                <tbody>

                                                <?php $i = 1; ?>
                                                @foreach($member->loanaccounts as $loan)
                                                    @if($loan->is_disbursed)
                                                        <tr>

                                                            <td> {{ $i }}</td>
                                                            <td>{{ $loan->loanproduct->name }}</td>
                                                            <td>{{ $loan->account_number }}</td>
                                                            <td>{{ asMoney(App\models\Loanaccount::getLoanAmount($loan))}}</td>
                                                            <td>{{ asMoney(App\models\Loantransaction::getLoanBalance($loan) )}}</td>
                                                            <td>

                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                        Action <span class="caret"></span>
                                                                    </button>

                                                                    <ul class="dropdown-menu" role="menu">
                                                                        <li><a href="{{url('loans/show/'.$loan->id)}}">View</a></li>



                                                                    </ul>
                                                                </div>

                                                            </td>



                                                        </tr>

                                                        <?php $i++; ?>
                                                    @endif
                                                @endforeach


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="savings" role="tabpanel">
                                        <div class="panel-heading">
                                            <a class="btn btn-info btn-sm" href="{{ url('savingaccounts/create/'.$member->id)}}">New Saving Account</a>
                                        </div>
                                        <div class="dt-responsive table-responsive">
                                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                                <thead>

                                                <th>#</th>

                                                <th>Savings Product</th>
                                                <th>Account Number</th>

                                                <th></th>

                                                </thead>

                                                <tbody>

                                                <?php $i = 1; ?>

                                                @foreach($member->savingaccounts as $saving)

                                                    <tr>

                                                        <td> {{ $i }}</td>

                                                        <td>{{ $saving->savingproduct->name }}</td>
                                                        <td>{{ $saving->account_number }}</td>
                                                        <td> <a href="{{ url('savingtransactions/show/'.$saving->id)}}" class="btn btn-primary btn-sm">View </a></td>
                                                    </tr>

                                                    <?php $i++; ?>
                                                @endforeach


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="shares" role="tabpanel">
                                        <div class="panel-heading">
                                            <h5>Share Account</h5>
                                        </div>
                                        <div class="dt-responsive table-responsive">
                                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                                <thead>

                                                <th>#</th>

                                                <th>Account Number</th>

                                                <th></th>

                                                </thead>

                                                <tbody>

                                                <tr>
                                                    <td> 1</td>
                                                    <td>{{ $member->shareaccount->account_number }}</td>
                                                    <td> <a href="{{ url('sharetransactions/show/'.$member->shareaccount->id)}}" class="btn btn-primary btn-sm">View </a></td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- [ page content ] end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.delbtn').click( function() {

                var id = $(this).attr("id");

                if(confirm("Are you sure you want to delete this document?")){
                    $.ajax({
                        type: "POST",
                        url: "{{url('deldoc')}}",
                        data: {'id': id},
                        cache: false,
                        success: function(s){
                            if(s == 0){
                                $(".del"+id).fadeOut('slow');
                            }
                        }
                    });
                }else{
                    return false;
                }
            });
        });
    </script>

@stop
