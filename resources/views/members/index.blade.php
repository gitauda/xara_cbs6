@extends('layouts.member')
@section('xara_cbs')

<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <!-- [ page content ] start -->
                <div class="card">
                    <div class="card-header">
                        <h5>Members</h5>

                     <div class="card-header-right">
                        <a class="dt-button btn-sm" href="{{ url('members/create')}}">New Member</a>
                     </div>

                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Member Number</th>
                                    <th>Member Name</th>
                                    <th>Member Branch</th>
                                    <th></th>
                                   
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($members as $member)
                                    <tr>
                                        <td> {{ $i }}</td>
                                        <td>{{ $member->membership_no }}</td>
                                        <td>{{ $member->name }}</td>
                                        <td>{{ $member->branch->name }}</td>

                                        <td>
                                            <a href="{{url('members/show/'.$member->id) }}" class="dt-button btn-sm">Manage</a>

                                        </td>
                                    </tr>

                                    <?php $i++; ?>
                                @endforeach


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- [ page content ] end -->
            </div>
        </div>
    </div>
</div>
@endsection
