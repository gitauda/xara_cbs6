@extends('layouts.loans')
@section('xara_cbs')
    <script type="text/javascript">
        $(document).ready(function() {

            $('#loanproduct_id').change(function(){
                if($(this).val() != ""){
                    $.get("{{ url('api/label')}}",
                        { option: $(this).val() },
                        function(data) {
                            $( "label#amt" ).replaceWith( '<label for="username" id="amt">Amount Applied (Instant Loan amount '+data+') <span style="color:red">*</span></label>');
                        });
                }else{
                    $( "label#amt" ).replaceWith( '<label for="username" id="amt">Amount Applied</label>');
                }
            });
            //auto-fill the repayment duration field when the loan product field is filled.
            $('.lproductinput').change(function(){
                var lpvalue=$(this).val();
                $.get("{{ url('ajaxfetchrduration')}}",
                    {lpvalue:lpvalue},
                    function(data) {
                        $( ".rduration" ).val(data);
                    });
            });

            $('.selectguarantor').change(function(){
                var lpvalue=$(this).val(); var hii=$(this);
                $.get("{{ url('ajaxfetchguarantorbal')}}",
                    {lpvalue:lpvalue},
                    function(data){
                        $(hii).siblings( ".savingsbalspan" ).html("Savings balance : "+data);
                    });
            });

            $('.lproductinput').change(function(){
                var lpvalue=$(this).val();
                var savings=$('.maxamountinput').attr('lang');
                $.get("{{ url('ajaxfetchmaxamount')}}",
                    {lpvalue:lpvalue},
                    function(data) {
                        var savings=$('.maxamountinput').attr('lang'); var max=parseInt(savings)*parseInt(data);
                        $('.maxamountinput').attr("src",max);
                    });
            });

            $('.guarantoramount').keyup(function(){
                var lpvalue=$(this).val(); var savings=$('.maxamountinput').attr('lang');
                var max=$('.maxamountinput').attr("src");
                if (max !=="") {$('.maxamountinput').val(max);}
                if(lpvalue<1 || lpvalue==""){ $('.maxamountinput').val(savings);}
                var total_guaranteed=0;
                $('.guarantoramount').each(function(){
                    var thisval=$(this).val(); if(thisval==""){thisval=0;}
                    total_guaranteed+=parseInt(thisval);
                });      var actualmaxamount=parseInt(savings)+total_guaranteed;

                $('.actualmaxamount').val(actualmaxamount);

                if(actualmaxamount>max & max !==""){$('.maxamountinput').val(actualmaxamount);}
            });


            var max_fields      = 10; //maximum input boxes allowed
            var wrapper         = $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

            var selectwrapper         = $(".select_fields_wrap"); //Fields wrapper
            var select_add_button      = $(".select_add_field_button"); //Add button ID

            var purposewrapper         = $(".purpose_fields_wrap"); //Fields wrapper
            var purpose_add_button      = $(".add_purpose_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div style="margin-top:10px;"><div class="col-lg-11 form-group" style="margin-left:-15px;"><input class="form-control" type="text" name="securities[]"/></div><a style="margin-top:10px;margin-left:-20px" href="#" class="col-lg-1 remove_field">Remove</a></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            });


            var z = 1; //initlal text box count
            $(purpose_add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(z < max_fields){ //max input box allowed
                    z++; //text box increment
                    $(purposewrapper).append('<div style="margin-top:10px;"><div class="col-lg-11 form-group" style="margin-left:-15px;"><textarea class="form-control" name="purposes[]" style="margin-bottom:10px"></textarea><input class="form-control" placeholder="" type="text" name="amount[]" id="amount"></div><a style="margin-top:70px;margin-left:-20px" href="#" class="col-lg-1 remove_purpose_field">Remove</a></div>'); //add input box
                }
            });

            $(purposewrapper).on("click",".remove_purpose_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); z--;
            });


            var y = 1; //initlal text box count
            $(select_add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(y < max_fields){ //max input box allowed
                    y++; //text box increment
                    $(selectwrapper).append('<div>'+

                        '<div style="margin-left:-15px;" class="form-group col-lg-11"><select class="form-control selectguarantor" name="guarantor_id[]">'+
                        '<option value="">select member</option>'+
                        '<option>--------------------------</option>'+
                        '@foreach($guarantors as $guarantor)'+
                        "<option value='{{$guarantor->id}}'>{{ $guarantor->membership_no  }} {{ $guarantor->name }}</option>"+
                        ' @endforeach'+
                        '</select>'+
                        '<div class="savingsbalspan"></div>'+
                        '<input type="number" class="guarantoramount numbers" name="guarantoramount[]" placeholder="guarantee amount">'+
                        '</div>'+
                        '<a href="#" style="margin-top:10px;margin-left:-15px" class="col-lg-1 select_remove_field">Remove</a>'

                        +'</div>'); //add input box
                }
                $('.selectguarantor').change(function(){
                    var lpvalue=$(this).val(); var hii=$(this);
                    $.get("{{ url('ajaxfetchguarantorbal')}}",
                        {lpvalue:lpvalue},
                        function(data){
                            $(hii).siblings( ".savingsbalspan" ).html("savings balance : "+data);
                        });
                });
                $('.guarantoramount').keyup(function(){
                    var lpvalue=$(this).val(); var savings=$('.maxamountinput').attr('lang');
                    var max=$('.maxamountinput').attr("src");
                    if (max !=="") {$('.maxamountinput').val(max);}
                    if(lpvalue<1 || lpvalue==""){ $('.maxamountinput').val(savings);}
                    var total_guaranteed=0;
                    $('.guarantoramount').each(function(){
                        var thisval=$(this).val(); if(thisval==""){thisval=0;}
                        total_guaranteed+=parseInt(thisval);
                    });   var actualmaxamount=parseInt(savings)+total_guaranteed;
                    $('.actualmaxamount').val(actualmaxamount);
                    if(actualmaxamount>max & max !==""){$('.maxamountinput').val(actualmaxamount);}
                });
            });


            $(selectwrapper).on("click",".select_remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); y--;
            });
            /*    function lpchanged(){
                    var lpvalue=$(this).val();
                    $.ajax({
                        url:"ajaxfetchrduration",
                        type:"get",
                        data:{lpvalue:lpvalue},
                        success:function(response){
                            $('.rduration').val(response); alert(response);
                        }
                    });
                }
                $('.lproductinput').on('change',lpchanged);*/

        });
    </script>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if(Session::has('glare'))
                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{ Session::get('glare') }}</strong>
                            </div>
                        @endif
                        @if (count($errors)> 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Loan Application</h3>


                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{ url('loans/apply') }}}" accept-charset="UTF-8"
                                  enctype="multipart/form-data">@csrf
                                <fieldset>
                                    <input class="form-control" placeholder="" type="hidden" name="member_id" id="member_id" value="{{$member->id}}">
                                    <div class="form-group">
                                        <label for="username">Loan Product</label>
                                        <select class="form-control lproductinput" name="loanproduct_id" id="loanproduct_id">
                                            <option value="">select product</option>
                                            <option>--------------------------</option>
                                            @foreach($loanproducts as $loanproduct)
                                                <option value="{{$loanproduct->id}}">{{ $loanproduct->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Application Date </label>
                                        <div class="right-inner-addon ">
                                            <i class="fa fa-calendar"></i>
                                            <input class="form-control datepicker" readonly placeholder="" type="text" name="application_date" id="application_date" value="{{date('Y-m-d')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Maximum amount</label>
                                        <input class="form-control numbers maxamountinput" readonly placeholder="" type="text" lang={{$savingsbal}} src='' name="maxamount" id="amount_applied" value={{$savingsbal}}>
                                        <input type='hidden' class='actualmaxamount' value={{$savingsbal}} name='actualmaxamount'>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Amount Applied</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="amount_applied" id="amount_applied" value="{{{ old('amount_applied') }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Repayment Period(months)</label>
                                        <input class="form-control numbers rduration" placeholder="" type="text" name="repayment_duration" id="repayment_duration" value="{{{ old('repayment_duration') }}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Guarantor Matrix Copy</label>
                                        <input  type="file" name="scanned_copy" id="signature" >
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Guarantor </label>
                                        <div class="select_fields_wrap">
                                            <button class="btn btn-info select_add_field_button">Add More</button>
                                            <div style="margin-top:10px;margin-bottom:10px;"><div class="form-group col-lg-11" style="margin-left:-15px;">
                                                    <select class="form-control selectguarantor" name="guarantor_id[]">
                                                        <option value="">select member</option>
                                                        <option>--------------------------</option>
                                                        @foreach($guarantors as $guarantor)
                                                            <option value="{{$guarantor->id}}">{{ $guarantor->membership_no  }} {{ $guarantor->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class='savingsbalspan'></div>
                                                    <input class='guarantoramount numbers' name='guarantoramount[]' placeholder='guarantee amount'>
                                                </div></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Guarantor Matrix</label>
                                        <select class="form-control" name="matrix">
                                            <option value="">select guarantor matrix</option>
                                            <option>--------------------------</option>
                                            @foreach($matrix as $loanproduct)
                                                <option value="{{$loanproduct->id}}">{{ $loanproduct->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="disbursement_id">Disbursement Option</label>
                                        <select name="disbursement_id" id="disbursement_id" class="form-control"
                                                required="required">
                                            <option></option>
                                            @foreach($disbursed as $disburse)
                                                <option value="{{$disburse->id }}"> {{ $disburse->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">Submit Application</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
