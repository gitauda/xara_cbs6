@extends('layouts.member')
@section('xara_cbs')
    <style>
         .ledgerdetails{display:flex; flex-direction:column; justify-content:space-around;
                flex-wrap:wrap; padding:5px 15px; box-sizing:border-box;
        }        .lrow{padding:1px 5px; box-sizing:border-box;}
        .col-lg-4dets{
            width:30%; box-sizing:border-box;
        }
        .ledger_table th,.ledger_table td{width:100px; padding:5px; text-align:center; box-sizing:border-box; font-size:14px;}
       .panelo{background-color:; padding-bottom:20px; height:auto; display:block; overflow-y:hidden; overflow-x:scroll;}
        @media only screen and (max-width: 1035px) {
            .col-lg-4dets{
                width:100%; box-sizing:border-box;
            }
        }
    </style>
    <br/>
    <?php
    function asMoney($value)
    {
        return number_format($value, 2);
    }
    ?>
    <div class="row">
        <div class="col-lg-4">
            <table class="table table-hover">
                <tr>
                    <td>Member</td>
                    <td>{{ $loanaccount->member->name }}</td>
                </tr>
                <tr>
                    <td>Loan Account</td>
                    <td>{{ $loanaccount->account_number }}</td>
                </tr>
            </table>
        </div>
        <div class="col-lg-8 pull-right">
            @if (Session::has('completed'))
                <div class="alert alert-info alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{{ Session::get('completed') }}}</strong>
                </div>
            @endif
            @if (Session::has('recover'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{{ Session::get('recover') }}}</strong>
                </div>
            @endif
            @if (Session::has('convert'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{{ Session::get('convert') }}}</strong>
                </div>
            @endif
            @if (Session::has('flash_message'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{{ Session::get('flash_message') }}}</strong>
                </div>
            @endif
            @if (Session::has('delete_message'))
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{{ Session::get('delete_message') }}}</strong>
                </div>
            @endif
        </div>
        <div class="col-lg-5 pull-right">
            <a class="btn btn-success btn-sm" href="{{ URL::to('loanrepayments/create/'.$loanaccount->id) }}"> <span
                        class="glyphicon glyphicon-file" aria-hidden="true"></span> Repay Loan</a>
            <a class="btn btn-success btn-sm" href="{{ URL::to('loanrepayments/offset/'.$loanaccount->id) }}"> <span
                        class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Offset Loan</a>

            <a class="btn btn-success btn-sm" href="{{ URL::to('loanaccounts/topup/'.$loanaccount->id) }}"> <span
                        class="glyphicon glyphicon-download" aria-hidden="true"></span> Top up Loan</a>
        </div>
        <div class="col-lg-5 pull-right" style="margin-top: 1.8%;">
            <a class="btn btn-primary btn-sm" href="{{ URL::to('loanrepayments/recover/'.$loanaccount->id) }}">
                <span class="fa fa-hand-paper-o" aria-hidden="true"></span> Recover Loan</a>
            <a class="btn btn-warning btn-sm" href="{{ URL::to('loanrepayments/convert/'.$loanaccount->id) }}">
                <span class="fa fa-line-chart" aria-hidden="true"></span> Convert Loan
            </a>
            <a class="btn btn-info btn-sm" href="{{ URL::to('loanrepayments/refinance/'.$loanaccount->id) }}">
                <span class="fa fa-plus-circle" aria-hidden="true"></span> Loan Refinance
            </a>
            @if(Loantransaction::getLoanBalance($loanaccount)<=0)
                <a class="btn btn-info btn-sm" href="{{ URL::to('loantransactions/certificate/'.$loanaccount->id)}}"
                   target="_blank">
                    <span class="fa fa-certificate" aria-hidden="true"></span>
                    Certificate
                </a>
            @endif
        </div>
    </div>
    <hr>
    <div class="row">
        @if ($errors->has())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}<br>
                @endforeach
            </div>
        @endif

        <div class="col-lg-4 col-lg-4dets">
            <table class="table table-bordered table-hover">
                <tr>
                    <td>Loan Type</td>
                    <td>{{ $loanaccount->loanproduct->name}}</td>
                </tr>
                <tr>
                    <td>Date Disbursed</td>
                    <td>{{ $loanaccount->date_disbursed}}</td>
                </tr>
                <tr>
                    <td>Amount Disbursed</td>
                    <td>{{ asMoney($loanaccount->amount_disbursed)}}</td>
                </tr>
                @if($loanaccount->is_top_up)
                    <tr>
                        <td>Top Up Amount</td>
                        <td>{{ asMoney($loanaccount->top_up_amount)}}</td>
                    </tr>
                @endif
            <!--
  <tr>
    <td>Interest Amount</td><td>{{ asMoney($interest)}}</td>
  </tr>
  <tr>
    <td>Loan Amount</td><td>{{ asMoney($loanaccount->amount_disbursed + $interest)}}</td>
  </tr>
-->
            </table>
        </div>
        <?php

        ?>
        <div class="col-lg-4 col-lg-4dets">
            <table class="table table-bordered table-hover">
                <tr>
                    <td>Principal Paid</td>
                    <td>{{ asMoney($principal_paid)}}</td>
                </tr>
                <tr>
                    <td>Interest Paid</td>
                    <td>{{ asMoney($interest_paid)}}</td>
                </tr>
                <tr>
                    <td>Principal Balance</td>
                    <td>{{ asMoney(App\Loanaccount::getPrincipalBal($loanaccount))}}</td>
                </tr>
                <tr>
                    <td>Loan Balance</td>
                    <td>{{asMoney(App\Loantransaction::getLoanBalance($loanaccount))}}</td>
                </tr>
            <!--
  <tr>
    <td>Interest Balance</td><td>{{ asMoney($interest - $interest_paid)}}</td>
  </tr>
-->
            </table>
        </div>
        <div class="col-lg-4 col-lg-4dets" style='display:;'>
            <table class="table table-bordered table-hover">
                <tr>
                    <td>Loan Period </td>
                    <td>{{ $loanaccount->period." months"}}</td>
                </tr>
                <tr>
                    <td>Interest rate</td>
                    <td>{{  $loanaccount->interest_rate." %"}}</td>
                </tr>
                <tr>
                    <td>Repayment Duration (Months)</td>
                    <td>
                        <form action="{{ url('loans/update-repayment-period', $loanaccount->id) }}" method="post">
                            <input type="number" name="repayment_duration" placeholder="Repayment Period"
                                   value="{{  $loanaccount->repayment_duration}}" required>
                            <br><br>
                            <button type="submit" class="btn btn-success pull-right">Update</button>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#remittance" aria-controls="remittance" role="tab"
                                                              data-toggle="tab">Loan Schedule</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Loan
                            Transactions</a></li>
                    <li role="presentation"><a href="#extraloaninfo" aria-controls="extraloaninfo" role="tab" data-toggle="tab">More information</a></li>
                    @if(Loantransaction::getLoanBalance($loanaccount)<=0)
                    <li role="presentation"><a href="#overpayments" aria-controls="overpayments" role="tab"
                                                   data-toggle="tab">Loan Overcharge</a></li>
                    @endif
                    <li role="presentation"><a href="#ledgers" aria-controls="ledgers" role="tab" data-toggle="tab">Loan documents</a></li>
                <!-- <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Loan Guarantors</a></li>-->
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="remittance">
                        <br>
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <p>Loan Schedule</p>
                                <!-- <a href="{{URL::to('loans/schedule/'.$loanaccount->id)}}" class="btn btn-success btn-sm"> <i class="glyphicon glyphicon-file"> </i> Print Schedule</a>
-->
                                </div>
                                <div class="panel-body">
                                    <table class="table table-condensed table-hover">
                                        <thead>
                                        <th>Installment #</th>
                                        <th>Date</th>
                                        <th>Principal</th>
                                        <th>Interest</th>
                                        <th>Total</th>
                                        <th>Loan Balance</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>0</td>
                                            <td>
                                                <?php
                                                $date = date("d-F-Y", strtotime($loanaccount->date_disbursed));
                                                ?>
                                                {{ $date }}
                                            </td>
                                            <?php
                                            $first_amount = $loanaccount->amount_disbursed + $loanaccount->top_up_amount;
                                            $first_rate = ($loanaccount->interest_rate) / 100;
                                            $first_interest = 0.00;
                                            $first_total = $first_amount + $first_interest;
                                           ?>
                                            <td>{{ asMoney($first_amount)}}</td>
                                            <td>{{ asMoney($first_interest)}}</td>
                                            <td>{{ asMoney($first_total)  }}</td>
                                            <td>{{ asMoney($first_total)  }}</td>
                                        </tr>
                                        <?php
                                        $dateX = strtotime($loanaccount->date_disbursed);
                                        $date = date("Y-m-t", $dateX);
                                        $dateY = strtotime($date) + 87000;
                                        //$xdate = strtotime($lastTstamp) + 87000;
                                        $date = date("t-F-Y", $dateY);
                                        $interest = Loanaccount::getInterestAmount($loanaccount);
                                        $principal = $loanaccount->amount_applied + $loanaccount->top_up_amount;
                                        $balance = $first_total;
                                        $days = 0;
                                        $totalint = 0;
                                        /*if ($loanaccount->repayment_duration != null) {
                                            $period = $loanaccount->repayment_duration;
                                        } else {
                                            $period = $loanaccount->period;
                                        }*/
                                        $period2=Loantransaction::getInstallment($loanaccount,'period');
                                        $period2=round($period2);
                                        #$principal_amount = $loanaccount->amount_applied / $period;
                                        $period = $loanaccount->period;
                                        $principal_amount=Loantransaction::getPrincipalDue($loanaccount);
                                        $total_principal = 0; $amount_given=$loanaccount->amount_disbursed;
                                        $loan_balance=(float)$loanaccount->amount_disbursed+(float)$loanaccount->top_up_amount;  $amountGiven=$loan_balance;
                                        $total_interest=0;
                                        $loanproduct=Loanproduct::findorfail($loanaccount->loanproduct_id);
		                                $formula=$loanproduct->formula; $amortization=$loanproduct->amortization;
                                        for($i = 1; $i <= $period; $i++){
                                           $installment= Loantransaction::getInstallment($loanaccount,'0');
                                           $rate=Loantransaction::getrate($loanaccount);
                                           #if($i==1){$interest_due=$amount_given*$rate;}else{
                                           $interest_due=$loan_balance*$rate;
                                           $loan_balance-=$installment;
                                            if($amortization=="EP" && $formula="SL"){
                                                $principal_due=(float)$amountGiven/(int)$period;
                                            }else{
                                                $principal_due=$installment-$interest_due;
                                            }
                                           if($i>$period2 && $period>$period2){
                                               $total_interest+=$installment;
                                           }else if($period==$period2){
                                                $total_interest=$loan_balance*-1;
                                           }
                                        ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{ $date  }}</td>
                                            <td>{{ asMoney($principal_due) }}</td>
                                            <td>
                                                {{ asMoney($interest_due)}}</td>
                                            </td>
                                            <td>{{ asMoney($principal_due + $interest_due)}}</td>
                                            <td>{{round($loan_balance,2)}}</td>
                                        </tr>
                                        <?php
                                        $days = $days + 30;
                                        $date = date('t-F-Y', strtotime($date . ' + 28 days'));
                                        }

                                        /* for($i=1; $i<=$period; $i++) { ?>
                                        <tr>
                                          <td>{{ $i }}</td>
                                          <td>
                                            {{ $date  }}
                                          </td>
                                          <td>
                                            <?php $total_principal = $total_principal + $principal_amount; ?>
                                            {{ asMoney($principal_amount)}} </td>
                                          <td>
                                            <?php
                                            if($loanaccount->loanproduct->formula == 'SL'){
                                              $interest_amount = $interest/$period;
                                            }
                                            if($loanaccount->loanproduct->formula == 'RB'){
                                              $interest_amount =((($principal - $total_principal) * ($loanaccount->interest_rate/100)));
                                            }
                                            ?>
                                            {{ asMoney($interest_amount)}} </td>
                                          <td>
                                            <?php $total = ($principal_amount + $interest_amount);
                                              $totalint = $totalint + $total;
                                            ?>
                                            {{ asMoney($total)}} </td>
                                          <td>
                                            {{ asMoney($balance - $total)}}
                                          </td>
                                           <td>
                                        </tr>
                                        <?php
                                          $balance = $balance - $total;
                                          $days = $days + 30;
                                          $date = date('d-F-Y', strtotime($date. ' + 30 days'));
                                          } */ ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <br>
                        <div class="col-lg-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <p>Loan Transactions
                                        <!--Check if loan balance is zero or below zero in order to show this link -->
                                        @if(Loantransaction::getLoanBalance($loanaccount)<=0)
                                            <a href="{{ URL::to('loantransactions/certificate/'.$loanaccount->id)}}"
                                               target="_blank">
                                                <span class="fa fa-certificate" aria-hidden="true"
                                                      style="margin-left: 55%;"></span> Clearance Certificate
                                            </a>
                                        @endif
                                    <!--Check if loan balance is zero or below zero in order to show this link -->
                                        <a href="{{ URL::to('loantransactions/statement/'.$loanaccount->id)}}"
                                           target="_blank" class="pull-right"> <span class="fa fa-file"
                                                                                     aria-hidden="true"></span> Loan
                                            Statements</a>
                                    </p>
                                </div>
                                <div class="panel-body">
                                    <table id="users"
                                           class="table table-condensed table-bordered table-responsive table-hover">
                                        <thead>

                                        <th>#</th>
                                        <th>Date</th>

                                        <th>Description</th>
                                        <th>Cr</th>

                                        <th>Dr</th>
                                        <!-- <th>Balance</th> -->
                                        <th></th>

                                        </thead>
                                        <tbody>

                                        <?php $i = 2;

                                        $balance = $loanaccount->amount_disbursed + Loanaccount::getInterestAmount($loanaccount);


                                        ?>

                                        <tr>

                                            <td> 1</td>
                                            <td>

                                                <?php

                                                $date = date("d-M-Y", strtotime($loanaccount->date_disbursed));
                                                ?>

                                                {{ $date}}</td>
                                            <td>Loan disbursement</td>

                                            <td> 0.00</td>
                                            <td>{{ asMoney($loanaccount->amount_disbursed)}}</td>
                                            <td>
                                            <!--
<a  href="{{ URL::to('loantransactions/receipt/')}}" target="_blank"> <span class="glyphicon glyphicon-file" aria-hidden="true"></span> Receipt</a>
-->

                                            </td>


                                        </tr>


                                        @foreach($loantransactions as $transaction)

                                            @if($transaction->description != 'loan disbursement')
                                                <tr>

                                                    <td> {{ $i }}</td>
                                                    <td>

                                                        <?php

                                                        $date = date("d-M-Y", strtotime($transaction->date));
                                                        ?>

                                                        {{ $date }}</td>
                                                    <td>{{ $transaction->description }}</td>
                                                    @if( $transaction->type == 'debit')
                                                        <td>
                                                            <?php $creditamount = 0; ?>
                                                            0.00
                                                        </td>
                                                        <td>{{ asMoney($transaction->amount)}}</td>

                                                    @endif
                                                    @if( $transaction->type == 'credit')

                                                        <td>
                                                            <?php $creditamount = $transaction->amount; ?>

                                                            {{ asMoney($transaction->amount) }}</td>
                                                        <td>0.00</td>
                                                    @endif

                                                <!--
          <td>
            <?php $balance = $balance - $creditamount; ?>
                                                {{ asMoney($balance) }}

                                                        </td>
-->

                                                    <td>

                                                        <a href="{{ URL::to('loantransactions/receipt/'.$transaction->id)}}"
                                                           target="_blank"> <span class="fa fa-file"
                                                                                  aria-hidden="true"></span> Receipt</a>


                                                    </td>


                                                </tr>

                                                <?php $i++; ?>
                                            @endif
                                        @endforeach


                                        </tbody>


                                    </table>
                                </div>


                            </div>


                        </div>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="extraloaninfo">
                        <br>
                        <div class="col-lg-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <p>Arrears & Overpayments</p>
                                </div>
                                <div class="panel-body">
                                <table id="users"
                                           class="table table-condensed table-bordered table-responsive table-hover">
                                        <thead>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Installment</th>
                                        <th>Amount paid</th>
                                        <th>Arrears</th>
                                        <th>Overpayment</th>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1;
                                            $tset_amount=0; $tamount_paid=0; $tarrears=0; $toverpayment=0;
                                        ?>

                                        @foreach($loantransactions as $transaction)

                                            @if($transaction->description == 'loan repayment' & $transaction->extra_amount_desc!='none' & !empty($transaction->extra_amount_desc))
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>
                                                        <?php $date = date("d-M-Y", strtotime($transaction->date));?>
                                                        {{ $date }}</td>
                                                    <?php
                                                        $loanaccount=Loanaccount::findorFail($transaction->loanaccount_id);
                                                        $set_amount=Loantransaction::getInstallment($loanaccount,'0');
                                                        if($transaction->extra_amount_desc=='arrears'){
                                                            $arrears=$transaction->extra_amount; $overpayment=0;
                                                        }else if($transaction->extra_amount_desc=='overpayment'){
                                                            $overpayment=$transaction->extra_amount; $arrears=0;
                                                        }else{ $overpayment=0; $arrears=0;} $overpayment=round($overpayment,2); $arrears=round($arrears,2);#changeo
                                                        $tset_amount+=$set_amount; $tamount_paid+=$transaction->amount;
                                                        $tarrears+=$arrears; $toverpayment+=$overpayment;
                                                    ?>
                                                    <td>{{ asMoney($set_amount) }}</td>
                                                    <td>{{ asMoney($transaction->amount) }}</td>
                                                    <td>{{ asMoney($arrears) }}</td>
                                                    <td>{{asMoney($overpayment)}}</td>

                                                </tr>

                                                <?php $i++; ?>
                                            @endif
                                        @endforeach
                                                    <tr>
                                                        <td></td>
                                                        <td><b>Total</b></td>
                                                        <td>{{asMoney($tset_amount)}}</td>
                                                        <td>{{asMoney($tamount_paid)}}</td>
                                                        <td>{{asMoney($tarrears)}}</td>
                                                        <td>{{asMoney($toverpayment)}}</td>
                                                    </tr>
                                        </tbody>
                                    </table>
                                    <p>Overpayment: <strong>{{asMoney(Loantransaction::getExtraAmount($loanaccount,'overpayments'))}}</strong></p>
                                    <p>Arrears due: <strong>{{asMoney(Loantransaction::getExtraAmount($loanaccount,'arrears'))}}</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="overpayments">
                        <br>
                        <div class="col-lg-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <p>Loan Overpayments
                                        <a href="{{ URL::to('loantransactions/overpayments/'.$loanaccount->id)}}"
                                           target="_blank" class="pull-right">
                                            <span class="fa fa-file" aria-hidden="true"></span>
                                            Overpayment Claim
                                        </a>
                                    </p>
                                </div>
                                <div class="panel-body">
                                    <p>Member Name: <strong>{{$loanaccount->member->name }}</strong></p>
                                    <p>Loan Account Number: <strong>{{$loanaccount->account_number}}</strong></p>
                                    <p>Amount Claimed:
                                        <strong>{{asMoney(-Loantransaction::getLoanBalance($loanaccount))}}</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="messages">
                        <br>
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a href="{{URL::to('loanguarantors/create/'.$loanaccount->id)}}"
                                       class="btn btn-primary">New Guarantor</a>
                                </div>
                                <div class="panel-body">
                                    <table id="mobile"
                                           class="table table-condensed table-bordered table-responsive table-hover">
                                        <thead>
                                        <th>#</th>
                                        <th>Member</th>

                                        <th>Loan Account</th>
                                        <th>Guaranteed Amount</th>


                                        <th></th>

                                        </thead>
                                        <tbody>

                                        <?php $i = 1; ?>


                                        @foreach($loanguarantors as $guarantor)

                                            <tr>

                                                <td> {{ $i }}</td>
                                                <td>{{ $guarantor->member->name }}</td>
                                                <td>{{ $guarantor->loanaccount->account_number }}</td>
                                                <td>{{ $guarantor->amount }}</td>


                                                <td>

                                                    <div class="btn-group">
                                                        <button type="button"
                                                                class="btn btn-info btn-sm dropdown-toggle"
                                                                data-toggle="dropdown" aria-expanded="false">
                                                            Action <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu" role="menu">

                                                            <li>
                                                                <a href="{{URL::to('loanguarantors/edit/'.$guarantor->id)}}">Update</a>
                                                            </li>

                                                            <li>
                                                                <a href="{{URL::to('loanguarantors/delete/'.$guarantor->id)}}">Remove</a>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                </td>


                                            </tr>

                                            <?php $i++; ?>
                                        @endforeach


                                        </tbody>


                                    </table>
                                </div>


                            </div>


                        </div>


                    </div>
                    <?php #STARTLEDGERS ?>
                    <div role="tabpanel" class="tab-pane" id="ledgers">
                        <br>
                        <div class="col-lg-12">
                            <div class="panelo panel-defaulto" style=''>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#loan_ledger" aria-controls="loan_ledger" role="tab"
                                                                                data-toggle="tab">Loan Ledger</a></li>
                                        <li role="presentation"><a href="#loan_tracksheet" aria-controls="loan_tracksheet" role="tab" data-toggle="tab">Loan
                                                tracksheet</a></li>
                                       <?php #<li role="presentation"><a href="#loan_register" aria-controls="loan_register" role="tab" data-toggle="tab">Loan register</a></li> ?>
				                    </ul>
                                    <div class='tab-content'>
                                    <div role="tabpanel" class="tab-pane active" id="loan_ledger">
                                        <br>
                                        <div class="col-lg-12">
                                        <?php
                                            $dueInterest=Loantransaction::getAmountDueAt($loanaccount,date('Y-m-d'),'interest');
                                            $paidInterest=Loanrepayment::getInterestPaid($loanaccount);
                                            $accruedInterest=$dueInterest-$paidInterest; if($accruedInterest<1){$accruedInterest=0;}
                                            if(empty($loanaccount->member->address)){$address='none';}else{$address=$loanaccount->member->address;}
                                        	if(empty($loanaccount->loan_purpose) || !isset($loanaccount->loan_purpose)){$loan_purpose='none';}else{$loan_purpose=$loanaccount->loan_purpose;}
					                    ?>
                                                    <div class='ledgerdetails'>
                                                        <div class='lrow'><b>Member name</b> : {{ $loanaccount->member->name }}</div>
                                                        <div class='lrow'><b>Address</b> : {{ $address }}</div>
                                                        <div class='lrow'><b>Loan disburse</b> : {{ asMoney($loanaccount->amount_disbursed+$loanaccount->top_up_amount)}}</div>
                                                        <div class='lrow'><b>Loan purpose</b> : {{ $loan_purpose }}</div>
                                                        <div class='lrow'><b>Loan security</b> : none</div>
                                                        <div class='lrow'><b>Account number</b> : {{ $loanaccount->account_number }}</div>
                                                        <?php #<div class='lrow'><b>Loan number</b> : 3489</div> table  table-condensed
                                                        #<div class='lrow'><b>Credit limit</b> : 3748592</div>?>
                                                    </div>
                                                    <div class='ledgerdetails'>
                                                        <table id="mobile" style='box-sizing:border-box;'
                                                            class="ledger_table table-bordered table-responsive table-hover">
                                                            <thead>
                                                                <th>Date</th>
                                                                <th>Principal</th>
                                                                <th>Amount disbursed</th>
                                                                <th>Guaranteed Amount</th>
                                                                <th>Code No.</th>
                                                                <th>Balance</th>
                                                                <th>Accrued interest</th>
                                                                <th>Interest repaid</th>
                                                                <th>Fines paid</th>
                                                                <th>Fines due</th>
                                                                <th>Signature</th>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>{{ $loanaccount->date_disbursed}}</td>
                                                                    <td>{{ asMoney(Loanaccount::getPrincipalBal($loanaccount))}}</td>
                                                                    <td>{{ asMoney($loanaccount->amount_disbursed+$loanaccount->top_up_amount)}}</td>
                                                                    <td>{{ asMoney(Loanaccount::guaranteedAmount($loanaccount))}}</td>
                                                                    <td>{{ $loanaccount->account_number }}</td>
                                                                    <td>{{ asMoney(Loantransaction::getLoanBalance($loanaccount))}}</td>
                                                                    <td>{{asMoney($accruedInterest)}}</td>
                                                                    <td>{{asMoney($interest_paid)}}</td>
                                                                    <td>{{asMoney(0)}}</td>
                                                                    <td>{{asMoney(0)}}</td>
                                                                    <td><img src=<?php public_path().'/uploads/photos/'.$loanaccount->member->signature ?>> </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                        </div>
                                    </div>

                                        <div role="tabpanel" class="tab-pane" id="loan_tracksheet">
                                            <br>
                                            <div class="col-lg-12">
                                                <a href="{{ URL::to('loantransactions/loan_tracksheet/'.$loanaccount->id)}}">
                                                    <button class="fa fa-file" aria-hidden="true">Loan tracksheet</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <?php #ENDLEDGERS ?>
                    <div role="tabpanel" class="tab-pane" id="settings">

                        <br>
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="panel panel-default">
                                    <div class="panel-heading">


                                    </div>

                                    <div class="panel-body">


                                        <table id="users"
                                               class="table table-condensed table-bordered table-responsive table-hover">


                                            <thead>

                                            <th>#</th>

                                            <th>Member</th>
                                            <th>Branch</th>
                                            <th>Amount Guaranteed</th>

                                            <th></th>

                                            </thead>

                                            <tbody>

                                            <?php $i = 1; ?>


                                            <tr>

                                                <td></td>

                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><a href="" class="btn btn-primary btn-sm">View </a></td>
                                            </tr>

                                            <?php $i++; ?>


                                            </tbody>


                                        </table>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
