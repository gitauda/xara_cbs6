@extends('layouts.ports')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3> Loan Reports</h3>
                        </div>
                        <div class="card-block">

                            <ul>
                                <li>
                                    <a href="{{ url('loanapplication/member') }}" target="_blank"> Contributions Listing Report</a>
                                </li>
                                <li>
                                    <a href="{{ url('reports/loanlisting') }}" target="_blank">Shares Listing Report</a>
                                </li>

                                @foreach($loanproducts as $loanproduct)
                                <li>
                                    <a href="{{url('reports/loanproduct/'.$loanproduct->id)}}"> {{ $loanproduct->name}} report</a>
                                </li>
                                @endforeach
                                <li>
                                    <a href="{{ url('reports/monthlyrepayments') }}" target="_blank">Shares Listing Report</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3> Interests Reports</h3>
                        </div>
                        <div class="card-block">

                            <ul>
                                <li>
                                    <a href="{{ url('reports/totalinterests') }}" target="_blank"> Contributions Listing Report</a>
                                </li>


                                @foreach($loanproducts as $loanproduct)
                                <li>
                                    <a href="{{url('reports/interest/'.$loanproduct->id)}}"> {{ $loanproduct->name}} Interest report</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
