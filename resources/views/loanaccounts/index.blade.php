@extends('layouts.loans')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->

                    <div class="card">
                        @if(Session::get('notice'))
                            <div class="alert alert-success">{{ Session::get('notice') }}</div>
                        @endif
                        <div class="card-header">
                            <h5>Loan Accounts</h5>

                        </div>
                        <div class="card-block">


                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs  tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#new" role="tab">New Applications</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#rejected" role="tab">Rejected Applications</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#disbursed" role="tab">Disbursed Loans</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#loanarrears" role="tab">Loans with arrears</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content tabs card-block">
                                <div class="tab-pane active" id="new" role="tabpanel">
                                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                        <thead>

                                        <th>Members</th>
                                        <th>Loan Type</th>
                                        <th>Date Applied</th>
                                        <th>Amount Applied</th>
                                        <th>Period (months)</th>
                                        <th>Interest Rate (monthly)</th>
                                        <th></th>
                                        </thead>
                                        <tbody>

                                        @foreach($loanaccounts as $loanaccount)
                                            @if($loanaccount->is_new_application)
                                                <tr>
                                                    <td>{{ $loanaccount->member->name}}</td>
                                                    <td>{{ $loanaccount->loanproduct->name}}</td>
                                                    <td>{{ $loanaccount->application_date}}</td>
                                                    <td>{{ asMoney($loanaccount->amount_applied)}}</td>
                                                    <td>{{ $loanaccount->repayment_duration}}</td>
                                                    <td>{{ $loanaccount->interest_rate}}</td>
                                                    <td>

                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                Action <span class="caret"></span>
                                                            </button>

                                                            <ul class="dropdown-menu" role="menu">
                                                            <!-- <li><a href="{{url('loans/edit/'.$loanaccount->id)}}">Amend</a></li> -->
                                                                <li><a href="{{url('loans/approve/'.$loanaccount->id)}}">Approve</a></li>
                                                                <li><a href="{{url('loans/reject/'.$loanaccount->id)}}">Reject</a></li>

                                                            </ul>
                                                        </div>

                                                    </td>
                                                </tr>

                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="" role="tabpanel">
                                    <div class="dt-responsive table-responsive">
                                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                            <thead>
                                            <th>Member</th>
                                            <th>Loan Type</th>
                                            <th>Date Applied</th>
                                            <th>Amount Applied</th>
                                            <th>Period (months)</th>
                                            <th>Interest Rate (monthly)</th>
                                            <th></th>

                                            </thead>
                                            <tbody>

                                            @foreach($loanaccounts as $loanaccount)
                                                @if($loanaccount->is_amended)

                                                    <tr>
                                                        <td>{{ $loanaccount->member->name}}</td>
                                                        <td>{{ $loanaccount->loanproduct->name}}</td>
                                                        <td>{{ $loanaccount->application_date}}</td>
                                                        <td>{{ $loanaccount->amount_applied}}</td>
                                                        <td>{{ $loanaccount->repayment_duration}}</td>
                                                        <td>{{ $loanaccount->interest_rate}}</td>
                                                        <td>

                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                    Action <span class="caret"></span>
                                                                </button>

                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="{{url('loans/edit/'.$loanaccount->id)}}">Amend</a></li>

                                                                    <li><a href="{{url('loans/approve/'.$loanaccount->id)}}">Approve</a></li>
                                                                    <li><a href="{{url('loans/reject/'.$loanaccount->id)}}">Reject</a></li>

                                                                </ul>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                @endif
                                            @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="rejected" role="tabpanel">
                                    <div class="dt-responsive table-responsive">
                                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                            <thead>

                                            <th>Member</th>
                                            <th>Loan Type</th>
                                            <th>Date Applied</th>
                                            <th>Amount Applied</th>
                                            <th>Period (months)</th>
                                            <th>Interest Rate (monthly)</th>
                                            <th>Rejection Reason</th>

                                            </thead>
                                            <tbody>

                                            @foreach($loanaccounts as $loanaccount)
                                                @if($loanaccount->is_rejected)

                                                    <tr>
                                                        <td>{{ $loanaccount->member->name}}</td>
                                                        <td>{{ $loanaccount->loanproduct->name}}</td>
                                                        <td>{{ $loanaccount->application_date}}</td>
                                                        <td>{{ $loanaccount->amount_applied}}</td>
                                                        <td>{{ $loanaccount->repayment_duration}}</td>
                                                        <td>{{ $loanaccount->interest_rate.' %'}}</td>
                                                        <td>{{ $loanaccount->rejection_reason}}</td>
                                                    </tr>

                                                @endif
                                            @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="approved" role="tabpanel">
                                    <div class="dt-responsive table-responsive">
                                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                            <thead>
                                            <th>Member</th>
                                            <th>Loan Type</th>
                                            <th>Date Applied</th>
                                            <th>Amount Applied</th>
                                            <th>Period (months)</th>
                                            <th>Interest Rate (monthly)</th>
                                            <th></th>

                                            </thead>
                                            <tbody>

                                            @foreach($loanaccounts as $loanaccount)
                                                @if($loanaccount->is_approved and !$loanaccount->is_disbursed )

                                                    <tr>
                                                        <td>{{ $loanaccount->member->name}}</td>
                                                        <td>{{ $loanaccount->loanproduct->name}}</td>
                                                        <td>{{ $loanaccount->application_date}}</td>
                                                        <td>{{ asMoney($loanaccount->amount_applied)}}</td>
                                                        <td>{{ $loanaccount->repayment_duration}}</td>
                                                        <td>{{ $loanaccount->interest_rate}}</td>
                                                        <td>

                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                    Action <span class="caret"></span>
                                                                </button>

                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="{{url('loans/disburse/'.$loanaccount->id)}}">Disburse</a></li>



                                                                </ul>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                @endif
                                            @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="disbursed" role="tabpanel">
                                    <div class="dt-responsive table-responsive">
                                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                            <thead>

                                            <th>Member</th>
                                            <th>Loan Type</th>
                                            <th>Date Applied</th>
                                            <th>Amount Applied</th>
                                            <th>Period (months)</th>
                                            <th>Interest Rate (monthly)</th>
                                            <th></th>

                                            </thead>
                                            <tbody>

                                            @foreach($loanaccounts as $loanaccount)
                                                @if($loanaccount->is_disbursed)

                                                    <tr>
                                                        <td>{{ $loanaccount->member->name}}</td>
                                                        <td>{{ $loanaccount->loanproduct->name}}</td>
                                                        <td>{{ $loanaccount->application_date}}</td>
                                                        <td>{{ asMoney($loanaccount->amount_applied)}}</td>
                                                        <td>{{ $loanaccount->repayment_duration}}</td>
                                                        <td>{{ $loanaccount->interest_rate}}</td>
                                                        <td>

                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                    Action <span class="caret"></span>
                                                                </button>

                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="{{url('loans/show/'.$loanaccount->id)}}">Manage</a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="loanarrears" role="tabpanel">
                                    <div class="card-block">
                                        <div id="loanarrears" class="">
                                            <section class='section1'>
                                                <div>Member</div>
                                                <div>Number of loans</div>
                                                <div>Total arrears</div>
                                                <div></div>
                                            </section>
                                            <nav class='tracksheetnav1'>
                                                <?php $i=0;
                                                foreach($members as $member){
                                                $i++;
                                                $loanaccounts= App\models\Loanaccount::where('member_id','=',$member->id)->where('is_disbursed','=',1)->get();
                                                $all_arrears=0;
                                                $loans_no=0;
                                                foreach ($loanaccounts as $loanaccount) {
                                                    $loans_no++;
                                                    $arrears=$arrears= App\models\Loantransaction::getExtraAmount($loanaccount,'arrears');
                                                    $amount_unpaid= App\models\Loantransaction::getAmountUnpaid($loanaccount);
                                                    $total_arrears=(float)$arrears+(float)$amount_unpaid;
                                                    $all_arrears+=$total_arrears;
                                                }
                                                if($all_arrears>0){
                                                ?>
                                                <section class='section2'>
                                                    <div>{{ $member->name}}</div>
                                                    <div>{{ $loans_no}}</div>
                                                    <div>{{ asMoney($all_arrears)}}</div>
                                                    <div><button data-toggle="collapse" data-target="#showloans{{$i}}" class='showloansbut'>Show loans</button></div>
                                                </section>
                                                <nav id="showloans{{$i}}" class="collapse showloanstable tracksheetnav2">
                                                    <section class='section3a'>
                                                        <div><b>Loan Type</b></div>
                                                        <div><b> Date Applied</b></div>
                                                        <div><b>Amount Applied</b></div>
                                                        <div><b>Loan balance</b></div>
                                                        <div><b>Arrears</b></div>
                                                    </section>
                                                    <?php
                                                    foreach ($loanaccounts as $loanaccount) {
                                                    $loans_no++;
                                                    $arrears=$arrears= App\models\Loantransaction::getExtraAmount($loanaccount,'arrears');
                                                    $amount_unpaid= App\models\Loantransaction::getAmountUnpaid($loanaccount);
                                                    $total_arrears=(float)$arrears+(float)$amount_unpaid;
                                                    $loanBal= App\models\Loantransaction::getLoanBalance($loanaccount);
                                                    ?>
                                                    <section class='section3b'>
                                                        <div>{{ $loanaccount->loanproduct->name}}</div>
                                                        <div>{{ $loanaccount->application_date}}</div>
                                                        <div>{{ asMoney($loanaccount->amount_applied)}}</div>
                                                        <div>{{asMoney($loanBal)}}</div>
                                                        <div>{{asMoney($total_arrears)}}</div>
                                                    </section>
                                                    <?php
                                                    }
                                                    ?>
                                                </nav>
                                                <?php } }?>
                                            </nav>
                                        </div>
                                    </div>
                                </div>






                            </div>
                        </div>
                        <!-- [ page content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>


    <br/>
    <?php
    function asMoney($value) {
        return number_format($value, 2);
    }
    ?>
    <style>
        #loanarrears{margin-bottom:0px;}
        .showloanstable{background-color:#eee;}
        section{display:flex; flex-direction:row; justify-content:space-between;  padding:5px; box-sizing:border-box;}
        .section1 div,.section2 div{width:25%; box-sizing:border-box;}
        .section1,.section3a{font-weight:bold; font-size:15px;}
        .section3a div,.section3b div{width:20%; box-sizing:border-box;}
        .tracksheetnav2 section{margin:0 auto; text-align:center;}
        .showloansbut{border:none; border-radius:3px; padding:4px; outline:none;}
    </style>



    <script type="text/javascript">
        $(document).ready(function() {
            $('#disbursed').DataTable({
                aaSorting: []
            });
        } );
    </script>
@stop
