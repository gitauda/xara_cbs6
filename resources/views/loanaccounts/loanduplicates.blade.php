@extends('layouts.loans')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Loan Duplicates</h3>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Loan Product
                                        </th>
                                        <th>
                                            Loan 1
                                        </th>
                                        <th>
                                            Loan 2
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>

                                    </thead>

                                    <tbody>
                                    @foreach($loans as $loan)
                                        <th>
                                            {{$loan['loanproduct']}}
                                        </th>
                                        <th>
                                            {{ $loan['loan1']}}
                                        </th>
                                        <th>
                                            {{$loan['loan2']}}
                                        </th>
                                        <th>

                                        </th>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
