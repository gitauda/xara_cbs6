@extends('layouts.loans')
@section('xara_cbs')
    <br/>
    <?php
    function asMoney($value) {
        return number_format($value, 2);
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <h3>Loan Top Up</h3>

            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <form method="POST" action="{{{ url('loanaccounts/topup/'.$loanaccount->id) }}}" accept-charset="UTF-8">{{ csrf_field() }}
                <fieldset>
                    <?php $date = date('Y-m-d'); ?>
                    <div class="form-group">
                        <label for="username">Top up Date </label>
                        <input class="form-control datepicker" placeholder="" type="text" name="top_up_date" id="application_date" value="{{$date}}">
                    </div>
                    <div class="form-group">
                        <label for="username">Maximum amount</label>
                        <input class="form-control numbers" readonly placeholder="" type="text" name="maxamount" id="maxamount" value="{{ asMoney($finalSavings)}}">
                    </div>
                    <div class="form-group">
                        <label for="username">Top Up Amount</label>
                        <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount_applied" value="{{{ Input::old('to_up_amount') }}}">
                    </div>
                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop
