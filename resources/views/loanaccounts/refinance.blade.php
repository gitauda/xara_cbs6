@extends('layouts.member')
@section('xara_cbs')
    <?php
    function asMoney($value)
    {
        return number_format($value, 2);
    }
    ?>
    <div class="container-fluid">
        <h3>Loan Refinance</h3>
        <div class="row">
            <div class="col-lg-4">
                <table class="table table-hover">
                    <tr>
                        <td>Member</td>
                        <td>{{ $loanaccount->member->name }}</td>
                    </tr>
                    <tr>
                        <td>Loan Account</td>
                        <td>{{ $loanaccount->account_number }}</td>
                    </tr>
                    <tr>
                        <td>Loan Balance</td>
                        <td>{{ asMoney(App\models\Loanaccount::getPrincipalBal($loanaccount) + App\models\Loanaccount::getInterestBal($loanaccount)) }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <form action="{{ url('loanrepayments/refinance', $loanaccount->id) }}" method="post">
                <div class="form-group">
                    <label for="username">Application Date </label>
                    <div class="right-inner-addon ">
                        <i class="fa fa-calendar"></i>
                        <input class="form-control datepicker" readonly placeholder="" type="text"
                               name="date"
                               id="application_date" value="{{{ date('Y-m-d') }}}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="username">Amount</label>
                    <input class="form-control numbers" placeholder="" type="number" step="0.01" name="amount" id="amount_applied"
                           value="{{{ old('amount') }}}" required>
                </div>
                <div class="form-group">
                    <label for="username">Repayment Period(months)</label>
                    <input class="form-control numbers" placeholder="" type="number" name="repayment_duration"
                           id="repayment_duration"
                           value="{{{ old('repayment_duration') }}}" required>
                </div>
                <div class="form-actions form-group">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
