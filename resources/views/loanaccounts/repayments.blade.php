@extends('layouts.ports')
@section('xara_cbs')



    <div class="row">
        <div class="col-lg-12">
            <h3> Loan repayments Report</h3>

            <hr>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-5">
          @if($errors->has())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
              <a href="#"  class="close" data-dismiss="alert" aria-label="close" >&times;</a>
            </div>
            @endif
            <form target="_blank" method="GET" action="{{URL::to('reports/loanrepayments')}}">{{ csrf_field() }}

                <div class="form-group">
                    <label for="username">Loan Product</label>
                    <select class="form-control" name="loanproduct" required>
                        <option value="All">All</option>
                        @foreach ($loanproducts as $loanproduct)
                            <option value="{{$loanproduct->id}}">{{$loanproduct->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="username">Period options</label>
                    <select class="form-control" name="period" id="period" required>
                        <option value="">select period</option>
                        <option value="custom">Custom range</option>
                        <option value="As at date">As at Date</option>
                        <option value="year">Year</option>
                        <option value="month">Month</option>
                    </select>
                </div>
              <!--  <div class="form-group">
                    <label for="username">Format</label>
                    <select class="form-control" name="format" id="format">
                        <option value="pdf">Pdf</option>
                        <option value="excel">Excel</option>
                    </select>
                </div>-->
                <div class="form-group" id="year">

                  <label for="username">Select Year <span style="color:red">*</span></label>
                  <div class="right-inner-addon ">
                      <i class="glyphicon glyphicon-calendar"></i>
                      <input class="form-control datepicker42" readonly="readonly" placeholder="" type="text"
                             name="year" id="date" value="{{date('Y')}}">
                  </div>

                </div>

                <div id="custom">


                        <div class="form-group">
                              <label for="username">From <span style="color:red">*</span></label>
                              <div class="right-inner-addon ">
                              <i class="glyphicon glyphicon-calendar"></i>
                              <input required class="form-control datepicker" readonly="readonly" placeholder="" type="text" name="from" id="from" value="{{{ Input::old('from') }}}">
                          </div>

                       </div>

                       <div class="form-group">
                            <label for="username">To <span style="color:red">*</span></label>
                            <div class="right-inner-addon ">
                            <i class="glyphicon glyphicon-calendar"></i>
                            <input required class="form-control datepicker" readonly="readonly" placeholder="" type="text" name="to" id="to" value="{{{ Input::old('to') }}}">
                        </div>

                       </div>
              </div>

                <div class="form-group" id="select_date">
                    <label for="username">Date <span style="color:red">*</span></label>
                    <div class="right-inner-addon ">
                        <i class="glyphicon glyphicon-calendar"></i>
                        <input class="form-control datepicker" readonly="readonly" placeholder="" type="text"
                               name="date" id="date" value="{{date('Y-m-d')}}">
                    </div>
                </div>
                <div class="form-group" id="month">
                    <label for="username">Select month <span style="color:red">*</span></label>
                    <div class="right-inner-addon ">
                        <i class="glyphicon glyphicon-calendar"></i>
                        <input class="form-control datepicker2" readonly="readonly" placeholder="" type="text"
                               name="month" id="date" value="{{date('m-Y')}}">
                    </div>
                </div>


            <!--  <div class="form-group">
            <label for="username">As at Date </label>
            <input class="form-control" placeholder="" type="date" name="date" id="date" value="{{date('Y-m-d')}}">
        </div> -->


                <div class="form-actions form-group">


                    <button type="submit" class="btn btn-primary btn-sm">Generate Report</button>
                </div>


            </form>

        </div>

    </div>

    <script type="text/javascript">
    $(document).ready(function () {
        $('#year').hide();
        $('#select_date').hide();
        $('#month').hide();
        $('#custom').hide();


        $('#period').change(function () {
            if ($(this).val() == "As at date" || $(this).val() == "day") {
                $('#year').hide();
                $('#select_date').show();
                $('#month').hide();
                $('#custom').hide();
            }else if ($(this).val() == "year") {
                $('#year').show();
                $('#select_date').hide();
                $('#month').hide();
                $('#custom').hide();
            } else if ($(this).val() == "month") {
              $('#year').hide();
              $('#select_date').hide();
              $('#month').show();
              $('#custom').hide();

            } else if ($(this).val() == "custom") {
              $('#year').hide();
              $('#select_date').hide();
              $('#month').hide();
              $('#custom').show();

            }
            else {
                $('#year').hide();
                $('#select_date').hide();
                $('#month').hide();
                $('#custom').hide();
            }

        });
    });

    </script>

@stop
