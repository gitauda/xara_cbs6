@extends('layouts.system')
@section('xara_cbs')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <img src="{{ asset('public/uploads/logo/'.$organization->logo) }}" alt="Logo" style="height: 150px;">


                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <tr>

                                        <td>System</td><td>XARA CBS</td>
                                    </tr>
                                    <tr>

                                        <td>Version</td><td>v3.1.10</td>
                                    </tr>

                                    <tr>

                                        <td>Licensed To</td><td>{{$organization->name}}</td>
                                    </tr>
                                    <tr>

                                        <td>License Type</td><td>{{$organization->license_type}}</td>
                                    </tr>

                                    <tr>
                                        <td>Licensed</td><td>{{$organization->licensed.' Members'}}</td>

                                    </tr>

                                    <tr>
                                        <td>License Code</td><td>{{$organization->license_code}}</td>
                                    </tr>

                                    <tr>
                                        <td>License Key</td><td>{{$organization->license_key}}</td>

                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>


@stop
