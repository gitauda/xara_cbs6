@extends('layouts.system')
@section('xara_cbs')
    <br/><br/>

    <div class="row">
        <div class="col-lg-1">



        </div>

        <div class="col-lg-12">

            @if (Session::get('error'))
                <div class="alert alert-danger">{{{ Session::get('error') }}}</div>
            @endif

            <p>SMS Details</p>
            <hr>

            <br>
        </div>


        <div class="col-lg-12 ">

            <div>
                <h4>Sms Details For the Month of {{date("F-Y",strtotime($date))}}</h4>
            </div>

            <table class="table table-bordered table-responsive table-hover" id="users">

                <thead>
                <th>Member Name</th>
                <th>Member Number</th>
                <th>Member Phone</th>
                <th>Monthly Count</th>
                </thead>

                <tbody>
                @foreach($members as $member)
                    <?php $member_info= App\models\Member::find($member);
                    $totalcount= App\models\Smslog::where('user','=',$member)->whereMonth('date','=',$date)->first();
                    ?>
                    <tr>
                        <td>{{$member_info->name}}</td>
                        <td>{{$member_info->membership_no}}</td>
                        <td>{{$member_info->phone}}</td>
                        <td>{{ $totalcount->monthlySmsCount}}</td>

                    </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td colspan="2">TOTAL SMS SENT</td>
                    <td>{{ $total}}</td>

                </tr>
                <tr>
                    <?php $charge= App\models\Smslog::whereMonth('date','=',$date)->first();?>

                    <td></td>
                    <td colspan="2">Total charges</td>
                    <td>{{ $total*$charge->charged}}</td>

                </tr>


                </tbody>

            </table>

        </div>



    </div>


@stop
