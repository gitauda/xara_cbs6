@extends('layouts.system')
@section('xara_cbs')
    <br/><br/><br/><br/>
    <br/><br/><br/><br/>
    <script type="text/javascript">
        $("input.date-picker").click(function(){
            $("#ui-datepicker-div").css("z-index",5000);
        });
    </script>
    <div class="row">
        <div class="col-lg-1">



        </div>

        <div class="col-lg-3">

            <img src="{{ asset('public/uploads/logo/'.$organization->logo) }}" alt="Logo" style="height: 150px;">


        </div>


        <div class="col-lg-5 ">


            <table class="table table-bordered table-condensed">

                <tr>

                    <td>System</td><td>XARA CBS</td>
                </tr>
                <tr>

                    <td>Version</td><td>v3.1.10</td>
                </tr>

                <tr>

                    <td>Licensed To</td><td>{{$organization->name}}</td>
                </tr>
                <tr>

                    <td>License Type</td><td>{{$organization->license_type}}</td>
                </tr>

                <tr>
                    <td>Licensed</td><td>{{$organization->licensed.' Members'}}</td>

                </tr>

                <tr>
                    <td>License Code</td><td>{{$organization->license_code}}</td>
                </tr>

                <tr>
                    <td>License Key</td><td>{{$organization->license_key}}</td>

                </tr>
                <tr>
                    <td></td>
                    <td>

                        @if($organization->license_type == 'evaluation')

                            <a href="{{URL::to('license/activate/'.$organization->id)}}" class="btn btn-success btn-sm">Activate License</a>
                        @endif


                        @if($organization->license_type != 'evaluation')

                            <a href="{{URL::to('license/activate/'.$organization->id)}}" class="btn btn-success btn-sm">Upgrade License</a>
                        @endif

                    </td>

                </tr>

                <tr>
                    <td>Annual Support </td><td><a href="{{URL::to('license/activate_annual/'.$organization->id)}}" class="btn btn-success btn-sm">Activate Key</a></td>

                </tr>

                <tr>





                    <td>SMS Charges </td><td><a href="#smsview" class="btn btn-success btn-sm">View Details</a></td>
                    <!-- SMS VIEW -->
                    <div id="smsview" class="modal fade">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="{{ URL::to('license/sms_view/'.$organization->id) }}" method="GET" accept-charset="utf-8">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            <span class="sr-only">Close</span>
                                        </button>
                                        <h4 class="modal-title"><font color="green"> SMS Count</font></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="background:#E1F5FE; padding: 10px;">
                                            <div class="form-group">
                                                <label for="username">Select Month</label>
                                                <div class="right-inner-addon ">
                                                    <i class="glyphicon glyphicon-calendar"></i>
                                                    <input class="form-control input-sm datepicker"  readonly="readonly" type="text" name="month" id="dropper-default" value="">
                                                </div>
                                            </div>


                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>&emsp;
                                                <input type="submit" class="btn btn-primary btn-sm" value="View Details">


                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- ./END SMS VIEW MODAL -->


                </tr>



            </table>



        </div>



    </div>


@stop
