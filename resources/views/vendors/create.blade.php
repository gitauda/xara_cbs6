@extends('layouts.css')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>new vendor</h3>

                        </div>
                        <div class="card-block">
                            <form method="post" action="{{url('vendors')}}">@csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="username">Vendor Name</label>
                                        <input class="form-control" type="text" name="name" id="name" value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Phone Number</label>
                                        <input class="form-control numbers" maxlength="10" type="text" name="phone" id="phone" value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Vendor Email</label>
                                        <input class="form-control" type="email" name="email" id="email" value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Description</label>
                                        <textarea class="form-control" name="description"></textarea>
                                    </div>



                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">Create Vendor</button>
                                    </div>

                                </fieldset>
                            </form>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@stop
