@extends('layouts.css')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors)> 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Vendors</h3>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm"  href="{{url('vendors/create')}}">New Vendor</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">

                                    <thead>
                                    <tr>
                                        <th>Name </th>
                                        <th>Phone </th>

                                        <th>Email </th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>


                                    </thead>
                                    <tbody>
                                    @foreach($vendors as $vendor)
                                        <tr>

                                            <td>{{ $vendor->name  }}</td>
                                            <td>{{ $vendor->phone }}</td>
                                            <td>{{ $vendor->email }}</td>
                                            <td>{{ $vendor->description }}</td>
                                            <td>{{ $vendor->status }}</td>


                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu" role="menu">


                                                        <li><a href="{{URL::to('vendors/edit/'.$vendor->id)}}">Edit</a> </li>


                                                        <li><a href="{{URL::to('vendors/delete/'.$vendor->id)}}"> Delete</a></li>
                                                    </ul>
                                                </div>

                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
