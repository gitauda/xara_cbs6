@extends('layouts.css')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>new vendor</h3>

                        </div>
                        <div class="card-block">
                            <form method="POST" action="{{{ url('vendors/update/'.$vendor->id) }}}">@csrf


                                <fieldset>
                                    <div class="form-group">
                                        <label for="name">Vendor Name</label>
                                        <input class="form-control" type="text" name="name" id="name" value="{{ $vendor->name}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Phone Number</label>
                                        <input class="form-control numbers" maxlength="10" type="text" name="phone" id="phone" value="{{ $vendor->phone}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Phone Email</label>
                                        <input class="form-control" type="email" name="email" id="email" value="{{ $vendor->email }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" name="description" id="description">{{ $vendor->description}}</textarea>
                                    </div>



                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-primary btn-sm">update vendor</button>
                                    </div>

                                </fieldset>
                            </form>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
