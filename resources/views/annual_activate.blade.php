@extends('layouts.main_hr')
@section('xar_cbs')


    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <img src="{{ asset('public/uploads/logo/'.$organization->logo) }}" alt="Logo" style="height: 150px;">
                            @if (count($errors)>0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif

                        </div>
                        <div class="card-block">

                            <form role="form" method="POST" action="{{{ URL::to('/license/activate_annual') }}}" accept-charset="UTF-8">@csrf



                                <input class="form-control" tabindex="1"  type="hidden" name="org_id" id="org_id" value="{{ $organization->id}}" >
                                <fieldset>

                                    <div class="form-group">
                                        <label for="email"> <i class="fa fa-home"></i> Organization</label>

                                        <input class="form-control" tabindex="1"  type="text" name="org_name" id="org_name" value="{{ $organization->name}}" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">  <i class="fa fa-tags"></i> License Code</label>

                                        <input class="form-control" tabindex="1"  type="text" name="license_code" id="license_code" value="{{ $organization->license_code}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">
                                            <i class="fa fa-sign-in"></i>
                                            Annual Support Key
                                        </label>

                                        <input class="form-control" tabindex="2"  type="text" name="license_key" id="license_key">

                                    </div>

                                    <div class="form-actions">

                                        <button tabindex="3" type="submit" class="btn btn-danger">Activate Annual Support</button>
                                    </div>


                                </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
