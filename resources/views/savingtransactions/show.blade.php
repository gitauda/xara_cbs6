@extends('layouts.member')
@section('xara_cbs')
    <?php
    function asMoney($value) {
        return number_format($value, 2);
    }
    ?>
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->

                    <div class="card">
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-condensed table-bordered table-hover">
                                    @if($interest[0] > 0)
                                        <tr>
                                            <td>Member Name:</td><td>{{ $account->member->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member No:</td><td>{{ $account->member->membership_no}}</td>
                                        </tr>
                                        <tr>
                                            <td>Account No:</td><td>{{ $account->account_number}}</td>
                                        </tr>

                                        <tr>
                                            <td>Account Balance:</td><td>{{ asMoney($balance)}}</td>
                                        </tr>
                                        <tr>
                                            <td>Interest Rate:</td><td>{{ $interest[0]}}%</td>
                                        </tr>

                                        <tr>
                                            <td>Interest Earned:</td><td>{{ asMoney($balance*$interest[0]/100)}}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Amount:</td><td>{{ asMoney(($balance*$interest[0]/100)+$balance)}}</td>
                                        </tr>

                                    @else
                                        <tr>
                                            <td>Member Name:</td><td>{{ $account->member->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Member No:</td><td>{{ $account->member->membership_no}}</td>
                                        </tr>
                                        <tr>
                                            <td>Account No:</td><td>{{ $account->account_number}}</td>
                                        </tr>

                                        <tr>
                                            <td>Account Balance:</td><td>{{ asMoney($balance)}}</td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h5>Members</h5>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm"  href="{{ url('savingtransactions/create/'.$account->id)}}"> New Transaction</a>
                                <a class="dt-button btn-sm"  href="{{ url('savingtransactions/statement/'.$account->id)}}" target="_blank"> Saving Statements</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Debit (DR)</th>
                                    <th>Credit (CR)</th>
                                    <th></th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    @foreach($account->transactions()->orderBy('date')->get() as $transaction)
                                        <tr>
                                            <?php
                                            $date = date("d-M-Y", strtotime($transaction->date ));
                                            ?>
                                            <td>{{ $date }}</td>
                                            <td>{{ $transaction->description }}</td>
                                            @if( $transaction->type == 'debit')
                                                <td >{{ asMoney($transaction->saving_amount)}}</td>
                                                <td>0.00</td>
                                            @endif
                                            @if( $transaction->type == 'credit')
                                                <td>0.00</td>
                                                <td>{{ asMoney($transaction->saving_amount) }}</td>
                                            @endif
                                            <td>
                                                <a  href="{{ url('savingtransactions/receipt/'.$transaction->id)}}" target="_blank"> <span class="fa fa-file" aria-hidden="true"></span> Receipt</a>
                                            </td>
                                            <td>
                                                <a  href="{{ url('savingtransactions/void/'.$transaction->id)}}" > <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Void</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>



@stop
