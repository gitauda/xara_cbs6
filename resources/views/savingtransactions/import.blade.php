@extends('layouts.savings')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (Session::get('notice'))
                            <div class="alert alert-success">{{ Session::get('notice') }}</div>
                        @endif
                        @if (Session::get('warning'))
                            <div class="alert alert-danger">{{ Session::get('warning') }}</div>
                        @endif
                        @if(count($errors) > 0)
                            <ul class="alert-danger">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            SAVINGS TRANSACTIONS MIGRATION

                            <p><strong>Import Loan Repayments </strong></p>
                        </div>
                        <div class="card-block">

                            <form method="post" action="{{ url('migration/import/savings') }}" accept-charset="UTF-8"
                                  enctype="multipart/form-data">@csrf
                                <div class="form-group">
                                    <label>Upload Transactions (CSV File)</label>
                                    <input type="file" name="file" required/>
                                </div>
                                <button type="submit" class="btn btn-primary">Import Transactions</button>
                                &nbsp;
                                <a href="{{ url('templates/savings') }}" class="btn btn-success">Download Template</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
