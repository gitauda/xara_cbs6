@extends('layouts.main')
@section('xara_cbs')

    <!--<script>
  $(document).ready(function(){
    $('#member').change(function(){
        $.get("{{ url('api/dropdownlist')}}",
        { option: $(this).val() },
        function(data) {
            $('#regno').empty();
            $('#regno').append("<option>----------------Select REG No.-------------------</option>");
            for (var i = 0; i < data.length; i++) {
            $('#invoice').append("<option value='" + data[i].id +"'>" + data[i].vehicle   + "</option>");
            };
        });
    });
});
</script>-->

    <?php
    function asMoney($value) {
        return number_format($value, 2);
    }
    ?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors)> 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <strong>Member: {{ $member->name }}</strong><br>
                            <strong>Member #: {{ $member->membership_no }}</strong><br>
                            <strong>Savings Account #: {{ $savingaccount->account_number }}</strong><br>
                            <strong>Account Balance #: {{ asMoney($balance) }}</strong><br>
                            @if($balance > App\models\Savingtransaction::getWithdrawalCharge($savingaccount))
                                <strong>Available Balance #: {{ asMoney($balance - App\models\Savingtransaction::getWithdrawalCharge($savingaccount)) }}</strong><br>
                            @endif
                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{ url('savingtransactions') }}}" accept-charset="UTF-8">@csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="type">Transaction </label>
                                        <select name="type" id="type" class="form-control" required>
                                            <option></option>
                                            <option value="credit"> Deposit</option>
                                            <option value="debit"> Withdraw</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="account_id" value="{{ $savingaccount->id}}">
                                    <div class="form-group">
                                        <label for="dropper-default"> Date</label>
                                        <div class="right-inner-addon ">
                                            <i class="fa fa-calendar"></i>
                                            <input class="form-control datepicker" readonly placeholder="Select your date" type="text" name="date" id="dropper-default" value="{{{ old('date') }}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="saving_amount"> Saving Amount</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="saving_amount" id="saving_amount" value="{{{ old('saving_amount') }}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="management_fee"> Management Fee</label>
                                        <input class="form-control numbers" placeholder="" type="text" name="management_fee" id="management_fee" value="{{{ old('management_fee') }}}" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="vehicle_reg">Vehicle Registration No.</label>
                                        <select name="vehicle_reg" id="vehicle_reg" class="form-control">
                                            <option></option>
                                            @foreach($vehicles as $vehicle)
                                                <option value="{{$vehicle->id }}"> {{ $vehicle->regno }}:{{ $vehicle->make }}</option>
                                            @endforeach

                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="description"> Description</label>
                                        <textarea class="form-control" name="description" id="description">{{{ old('description') }}}</textarea>

                                    </div>
                                    <div class="form-group">
                                        <label for="bank_reference"> Bank Reference</label>
                                        <textarea class="form-control" name="bank_reference" id="bank_reference">{{{ old('bank_reference') }}}</textarea>

                                    </div>

                                    <div class="form-group">
                                        <label for="pay_method">Transaction Method </label>
                                        <select name="pay_method" id="pay_method" class="form-control" required>
                                            <option value="bank"> Bank</option>
                                            <option value="cash"> Cash</option>
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="transacted_by"> Transacted by</label>
                                        <input class="form-control" placeholder="" type="text" name="transacted_by" id="transacted_by" value="{{{ Auth::user()->username }}}" readonly='readonly' required>

                                    </div>
                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                                    </div>

                                </fieldset>
                            </form>

                        </div>
                    </div>
@stop

