@extends('layouts.savings')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}<br>
                                        @endforeach
                                    </div>
                                @endif

                                <div class="card-header">
                                    <h3>Update {{$loanproduct->name}}</h3>
                                </div>

                                <div class="card-block">
                                    <form method="POST" action="{{ url('loanproducts/update/'.$loanproduct->id) }}">@csrf

                                        <fieldset>
                                            <div class="form-group">
                                                <label for="username">Product Name</label>
                                                <input class="form-control" placeholder="" type="text" name="name" id="name" value="{{ $loanproduct->name }}" required>
                                            </div>


                                            <div class="form-group">
                                                <label for="username">Product Short Name</label>
                                                <input class="form-control" placeholder="" type="text" name="short_name" id="short_name" value="{{ $loanproduct->short_name }}" required>
                                            </div>

                                            <div class="form-group">
                                                <label for="username">Currency</label>
                                                <select class="form-control" name="currency" required>
                                                    <option value="{{ $loanproduct->currency }}"> {{ $loanproduct->currency}}</option>

                                                    @foreach($currencies as $currency)
                                                        <option value="{{ $currency->shortname }}"> {{ $currency->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <label for="username">Interest rate (Monthly)</label>
                                                <input class="form-control numbers" placeholder="" type="text" name="interest_rate" id="interest_rate" value="{{ $loanproduct->interest_rate }}" required>
                                            </div>

                                            <div class="form-group">
                                                <label for="username">Period (Months)</label>
                                                <input class="form-control numbers" placeholder="" type="text" name="period" id="period" value="{{ $loanproduct->period }}" required>
                                            </div>


                                            <div class="form-group">
                                                <label for="username">Interest Formula</label>
                                                <select class="form-control" name="formula" required>

                                                    <option value="{{$loanproduct->formula}}" selected>
                                                        @if($loanproduct->formula == 'SL')
                                                            Straight Line (SL)
                                                        @endif

                                                        @if($loanproduct->formula == 'RB')
                                                            Reducing Balance (RB)
                                                        @endif
                                                    </option>
                                                    <option>--------------------------------</option>
                                                    <option value="SL"> Straight Line (SL)</option>
                                                    <option value="RB"> Reducing Balance (RB)</option>

                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <label for="amortization">Amortization Method</label>
                                                <select class="form-control" name="amortization" id="amortization" required>

                                                    @if($loanproduct->amortization == 'EI')

                                                        <option value="{{$loanproduct->amortization}}"> Equal Installments</option>

                                                    @endif

                                                    @if($loanproduct->amortization == 'EP')
                                                        <option value="{{$loanproduct->amortization}}"> Equal Principals</option>

                                                    @endif
                                                    <option>--------------------------------</option>

                                                    <option value="EI"> Equal Installments</option>
                                                    <option value="EP"> Equal Principals</option>

                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="autoloanlimit">Auto Loan Limit</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">{{$currency->shortname}}</span>
                                                    <input class="form-control numbers" placeholder="" type="text" name="autoloanlimit" id="autoloanlimit" value="{{$loanproduct->auto_loan_limit}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="maxmultiplier">Maximum loan variable</label>
                                                <input class="form-control numbers" placeholder="" type="text" name="maxmultiplier" id="maxmultiplier" value="{{$loanproduct->max_multiplier}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="membership_duration">Membership duration</label>
                                                <input class="form-control numbers" placeholder="" type="text" name="membership_duration" id="membership_duration" value="{{$loanproduct->membership_duration}}">
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12"><hr></div>

                                            </div>

                                            <div class="row">
                                                <div class="col-lg-10">

                                                    <div class="form-actions form-group">

                                                        <button type="submit" class="btn btn-primary btn-sm pull-centre">Update Product</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
