@extends('layouts.loans')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}
                            </div>
                        @endif
                        @if (Session::has('delete_message'))
                            <div class="alert alert-danger">
                                {{ Session::get('delete_message') }}
                            </div>
                        @endif
                        <div class="card-header">
                            <h3>Loan Products</h3>

                            <div class="card-header-right">
                                <a class="dt-button btn-sm" href="{{ url('loanproducts/create')}}">New Loan Product</a>
                            </div>

                        </div>
                        <div class="card-block">
                            <?php
                            function asMoney($value) {
                                return number_format($value, 2);
                            }
                            ?>
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <th>#</th>
                                    <th>Product Name</th>
                                    <th>Short name</th>
                                    <th>Formula</th>
                                    <th>Interest Rate</th>
                                    <th>Period</th>
                                    <th>Currency</th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($loanproducts as $product)
                                        <tr>
                                            <td> {{ $i }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->short_name }}</td>
                                            <td>{{ $product->formula }}</td>
                                            <td>{{ $product->interest_rate }} % monthly</td>
                                            <td>{{ $product->period }} Months</td>
                                            <td>{{$product->currency}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="{{url('loanproducts/show/'.$product->id)}}">View</a></li>
                                                        <li><a href="{{url('loanproducts/edit/'.$product->id)}}">Update</a></li>

                                                        <li><a href="{{url('loanproducts/delete/'.$product->id)}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
