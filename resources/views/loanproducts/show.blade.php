@extends('layouts.loans')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>{{$loanproduct->name}}</h3>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <tr>
                                        <td>Loan Product Name</td><td>{{$loanproduct->name}}</td>

                                    </tr>
                                    <tr>
                                        <td>Short Name</td><td>{{$loanproduct->short_name}}</td>

                                    </tr>
                                    <tr>
                                        <td>Interest Rate</td><td>{{$loanproduct->interest_rate}} % monthly</td>

                                    </tr>
                                    <tr>
                                        <td>Period</td><td>{{$loanproduct->period}} Months</td>

                                    </tr>
                                    <tr>
                                        <td>Interest Formula</td>
                                        <td>
                                            @if($loanproduct->formula == 'SL')Straight Line (SL) @endif
                                            @if($loanproduct->formula == 'RB')Reducing Balance (RB) @endif
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Amortization Method</td>
                                        <td>
                                            @if($loanproduct->amortization == 'EI') Equal Installments @endif
                                            @if($loanproduct->amortization == 'EP') Equal Principal @endif
                                        </td>

                                    </tr>

                                    <tr>
                                        <td>Auto Loan Limit</td><td>{{number_format($loanproduct->auto_loan_limit,2)}}</td>

                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
