{{--TODO: Update teller member routes--}}
<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">

                <li class="">
                    <a href="{{ url('portal')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Members</span>

                    </a>
                </li>
            </ul>

        </div>
    </div>
</nav>

