<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="">
                    <a href="{{ url('accounts') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Chart of Accounts</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('journals') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Journal Entries</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('journals/create') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Add Journal Entr</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('particulars') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Particulars</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('budget/expenses') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Expenses</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('budget/incomes') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Income</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('petty_cash') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Petty Cash</span>

                    </a>
                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-grid"></i></span>
                        <span class="pcoded-mtext">Budget</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="active">
                            <a href="{{ url('budget/projections') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Projections</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('budget/interests') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Interests</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{ url('budget/income') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Other Income</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('budget/expenditure') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Expenditure</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-grid"></i></span>
                        <span class="pcoded-mtext">Banking</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="active">
                            <a href="{{ url('bankAccounts') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Bank Accounts</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('bankReconciliation/report') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Reconciliation Report</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{ url('bank/bank_deposit') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Bank Deposit</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('bankReconciliation/receipt') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Add Receipt</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('bankReconciliation/transact') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Disbursals and Payments</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('transactions/'.date('m-Y')) }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Transactions</span>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>

