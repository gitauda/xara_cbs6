<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="">
                    <a href="{{ url('payrollReports/selectPeriod') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Monthly Payslips</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('payrollReports/selectSummaryPeriod') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Payroll Summary</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('payrollReports/selectRemittancePeriod') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Pay Remittance</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('payrollReports/selectEarning') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Earning Report</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('payrollReports/selectAllowance') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Allowance Report</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('payrollReports/selectnontaxableincome') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Non Taxable Income Report</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('payrollReports/selectOvertime') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Overtimes Report</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('payrollReports/selectDeduction') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Deduction Report</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('payrollReports/selectPension') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Pension Report</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('payrollReports/selectRelief') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Relief Report</span>

                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
