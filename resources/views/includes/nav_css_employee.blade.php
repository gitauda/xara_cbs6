<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">


                <li class="">
                    <a href="{{ url('dashboard') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Home</span>

                    </a>
                </li>

                <li class="">
                    <a href="{{ url('css/leave') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Vacation Applications</span>

                    </a>
                </li>

                <li class="">
                    <a href="{{ url('css/balances') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Vacation Balances</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('/statement') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Pension statement</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('css/payslips') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Payslips</span>

                    </a>
                </li>
                <?php
                $organization = App\models\Organization::find(Auth::user()->organization_id);
                $psup = (strtotime($organization->payroll_support_period)-strtotime(date("Y-m-d"))) / 86400;
                ?>
                @if($psup<0 && $organization->payroll_license_key == 1)
                    <h4 style="color:red">
                        Your annual support license for payroll product has expired!!!....
                        Please upgrade your license by clicking on the link below.</h4>
                    <a href="{{ url('activatedproducts') }}">Upgrade license</a>
                @else
                @endif

            </ul>
        </div>
    </div>
</nav>

