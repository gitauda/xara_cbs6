<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="">
                    <a href="{{ url('disbursements') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Disbursement Options</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('matrices') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Guarantor Matrix</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('loanproducts') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Loan Products</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('loans') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Loan Applications</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('import_repayments') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Import Repayments</span>

                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
