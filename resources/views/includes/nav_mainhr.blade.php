<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-grid"></i></span>
                        <span class="pcoded-mtext">Employees</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="active">
                            <a href="{{ url('employees/create') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">New Employee</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('employees') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Manage Employees</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('Appraisals') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Employee Appraisal</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('occurences') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Employee Occurence </span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('deactives') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Activate Employee</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('employee_promotion') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Promote/Transfer</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('EmployeeForm') }}" target="_blank" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Employee Detail Form</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('payrollReports/selectPeriod') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Payslips</span>
                            </a>
                        </li>

                    </ul>
                </li>
{{--                <li class="pcoded-hasmenu">--}}
{{--                    <a href="javascript:void(0)" class="waves-effect waves-dark">--}}
{{--                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>--}}
{{--                        <span class="pcoded-mtext">Reminders</span>--}}
{{--                    </a>--}}
{{--                    <ul class="pcoded-submenu">--}}
{{--                        <li class="">--}}
{{--                            <a href="{{url('reminderview')}}" class="waves-effect waves-dark">--}}
{{--                                <span class="pcoded-mtext">Contract Reminders</span>--}}
{{--                            </a>--}}
{{--                        <li class="pcoded-submenu">--}}
{{--                        <li class="">--}}
{{--                            <a href="{{ url('reminderdoc/indexdoc')}}" class="waves-effect waves-dark">--}}
{{--                                <span class="pcoded-mtext">Document Reminders</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
                <li class="">
                    <a href="{{ url('leavemgmt')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Vacation Management</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('Properties')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Company Property</span>

                    </a>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Company Information</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{ url('organizations')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Organization Settings</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('branches')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Branches</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('departments')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Departments</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('portal')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Portal</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Bank Information</span>

                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{ url('banks')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Bank</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('bank_branch')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Bank Branches</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">HR Reports</span>

                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{url('employee/select')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Individual Employee report</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('reports/selectEmployeeStatus')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Employee List report</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('reports/nextofkin/selectEmployee')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Next of Kin Report</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('reports/selectEmployeeOccurence')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Employee Occurence report</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('reports/CompanyProperty/selectPeriod')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Company Property report</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('reports/Appraisals/selectPeriod')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Appraisal report</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Preferences</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{url('departments')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">HR Settings</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('system')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">System Settings</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('leavetypes')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Leave Types</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('holidays')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Holiday Management</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('deactives')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Activate Employee</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('migrate')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Data Migration</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('activatedproducts')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Upgrade License</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">General Settings</span>

                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{url('benefitsettings')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Benefit Settings</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('employee_type')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Employee Types</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('job_group')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Job Groups</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('occurencesettings')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Occurence Settings</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('citizenships')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Citizenship</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('appraisalcategories')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Appraisal Category</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('AppraisalSettings')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Appraisal Setting</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="">
                    <a href="{{ url('departments')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Departments</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('banks')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Banks</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('bank_branch')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Bank Branches</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('employee_type')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Employee Types</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('job_group')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Job Groups</span>

                    </a>
                </li>


                <?php
                $organization = App\models\Organization::find(Auth::user()->organization_id);
                $pdate = (strtotime($organization->payroll_support_period)-strtotime(date("Y-m-d"))) / 86400;
                ?>
                @if($pdate<0 && $organization->payroll_license_key ==1)
                    <h4 style="color:red">
                        Your annual support license for payroll product has expired!!!....
                        Please upgrade your license by clicking on the link below.</h4>
                    <a href="{{ url('activatedproducts') }}">Upgrade license</a>
                @else
                @endif
            </ul>
        </div>
    </div>
</nav>

