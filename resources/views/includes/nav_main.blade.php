<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                @if(Auth::user()->user_type !== 'teller')
                    <li class="">
                        <a href="{{ url('members')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                            <span class="pcoded-mtext">{{ Lang::get('messages.members') }}</span>

                        </a>
                    </li>
                @endif
                @if(Auth::user()->user_type == 'teller')
                    <li class="">
                        <a href="{{ url('/')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                            <span class="pcoded-mtext">{{ Lang::get('messages.members') }}</span>

                        </a>
                    </li>
                @endif

            </ul>
        </div>
    </div>
</nav>

