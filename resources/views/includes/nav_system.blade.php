<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">

                <li class="">
                    <a href="{{ url('users')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">System Users</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('roles')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">System Roles</span>

                    </a>
                </li>


                <li class="">
                    <a href="{{ url('tellers')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Tellers</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('import')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Bulk Import</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('audits')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Audit Trail</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('backups')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Backup & Restore</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('license')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Licensing</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('automated/loans')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Auto process Loans</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('automated/savings')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Auto process Savings</span>

                    </a>
                </li>


            </ul>
        </div>
    </div>
</nav>

