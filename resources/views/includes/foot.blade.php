<!-- Required Jquery -->
<script type="text/javascript" src="{{ asset('assets/bower_components/jquery/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/bower_components/popper.js/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- waves js -->
<script src="{{ asset('assets/assets/pages/waves/js/waves.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{asset('assets/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>

@if(Auth::user()->stripe_id == null)
<script type="text/javascript" src="{{ asset('assets/assets/pages/session-idle-Timeout/session_timeout.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/pages/session-idle-Timeout/session-idle-Timeout-custom.js')}}"></script>
@endif
<!-- Editable Table js -->
<script type="text/javascript" src="{{ asset('assets/assets/pages/edit-table/jquery.tabledit.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/pages/edit-table/editable.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/assets/pages/advance-elements/moment-with-locales.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/pages/advance-elements/bootstrap-datetimepicker.min.js')}}"></script>
<!-- Date-range picker js -->
<script type="text/javascript" src="{{asset('assets/bower_components/bootstrap-daterangepicker/js/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bower_components/datedropper/js/datedropper.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/bower_components/swiper/js/swiper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/js/swiper-custom.js')}}"></script>
<!-- Masking js -->
<script type="text/javascript" src="{{asset('assets/assets/pages/form-masking/inputmask.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/pages/form-masking/jquery.inputmask.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/pages/form-masking/autoNumeric.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/pages/form-masking/form-mask.js')}}"></script>
<!-- list-scroll js -->
<script type="text/javascript" src="{{ asset('assets/bower_components/stroll/js/stroll.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/pages/list-scroll/list-custom.js')}}"></script>

<!-- Custom js -->
<script type="text/javascript" src="{{asset('assets/assets/pages/advance-elements/custom-picker.js')}}"></script>
<script src="{{ asset('assets/assets/js/pcoded.min.js')}}"></script>
<script src="{{ asset('assets/assets/js/vertical/vertical-layout.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/pages/dashboard/custom-dashboard.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/assets/js/script.min.js')}}"></script>

