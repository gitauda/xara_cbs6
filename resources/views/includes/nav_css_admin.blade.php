<!-- [ navigation menu ] start -->
<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-grid"></i></span>
                        <span class="pcoded-mtext">Members</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="active">
                            <a href="{{ url('members/create') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">New Member</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('portal') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Members</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-sidebar"></i></span>
                        <span class="pcoded-mtext">Loans</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{('disbursements')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Disbursement Options</span>
                            </a>
                        <li class="pcoded-submenu">
                        <li class="">
                            <a href="{{ url('matrices')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Guarantor Matrix</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('loanproducts')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Loan Products</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{('loans')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Loan Applications</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('loanduplicates')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Loan Duplicates</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('import_repayments') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Import Repayments</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-wallet"></i></span>
                        <span class="pcoded-mtext">Savings</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{ url('savingproducts/create')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">New Product</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('savingproducts')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Saving Products</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('migration/import/savings')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Import Savings Transactions</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Share Capital</span>

                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{ url('shares/show/1')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Settings</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('sharecapital/contribution')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Contribution</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('sharecapital/dividend')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Dividends</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Vendors</span>

                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{('vendors/create')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">New Vendor</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{('vendors')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Vendors</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-basket-loaded"></i></span>
                        <span class="pcoded-mtext">Investments</span>

                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{url('investmentscats')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Categories|Classes|Types</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('saccoinvestments')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Investments</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{('projects')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Projects</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('projects/orders/show')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Project Orders</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="{{ url('products')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Products</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('orders')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-basket"></i></span>
                        <span class="pcoded-mtext">Orders</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('reports')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Reports</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('transaudits')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Transactions</span>

                    </a>

                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- [ navigation menu ] end -->
