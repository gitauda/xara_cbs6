<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="">
                    <a href="{{ url('member') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Member</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('memberloans') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Loans</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('loanliabilities') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Liabilities</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('guarantorapproval') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Guarantor Approval</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('savings') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Savings</span>

                    </a>
                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-basket-loaded"></i></span>
                        <span class="pcoded-mtext">Investments</span>

                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{url('projects/orders/show')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Project Orders</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>







