<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-grid"></i></span>
                        <span class="pcoded-mtext">Payroll Management</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="active">
                            <a href="{{ url('other_earnings') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Earnings</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('employee_allowances') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Allowances</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('overtimes') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Overtimes</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('employee_deductions') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Deductions </span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('pensions') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Pension</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('employee_relief') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Relief</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('employeenontaxables') }}" target="_blank" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Non-Taxable Income</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('payrollcalculator') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Payroll Calculator</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('email/payslip') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Email Payslips</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Process</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{ url('advance')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Advance Salaries</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{ url('payroll')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Payroll</span>
                            </a>
                        </li>
                        @can('process_payroll')
                        <li class="">
                            <a href="{{ url('unlockpayroll/index')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Approve Payroll Rerun</span>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Reports</span>

                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{ url('advanceReports')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Advance Reports</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('payrollReports')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Payroll Report</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('statutoryReports')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Statutory Reports</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Preferences</span>

                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{ url('accounts')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Accounts Settings</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('migrate')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Data Migration</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="">
                    <a href="{{ url('allowances')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Payroll Settings</span>

                    </a>
                </li>

                <?php
                $organization = App\models\Organization::find(Auth::user()->organization_id);
                $pdate = (strtotime($organization->payroll_support_period)-strtotime(date("Y-m-d"))) / 86400;
                ?>
                @if($pdate<0 && $organization->payroll_license_key ==1)
                    <h4 style="color:red">
                        Your annual support license for payroll product has expired!!!....
                        Please upgrade your license by clicking on the link below.</h4>
                    <a href="{{ url('activatedproducts') }}">Upgrade license</a>
                @else
                @endif
            </ul>
        </div>
    </div>
</nav>
