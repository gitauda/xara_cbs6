<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">

                <li class="">
                    <a href="{{ url('organizations')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Organization</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('branches')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Branches</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('groups')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Groups</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('currencies')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Currency</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('banks')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Banks</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('bankbranches')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Bank Branches</span>

                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
