
<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                <li class="">
                    <a href="{{ url('reports') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Member Reports</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('sharereports') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Share Reports</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('savingreports') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Saving Reports</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('loanreports') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Loan  Reports</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('loanrepayments') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Loan Repayment Reports</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('financialreports') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Financial Reports</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('statutoryreports') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Statutory Reports</span>

                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

