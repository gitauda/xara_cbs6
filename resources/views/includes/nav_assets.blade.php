<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">


                <li class="">
                    <a href="{{ url('assets') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Assets</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('assets_allocation') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Asset allocation</span>

                    </a>
                </li>


            </ul>
        </div>
    </div>
</nav>
