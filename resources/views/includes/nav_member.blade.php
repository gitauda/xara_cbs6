<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <div class="divider"></div>
            <ul class="pcoded-item pcoded-left-item">
                @if(Auth::user()->user_type != 'teller')
                <li class="">
                    <a href="{{ url('members/create') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">New Member</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ url('members') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Members</span>

                    </a>
                </li>

                <li class="">
                    <a href="{{ url('member_config') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Member configuration</span>
 
                    </a>
                </li>
                <li class="">
                    <a href="{{ url('vehicles') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Vehicles</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('tlbpayments') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Tlb Payments</span>

                    </a>
                </li> <li class="">
                    <a href="{{ url('assignvehicles') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Assign Vehicle</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('vehicleincomes') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Vehicle Income</span>

                    </a>
                </li>
                <li class="">
                    <a href="{{ url('vehicleexpenses') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
                        <span class="pcoded-mtext">Vehicle Expenses</span>

                    </a>
                </li>
                @endif
                @if(Auth::user()->user_type == 'admin' )

                 @endif

                @if(Auth::user()->user_type == 'teller')
                <li>
                    <a href="{{ url('/') }}">
                        <i class="fa fa-users fa-fw"></i>
                        Members
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>


 {{-- <nav class="navbar-default navbar-static-side" role="navigation" id="wrap">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
           @if(Auth::user()->user_type != 'teller')
            <li>
                <a href="{{ url('members/create') }}"><i class="glyphicon glyphicon-user fa-fw"></i> New Member</a>
            </li>
            <li>
                <a href="{{ url('members') }}"><i class="fa fa-users fa-fw"></i> Members</a>
            </li>
            <li>
                <a href="{{ url('member_config') }}"><i class="fa fa-cog fa-fw"></i>Member configuration</a>
            </li>
            <li>
                <a href="{{ url('vehicles') }}"><i class="glyphicon glyphicon-list-alt fa-fw"></i> Vehicles</a>
            </li>

            <li>
                <a href="{{ url('tlbpayments') }}"><i class="glyphicon glyphicon-edit"></i> Tlb Payments</a>
            </li>

             <li>
                <a href="{{ url('assignvehicles') }}"><i class="fa fa-user fa-fw"></i> Assign Vehicle</a>
            </li>

            <li>
                <a href="{{ url('vehicleincomes') }}"><i class="glyphicon glyphicon-folder-close fa-fw"></i> Vehicle Income</a>
            </li>

            <li>
                <a href="{{ url('vehicleexpenses') }}"><i class="glyphicon glyphicon-check fa-fw"></i> Vehicle Expenses</a>
            </li>

            @endif

            @if(Auth::user()->user_type == 'admin' )

            @endif

            @if(Auth::user()->user_type == 'teller')
            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-users fa-fw"></i>
                     Members
                 </a>
            </li>
            @endif
        </ul>
        <!-- /#side-menu -->
    </div>
    <!-- /.sidebar-collapse -->
</nav> --}}
<!-- /.navbar-static-side -->
