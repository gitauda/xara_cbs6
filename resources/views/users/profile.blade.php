@extends('layouts.system')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Users Profile</h3>


                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                    <tr>
                                        <td>username</td><td>{{ $user->username}}</td>
                                    </tr>
                                    <tr>
                                        <td>email</td><td>{{ $user->email}}</td>
                                    </tr>
                                    <tr>
                                        <td>password</td>
                                        <td>
                                            <a class="btn btn-info btn-xs" href="{{url('users/password/'.$user->id)}}">change
                                            </a>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <a class="btn btn-info btn-xs" href="{{url('users/edit/'.$user->id)}}">update</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@stop
