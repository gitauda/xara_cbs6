@extends('layouts.system')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>New  User</h3>

                            @if (Session::get('error'))
                                <div class="alert alert-error alert-danger">
                                    @if (is_array(Session::get('error')))
                                        {{ head(Session::get('error')) }}
                                    @endif
                                </div>
                            @endif

                            @if (Session::get('notice'))
                                <div class="alert">{{ Session::get('notice') }}</div>
                            @endif

                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{ url('users/newuser') }}}">@csrf
                                <input class="form-control" type="hidden" name="user_type" id="user_type" value="admin">

                                <fieldset>
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input class="form-control" placeholder="Enter Username" type="text" name="username" id="username" value="{{{ old('username') }}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email Address <small style="color: red;">*</small></label>
                                        <input class="form-control" placeholder="Enter Email address" type="text" name="email" id="email" value="{{{ old('email') }}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input class="form-control" placeholder="Enter Password" type="password" name="password" id="password" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Confirm Password</label>
                                        <input class="form-control" placeholder="Re-enter Password" type="password" name="password_confirmation" id="password_confirmation" required>
                                    </div>


                                    <div class="form-group">
                                        <label>Assign Roles</label>
                                        <table class="table table-striped table-bordered nowrap">

                                            <tr>

                                                @foreach($roles as $role)


                                                    <td>

                                                        <input type="checkbox" name="role[]" value="{{ $role->id }}"> {{$role->name}}


                                                    </td>

                                                @endforeach


                                            </tr>

                                        </table>
                                    </div>



                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Create User</button>
                                    </div>

                                </fieldset>
                            </form>             </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
