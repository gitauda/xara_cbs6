@extends('layouts.system')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Update  Password</h3>

                            @if (Session::has('error'))
                                <div class="alert alert-error alert-danger">

                                    {{ Session::get('error') }}

                                </div>
                            @endif

                        </div>
                        <div class="card-block">

                            <form method="post" action="{{url('users/password/'.$user->id)}}">@csrf


                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input class="form-control" placeholder="Enter Password" type="password" name="password" id="password">
                                </div>


                                <div class="form-group">
                                    <label for="password_confirmation">Confirm Password</label>
                                    <input class="form-control" placeholder="Re-enter Password" type="password" name="password_confirmation" id="password_confirmation">
                                </div>

                                <div class="form-actions form-group">

                                    <button type="submit" class="btn btn-info btn-sm">Update Password</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
