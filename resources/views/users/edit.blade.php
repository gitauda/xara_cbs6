@extends('layouts.system')
@section('xara_cbs')



    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3>Update  User</h3>

                            @if (Session::get('error'))
                                <div class="alert alert-error alert-danger">
                                    @if (is_array(Session::get('error')))
                                        {{ head(Session::get('error')) }}
                                    @endif
                                </div>
                            @endif

                            @if (Session::get('notice'))
                                <div class="alert">{{ Session::get('notice') }}</div>
                            @endif

                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{  url('users/update/'.$user->id) }}}">@csrf
                                <input class="form-control" type="hidden" name="user_type" id="user_type" value="admin">

                                <fieldset>
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input class="form-control" placeholder="Enter Username" type="text" name="username" id="username" value="{{ $user->username }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email Address <small style="color: red;">*</small></label>
                                        <input class="form-control" placeholder="Enter Email address" type="text" name="email" id="email" value="{{ $user->email }}" required>
                                    </div>

                                    <div class="form-actions form-group">

                                        <button type="submit" class="btn btn-primary btn-sm">Update User</button>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
