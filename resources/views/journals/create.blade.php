@extends('layouts.accounting')
@section('xara_cbs')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <div class="card-header">
                            <h3>New Journal Entry</h3>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif

                        </div>
                        <div class="card-block">

                            <form method="POST" action="{{{ url('journals') }}}" >@csrf

                                <fieldset>
                                    <div class="form-group">
                                        <label for="date">Date</label>
                                        <div class="right-inner-addon ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                            <input class="form-control datepicker" readonly placeholder="Date" type="text" name="date"
                                                   id="date" value="{{{ old('date') }}}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="username">Particular</label>
                                            <select class="form-control selectable" name="particular" id="particular" required>
                                                <option> --Select Particular--</option>
                                                @foreach($particulars as $particular)
                                                    @if(($particular->mode)=='user')
                                                        <option value="{{ $particular->id }}">{{ $particular->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Description</label>
                                            <textarea name="description" id="description" class="form-control"> </textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="username">Amount</label>
                                            <input class="form-control numbers" placeholder="" type="text" name="amount" id="amount"
                                                   value="{{{ old('amount') }}}">
                                        </div>

                                        {{--  <div class="form-group">
                                              <label for="username">Debit Account</label>
                                              <select class="form-control" name="debit_account">

                                                  <option></option>
                                                  @foreach($accounts as $account)
                                                  <option value="{{ $account->id }}">{{ $account->name."(".$account->code.")" }}</option>
                                                  @endforeach


                                              </select>
                                          </div>
                                  --}}

                                        {{-- <div class="form-group">
                                             <label for="username">Credit Account</label>
                                             <select class="form-control" name="credit_account">

                                                 <option></option>
                                                 @foreach($accounts as $account)
                                                 <option value="{{ $account->id }}">{{ $account->name."(".$account->code.")" }}</option>
                                                 @endforeach


                                             </select>
                                         </div>--}}

                                        <div class="form-group">
                                            <label for="narration">Narration</label>
                                            <select class="form-control selectable" name="narration" id="narration" required>
                                                <option value="0">Moto Sacco</option>
                                                @foreach($members as $member)
                                                    <option value="{{ $member->id }}">{{ $member->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                        <input type="hidden" name="user" value="{{ Auth::user()->username }}">


                                        <div class="form-actions form-group">

                                            <button type="submit" class="btn btn-primary btn-sm">Submit Entry</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
