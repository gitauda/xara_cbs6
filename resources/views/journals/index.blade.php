@extends('layouts.accounting')
@section('xara_cbs')
     <?php


    function asMoney($value) {
        return number_format((float)$value, 2);
    }

    ?>


     <div class="pcoded-inner-content">
         <div class="main-body">
             <div class="page-wrapper">
                 <div class="page-body">
                     <!-- [ page content ] start -->
                     <div class="card">
                         <div class="card-header">
                             <h3>Journal Entries</h3>


                             <div class="card-header-right">
                                 <a class="dt-button btn-sm" href="{{ url('journals/create')}}">New Journal Entry</a>
                             </div>

                         </div>
                         <div class="card-block">
                             <div class="dt-responsive table-responsive">
                                 <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                     <thead>


                                     <th>Transaction #</th>

                                     <th>Account Category</th>
                                     <th>Account Name</th>
                                     <th>Date</th>
                                     <th>Amount </th>
                                     <th>Type </th>
                                     <th>status </th>

                                     <th></th>

                                     </thead>
                                     <tbody>

                                     <?php $i = 1; ?>
                                     @foreach($journals as $journal)

                                         <tr>


                                             <td>{{ $journal->trans_no }}</td>

                                             <td>{{ $journal->account->category }}</td>
                                             <td>{{ $journal->account->name."(".$journal->account->code.")" }}</td>
                                             <td>{{ $journal->date }}</td>
                                             <td>

                                                 <?php


                                                 echo asMoney($journal->amount);

                                                 ?>
                                             </td>
                                             <td>{{ $journal->type }}</td>
                                             <td> @if($journal->void) Void @endif</td>
                                             <td>

                                                 <div class="btn-group">
                                                     <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                         Action <span class="caret"></span>
                                                     </button>

                                                     <ul class="dropdown-menu" role="menu">

                                                         <li><a href="{{url('journals/show/'.$journal->id)}}">View</a></li>
                                                         <li><a href="{{url('journals/delete/'.$journal->id)}}">Void</a></li>


                                                     </ul>
                                                 </div>

                                             </td>



                                         </tr>

                                         <?php $i++; ?>
                                     @endforeach


                                     </tbody>
                                 </table>
                             </div>

                         </div>
                     </div>
                     <!-- [ page content ] end -->
                 </div>
             </div>
         </div>
     </div>
@stop
