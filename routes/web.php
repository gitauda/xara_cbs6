<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Account;
use App\models\Allowance;
use App\models\Appraisalquestion;
use App\models\Audit;
use App\models\Bank;
use App\models\BBranch;
use App\models\Branch;
use App\models\Currency;
use App\models\Deduction;
use App\models\Department;
use App\models\EAllowances;
use App\models\Earnings;
use App\models\EDeduction;
use App\models\Employee;
use App\models\ERelief;
use App\models\Erporder;
use App\models\Erporderitem;
use App\models\EType;
use App\models\FleetCashCollector;
use App\models\Holiday;
use App\models\Investment;
use App\models\Investmentcategory;
use App\models\Investmentclass;
use App\models\Jobgroup;
use App\models\Journal;
use App\models\Leaveapplication;
use App\models\Leavetype;
use App\models\Loanaccount;
use App\models\Loanguarantor;
use App\models\Loantransaction;
use App\models\Loanproduct;
use App\models\Loanrepayment;
use App\models\Member;
use App\models\Notification;
use App\models\Organization;
use App\models\Particular;
use App\models\Payment;
use App\models\Pension;
use App\models\Product;
use App\models\Projectorder;
use App\models\Promotion;
use App\models\Relief;
use App\models\Savingaccount;
use \App\models\Savingproduct;
use \App\models\Savingtransaction;
use App\models\Supervisor;
use App\models\Site;
use App\models\Valuation;
use App\models\Vehicle;
use App\models\Vendor;
use App\User;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use PhpOffice\PhpSpreadsheet\NamedRange;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use \Barryvdh\DomPDF\Facade as PDF;

Route::get('/', function () {

    $count = count(User::all());

    if ($count == 0) {

        return view('signup');
    }

    //Member::dormantDeactivation();
    if (Auth::user()) {
        return  Redirect::to('/dashboard');
    } else {
        return view('sign_in');
    }

});

//Links outside filter

Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');


Route::get('/sign-in/google','UsersController@signInGoogle');
Route::get('/sign-in/google/redirect','UsersController@googleRedirect');

Route::get('/sign-in/facebook','UsersController@signInFacebook');
Route::get('/sign-in/facebook/redirect','UsersController@facebookRedirect');
Route::get('license', function () {
    $organization = Organization::find(1);
    return View::make('system.license', compact('organization'));
});
//first change before to middleware
//then create middleware with php artisan make:middleware loggedin
//write ur custom code in the middleware class
//register the middleware in App/Http/Kernel
Route::group(['middleware' => 'loggedin'], function () {
    Route::get('/dashboard', function () {
        if (Auth::user()) {
            $organization = Organization::find(1);
            $installationdate= date('Y-m-d',strtotime($organization->installation_date));

            $splitdate = explode('-', $installationdate);
            //split to obtain month and day from the installation date
            $day= $splitdate[2];
            $month= $splitdate[1];
            $year= date('Y');
            // The due date for annual subscription fee.
            $date = date('d-F-Y',strtotime($day.'-'.$month.'-'.$year));
            $notificationperiod = date('d-F-Y',strtotime($date.'-20 days'));
            //set the dates parameters for comparison purposes to avoid incorrct output which occurs from unformatted date comparison
            $todaydate=date('d-F-Y');
            $not_compare=strtotime($notificationperiod);
            $todaydate_compare=strtotime($todaydate);
            $date_compare=strtotime($date);
            // $notificationperiod =date('Y-m-d',strtotime($notification->created_at));

            /*today's date in y-m-d format*/
            $dateformat=date('Y-m-d');
            //obtain the two digit number of today's date for the purpose of using modulus operator to facilitate skipping a day during notifications period.
            $splittodaydate = explode('-', $dateformat);
            $dayofthemonth= $splittodaydate[2];
            $notification= DB::table('notifications')->where('created_at','>=',$dateformat)->orderBy('created_at','DESC')->first();
            //deactivate dormant users
            //Member::dormantDeactivation();
            //end Deactivation ofusers
            if ($todaydate_compare>=$not_compare && $todaydate_compare<=$date_compare)
                //if today's notification does not exist and the day is even(skip odd day)
            {
                if(empty($notification)&& $dayofthemonth% 2===0)
                {
                    $userid= Auth::user()->id;
                    $key = md5(uniqid());
                    Notification::notifyUser($userid,"Hello, Please make payments for annual suscription fees before ".$date,"payment reminder","paymentnotification/invoiceshow/".$key."/".$userid,$key);

                }
            }

            if (Auth::user()->hasRole('Admin')) {
                $members = Member::all();
                //Grab all available loans
                $loanaccounts =Loanaccount::all();
                foreach ($loanaccounts as $loanaccount) {
                    $period = $loanaccount->period;
                    $endpoint = 30 * $period;
                    $end = '+$endpoint';
                    //Take member ID
                    $id = $loanaccount->member_id;
                    //Get all the member details
                    $member = Member::find($id);
                    //Take the repayment period
                    $startdate = date('Y-m-d', strtotime($loanaccount->repayment_start_date));
                    for ($date = $startdate; $date < date('Y-m-d', strtotime($date . "+$endpoint days"));
                         $date = date('Y-m-d', strtotime($date . "+28 days"))) {
                        //Reset execution limit
                        set_time_limit(60);
                        //Get current month
                        $now = date('Y-m-d');
                        $today = date('Y-m-d', strtotime($now));
                        $month = date('m');
                        //Get payment date
                        $paydate = date('Y-m-d', strtotime($date . "+30 days"));
                        $defaultdate = date('Y-m-d', strtotime($date . "+31 days"));
                        //Get Payment month
                        $pay_month = date('m', strtotime($paydate));
                        //When payment month equals current month send an email
                        if ($month == $pay_month && $today <= $paydate) {
                            /*Mail::send( 'emails.notification', array('name'=>$member->name,
                            'pay_date'=>date('d-F-Y',strtotime($date."+30 days"))), function( $message )
                             use ($member){
                                 $message->to($member->email)->subject( 'Loan Repayment Notification' );
                                });  */
                            break;
                        } else if ($month == $pay_month && $today == $defaultdate) {
                            $loanaccount->is_defaulted = TRUE;
                            $loanaccount->save();
                            $defaultrepay =new Loanrepayment;
                            $defaultrepay->loanaccount_id = $loanaccount->id;
                            $defaultrepay->date = $defaultdate;
                            $defaultrepay->principal_paid = 0.00;
                            $defaultrepay->interest_paid = 0.00;
                            $defaultrepay->default_period = date('m-Y', strtotime($date . "+31 days"));
                            $defaultrepay->save();
                        }
                        break;
                    }
                }
                return view('dashboard', compact('members'));
            }
            if (Auth::user()->hasRole('teller')) {

                $members = Member::all();

                return view('tellers.dashboard', compact('members'));

            }

            if (Auth::user()->hasRole('credit')) {

                $loanaccounts = Loanaccount::all();
                $members = Member::all();
                return view('credits.dashboard', compact('loanaccounts','members'));

            }

            if (Auth::user()->hasRole('member')) {

                $loans = Loanproduct::all();
                $member = Member::where('email', Auth::user()->email)->first();
                $products = Product::all();
                //$rproducts = Product::getRemoteProducts();

                return view('css.memberindex', compact('loans', 'member', 'products'));

            }


        } else {
            return view('sign_in');
        }
    });

    Route::get('user/logout', 'UsersController@logout');


    Route::get('users/create', 'UsersController@create');
    Route::get('users/edit/{user}', 'UsersController@edit');
    Route::post('users/update/{user}', 'UsersController@update');


    Route::resource('users', 'UsersController');
    Route::get('users/activate/{user}', 'UsersController@activate');
    Route::get('users/deactivate/{user}', 'UsersController@deactivate');
    Route::get('users/destroy/{user}', 'UsersController@destroy');
    Route::get('users/password/{user}', 'UsersController@Password');
    Route::post('users/password/{user}', 'UsersController@changePassword');
    Route::get('users/profile/{user}', 'UsersController@profile');
    Route::get('users/add', 'UsersController@add');
    Route::post('users/newuser', 'UsersController@newuser');


    Route::get('tellers', 'UsersController@tellers');
    Route::get('tellers/create/{id}', 'UsersController@createteller');
    Route::get('tellers/activate/{id}', 'UsersController@activateteller');
    Route::get('tellers/deactivate/{id}', 'UsersController@deactivateteller');

    Route::get('credits', 'UsersController@credits');
    Route::get('credits/create/{id}', 'UsersController@createcredit');
    Route::get('credits/activate/{id}', 'UsersController@activatecredit');
    Route::get('credits/deactivate/{id}', 'UsersController@deactivatecredit');

    Route::get('members/profile', 'UsersController@password2');
    Route::post('users/pass', 'UsersController@changePassword2');

    Route::get('payment_options', 'SubscriptionController@options');
    Route::get('create/plan', 'SubscriptionController@createPlan');
    Route::post('storePlan', 'SubscriptionController@storePlan');
    Route::post('pay', 'SubscriptionController@paysubs');
    Route::get('payment/{plan}', 'SubscriptionController@payment');



    Route::resource('banks', 'BanksController');
    Route::post('banks/update/{id}', 'BanksController@update');
    Route::get('banks/delete/{id}', 'BanksController@destroy');
    Route::get('banks/edit/{id}', 'BanksController@edit');

    /*
    * bank branch routes
    */
    Route::get('bankReconcile/reconciliation/{month}', 'BankAccountController@reconcileTransactions');

    Route::resource('bankbranches', 'BankBranchController');
    Route::post('bankbranches/update/{id}', 'BankBranchController@update');
    Route::get('bankbranches/delete/{id}', 'BankBranchController@destroy');
    Route::get('bankbranches/edit/{id}', 'BankBranchController@edit');
    Route::get('bankbranchesimport', function () {
        return View::make('bank_branch.import');
    });

    /**
     * Bank Reconciliations
     */
    Route::get('bankAccounts/reconcile/{id}', 'BankAccountController@showReconcile');
    Route::post('bankAccounts/uploadStatement', 'BankAccountController@uploadBankStatement');
    Route::post('bankAccount/reconcile', 'BankAccountController@reconcileStatement');

    Route::get('bankAccount/reconcile/add/{id}/{id2}/{id3}/{id4}', 'BankAccountController@addStatementTransaction');
    Route::post('bankAccount/reconcile/add', 'BankAccountController@saveStatementTransaction');
    Route::get('bankReconciliation/receipt', function(){
        return View::make('banking.receipt');
    });

    Route::get('transactions/{month}', 'BankAccountController@transactions');
    Route::post('bankReconciliation/receipt', 'AccountsController@savereceipt');
    Route::post('bankReconciliation/payment', 'AccountsController@addBankTransaction');
    Route::get('bankReconciliation/report', 'ReportsController@displayRecOptions');
    Route::post('bankReconciliartion/generateReport', 'ReportsController@showRecReport');
    Route::post('BankReconciliation/edit', 'BankAccountController@edittrans');
    Route::get('bankReconciliation/reconcilestatement/{bid}/{aid}', 'BankAccountController@reconcile');
    Route::get('deletezeros', function(){
        $journals = Journal::where('amount', 0)->get();
        foreach ($journals as $journal) {
            // code...
            $journal->delete();
        }
    });


    /*
    * departments routes
    */
    Route::resource('departments', 'DepartmentsController');
    Route::post('departments/update/{id}', 'DepartmentsController@update');
    Route::get('departments/delete/{id}', 'DepartmentsController@destroy');
    Route::get('departments/edit/{id}', 'DepartmentsController@edit');


    /*
    * bank branch routes
    */

    Route::resource('bank_branch', 'BankBranchController');
    Route::post('bank_branch/update/{id}', 'BankBranchController@update');
    Route::get('bank_branch/delete/{id}', 'BankBranchController@destroy');
    Route::get('bank_branch/edit/{id}', 'BankBranchController@edit');

    /*
    * allowances routes
    */

    Route::resource('allowances', 'AllowancesController');
    Route::post('allowances/update/{id}', 'AllowancesController@update');
    Route::get('allowances/delete/{id}', 'AllowancesController@destroy');
    Route::get('allowances/edit/{id}', 'AllowancesController@edit');

    /*
    * earningsettings routes
    */

    Route::resource('earningsettings', 'EarningsettingsController');
    Route::post('earningsettings/update/{id}', 'EarningsettingsController@update');
    Route::get('earningsettings/delete/{id}', 'EarningsettingsController@destroy');
    Route::get('earningsettings/edit/{id}', 'EarningsettingsController@edit');

    /*
    * benefits setting routes
    */

    Route::resource('benefitsettings', 'BenefitSettingsController');
    Route::post('benefitsettings/update/{id}', 'BenefitSettingsController@update');
    Route::get('benefitsettings/delete/{id}', 'BenefitSettingsController@destroy');
    Route::get('benefitsettings/edit/{id}', 'BenefitSettingsController@edit');

    /*
    * reliefs routes
    */

    Route::resource('reliefs', 'ReliefsController');
    Route::post('reliefs/update/{id}', 'ReliefsController@update');
    Route::get('reliefs/delete/{id}', 'ReliefsController@destroy');
    Route::get('reliefs/edit/{id}', 'ReliefsController@edit');

    /*
    * deductions routes
    */

    Route::resource('deductions', 'DeductionsController');
    Route::post('deductions/update/{id}', 'DeductionsController@update');
    Route::get('deductions/delete/{id}', 'DeductionsController@destroy');
    Route::get('deductions/edit/{id}', 'DeductionsController@edit');

    /*
    * nontaxables routes
    */

    Route::resource('nontaxables', 'NonTaxablesController');
    Route::post('nontaxables/update/{id}', 'NonTaxablesController@update');
    Route::get('nontaxables/delete/{id}', 'NonTaxablesController@destroy');
    Route::get('nontaxables/edit/{id}', 'NonTaxablesController@edit');

    /*
    * nssf routes
    */

    Route::resource('nssf', 'NssfController');
    Route::post('nssf/update/{id}', 'NssfController@update');
    Route::get('nssf/delete/{id}', 'NssfController@destroy');
    Route::get('nssf/edit/{id}', 'NssfController@edit');

    /*
    * nhif routes
    */

    Route::resource('nhif', 'NhifController');
    Route::post('nhif/update/{id}', 'NhifController@update');
    Route::get('nhif/delete/{id}', 'NhifController@destroy');
    Route::get('nhif/edit/{id}', 'NhifController@edit');

    /*
    * job group routes
    */

    Route::resource('job_group', 'JobGroupController');
    Route::post('job_group/update/{id}', 'JobGroupController@update');
    Route::get('job_group/delete/{id}', 'JobGroupController@destroy');
    Route::get('job_group/edit/{id}', 'JobGroupController@edit');
    Route::get('job_group/show/{id}', 'JobGroupController@show');

    /**
     * Employee routes
     */
    Route::resource('employees', 'EmployeesController');
    Route::get('employees/show/{id}', 'EmployeesController@show');


    Route::get('employees/create', 'EmployeesController@create');

    Route::resource('employees', 'EmployeesController');
    Route::post('employees/update/{id}', 'EmployeesController@update');
    Route::get('employees/deactivate/{id}', 'EmployeesController@deactivate');
    Route::get('employees/activate/{id}', 'EmployeesController@activate');
    Route::get('employees/edit/{id}', 'EmployeesController@edit');
    Route::get('employees/view/{id}', 'EmployeesController@view');
    Route::get('employees/viewdeactive/{id}', 'EmployeesController@viewdeactive');

    Route::post('createCitizenship', 'EmployeesController@createcitizenship');
    Route::post('createEducation', 'EmployeesController@createeducation');
    Route::post('createBank', 'EmployeesController@createbank');
    Route::post('createBankBranch', 'EmployeesController@createbankbranch');
    Route::post('createBranch', 'EmployeesController@createbranch');
    Route::post('createDepartment', 'EmployeesController@createdepartment');
    Route::post('createType', 'EmployeesController@createtype');
    Route::post('createJobtitle', 'EmployeesController@createjobtitle');

    Route::post('createGroup', 'EmployeesController@creategroup');
    Route::post('createEmployee', 'EmployeesController@serializeDoc');
    Route::get('employeeIndex', 'EmployeesController@getIndex');

    // ======================JOURNAL ROUTES =============================//
    Route::resource('journals', 'JournalsController');
    Route::post('journals/update/{id}', 'JournalsController@update');
    Route::get('journals/delete/{id}', 'JournalsController@destroy');
    Route::get('journals/edit/{id}', 'JournalsController@edit');
    Route::get('journals/show/{id}', 'JournalsController@show');
//=========================END JOURNAL ROUTES =======================//

//=========== CHARGES ROUTES ==================================//
    Route::resource('charges', 'ChargesController');
    Route::post('charges/update/{id}', 'ChargesController@update');
    Route::get('charges/delete/{id}', 'ChargesController@destroy');
    Route::get('charges/edit/{id}', 'ChargesController@edit');
    Route::get('charges/show/{id}', 'ChargesController@show');
    Route::get('charges/disable/{id}', 'ChargesController@disable');
    Route::get('charges/enable/{id}', 'ChargesController@enable');
//=========== END  CHARGES ROUTES ==================================//
    /**
     * Budget Routes
     */
    Route::get('budget/projections', 'AccountsController@projections');
    Route::get('budget/projections/create', 'AccountsController@createProjection');
    Route::post('budget/projections/create', 'AccountsController@storeProjection');
    Route::get('budget/interests', 'AccountsController@proposalInterests');
    Route::get('budget/income', 'AccountsController@proposalOtherIncome');
    Route::get('budget/expenditure', 'AccountsController@proposalExpenditure');
    Route::get('budget/proposal/create', 'AccountsController@createProposal');
    Route::post('budget/proposal/create', 'AccountsController@storeProposal');
    Route::get('budget/expenses', 'AccountsController@showExpenses');
    Route::get('budget/incomes', 'AccountsController@showIncomes');
    Route::get('budget/incomes/create', 'AccountsController@createIncomes');
    Route::get('budget/expenses/create', 'AccountsController@createExpenses');
    Route::post('budget/expenses/create', 'AccountsController@storeExpenses');

    Route::get('EmployeeForm', function(){

        $organization = Organization::find(Auth::user()->organization_id);

        $pdf = PDF::loadView('pdf.employee_form', compact('organization'))->setPaper('a4');

        return $pdf->stream('Employee_Form.pdf');

    });

    /*
    * employee type routes
    */
    Route::resource('employee_type', 'EmployeeTypeController');
    Route::post('employee_type/update/{id}', 'EmployeeTypeController@update');
    Route::get('employee_type/delete/{id}', 'EmployeeTypeController@destroy');
    Route::get('employee_type/edit/{id}', 'EmployeeTypeController@edit');

    /*
    * employee earnings routes
    */
    Route::resource('other_earnings', 'EarningsController');
    Route::post('other_earnings/update/{id}', 'EarningsController@update');
    Route::get('other_earnings/delete/{id}', 'EarningsController@destroy');
    Route::get('other_earnings/edit/{id}', 'EarningsController@edit');
    Route::get('other_earnings/view/{id}', 'EarningsController@view');
    Route::post('createEarning', 'EarningsController@createearning');

    /*
    * employee reliefs routes
    */
    Route::resource('employee_relief', 'EmployeeReliefController');
    Route::post('employee_relief/update/{id}', 'EmployeeReliefController@update');
    Route::get('employee_relief/delete/{id}', 'EmployeeReliefController@destroy');
    Route::get('employee_relief/edit/{id}', 'EmployeeReliefController@edit');
    Route::get('employee_relief/view/{id}', 'EmployeeReliefController@view');
    Route::post('createRelief', 'EmployeeReliefController@createrelief');

    /*
   * employee allowances routes
   */
    Route::resource('employee_allowances', 'EmployeeAllowancesController');
    Route::post('employee_allowances/update/{id}', 'EmployeeAllowancesController@update');
    Route::get('employee_allowances/delete/{id}', 'EmployeeAllowancesController@destroy');
    Route::get('employee_allowances/edit/{id}', 'EmployeeAllowancesController@edit');
    Route::get('employee_allowances/view/{id}', 'EmployeeAllowancesController@view');
    Route::post('createAllowance', 'EmployeeAllowancesController@createallowance');
    Route::post('reloaddata', 'EmployeeAllowancesController@display');

    /*
    * employee nontaxables routes
    */

    Route::resource('employeenontaxables', 'EmployeeNonTaxableController');
    Route::post('employeenontaxables/update/{id}', 'EmployeeNonTaxableController@update');
    Route::get('employeenontaxables/delete/{id}', 'EmployeeNonTaxableController@destroy');
    Route::get('employeenontaxables/edit/{id}', 'EmployeeNonTaxableController@edit');
    Route::get('employeenontaxables/view/{id}', 'EmployeeNonTaxableController@view');
    Route::post('createNontaxable', 'EmployeeNonTaxableController@createnontaxable');

    /*
    * employee deductions routes
    */
    Route::resource('employee_deductions', 'EmployeeDeductionsController');
    Route::post('employee_deductions/update/{id}', 'EmployeeDeductionsController@update');
    Route::get('employee_deductions/delete/{id}', 'EmployeeDeductionsController@destroy');
    Route::get('employee_deductions/edit/{id}', 'EmployeeDeductionsController@edit');
    Route::get('employee_deductions/view/{id}', 'EmployeeDeductionsController@view');
    Route::post('createDeduction', 'EmployeeDeductionsController@creatededuction');

    /*
   * employee documents routes
   */

    Route::resource('documents', 'DocumentsController');
    Route::post('documents/update/{id}', 'DocumentsController@update');
    Route::get('documents/delete/{id}', 'DocumentsController@destroy');
    Route::get('documents/edit/{id}', 'DocumentsController@edit');
    Route::get('documents/download/{id}', 'DocumentsController@getDownload');
    Route::get('documents/create/{id}', 'DocumentsController@create');
    Route::post('createDoc', 'DocumentsController@serializecheck');

    Route::resource('NextOfKins', 'NextOfKinsController');
    Route::post('NextOfKins/update/{id}', 'NextOfKinsController@update');
    Route::get('NextOfKins/delete/{id}', 'NextOfKinsController@destroy');
    Route::get('NextOfKins/edit/{id}', 'NextOfKinsController@edit');
    Route::get('NextOfKins/view/{id}', 'NextOfKinsController@view');
    Route::get('NextOfKins/create/{id}', 'NextOfKinsController@create');
    Route::post('createKin', 'NextOfKinsController@serializecheck');

    Route::resource('Appraisals', 'AppraisalsController');
    Route::post('Appraisals/update/{id}', 'AppraisalsController@update');
    Route::get('Appraisals/delete/{id}', 'AppraisalsController@destroy');
    Route::get('Appraisals/edit/{id}', 'AppraisalsController@edit');
    Route::get('Appraisals/view/{id}', 'AppraisalsController@view');
    Route::post('createQuestion', 'AppraisalsController@createquestion');

    Route::resource('Properties', 'PropertiesController');
    Route::post('Properties/update/{id}', 'PropertiesController@update');
    Route::get('Properties/delete/{id}', 'PropertiesController@destroy');
    Route::get('Properties/edit/{id}', 'PropertiesController@edit');
    Route::get('Properties/view/{id}', 'PropertiesController@view');

    Route::resource('AppraisalSettings', 'AppraisalSettingsController');
    Route::post('AppraisalSettings/update/{id}', 'AppraisalSettingsController@update');
    Route::get('AppraisalSettings/delete/{id}', 'AppraisalSettingsController@destroy');
    Route::get('AppraisalSettings/edit/{id}', 'AppraisalSettingsController@edit');
    Route::post('createCategory', 'AppraisalSettingsController@createcategory');

    Route::resource('appraisalcategories', 'AppraisalCategoryController');
    Route::post('appraisalcategories/update/{id}', 'AppraisalCategoryController@update');
    Route::get('appraisalcategories/delete/{id}', 'AppraisalCategoryController@destroy');
    Route::get('appraisalcategories/edit/{id}', 'AppraisalCategoryController@edit');

    /*
    * advance routes
    */
    Route::resource('advance', 'AdvanceController');
    Route::post('deleteadvance', 'AdvanceController@del_exist');
    Route::post('advance/preview', 'AdvanceController@create');
    Route::post('createAccount', 'AdvanceController@createaccount');

    /**
     * occurence settings routes
     */
    Route::resource('occurencesettings', 'OccurencesettingsController');
    Route::post('occurencesettings/update/{id}', 'OccurencesettingsController@update');
    Route::get('occurencesettings/delete/{id}', 'OccurencesettingsController@destroy');
    Route::get('occurencesettings/edit/{id}', 'OccurencesettingsController@edit');

    /**
     * Occurence routes
     */
    Route::resource('occurences', 'OccurencesController');
    Route::post('occurences/update/{id}', 'OccurencesController@update');
    Route::get('occurences/delete/{id}', 'OccurencesController@destroy');
    Route::get('occurences/edit/{id}', 'OccurencesController@edit');
    Route::get('occurences/view/{id}', 'OccurencesController@view');
    Route::get('occurences/download/{id}', 'OccurencesController@getDownload');
    Route::post('createOccurence', 'OccurencesController@createoccurence');


    /**
     * citizenship routes
     */

    Route::resource('citizenships', 'CitizenshipController');
    Route::post('citizenships/update/{id}', 'CitizenshipController@update');
    Route::get('citizenships/delete/{id}', 'CitizenshipController@destroy');
    Route::get('citizenships/edit/{id}', 'CitizenshipController@edit');


    Route::get('authorizepurchaseorder/{id}','ErpReportsController@authorizepurchaseorder');
    Route::get('approvepurchaseorder/{id}','ErpReportsController@approvepurchaseorder');
    Route::get('reviewpurchaseorder/{id}','ErpReportsController@reviewpurchaseorder');
    Route::get('submitpurchaseorder/{id}','ErpReportsController@submitpurchaseorder');

    Route::get('bankReconciliation/transact', function () {

        return View::make('banking.bankTransactionEntries');
    });

    /*
    * payroll routes
    */
    Route::resource('payroll', 'PayrollController');
    Route::post('deleterow', 'PayrollController@del_exist');
    Route::post('showrecord', 'PayrollController@display');
    Route::post('shownet', 'PayrollController@disp');
    Route::post('showgross', 'PayrollController@dispgross');
    Route::post('payroll/preview', 'PayrollController@create');
    Route::get('payrollpreviewprint/{period}', 'PayrollController@previewprint');
    Route::get('unlockpayroll/index', 'PayrollController@unlockindex');
    Route::get('payroll/view/{id}', 'PayrollController@viewpayroll');
    Route::get('unlockpayroll/{id}', 'PayrollController@unlockpayroll');
    Route::post('unlockpayroll', 'PayrollController@dounlockpayroll');
    Route::post('createNewAccount', 'PayrollController@createaccount');

    Route::get('payrollcalculator', function(){
        $currency = Currency::find(1);
        return view('payroll.payroll_calculator',compact('currency'));

    });

    Route::get('deactives', function(){

        $employees = Employee::getDeactiveEmployee();

        return view('employees.activate', compact('employees'));

    } );

    /**
     * Advance reports
     */
    Route::get('advanceReports', function(){
        return view('employees.advancereports');
    });



    /**
     * statutory reports
     */
    Route::get('statutoryReports', function(){
        return view('employees.statutoryreports');
    });

    /***************
     * Employee promotions
     */
    Route::get('employee_promotion', function(){
        $promotions = Promotion::whereNull('organization_id')->orWhere('organization_id',Auth::user()->organization_id)->get();

        Audit::logaudit('Promotions', 'view', 'viewed promotions');

        return view('promotions.index', compact('promotions'));

    });

    //email payslip routes
    //Route::post('employee_promotion', 'EmployeesController@promote_transfer');
    Route::get('email/payslip', 'payslipEmailController@index');
    Route::post('email/payslip/employees', 'payslipEmailController@sendEmail');


    /**
     * employee leave application
     * surbodinate leave
     * check payslip
     * employee leave balance
     */
    Route::get('css/leave', function(){

        $employeeid = DB::table('employee')
            ->where('organization_id',Auth::user()->organization_id)
            ->where('personal_file_number', '=', Auth::user()->username)
            ->pluck('id');


        $employee = Employee::findorfail($employeeid);

        $leaveapplications = DB::table('leaveapplications')->where('organization_id',Auth::user()->organization_id)->where('employee_id', '=', $employee->id)->get();

        return view('css.leave', compact('employee', 'leaveapplications'));
    });

    Route::get('css/subordinateleave', function(){

        $employeeid = DB::table('employee')->where('personal_file_number', '=', Auth::user()->username)->pluck('id');
        $c = Supervisor::where('supervisor_id', $employeeid)->count();

        $employee = Employee::findorfail($employeeid);

        //$leaveapplications = DB::table('leaveapplications')->where('employee_id', '=', $employee->id)->get();

        return view('css.approveleave', compact('c','leaveapplications'));
    });

    Route::get('css/payslips', function(){

        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)
            ->where('personal_file_number', '=', Auth::user()->username)
            ->pluck('id');

        $employee = Employee::findorfail($employeeid);

        return view('css.payslip', compact('employee'));
    });

    Route::get('employeeleave/view/{id}', 'LeaveapplicationsController@cssleaveapprove');
    Route::get('supervisorapproval/{id}', 'LeaveapplicationsController@supervisorapprove');
    Route::get('supervisorreject/{id}', 'LeaveapplicationsController@supervisorreject');


    Route::get('css/leaveapply', function(){

        $employeeid = DB::table('employee')
            ->where('organization_id',Auth::user()->organization_id)
            ->where('personal_file_number', '=', Auth::user()->username)
            ->pluck('id');

        $employee = Employee::findorfail($employeeid);
        $leavetypes = Leavetype::where('organization_id',Auth::user()->organization_id)->get();

        return view('css.leaveapply', compact('employee', 'leavetypes'));
    });

    Route::get('css/balances', function(){

        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', Auth::user()->username)->pluck('id');

        $employee = Employee::findorfail($employeeid);
        $leavetypes = Leavetype::where('organization_id',Auth::user()->organization_id)->get();

        return view('css.balances', compact('employee', 'leavetypes'));
    });


    /**
     * Reports Controller
     */
    Route::get('itax/download', 'ReportsController@getDownload');

    Route::get('statement', 'StatementController@index');
    Route::get('statement/report', 'ReportsController@statement');


    Route::get('reports/negativeleaves', 'ReportsController@negativeleaves');

    #Route::get('reports/selectEmployeeStatus', 'ReportsController@selstate');
    Route::post('reports/employeelist', 'ReportsController@employees');
   # Route::get('employee/select', 'ReportsController@employee_earnings');
    #Route::post('reports/employee', 'ReportsController@individual');
    Route::get('reports/compliance/selectEmployee', 'ReportsController@selEmpDisc');
    Route::post('reports/compliance', 'ReportsController@discipline');
    Route::get('reports/promotion/selectEmployee', 'ReportsController@selPromEmp');
    Route::post('reports/promotion', 'ReportsController@promotion');
    Route::get('payrollReports/selectPeriod', 'ReportsController@period_payslip');
    Route::post('payrollReports/payslip', 'ReportsController@payslip');
    Route::get('payrollReports/selectAllowance', 'ReportsController@employee_allowances');
    Route::post('payrollReports/allowances', 'ReportsController@allowances');
    Route::get('payrollReports/selectEarning', 'ReportsController@employee_earnings');
    Route::post('payrollReports/earnings', 'ReportsController@earnings');
    Route::get('payrollReports/selectOvertime', 'ReportsController@employee_overtimes');
    Route::post('payrollReports/overtimes', 'ReportsController@overtimes');
    Route::get('payrollReports/selectRelief', 'ReportsController@employee_reliefs');
    Route::post('payrollReports/reliefs', 'ReportsController@reliefs');
    Route::get('payrollReports/selectDeduction', 'ReportsController@employee_deductions');
    Route::post('payrollReports/deductions', 'ReportsController@deductions');
    Route::get('payrollReports/selectnontaxableincome', 'ReportsController@employeenontaxableselect');
    Route::post('payrollReports/nontaxables', 'ReportsController@employeenontaxables');
    Route::get('payrollReports/selectPayePeriod', 'ReportsController@period_paye');
    Route::post('payrollReports/payeReturns', 'ReportsController@payeReturns');
    Route::post('payrollReports/p9form', 'ReportsController@p9form');
    Route::get('payrollReports/selectRemittancePeriod', 'ReportsController@period_rem');
    Route::post('payrollReports/payRemittances', 'ReportsController@payeRems');
    Route::get('payrollReports/selectSummaryPeriod', 'ReportsController@period_summary');
    Route::post('payrollReports/payrollSummary', 'ReportsController@paySummary');
    Route::get('payrollReports/selectNssfPeriod', 'ReportsController@period_nssf');
    Route::post('payrollReports/nssfReturns', 'ReportsController@nssfReturns');
    Route::get('payrollReports/selectNhifPeriod', 'ReportsController@period_nhif');
    Route::post('payrollReports/nhifReturns', 'ReportsController@nhifReturns');
    Route::get('payrollReports/selectNssfExcelPeriod', 'ReportsController@period_excel');
    Route::post('payrollReports/nssfExcel', 'ReportsController@export');
    Route::get('reports/selectEmployeeOccurence', 'ReportsController@selEmp');
    Route::post('reports/occurence', 'ReportsController@occurence');
    Route::get('reports/CompanyProperty/selectPeriod', 'ReportsController@propertyperiod');
    Route::post('reports/companyproperty', 'ReportsController@property');
    Route::get('reports/Appraisals/selectPeriod', 'ReportsController@appraisalperiod');
    Route::post('reports/appraisal', 'ReportsController@appraisal');
    Route::get('reports/nextofkin/selectEmployee', 'ReportsController@selempkin');
    Route::post('reports/EmployeeKin', 'ReportsController@nextkin');
    Route::get('advanceReports/selectRemittancePeriod', 'ReportsController@period_advrem');
    Route::post('advanceReports/advanceRemittances', 'ReportsController@payeAdvRems');
    Route::get('advanceReports/selectSummaryPeriod', 'ReportsController@period_advsummary');
    Route::post('advanceReports/advanceSummary', 'ReportsController@payAdvSummary');

    /**
     * Reports
     */
    Route::get('reports/listing', 'ReportsController@members');
    Route::post('reports/listing', 'ReportsController@membersListing');
    Route::get('reports/remittance', 'ReportsController@remittance');
    Route::get('reports/blank', 'ReportsController@template');
    Route::get('reports/loanlisting', 'ReportsController@allListing');

    Route::get('reports/loanproduct/{id}', 'ReportsController@loanproduct');
    Route::get('reports/interest/{id}', 'ReportsController@interest');
    Route::get('reports/totalinterests', 'ReportsController@totalInterest');
    Route::get('reports/loanrepayments', 'ReportsController@loanrepayments');
    Route::get('reports/creditreport/{id}/{loan}', 'ReportsController@creditappraisal');
    Route::get('reports/savinglisting', 'ReportsController@savinglisting');

    Route::get('reports/savingproduct/{id}', 'ReportsController@savingproduct');

    Route::get('reports/selecttransactionPeriod', 'ReportsController@selecttransactionPeriod');
    Route::post('reports/transaction', 'ReportsController@transaction');


    Route::post('reports/financials', 'ReportsController@financials');
    Route::get('reports/redirect', 'ReportsController@intercept');

    Route::post('report/monthlyrepayments', 'ReportsController@monthlyrepayments');

    Route::get('reports', function () {

        return View::make('members.reports');
    });

    Route::get('reports/combined', function () {

        $members = Member::all();

        return View::make('members.combined', compact('members'));
    });

    Route::post('reports/combinedstatement', function (Request $request) {
        $id = $request->get('member');
        $guaranteed =Loanaccount::amountGuarantee($id);
        $member = Member::where('id', '=', $id)->first();
        $account= Savingaccount::where('member_id', '=',$member->id)->where('savingproduct_id','=',4)->first();
        $saving= Savingaccount::where('member_id', '=',$member->id)->where('savingproduct_id','=',5)->first();
        $interest=DB::table('savingproducts')->where('id', '=',5)->pluck('Interest_Rate');
        //$guarantors=DB::table('loanguarantors')->where('member_id', '=',$member)->get();
        $guarantors= Loanguarantor::where('member_id', '=',$id)->get();

        if (!empty($account)) {

            $date = date("d-M-Y");

            /*End Prerequisites and Start Saving Transactions*/
            $savingaccounts = Savingaccount::where('member_id', '=',$member->id)->get();

            if (!empty($account)) {

                $transactions = $account->transactions;

                $credit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)
                    ->where('savingaccount_id', '=', $account->id)
                    ->where('type', '=', 'credit')
                    ->sum('saving_amount');
                $debit = DB::table('savingtransactions')->where('savingaccount_id', '=', $account->id)
                    ->where('type', '=', 'debit')->sum('saving_amount');
                $balance = $credit - $debit;
                /*End Saving Transactions and Start Loans Transactions Summary*/
                $loans = Loanaccount::where('member_id', '=', $id)->where('is_disbursed','=',1)->get();
                /*End Loan Transactions Summary*/
                $organization = Organization::find(1);
                $pdf = PDF::loadView('pdf.comprehensive', compact('member','saving','interest','transactions','guarantors','savingaccounts','organization','account', 'balance', 'loans','guaranteed'))
                    ->setPaper('a4');
                return $pdf->stream($member->name . ' ' . $member->membership_no . ' Comprehensive Statement.pdf');
            } else {
                /*End Saving Transactions and Start Loans Transactions Summary*/
                $loans = Loanaccount::where('member_id', '=', $id)->where('is_disbursed','=',1)->get();
                /*End Loan Transactions Summary*/
                $organization = Organization::find(1);
                $pdf = PDF::loadView('pdf.comprehensive', compact('member', 'organization', 'loans','guarantors','savingaccounts','guaranteed'))->setPaper('a4');
                return $pdf->stream($member->name . ' ' . $member->membership_no . ' Comprehensive Statement.pdf');
            }
        }else{
            return Redirect::back()->withErrors('Saving or loan account for the selected member does not exist');
        }
    });

    Route::get('loanreports', function () {

        $loanproducts = Loanproduct::all();

        return View::make('loanaccounts.reports', compact('loanproducts'));
    });

    Route::get('statutoryreports', function () {
        return View::make('reports.statutory_reports');
    });

    Route::get('statutory_reports/{what}', function ($what) {
        $organization = Organization::where('active','=',1)->first();
        if($what=='license_application_form'){
            $pdf = PDF::loadView('pdf.statutory.license_application',compact('organization'))->setPaper('a4');
            return $pdf->stream('license_application.pdf');
        }else if($what=='fitpropertest'){
            $pdf = PDF::loadView('pdf.statutory.fitpropertest',compact('organization'))->setPaper('a4');
            return $pdf->stream('fit/proper test.pdf');
        }else if($what=='capital_adequacy'){
            $pdf = PDF::loadView('pdf.statutory.capital_adequacy',compact('organization'))->setPaper('a4');
            return $pdf->stream('capital_adequancy.pdf');
        }else if($what=='liquidity_statement'){
            $pdf = PDF::loadView('pdf.statutory.liquidity_statement',compact('organization'))->setPaper('a4');
            return $pdf->stream('liquidity_statement.pdf');
        }else if($what=='deposit_return'){
            $pdf = PDF::loadView('pdf.statutory.deposit_return',compact('organization'))->setPaper('a4');
            return $pdf->stream('deposit_return.pdf');
        }else if($what=='assets_classification'){
            $pdf = PDF::loadView('pdf.statutory.assets_classification',compact('organization'))->setPaper('a4');
            return $pdf->stream('assets_classification.pdf');
        }else if($what=='investment_return'){
            $pdf = PDF::loadView('pdf.statutory.investment_return',compact('organization'))->setPaper('a4');
            return $pdf->stream('investment_return.pdf');
        }else if($what=='other_disclosures'){
            $pdf = PDF::loadView('pdf.statutory.other_disclosures',compact('organization'))->setPaper('a4');
            return $pdf->stream('other_disclosures.pdf');
        }else if($what=='financial_position'){
            $pdf = PDF::loadView('pdf.statutory.financial_position',compact('organization'))->setPaper('a4');
            return $pdf->stream('financial_position.pdf');
        }
    });

    Route::get('loanrepayments', function () {

        $loanproducts = Loanproduct::all();

        return View::make('loanaccounts.repayments', compact('loanproducts'));
    });


    Route::get('reports/monthlyrepayments', function () {
        $loans = Loanaccount::where('is_disbursed', '=', TRUE)
            ->groupBy('member_id')
            ->get();
        return View::make('loanaccounts.monthlyrepayments', compact('loans'));
    });

    Route::get('savingreports', function () {

        $savingproducts = Savingproduct::all();

        return View::make('savingaccounts.reports', compact('savingproducts'));
    });


    Route::get('financialreports', function () {
        $incomeAccounts = Account::select('id')->where('category', 'INCOME')->get()->toArray();
        $incomeParticulars = Particular::whereIn('creditaccount_id', $incomeAccounts)->get();

        $expenseAccounts = Account::select('id')->where('category', 'EXPENSE')->get()->toArray();
        $expenseParticulars = Particular::whereIn('debitaccount_id', $expenseAccounts)->get();

        return View::make('pdf.financials.reports', compact( 'incomeParticulars', 'expenseParticulars'));
    });

    Route::post('cashbook', function (Request $request) {
        $organization = Organization::find(1);
        $from = $request->get('from');
        $to = $request->get('to');

        $creditbf = DB::table('journals')->whereIn('account_id', array(1, 6, 44))
            ->where('created_at', '<', date('Y-m-d H:i:s', strtotime($from)))
            ->where('type', 'credit')->sum('amount');
        $debitbf = DB::table('journals')->whereIn('account_id', array(1, 6, 44))
            ->where('created_at', '<', date('Y-m-d H:i:s', strtotime($from)))
            ->where('type', 'debit')->sum('amount');
        $balbf = $creditbf - $debitbf;
        $creditaf = DB::table('journals')->whereIn('account_id', array(1, 6, 44))
            ->where('created_at', '<', date('Y-m-d H:i:s', strtotime($to)))
            ->where('type', 'credit')->sum('amount');
        $debitaf = DB::table('journals')->whereIn('account_id', array(1, 6, 44))
            ->where('created_at', '<', date('Y-m-d H:i:s', strtotime($to)))
            ->where('type', 'debit')->sum('amount');
        $balcf = $creditaf - $debitaf;
        /*
        $credits = DB::table('journals')->join('particulars', 'journals.particulars_id', '=', 'particulars.id')->whereIn('account_id', array(1,9))->whereBetween('created_at' ,array($from, $to))->where('type', 'credit')->select(DB::raw('journals.date as date, journals.trans_no as transno, particulars.name as particulars, journals.amount as amount'))->first();
        $debits = DB::table('journals')->whereIn('account_id', array(1,9))->whereBetween('created_at' ,array($from, $to))->where('type', 'debit')->get();
        */

        $entries = DB::table('journals')
            ->whereIn('account_id', array(1, 6, 44))
            ->whereBetween('date', array($from, $to))
            ->where('amount', '!=', '0')
            ->get();
        // return var_dump($entries);
        $dates = array();
        foreach ($entries as $entry) {
            if (!array_key_exists($entry->date, $dates))
                $dates[$entry->date] = array();

            array_push($dates[$entry->date], $entry);
        }

        return Excel::create('CashBook', function ($excel) use ($dates) {




            $excel->sheet('CashBook', function ($sheet) use ($dates) {

                $sheet->setAllBorders('thin');

                $sheet->setWidth(array(
                    'A' => 15,
                    'B' => 60,
                    'C' => 15,
                    'D' => 15,
                    'E' => 1,
                    'F' => 60,
                    'G' => 15,
                    'H' => 15
                ));

                $sheet->mergeCells('A1:H1');

                $sheet->row(1, array("MOTO STAFF SACCO LTD"));

                $sheet->cells('A1:H1', function ($cells) {
                    $cells->setAlignment('center');
                    // $cells->setBackground('#777777');
                    $cells->setFont(array(
                        'family' => 'Calibri',
                        'bold' => true
                    ));
                });

                $sheet->mergeCells('A2:D2');
                $sheet->mergeCells('F2:H2');

                $sheet->cell('A2', function ($cell) {
                    $cell->setValue('Dr');
                    $cell->setFont(array(
                        'family' => 'Calibri',
                        'bold' => true
                    ));
                });

                $sheet->cell('F2', function ($cell) {
                    $cell->setValue('Cr');
                    $cell->setFont(array(
                        'family' => 'Calibri',
                        'bold' => true
                    ));
                });

                $sheet->row(3, array(
                    "Date", "Description", "Trans. No.", "Amount", "", "Description", "Trans. No.", "Amount"
                ));

                $sheet->cells('A3:H3', function ($cells) {
                    $cells->setFont(array(
                        'bold' => true
                    ));
                });

                if (ob_get_level() > 0) {
                    ob_end_clean();
                }

                $debit_row = 3;
                $credit_row = 3;
                $debit_total = 0;
                $credit_total = 0;
                foreach ($dates as $date => $entries) {
                    $max = max($debit_row, $credit_row);
                    $max++;

                    $debit_row = $max;
                    $credit_row = $max;

                    $sheet->cell('A' . $max, function ($cell) use ($date) {
                        $cell->setValue($date);
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });

                    foreach ($entries as $entry) {
                        if ($entry->amount > 0) {

                            $narration = DB::table('narration')->where('trans_no', $entry->trans_no)->first();
                            if($narration != null) {
                                if ($narration->member_id == '0') {
                                    $narration = "Moto Sacco";
                                } else {
                                    $narration = Member::find($narration->member_id)->name;
                                }
                            } else {
                                $narration = "";
                            }

                            if ($entry->type == "debit") {
                                $sheet->cell('B' . $debit_row, function ($cell) use ($entry, $narration) {
                                    $cell->setValue($entry->description . " " . $narration);
                                });
                                $sheet->cell('C' . $debit_row, function ($cell) use ($entry) {
                                    $cell->setValue($entry->trans_no);
                                });
                                $sheet->cell('D' . $debit_row, function ($cell) use ($entry) {
                                    $cell->setValue($entry->amount);
                                });

                                $debit_row++;
                                $debit_total += $entry->amount;
                            } elseif ($entry->type == "credit") {
                                $sheet->cell('F' . $credit_row, function ($cell) use ($entry, $narration) {
                                    $cell->setValue($entry->description . " " . $narration);
                                });
                                $sheet->cell('G' . $credit_row, function ($cell) use ($entry) {
                                    $cell->setValue($entry->trans_no);
                                });
                                $sheet->cell('H' . $credit_row, function ($cell) use ($entry) {
                                    $cell->setValue($entry->amount);
                                });

                                $credit_row++;
                                $credit_total += $entry->amount;
                            }
                        }
                    }
                }

                $max = max($debit_row, $credit_row);
                $max++;

                $sheet->cell('A' . $max, function ($cell) {
                    $cell->setValue("TOTAL");
                    $cell->setFont(array(
                        'bold' => true
                    ));
                });

                $sheet->cell('D' . $max, function ($cell) use ($debit_total) {
                    $cell->setValue($debit_total);
                    $cell->setFont(array(
                        'bold' => true
                    ));
                });

                $sheet->cell('H' . $max, function ($cell) use ($credit_total) {
                    $cell->setValue($credit_total);
                    $cell->setFont(array(
                        'bold' => true
                    ));
                });
            });

        })->export('xlsx');

        /*$debits=array();
            $credits=array();
            $debits_sum=0;
            $credts_sum=0;
            foreach ($entries as $entry) {
              if ($entry->amount > 0){
              if($entry->particulars_id == null || $entry->particulars_id == '0'){
                $particular=" ";
              }else{
                $particular = Particular::find($entry->particulars_id)->name;
              }

              if(!array_key_exists($entry->date, $credits)){
                 $credits[$entry->date]=array();
              }

              if(!array_key_exists($entry->date, $debits)){
                 $debits[$entry->date]=array();
              }

              if($entry->type=="credit"){
                if(!array_key_exists($particular, $credits[$entry->date])){
                  $credits[$entry->date][$particular]=array();
                }

                 $credits[$entry->date][$particular][$entry->trans_no] = $entry->amount;

                $credts_sum+=$entry->amount;
              } elseif ($entry->type=="debit") {
                if(!array_key_exists($particular, $debits[$entry->date])){
                  $debits[$entry->date][$particular]=array();
                }

                 $debits[$entry->date][$particular][$entry->trans_no] = $entry->amount;
                $debits_sum+=$entry->amount;
              }
            }
            }*/

        //return var_dump($debits);
        //return $credits;
        // return View::make('pdf.financials.cashbook', compact('entries', 'from', 'to', 'organization','debits_sum','credts_sum','credits','debits','balcf','balbf'));

        //return $pdf = PDFF::html('pdf.financials.cashbook', compact('entries', 'from', 'to', 'organization', 'debits_sum', 'credts_sum', 'credits', 'debits', 'balcf', 'balbf'))->setOrientation('landscape');
        /*$pdf->setOptions(array(
    'orientation' => 'landscape'
));*/
        //return $pdf->stream('Cash Book.pdf');
        /*$pdf = PDF::loadView('pdf.financials.cashbook', compact('entries', 'from', 'to', 'organization','debits_sum','credts_sum','credits','debits','balcf','balbf'))->setPaper('a4')
    ->setOrientation('landscape');
return $pdf->stream('Cash Book.pdf');
        */
    });

    /**
     * Currencies
     */
    Route::resource('currencies', 'CurrenciesController');
    Route::get('currencies/edit/{id}', 'CurrenciesController@edit');
    Route::post('currencies/update/{id}', 'CurrenciesController@update');
    Route::get('currencies/delete/{id}', 'CurrenciesController@destroy');
    Route::get('currencies/create', 'CurrenciesController@create');

    /**
     * Compliance/ DisciplineController
     */
    Route::resource('compliance', 'DisciplineController');
    Route::get('compliance/edit/{id}', 'DisciplineController@edit');
    Route::post('compliance/update/{id}', 'DisciplineController@update');
    Route::get('compliance/delete/{id}', 'DisciplineController@destroy');
    Route::get('compliance/create', 'DisciplineController@create');
    Route::get('compliance/show/{id}', 'DisciplineController@show');

    /**
     * Promotions controller
     */
    Route::resource('promotions', 'PromotionsController');
    Route::get('promotions/edit/{id}', 'PromotionsController@edit');
    Route::post('promotions/update/{id}', 'PromotionsController@update');
    Route::get('promotions/delete/{id}', 'PromotionsController@destroy');
    Route::get('promotions/create', 'PromotionsController@create');
    Route::get('promotions/letters/{id}', 'PromotionsController@promotionletter');
    Route::get('transfer/letters/{id}', 'PromotionsController@transferletter');
    Route::get('promotions/show/{id}', 'PromotionsController@show');

    Route::resource('expenses', 'ExpensesController');
    Route::get('expenses/edit/{id}', 'ExpensesController@edit');
    Route::post('expenses/update/{id}', 'ExpensesController@update');
    Route::get('expenses/delete/{id}', 'ExpensesController@destroy');
    /*SUBMIT PETTY TRANSACTION FOR APPROVAL*/
    Route::get('petty_cash/submitforapprova/{id}', function($id){
        $pettyid="";
        $pettyid=$id;
        $petty = DB::table('account_transactions')->where('id', $id)->first();
        $user = User::permission('approve_pettycash')->get();
        if ($user ){

            $users = DB::table('roles')
                ->join('assigned_roles', 'roles.id', '=', 'assigned_roles.role_id')
                ->join('users', 'assigned_roles.user_id', '=', 'users.id')
                ->join('permission_role', 'roles.id', '=', 'permission_role.role_id')
                ->where("permission_id",33)
                ->select("users.id","email","username")
                ->get();

            $key = md5(uniqid());

            foreach ($users as $user) {
                Notification::notifyUser($user->id,"Hello, Please approve Petty Cash Transaction ".$petty->description,"Approve Petty Cash","petty_cash/approve/".$pettyid,$key);
            }
            return Redirect::action('PettyCashController@index')->with('success', "Submitted.Please await approval to complete Transaction");
        }
    });

    /***MEMBER ROUTES  */
    Route::resource('members', 'MembersController');
    Route::post('members/update/{id}', 'MembersController@update');
    Route::post('member/update/{id}', 'MembersController@update');
    Route::get('members/delete/{id}', 'MembersController@destroy');
    Route::get('members/edit/{id}', 'MembersController@edit');
    Route::get('member/edit/{id}', 'MembersController@edit');
    Route::post('createbank', 'MembersController@createbank');
    Route::post('createbankbranch', 'MembersController@createbankbranch');
    Route::get('members/show/{id}', 'MembersController@show');
    Route::get('members/summary/{id}', 'MembersController@summary');
    Route::get('member/show/{id}', 'MembersController@show');
    Route::post('deldoc', 'MembersController@deletedoc');
    Route::get('members/loanaccounts/{id}', 'MembersController@loanaccounts');
    Route::get('memberloans', 'MembersController@loanaccounts2');

    Route::get('language/{lang}',
        array(
            'as' => 'language.select',
            'uses' => 'OrganizationsController@language'
        )
    );

    /**
     * branches routes
     */

    Route::resource('branches', 'BranchesController');
    Route::post('branches/update/{id}', 'BranchesController@update');
    Route::get('branches/delete/{id}', 'BranchesController@destroy');
    Route::get('branches/edit/{id}', 'BranchesController@edit');

    //=================GROUP ROUTES ===============================//
    Route::resource('groups', 'GroupsController');
    Route::post('groups/update/{id}', 'GroupsController@update');
    Route::get('groups/delete/{id}', 'GroupsController@destroy');
    Route::get('groups/edit/{id}', 'GroupsController@edit');
//==============================END GROUP ROUTES =============//

//============ MEMBER CONFIG ROUTES ===============================//
    Route::get('member_config', 'MembersController@config');
    Route::post('member_config', 'MembersController@configure');
    Route::get('members/loanhistory/{id}', 'MembersController@loanHistory');
    Route::get('members/loanhistory/{id}/report', 'MembersController@loanHistoryReport');
//==================END MEMBER CONFIG ROUTES ==========================================//

//=============== SACCO INVESTMENT ROUTES =======================================================//
    Route::get('portal', function () {
        $members = Member::all();
        return view('css.members', compact('members'));
    });

    Route::get('saccoinvestments', function () {
        $investment = Investment::all();
        //Calculate the valuationda
        foreach ($investment as $invest) {
            Valuation::calculateValuation($invest->id);
        }
        $organization = Organization::find(1);
        return view('css.saccoinvestments', compact('investment', 'organization'));
    });

    Route::get('saccoinvestments/create', function () {
        $vendor = Vendor::all();
        $organization = Organization::find(1);
        $cats = Investmentcategory::all();
        return view('css.createinvestment', compact('vendor', 'organization', 'cats'));
    });

#TODO: Move to controller
    Route::post('saccoinvestments/create', function (Request $request) {
        $data = $request->all();
        $invest = new Investment;
        $invest->name = $data['investment'];
        $invest->vendor_id = $data['vendor'];
        $invest->valuation = $data['valuation'];
        $invest->growth_type = $data['growth_type'];
        $invest->growth_rate = $data['growth_rate'];
        $invest->description = $data['desc'];
        $invest->category_id = $data['category'];
        $invest->date = date('Y-m-d');
        $invest->save();
        $out = "The sacco Investment successfully created!!!";
        $investment = Investment::all();
        $organization = Organization::find(1);
        return view('css.saccoinvestments', compact('out', 'investment', 'organization'));
    });

    Route::get('saccoinvestments/edit/{id}', 'InvestmentController@update');
    Route::post('saccoinvestments/edit', 'InvestmentController@doupdate');
    Route::get('saccoinvestments/delete/{id}', 'InvestmentController@destroy');

    Route::get('portal/activate/{id}', 'MembersController@activateportal');
    Route::get('portal/deactivate/{id}', 'MembersController@deactivateportal');

    Route::get('memtransactions/{id}', 'MembersController@savingtransactions');
//=============== END SACCO INVESTMENT ROUTES ===========================//

    /**
     * Saving account routes
     */
    Route::resource('savingaccounts', 'SavingaccountsController');
    Route::get('savingaccounts/create/{id}', 'SavingaccountsController@create');
    Route::get('member/savingaccounts/{id}', 'SavingaccountsController@memberaccounts');


    /**
     * Saving transaction routes
     */
    Route::get('savingtransactions/show/{id}', 'SavingtransactionsController@show');
    Route::resource('savingtransactions', 'SavingtransactionsController');
    Route::get('savingtransactions/create/{id}', 'SavingtransactionsController@create');
    Route::get('savingtransactions/receipt/{id}', 'SavingtransactionsController@receipt');
    Route::get('savingtransactions/statement/{id}', 'SavingtransactionsController@statement');
    Route::post('savingtransactions/import', 'SavingtransactionsController@import');


    Route::get('templates/savings', function () {
        return Excel::download(function ($excel) {



            $excel->sheet('SavingsTransactions', function ($sheet) {


                $sheet->row(1, array(
                    'DATE', 'ACCOUNT NUMBER', 'SAVING AMOUNT','MANAGEMENT FEE', 'TYPE', 'DESCRIPTION','BANK REF','TRANSACTION METHOD','VEHICLE REGISTRATION',
                ));

                $sheet->setWidth(array(
                    'A' => 30,
                    'B' => 60,
                    'C' => 30,
                    'D' => 30,
                    'E' => 30,
                    'F' => 30,
                    'G' => 30,
                    'H' => 30,
                    'I' => 60,
                ));

                /*$sheet->getStyle('A2:A100')
            ->getNumberFormat()
            ->setFormatCode('yyyy-mm-dd');*/

                $sheet->setColumnFormat(array(
                    "A" => "yyyy-mm-dd",
                ));

                $row = 2;
                $row1 = 2;
                $r = 2;
                $savingsAccounts = Savingaccount::all();
                $vehicles = Vehicle::all();

                if (ob_get_level() > 0) {
                    ob_end_clean();
                }

                for ($i = 0; $i < count($savingsAccounts); $i++) {
                    $member = Member::find($savingsAccounts[$i]->member_id);
                    $sheet->SetCellValue("AA" . $row, $member->name . ": " . $savingsAccounts[$i]->account_number);
                    $row++;
                }
                $sheet->_parent->addNamedRange(
                    new NamedRange(
                        'accounts', $sheet, 'AA2:AA' . (count($savingsAccounts) + 1)
                    )
                );


                for ($i = 0; $i < count($vehicles); $i++) {

                    $member = Member::find($vehicles[$i]->member_id);
                    $sheet->SetCellValue("AE" . $row1, $member->name . ": " . $vehicles[$i]->regno);
                    $row1++;
                }



                $sheet->_parent->addNamedRange(
                    new NamedRange(
                        'vehicles', $sheet, 'AE2:AE' . (count($vehicles)+1)                )
                );



                for ($i = 2; $i <= 1000; $i++) {

                    $objValidation = $sheet->getCell('B' . $i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('accounts'); //note this!

                    $objValidation = $sheet->getCell('E' . $i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"credit, debit"'); //note this!

                    $objValidation = $sheet->getCell('H' . $i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"bank, cash"'); //note this!

                    $objValidation = $sheet->getCell('I' . $i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('vehicles'); //note this!


                }

            });

        },new Savingtransaction,'xlsx');
    });

    Route::post('migration/import/savings', 'SavingtransactionsController@importSavings');
    Route::get('migration/import/savings', function () {
        return view('savingtransactions.import');
    });

//============== SAVING PRODUCT ROUTES =============================================//
    Route::resource('savingproducts', 'SavingproductsController');
    Route::get('savingproducts/update/{id}', 'SavingproductsController@selectproduct');
    Route::post('savingproducts/update', 'SavingproductsController@update');
    Route::get('savingproducts/delete/{id}', 'SavingproductsController@destroy');
    Route::get('savingproducts/edit/{id}', 'SavingproductsController@edit');
    Route::get('savingproducts/show/{id}', 'SavingproductsController@show');
//=========== SAVING PRODUCT  ROUTES ==================================//


// ============ SHARES ROUTES =============================================//

    /**
     * Shares routes
     */
    Route::resource('shares', 'SharesController');
    Route::post('shares/update/{id}', 'SharesController@update');
    Route::get('shares/delete/{id}', 'SharesController@destroy');
    Route::get('shares/edit/{id}', 'SharesController@edit');
    Route::get('shares/show/{id}', 'SharesController@show');

    Route::get('transfershares', function (Request $request) {
        $id = $request->get('member');
        $members = Member::all();
        return View::make('members.transfershares', compact('members'));
    });

    /**
     * Share Capital
     */
    Route::get('sharecapital/contribution', 'SharesController@contribution');
    Route::get('sharecapital/dividend', 'SharesController@dividend');
    Route::get('sharecapital/parameters', 'SharesController@parameters');
    Route::post('sharecapital/parameters', 'SharesController@parameterize');
    Route::get('sharecapital/editparameters', 'SharesController@editparameters');
    Route::post('sharecapital/editparameters', 'SharesController@doparameterize');

    /**
     * Share reports
     */
    Route::resource('sharereports', 'sharereportsController');
    Route::get('sharereports/individualcontribution', 'sharereportsController@show');
    Route::post('sharereports/individualcontribution', 'sharereportsController@individual');


    /**
     * share transaction routes
     */
    Route::get('sharetransactions/show/{id}', 'SharetransactionsController@show');
    Route::resource('sharetransactions', 'SharetransactionsController');
    Route::get('sharetransactions/create/{id}', 'SharetransactionsController@create');
// ============ END SHARES ROUTES ===============================//

    /*
    * Vehicles Routes
    */
    Route::resource('vehicles', 'VehiclesController');
    Route::post('vehicles/update/{id}', 'VehiclesController@update');
    Route::get('vehicles/delete/{id}', 'VehiclesController@destroy');
    Route::get('vehicles/edit/{id}', 'VehiclesController@edit');
    Route::get('vehicles/show/{id}', 'VehiclesController@show');
    Route::get('vehicles/create', 'VehiclesController@create');

    /**########## ASSIGN VEHICLE ############## */
    Route::resource('assignvehicles', 'AssignVehiclesController');
    Route::post('assignvehicles/update/{id}', 'AssignVehiclesController@update');
    Route::get('assignvehicles/delete/{id}', 'AssignVehiclesController@destroy');
    Route::get('assignvehicles/edit/{id}', 'AssignVehiclesController@edit');
    Route::get('assignvehicles/show/{id}', 'AssignVehiclesController@show');
    Route::get('assignvehicles/create', 'AssignVehiclesController@create');

    /**########## VEHICLE INCOME ############## */
    Route::resource('vehicleincomes', 'VehicleIncomesController');
    Route::post('vehicleincomes/update/{id}', 'VehicleIncomesController@update');
    Route::get('vehicleincomes/delete/{id}', 'VehicleIncomesController@destroy');
    Route::get('vehicleincomes/edit/{id}', 'VehicleIncomesController@edit');
    Route::get('vehicleincomes/show/{id}', 'VehicleIncomesController@show');
    Route::get('vehicleincomes/create', 'VehicleIncomesController@create');

    /**########## VEHICLE  EXPENSES ############## */
    Route::resource('vehicleexpenses', 'VehicleExpensesController');
    Route::post('vehicleexpenses/update/{id}', 'VehicleExpensesController@update');
    Route::get('vehicleexpenses/delete/{id}', 'VehicleExpensesController@destroy');
    Route::get('vehicleexpenses/edit/{id}', 'VehicleExpensesController@edit');
    Route::get('vehicleexpenses/show/{id}', 'VehicleExpensesController@show');
    Route::get('vehicleexpenses/create', 'VehicleExpensesController@create');

    /**
     * Tlb payment routes
     */
    Route::resource('tlbpayments', 'TlbPaymentsController');
    Route::get('tlbpayments/edit/{id}', 'TlbPaymentsController@edit');
    Route::post('tlbpayments/update/{id}', 'TlbPaymentsController@update');
    Route::get('tlbpayments/delete/{id}', 'TlbPaymentsController@destroy');
    Route::get('tlbpayments/create', 'TlbPaymentsController@create');
    Route::post('reports/tbl', 'ReportsController@tlbrep');

// ============= DISBURSEMENTS ROUTES ============================== //
    Route::resource('disbursements', 'DisbursementController');
    Route::get('disbursements/create', 'DisbursementController@create');
    Route::post('disbursements/create', 'DisbursementController@docreate');
    Route::get('disbursements/update/{id}', 'DisbursementController@update');
    Route::post('disbursements/update', 'DisbursementController@doupdate');
    Route::get('disbursements/delete/{id}', 'DisbursementController@destroy');
// ============= END  DISBURSEMENTS ROUTES ============================== //

//=============== LOAN PRODUCT ROUTES ================================ //
    Route::resource('loanproducts', 'LoanproductsController');
    Route::post('loanproducts/update/{id}', 'LoanproductsController@update');
    Route::get('loanproducts/delete/{id}', 'LoanproductsController@destroy');
    Route::get('loanproducts/edit/{id}', 'LoanproductsController@edit');
    Route::get('loanproducts/show/{id}', 'LoanproductsController@show');
    Route::get('memberloanshow/{id}', 'LoanproductsController@memberloanshow');
//=============== LOAN PRODUCT ROUTES ================================ //

// ================== LOAN ROUTES =================================//
    Route::resource('loans', 'LoanaccountsController');
    Route::get('loans/apply/{id}', 'LoanaccountsController@apply');
    Route::get('guarantorapproval', 'LoanaccountsController@guarantor');
    Route::post('loans/apply', 'LoanaccountsController@doapply');
    Route::post('loans/application', 'LoanaccountsController@doapply2');
    Route::get('loanduplicates', 'LoanaccountsController@management');

    Route::get('ajaxfetchrduration', function (Request $request) {
        $lpvalue= $request->get('lpvalue');
        $loanperiod = DB::table('loanproducts')
            ->where('id',$lpvalue)
            ->first();
        return $loanperiod->period;
    });

    Route::get('ajaxfetchguarantorbal', function (Request $request) {
        $memberid=$request->get('lpvalue');
        #$member =  Member::where('id','=', $memberid)->first();
        //$savingsbal=Savingaccount::getUserSavingsBalance($memberid);
        /*$amountguaranteed=Loanaccount::amountGuarantee($memberid);
        $savingsbal=(int)$rawsavingsbal-(int)$amountguaranteed;*/
        $savingsbal=Savingaccount::getFinalDepositBalance($memberid);
        if($savingsbal<1){$savingsbal=0;}
        return round($savingsbal,2);
    });
    Route::get('loanfetch', function () {
        $principal_paid = Loanrepayment::getPrincipalPaid($loanaccount);
        $interest_paid = Loanrepayment::getInterestPaid($loanaccount);
        $amountpaid=(int)$principal_paid + (int)$interest_paid;
        return $amountpaid;
    });
    Route::get('ajaxfetchmaxamount', function (Request $request) {
        $lpvalue= $request->get('lpvalue');
        $loanperiod = DB::table('loanproducts')
            ->where('id',$lpvalue)
            ->first();
        return $loanperiod->max_multiplier;
    });

    Route::get('loans/edit/{id}', 'LoanaccountsController@edit');
    Route::get('loanmanagement', 'LoanaccountsController@index');

    Route::post('loans/update/{id}', 'LoanaccountsController@update');

    Route::get('loans/approve/{id}', 'LoanaccountsController@approve');
    Route::post('loans/approve/{id}', 'LoanaccountsController@doapprove');
    Route::post('gurantorapprove/{id}', 'LoanaccountsController@guarantorapprove');
    Route::post('gurantorreject/{id}', 'LoanaccountsController@guarantorreject');

    Route::get('loans/reject/{id}', 'LoanaccountsController@reject');
    Route::post('rejectapplication', 'LoanaccountsController@rejectapplication');

    Route::get('loans/disburse/{id}', 'LoanaccountsController@disburse');
    Route::post('loans/disburse/{id}', 'LoanaccountsController@dodisburse');

    Route::get('loans/show/{id}', 'LoanaccountsController@show');
    Route::post('loans/update-repayment-period/{id}', 'LoanaccountsController@updateRepaymentPeriod');
    Route::get('memberloan/show/{id}', 'LoanaccountsController@show');

    Route::post('loans/amend/{id}', 'LoanaccountsController@amend');

    Route::get('loans/reject/{id}', 'LoanaccountsController@reject');
    Route::post('loans/reject/{id}', 'LoanaccountsController@rejectapplication');


    Route::get('loanaccounts/topup/{id}', 'LoanaccountsController@gettopup');
    Route::post('loanaccounts/topup/{id}', 'LoanaccountsController@topup');

    Route::get('memloans/{id}', 'LoanaccountsController@show2');
// ================== END LOAN  ROUTES =================================//

//==============LOAN REPAYMENT ROUTES ========================= //
    Route::resource('loanrepayments', 'LoanrepaymentsController');

    Route::get('loanrepayments/create/{id}', 'LoanrepaymentsController@create');
    Route::get('memberloanrepayments/create/{id}', 'LoanrepaymentsController@create');
    Route::get('loanrepayments/offset/{id}', 'LoanrepaymentsController@offset');
    Route::get('repayments_template', 'LoanrepaymentsController@createTemplate');
    Route::get('import_repayments', 'LoanrepaymentsController@importView');
    Route::post('import_repayments', 'LoanrepaymentsController@importRepayment');
//============== END LOAN REPAYMENT ROUTES =========================

//===================Converting and recovering loans routes =============================
    Route::get('loanrepayments/recover/{id}', 'LoanrepaymentsController@recoverloan');
    Route::get('loanrepayments/convert/{id}', 'LoanrepaymentsController@convert');
    Route::get('loanrepayments/refinance/{id}', 'LoanrepaymentsController@refinanceCreate');
    Route::post('loanrepayments/refinance/{id}', 'LoanrepaymentsController@refinance');
    Route::post('loanrepayments/recover/complete', 'LoanrepaymentsController@doRecover');
    Route::post('loanrepayments/convert/commit', 'LoanrepaymentsController@doConvert');
//=================== End Converting and recovering loans routes =============================
    /**
     * Loan Transactions Audits
     */
    Route::get('transaudits', function () {
        $transactions = Loantransaction::all();
        return view('transaud', compact('transactions'));
    });


    Route::post('transaudits', function (Request $request) {
        $date = $request->get('date');
        $type = $request->get('type');
        if ($type == 'loan') {
            $transactions = DB::table('loantransactions')->where('date', '=', $date)->get();
            return view('transaudit', compact('transactions', 'type', 'date'));
        }

        if ($type == 'savings') {
            $transactions = DB::table('savingtransactions')->where('date', '=', $date)->get();
            return view('transaudit', compact('transactions', 'type', 'date'));
        }
    });



// ================= matrices routes ================================= //
    Route::resource('matrices', 'MatrixController');
    Route::get('matrices/create', 'MatrixController@create');
    Route::post('matrices/create', 'MatrixController@docreate');
    Route::get('matrices/update/{id}', 'MatrixController@update');
    Route::post('matrices/update', 'MatrixController@doupdate');
    Route::get('matrices/delete/{id}', 'MatrixController@destroy');
// ================= end matrices routes ================================= //

    /*
    * Vendor controllers
    */
    Route::resource('vendors', 'VendorsController');
    Route::get('vendors/create', 'VendorsController@create');
    Route::post('vendors/update/{id}', 'VendorsController@update');
    Route::get('vendors/edit/{id}', 'VendorsController@edit');
    Route::get('vendors/delete/{id}', 'VendorsController@destroy');
    Route::get('vendors/products/{id}', 'VendorsController@products');
    Route::get('vendors/orders/{id}', 'VendorsController@orders');

// ================= INVESTMENTS ROUTES ========================================= //
    Route::resource('investmentscats', 'InvestmentcategoriesController');
    Route::get('investmentscats/create/{what}', 'InvestmentcategoriesController@create');
    Route::get('investmentscats/update/{what}/{id}', 'InvestmentcategoriesController@update');
    Route::post('investmentscats/update/{id}', 'InvestmentcategoriesController@doupdate');
    Route::get('investmentscats/delete/{what}/{id}', 'InvestmentcategoriesController@destroy');
    Route::get('ajaxinvestmentclasses', function (Request $request) {
        $cat_id = $request->get('lpvalue');
        $classes = Investmentclass::where('category','=' , $cat_id)->get();
        return $classes;
    });
// ================= END INVESTMENTS ROUTES ========================================= //
    /** PETTY CASH ROUTES */
    Route::get('petty_cash/delete/{id}', 'PettyCashController@deletePetty');
    Route::resource('petty_cash', 'PettyCashController');
    Route::post('petty_cash/addMoney', 'PettyCashController@addMoney');
    Route::post('petty_cash/addContribution', 'PettyCashController@addContribution');
    Route::post('petty_cash/newTransaction', 'PettyCashController@newTransaction');
    Route::get('newpettycash', function(Request $request){
        // Session::put('newTransaction', [
        // 	'transactTo'=>$request->get('transact_to'),
        // 	'trDate'=>$request->get('tr_date'),
        // 	'description'=>$request->get('description'),
        // 	'expense_ac'=>$request->get('expense_ac'),
        // 	'credit_ac'=>$request->get('credit_ac')
        // ]);
        //
        // $newTr = Session::get('newTransaction');
        //return $request->get();

        $account = Account::where('name', 'like', '%'.'Petty Cash'.'%')->first();
        if($request->get('item') != NULL){

            Session::push('trItems', array(

                'item_name' => Particular::find($request->get('item'))->name,
                'description' => $request->get('desc'),
                'quantity' => $request->get('qty'),
                'date' => $request->get('date'),
                'unit_price' => $request->get('unit_price'),
                'particulars_id' => $request->get('item'),
                'receipt' => $request->get('receipt'),
                'credit_ac' => $account->id,
                'totalA' =>  $request->get('qty')*$request->get('unit_price')
            ));
        }

        $trItems = Session::get('trItems');

        $particulars = Particular::whereNotNull('debitaccount_id')->whereNotNull('creditaccount_id')->get();
        foreach ($particulars as $key => $particular) {
            if ($particular->name == "Expense (Loan Insurance)" || $particular->id == '32' || $particular->debitAccount['category'] != 'EXPENSE') {
                unset($particulars[$key]);
            }
        }


        return View::make('petty_cash.transactionItems', compact('particulars','trItems'));
    });
    Route::post('petty_cash/commitTransaction', 'PettyCashController@commitTransaction');
    Route::get('petty_cash/transaction/{id}', 'PettyCashController@receiptTransactions');

// Edit and delete petty cash items
    Route::get('petty_cash/newTransaction/remove/{count}', 'PettyCashController@removeTransactionItem');

    /**
     * Projects controllers
     */
    Route::resource('projects', 'ProjectsController');
    Route::get('projects/create', 'ProjectsController@create');
    Route::post('projects/update', 'ProjectsController@doupdate');
    Route::get('projects/update/{id}', 'ProjectsController@update');
    Route::get('projects/delete/{id}', 'ProjectsController@destroy');
    Route::get('projects/orders/show', 'ProjectsController@show');
    Route::get('projects/orders/create/{id}', 'ProjectsController@createOrder');
    Route::post('projects/orders/create', 'ProjectsController@docreateOrder');
    Route::get('projects/orders/update/{id}', 'ProjectsController@updateOrder');
    Route::post('projects/orders/update', 'ProjectsController@doupdateOrder');
    Route::get('projects/orders/delete/{id}', 'ProjectsController@diminishOrder');
    Route::get('projects/orders/approve/{id}', 'ProjectsController@approve');
    Route::post('projects/orders/approve', 'ProjectsController@doapprove');
    Route::get('projects/orders/reject/{id}', 'ProjectsController@reject');
    Route::post('projects/orders/reject', 'ProjectsController@doreject');
    Route::get('reports/projects', function () {
        $members = DB::table('members')
            ->join('project_orders', 'members.id', '=', 'project_orders.member_id')
            ->where('project_orders.is_approved', '=', 1)
            ->select('members.name as name', 'members.membership_no as membership_no',
                'members.id as id')
            ->groupBy('members.name')
            ->get();
        return View::make('project', compact('members'));

    });

    Route::post('projects/statement', function (Request $request) {
        $memid = $request->get('memberid');
        $member = Member::where('id', '=', $memid)->get()->first();
        $projects = Projectorder::where('member_id', '=', $memid)
            ->where('is_approved', '=', 1)
            ->get();
        $organization = Organization::findOrFail(1);
        $pdf = PDF::loadView('pdf.investments.statement', compact('organization', 'member', 'projects'))
            ->setPaper('a4','portrait');
        return $pdf->stream('Member Project Statement.pdf');
    });

    /**
     * orders controllers
     */
    Route::resource('orders', 'OrdersController');
    Route::post('orders/update/{id}', 'OrdersControler@update');
    Route::get('orders/edit/{id}', 'OrdersControler@edit');
    Route::get('orders/delete/{id}', 'OrdersControler@destroy');

    /**
     * products controllers
     */
    Route::resource('products', 'ProductsController');
    Route::post('products/update/{id}', 'ProductsController@update');
    Route::get('products/edit/{id}', 'ProductsController@edit');
    Route::get('products/create', 'ProductsController@create');
    Route::get('products/delete/{id}', 'ProductsController@destroy');
    Route::get('products/orders/{id}', 'ProductsController@orders');
    Route::get('shop', 'ProductsController@shop');

    /**
     * reports routes
     */
    Route::get('reports', function(){

        return View::make('members.reports');
    });

    Route::resource('roles', 'RolesController');
    Route::get('roles/create', 'RolesController@create');
    Route::get('roles/edit/{id}', 'RolesController@edit');
    Route::get('roles/show/{id}', 'RolesController@show');
    Route::post('roles/update/{id}', 'RolesController@update');
    Route::get('roles/delete/{id}', 'RolesController@destroy');

    Route::resource('permissions','PermissionsController');

    /**
     * Employee template
     */
    Route::get('template/employees', function(){

        $bank_data = Bank::where('organization_id',Auth::user()->organization_id)->get();

        $data = Employee::where('organization_id',Auth::user()->organization_id)->get();

        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();

        $bankbranch_data = BBranch::where('organization_id',Auth::user()->organization_id)->get();

        $branch_data = Branch::where('organization_id',Auth::user()->organization_id)->get();

        $department_data = Department::where('organization_id',Auth::user()->organization_id)->get();

        $employeetype_data = EType::where('organization_id',Auth::user()->organization_id)->get();

        $jobgroup_data = Jobgroup::where('organization_id',Auth::user()->organization_id)->get();

        Excel::create('Employees', function($excel) use($bank_data, $bankbranch_data, $branch_data,
            $department_data, $employeetype_data, $jobgroup_data,$employees, $data) {



            $excel->sheet('employees', function($sheet) use($bank_data, $bankbranch_data, $branch_data,
                $department_data, $employeetype_data, $jobgroup_data, $data, $employees){

                $sheet->row(1, array('EMPLOYMENT NUMBER','FIRST NAME','SURNAME',
                    'OTHER NAMES',
                    'ID NUMBER',
                    'KRA PIN',
                    'NSSF NUMBER',
                    'NHIF NUMBER',
                    'EMAIL ADDRESS','BASIC PAY'));


                $empdata = array();

                foreach($employees as $d){

                    $empdata[] = $d->personal_file_number.':'.$d->first_name.' '.$d->last_name.' '.$d->middle_name;
                }

                $emplist = implode(", ", $empdata);



                $listdata = array();

                foreach($data as $d){

                    $listdata[] = $d->allowance_name;
                }

                $list = implode(", ", $listdata);
            });
        })->export('xls');
    });

    /*
    *allowance template
    *
    */

    Route::get('template/allowances', function(){

        $data = Allowance::where('organization_id',Auth::user()->organization_id)->get();
        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();


        Excel::create( function($excel) use($data, $employees) {



            $excel->sheet('allowances', function($sheet) use($data, $employees){


                $sheet->row(1, array(
                    'EMPLOYEE', 'ALLOWANCE TYPE', 'FORMULAR', 'INSTALMENTS','AMOUNT','ALLOWANCE DATE',
                ));

                $sheet->setWidth(array(
                    'A'     =>  30,
                    'B'     =>  30,
                    'C'     =>  30,
                    'D'     =>  30,
                    'E'     =>  30,
                    'F'     =>  30,
                ));

                $sheet->getStyle('F2:F1000')
                    ->getNumberFormat()
                    ->setFormatCode('yyyy-mm-dd');



                $row = 2;
                $r = 2;

                for($i = 0; $i<count($employees); $i++){

                    $sheet->SetCellValue("YY".$row, $employees[$i]->personal_file_number." : ".$employees[$i]->first_name.' '.$employees[$i]->last_name);
                    $row++;
                }

                $sheet->_parent->addNamedRange(new NamedRange('names', $sheet, 'YY2:YY'.(count($employees)+1)));



                for($i = 0; $i<count($data); $i++){

                    $sheet->SetCellValue("YZ".$r, $data[$i]->allowance_name);
                    $r++;
                }

                $sheet->_parent->addNamedRange(
                    new NamedRange(
                        'allowances', $sheet, 'YZ2:YZ'.(count($data)+1)));


                for($i=2; $i <= 1000; $i++){

                    $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('allowances'); //note this!

                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('names'); //note this!

                    $objValidation = $sheet->getCell('C'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"One Time, Recurring, Instalments"'); //note this!
                }

            });

        },'Allowances.xlsx');



    });

    /*
    *deduction template
    *
    */

    Route::get('template/deductions', function(){

        $data = Deduction::where('organization_id',Auth::user()->organization_id)->get();
        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();


        Excel::download(function($excel) use($data, $employees) {


            $excel->sheet('deductions', function($sheet) use($data, $employees){


                $sheet->row(1, array(
                    'EMPLOYEE', 'DEDUCTION TYPE', 'FORMULAR','INSTALMENTS','AMOUNT','DATE'
                ));


                $sheet->setWidth(array(
                    'A'     =>  30,
                    'B'     =>  30,
                    'C'     =>  30,
                    'D'     =>  30,
                    'E'     =>  30,
                    'F'     =>  30,
                ));

                $sheet->getStyle('F2:F1000')
                    ->getNumberFormat()
                    ->setFormatCode('yyyy-mm-dd');

                $row = 2;
                $r = 2;

                for($i = 0; $i<count($employees); $i++){

                    $sheet->SetCellValue("YY".$row, $employees[$i]->personal_file_number." : ".$employees[$i]->first_name.' '.$employees[$i]->last_name);
                    $row++;
                }

                $sheet->_parent->addNamedRange(
                    new NamedRange(
                        'names', $sheet, 'YY2:YY'.(count($employees)+1)
                    )
                );



                for($i = 0; $i<count($data); $i++){

                    $sheet->SetCellValue("YZ".$r, $data[$i]->deduction_name);
                    $r++;
                }

                $sheet->_parent->addNamedRange(
                    new NamedRange(
                        'deductions', $sheet, 'YZ2:YZ'.(count($data)+1)
                    )
                );


                for($i=2; $i <= 1000; $i++){

                    $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('deductions'); //note this!



                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('names'); //note this!

                    $objValidation = $sheet->getCell('C'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"One Time, Recurring, Instalments"');

                }
            });
        },new Deduction,'xlsx');



    });

    /*
    *earning template
    *
    */

    Route::get('template/earnings', function(){
        $data = Employee::where('organization_id',Auth::user()->organization_id)->get();

        Excel::download(function($excel) use($data) {
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");



            $excel->sheet('Earnings', function($sheet) use($data) {

                $sheet->row(1, array(
                    ['EMPLOYEE', 'EARNING TYPE','NARRATIVE', 'FORMULAR', 'INSTALMENTS','AMOUNT','EARNING DATE',]
                ));

                $sheet->setWidth(array(
                    ['A'     =>  30,
                        'B'     =>  30,
                        'C'     =>  30,
                        'D'     =>  30,
                        'E'     =>  30,
                        'F'     =>  30,
                        'G'     =>  30,]
                ));

                $sheet->getStyle('G2:G1000')
                    ->getNumberFormat()
                    ->setFormatCode('yyyy-mm-dd');

                $row = 2;

                for($i = 0; $i<count($data); $i++){

                    $sheet->SetCellValue("ZZ".$row, $data[$i]->personal_file_number." : ".$data[$i]->first_name.' '.$data[$i]->last_name);
                    $row++;
                }

                $sheet->_parent->addNamedRange(
                    new NamedRange(
                        'names', $sheet, 'ZZ2:ZZ'.(count($data)+1)
                    )
                );

                $objPHPExcel = new Spreadsheet;
                $objSheet = $objPHPExcel->getActiveSheet();

                $objSheet->protectCells('ZZ2:ZZ'.(count($data)+1), 'PHP');

                $objSheet->getStyle('G2:G1000')->getNumberFormat()->setFormatCode('yyyy-mm-dd');


                for($i=2; $i <= 1000; $i++){

                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('names'); //note this!

                    $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"Bonus, Commission, Others"'); //note this!

                    $objValidation = $sheet->getCell('D'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('"One Time, Recurring, Instalments"'); //note this!
                }
            });
        },new Earnings,'xlsx');

    });

    /*
    *Relief template
    *
    */

    Route::get('template/reliefs', function(){

        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();

        $data = Relief::where('organization_id',Auth::user()->organization_id)->get();

        Excel::create('Reliefs', function($excel) use($employees, $data) {


            $excel->sheet('reliefs', function($sheet) use($employees, $data){
                $sheet->row(1, array( 'EMPLOYEE', 'RELIEF TYPE', 'AMOUNT'));


                $sheet->setWidth(array( 'A' =>  30,'B'  =>  30,'C' =>  30,));

                $row = 2;
                $r = 2;

                for($i = 0; $i<count($employees); $i++){
                    $sheet->SetCellValue("YY".$row, $employees[$i]->personal_file_number." : ".$employees[$i]->first_name.' '.$employees[$i]->last_name);
                    $row++;
                }

                $sheet->_parent->addNamedRange(
                    new NamedRange(
                        'names', $sheet, 'YY2:YY'.(count($employees)+1)
                    ));



                for($i = 0; $i<count($data); $i++){
                    $sheet->SetCellValue("YZ".$r, $data[$i]->relief_name);
                    $r++;
                }

                $sheet->_parent->addNamedRange(
                    new NamedRange(
                        'reliefs', $sheet, 'YZ2:YZ'.(count($data)+1)
                    ));


                for($i=2; $i <= 1000; $i++){

                    $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('reliefs'); //note this!


                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value is not in list.');
                    $objValidation->setPromptTitle('Pick from list');
                    $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('names'); //note this!

                }
            });

        })->export('xlsx');



    });

    /* #################### IMPORT EMPLOYEES ################################## */

    Route::post('import/employees', function(Request $request){

        if($request->hasFile('employees')){

            $destination = public_path().'/migrations/';

            $filename = Str::random(12);

            $ext = $request->file('employees')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            $request->file('employees')->move($destination, $file);

            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();
                $organization = Organization::find(Auth::user()->organization_id);

                $cres = count($results);
                $cemp = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->count();
                $limit = $organization->payroll_licensed;


                if($limit<$cres){
                    return Redirect::route('migrate')->withDeleteMessage('The imported employees exceed the licensed limit! Please upgrade your license');
                } else if($limit<($cres+$cemp)){
                    return Redirect::route('migrate')->withDeleteMessage('The imported employees exceed the licensed limit! Please upgrade your license');
                }else{

                    foreach ($results as $result) {

                        $employee = new Employee;

                        $employee->personal_file_number = $result->employment_number;

                        $employee->first_name = $result->first_name;
                        $employee->last_name = $result->surname;
                        $employee->middle_name = $result->other_names;
                        $employee->identity_number = $result->id_number;
                        $employee->pin = $result->kra_pin;
                        $employee->social_security_number = $result->nssf_number;
                        $employee->hospital_insurance_number = $result->nhif_number;
                        $employee->email_office = $result->email_address;
                        $employee->basic_pay = str_replace( ',', '', $result->basic_pay);
                        $employee->organization_id = Auth::user()->organization_id;
                        $employee->save();

                    }
                }

            });

        }

        return Redirect::back()->with('notice', 'Employees have been succeffully imported');
    });

    /* #################### IMPORT TRIAL EMPLOYEES ################################## */

    Route::post('import/trialemployees', function(Request $request){


        if($request->hasFile('trialemployees')){

            $destination = public_path().'/migrations/';

            $filename = Str::random(12);

            $ext = $request->file('trialemployees')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;

            $request->file('trialemployees')->move($destination, $file);

            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();
                $organization = Organization::find(Auth::user()->organization_id);

                $cres = count($results);
                $cemp = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->count();
                $limit = $organization->payroll_licensed;

                /*
                if($limit<$cres){
                  return Redirect::route('migrate')->withDeleteMessage('The imported employees exceed the licensed limit! Please upgrade your license');
                } else if($limit<($cres+$cemp)){
                    return Redirect::route('migrate')->withDeleteMessage('The imported employees exceed the licensed limit! Please upgrade your license');
                }else{*/

                $x = 1;
                if(count($results)>0){
                    foreach ($results as $result) {
                        $emp_number=$result->employment_number; if(!isset($emp_number)){ continue;}//$emp_number=1;}
                        $employee = new Employee;

                        $employee->personal_file_number = $emp_number;

                        $employee->first_name = 'firstname '.$x;
                        $employee->last_name = 'lastname '.$x;
                        $employee->middle_name = 'middlename '.$x;
                        $employee->identity_number = $x;
                        $employee->branch_id = 1;
                        $employee->department_id = 1;
                        $employee->email_office = 'email'.$x.'@lixnet.net';
                        $employee->basic_pay = str_replace( ',', '', $result->basic_pay);
                        $employee->organization_id = Auth::user()->organization_id;
                        $employee->save();
                    }
                    $travelallowance = Allowance::where('allowance_name','Travel Allowance')->first();
                    $actingallowance = Allowance::where('allowance_name','Acting Allowance')->first();
                    $otherallowance  = Allowance::where('allowance_name','Other Allowance')->first();

                    if($result->travel_allowance != ''){

                        $allowance = new EAllowances;

                        $allowance->employee_id = $employee->id;

                        $allowance->allowance_id = $travelallowance->id;

                        $allowance->allowance_amount = str_replace( ',', '', $result->travel_allowance);

                        $allowance->save();
                    }

                    if($result->no >= 1 && $result->no <= 72){

                        $pension = new Pension;

                        $pension->employee_id = $employee->id;
                        $pension->employee_contribution=str_replace(",","",$result->basic_pay) * 0.1;
                        $pension->employer_contribution=str_replace(",","",$result->basic_pay) * 0.1;
                        $pension->employee_percentage=10;
                        $pension->employer_percentage=10;
                        $pension->type='Percentage';
                        $pension->save();
                    }


                    if($result->other_allowance != ''){

                        $allowance = new EAllowances;

                        $allowance->employee_id = $employee->id;

                        $allowance->allowance_id = $otherallowance->id;

                        $allowance->allowance_amount = str_replace( ',', '', $result->other_allowance);

                        $allowance->save();
                    }

                    if($result->acting_allowance != ''){
                        $allowance = new EAllowances;

                        $allowance->employee_id = $employee->id;

                        $allowance->allowance_id = $actingallowance->id;

                        $allowance->allowance_amount = str_replace( ',', '', $result->acting_allowance);

                        $allowance->save();
                    }


                    $deductionid = DB::table('deductions')->where('deduction_name', '=', 'Arrears')->pluck('id');

                    if($result->arrears != ''){

                        $deduction = new EDeduction;

                        $deduction->employee_id = $employee->id;

                        $deduction->deduction_id = $deductionid;

                        $deduction->deduction_amount = str_replace( ',', '', $result->arrears);

                        $deduction->deduction_date = date('Y-m-d');

                        $deduction->save();
                    }
                    $x++;
                }
                //}

            });

        }

        return Redirect::back()->with('notice', 'Employees have been succeffully imported');

    });

    /* #################### IMPORT EARNINGS ################################## */

    Route::post('import/earnings', function(Request $request){


        if($request->hasFile('earnings')){

            $destination = public_path().'/migrations/';

            $filename = Str::random(12);

            $ext = $request->file('earnings')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            $request->file('earnings')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();


                foreach ($results as $result) {

                    if($result->employee != null){


                        $name = explode(' : ', $result->employee);



                        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', $name[0])->pluck('id');


                        $earning = new Earnings;

                        $earning->employee_id = $employeeid;

                        $earning->earnings_name = $result->earning_type;

                        $earning->narrative = $result->narrative;

                        $earning->formular = $result->formular;



                        if($result->formular == 'Instalments'){
                            $earning->instalments = $result->instalments;
                            $insts = $result->instalments;

                            $a = str_replace( ',', '',$result->amount);
                            $earning->earnings_amount = $a;

                            $earning->earning_date = $result->earning_date;

                            $effectiveDate = date('Y-m-d', strtotime("+".($insts-1)." months", strtotime($result->earning_date)));

                            $First  = date('Y-m-01', strtotime($result->earning_date));
                            $Last   = date('Y-m-t', strtotime($effectiveDate));

                            $earning->first_day_month = $First;

                            $earning->last_day_month = $Last;

                        }else{
                            $earning->instalments = '1';
                            $a = str_replace( ',', '', $result->amount );
                            $earning->earnings_amount = $a;

                            $earning->earning_date = $result->earning_date;

                            $First  = date('Y-m-01', strtotime($result->earning_date));
                            $Last   = date('Y-m-t', strtotime($result->earning_date));


                            $earning->first_day_month = $First;

                            $earning->last_day_month = $Last;

                        }


                        $earning->save();


                    }
                }
            });
        }

        return Redirect::back()->with('notice', 'earnings have been successfully imported');

    });

    /* #################### IMPORT RELIEFS ################################## */

    Route::post('import/reliefs', function(Request $request){


        if($request->hasFile('reliefs')){

            $destination = public_path().'/migrations/';

            $filename = Str::random(12);

            $ext = $request->file('reliefs')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            $request->file('reliefs')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {
                    if($result->employee != null){

                        $name = explode(':', $result->employee);


                        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', $name[0])->pluck('id');

                        $reliefid = DB::table('relief')->where('relief_name', '=', $result->relief_type)->pluck('id');

                        $relief = new ERelief;

                        $relief->employee_id = $employeeid;

                        $relief->relief_id = $reliefid;

                        $relief->relief_amount = $result->amount;

                        $relief->save();

                    }

                }
            });
        }

        return Redirect::back()->with('notice', 'reliefs have been succeffully imported');

    });

    /* #################### IMPORT ALLOWANCES ################################## */
    Route::post('import/allowances', function(Request $request){


        if($request->hasFile('allowances')){

            $destination = public_path().'/migrations/';

            $filename = Str::random(12);

            $ext = $request->file('allowances')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            $request->file('allowances')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {

                    if($result->employee != null){

                        $name = explode(':', $result->employee);

                        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', $name[0])->pluck('id');

                        $allowanceid = DB::table('allowances')->where('allowance_name', '=', $result->allowance_type)->pluck('id');

                        $allowance = new EAllowances;

                        $allowance->employee_id = $employeeid;

                        $allowance->allowance_id = $allowanceid;

                        $allowance->formular = $result->formular;



                        if($result->formular == 'Instalments'){
                            $allowance->instalments = $result->instalments;
                            $insts = $result->instalments;

                            $a = str_replace( ',', '',$result->amount);
                            $allowance->allowance_amount = $a;

                            $allowance->allowance_date = $result->allowance_date;

                            $effectiveDate = date('Y-m-d', strtotime("+".($insts-1)." months", strtotime($result->allowance_date)));

                            $First  = date('Y-m-01', strtotime($result->allowance_date));
                            $Last   = date('Y-m-t', strtotime($effectiveDate));

                            $allowance->first_day_month = $First;

                            $allowance->last_day_month = $Last;

                        }else{
                            $allowance->instalments = '1';
                            $a = str_replace( ',', '', $result->amount );
                            $allowance->allowance_amount = $a;

                            $allowance->allowance_date = $result->allowance_date;

                            $First  = date('Y-m-01', strtotime($result->allowance_date));
                            $Last   = date('Y-m-t', strtotime($result->allowance_date));


                            $allowance->first_day_month = $First;

                            $allowance->last_day_month = $Last;

                        }

                        $allowance->save();

                    }

                }
            });
        }

        return Redirect::back()->with('notice', 'allowances have been succefully imported');

    });


    /* #################### IMPORT DEDUCTIONS ################################## */
    Route::post('import/deductions', function(Request $request){


        if($request->hasFile('deductions')){

            $destination = public_path().'/migrations/';

            $filename = Str::random(12);

            $ext = $request->file('deductions')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;

            $request->file('deductions')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {

                    if($result->employee != null){


                        $name = explode(':', $result->employee);

                        $employeeid = DB::table('employee')->where('organization_id',Auth::user()->organization_id)->where('personal_file_number', '=', $name[0])->pluck('id');

                        $deductionid = DB::table('deductions')->where('deduction_name', '=', $result->deduction_type)->pluck('id');

                        $deduction = new EDeduction;

                        $deduction->employee_id = $employeeid;

                        $deduction->deduction_id = $deductionid;

                        $deduction->formular = $result->formular;

                        $a = str_replace( ',', '', $result->amount );
                        $deduction->deduction_amount = $a;

                        $deduction->deduction_date = $result->date;

                        if($result->formular == 'Instalments'){
                            $deduction->instalments = $result->instalments;
                            $insts = $result->instalments;

                            $effectiveDate = date('Y-m-d', strtotime("+".($insts-1)." months", strtotime($result->date)));

                            $First  = date('Y-m-01', strtotime($result->date));
                            $Last   = date('Y-m-t', strtotime($effectiveDate));

                            $deduction->first_day_month = $First;

                            $deduction->last_day_month = $Last;

                        }else{
                            $deduction->instalments = '1';

                            $First  = date('Y-m-01', strtotime($result->date));
                            $Last   = date('Y-m-t', strtotime($result->date));


                            $deduction->first_day_month = $First;

                            $deduction->last_day_month = $Last;

                        }

                        $deduction->save();

                    }

                }


            });

        }

        return Redirect::back()->with('notice', 'deductions have been succefully imported');
    });

    /* #################### IMPORT BANK BRANCHES ################################## */
    Route::post('import/bankBranches', function(Request $request){


        if($request->hasFile('bbranches')){

            $destination = public_path().'/migrations/';

            $filename = Str::random(12);

            $ext = $request->file('bbranches')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            $request->file('bbranches')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {
                    $branch_code=$result->branch_code; $branch_name=$result->branch_name;
                    if(!isset($branch_code) || !isset($branch_name)){continue;}

                    $bbranch = new BBranch;

                    $bbranch->branch_code = $result->branch_code;

                    $bbranch->bank_branch_name = $result->branch_name;

                    $bbranch->bank_id = $result->bank_id;

                    $bbranch->organization_id = $result->organization_id;

                    $bbranch->save();

                }

            });

        }
        return Redirect::back()->with('notice', 'bank branches have been succefully imported');

    });

    /* #################### IMPORT BANKS ################################## */

    Route::post('import/banks', function(Request $request){


        if($request->hasFile('banks')){

            $destination = public_path().'/migrations/';

            $filename = Str::random(12);

            $ext = $request->file('banks')->getClientOriginalExtension();
            $file = $filename.'.'.$ext;


            $request->file('banks')->move($destination, $file);


            Excel::selectSheetsByIndex(0)->load(public_path().'/migrations/'.$file, function($reader){

                $results = $reader->get();

                foreach ($results as $result) {
                    $bnk_name=$result->bank_name; if(!isset($bnk_name) || empty($bnk_name)){continue;}
                    $bank = new Bank;

                    $bank->bank_name = $result->bank_name;

                    $bank->bank_code = $result->bank_code;

                    $bank->organization_id = $result->organization_id;

                    $bank->save();

                }

            });

        }

        return Redirect::back()->with('notice', 'banks have been succefully imported');

    });


    /**
     * licence routes
     */
    Route::post('license/key', 'OrganizationsController@generate_license_key');
    Route::post('license/activate', 'OrganizationsController@activate_license');
    Route::get('license/activate/{id}', 'OrganizationsController@activate_license_form');
    Route::post('license/activate_annual', 'OrganizationsController@annual_subscription_license');
    Route::get('license/activate_annual/{id}', 'OrganizationsController@activate_annual_license_form');

    /**
     * Human Resources Module
     */
    Route::get('hrdashboard', function (){
        $employees = Employee::getActiveEmployee();
        return view('hrdashboard',compact('employees'));
    });


    /**
     * Payroll Module
     */
    Route::get('payrolldashboard', function (){
        return view('payrolldashboard');
    });
});

Route::fallback(function (){
    return view('errors.404');
});

Route::middleware('loggedin')->get('reports/employees',function(){
    return view('reports');
});

Route::middleware('loggedin')->get('hrdashboard',  function(){
    $employees = Employee::getActiveEmployee();
    return view('hrdashboard',compact('employees'));
});


Route::group(['middleware' => 'can:manage_audits'],function(){
    Route::resource('audits', 'AuditsController');
});


Route::get('payrolldashboard', array('middleware' => 'loggedin', function(){
    return view('payrolldashboard');
}));

Route::group(['middleware' => ['can:manage_organization']], function() {

    Route::resource('organizations', 'OrganizationsController');

    Route::post('organizations/update/{id}', 'OrganizationsController@update');
    Route::post('organizations/logo/{id}', 'OrganizationsController@logo');
    Route::get('language/{lang}',
        array(
            'as' => 'language.select',
            'uses' => 'OrganizationsController@language'
        )
    );

    Route::resource('fleet_management', 'FleetMgntController');
    Route::resource('fleetCashCollection','FleetCashColController');
    Route::post('fleetCashCollection/update/{id}', 'FleetCashColController@update');
    Route::get('fleetCashCollection/delete/{id}','FleetCashColController@destroy');
    Route::get('ajaxCollector', function (Request $request) {
        $vehicle = Vehicle::find($request->get('option'));
        $collector= FleetCashCollector::find($vehicle->cash_collector);
        $data=[$vehicle->cash_collector,$collector->name];
        return $data;
    });

    Route::resource('fleetCashCollectors','FleetCashCollectorController');
    Route::post('fleetCashCollectors/update/{id}', 'FleetCashCollectorController@update');
    Route::get('fleetCashCollectors/delete/{id}', 'FleetCashCollectorController@destroy');
});

Route::group(['middleware'=>['can:manage_system']],function (){
    Route::get('system', function(){
        $organization = Organization::find(1);

        return View::make('system.index', compact('organization'));
    });
});

Route::group(['middleware'=>['can:create_employee']],function (){
    Route::get('employees/create', 'EmployeesController@create');
});

Route::middleware('can:leavemgmt')->get('leavemgmt',function(){
    $leaveapplications = Leaveapplication::where('organization_id',Auth::user()->organization_id)->get();

    return View::make('leavemgmt', compact('leaveapplications'));
});

Route::group(['middleware'=>['can:manage_holiday']],function (){
    /**
     * Holiday routes
     */
    Route::resource('holidays', 'HolidaysController');
    Route::get('holidays/edit/{id}', 'HolidaysController@edit');
    Route::get('holidays/delete/{id}', 'HolidaysController@destroy');
    Route::post('holidays/update/{id}', 'HolidaysController@update');
});

Route::group(['middleware'=>['can:manage_leavetype']],function (){
    Route::resource('leavetypes', 'LeavetypesController');
    Route::get('leavetypes/edit/{id}', 'LeavetypesController@edit');
    Route::get('leavetypes/delete/{id}', 'LeavetypesController@destroy');
    Route::post('leavetypes/update/{id}', 'LeavetypesController@update');
});

Route::group(['middleware'=>['can:manage_leave']],function (){
    /**LEAVE APPLICATION ROUTES */
    Route::resource('leaveapplications', 'LeaveapplicationsController');
    Route::get('leaveapplications/edit/{id}', 'LeaveapplicationsController@edit');
    Route::get('leaveapplications/delete/{id}', 'LeaveapplicationsController@destroy');
    Route::post('leaveapplications/update/{id}', 'LeaveapplicationsController@update');
    Route::get('leaveapplications/approve/{id}', 'LeaveapplicationsController@approve');
    Route::post('leaveapplications/approve/{id}', 'LeaveapplicationsController@doapprove');
    Route::get('leaveapplications/cancel', 'LeaveapplicationsController@cancel');
    Route::get('leaveapplications/reject/{id}', 'LeaveapplicationsController@reject');
    Route::get('leaveapplications/show/{id}', 'LeaveapplicationsController@show');

    Route::get('leaveapplications/approvals', 'LeaveapplicationsController@approvals');
    Route::get('leaveapplications/rejects', 'LeaveapplicationsController@rejects');
    Route::get('leaveapplications/cancellations', 'LeaveapplicationsController@cancellations');
    Route::get('leaveapplications/amends', 'LeaveapplicationsController@amended');

    Route::get('leaveapprovals', function(){

        $leaveapplications = Leaveapplication::all();

        return View::make('leaveapplications.approved', compact('leaveapplications'));

    });

    Route::get('leaverejects', function(){

        $leaveapplications = Leaveapplication::all();

        return View::make('leaveapplications.rejected', compact('leaveapplications'));

    } );
    /**EnD LEAVE APPLICATION */

    Route::get('leaveamends', function(){

        $leaveapplications = Leaveapplication::all();

        return View::make('leaveapplications.amended', compact('leaveapplications'));

    });
});

Route::group(['middleware'=>['can:process_payroll']],function (){
    Route::get('payrollmgmt', function(){

        $employees = Employee::all();

        return View::make('payrollmgmt', compact('employees'));

    });

    Route::resource('accounts', 'AccountsController');
    Route::post('accounts/update/{id}', 'AccountsController@update');
    Route::get('accounts/delete/{id}', 'AccountsController@destroy');
    Route::get('accounts/edit/{id}', 'AccountsController@edit');
    Route::get('accounts/show/{id}', 'AccountsController@show');
    Route::get('accounts/create/{id}', 'AccountsController@create');



    /**
     * payroll routes
     */
    Route::resource('payroll', 'PayrollController');
    Route::post('deleterow', 'PayrollController@del_exist');
    Route::post('payroll/preview', 'PayrollController@create');
    Route::post('payroll/edit{id}', 'PayrollController@edit');

    Route::post('showrecord', 'PayrollController@display');
    Route::post('shownet', 'PayrollController@disp');
    Route::post('showgross', 'PayrollController@dispgross');
    Route::get('payrollpreviewprint/{period}', 'PayrollController@previewprint');
    Route::get('unlockpayroll/index', 'PayrollController@unlockindex');
    Route::get('payroll/view/{id}', 'PayrollController@viewpayroll');
    Route::get('unlockpayroll/{id}', 'PayrollController@unlockpayroll');
    Route::post('unlockpayroll', 'PayrollController@dounlockpayroll');
    Route::post('createNewAccount', 'PayrollController@createaccount');

    Route::get('payrollcalculator', function(){
        $currency = Currency::find(1);
        return View::make('payroll.payroll_calculator',compact('currency'));

    });

    Route::get('payrollReports', function(){

        return view('employees.payrollreports');
    });

    Route::get('payrollReports/selectYear', function(){
        $branches = Branch::whereNull('organization_id')->orWhere('organization_id',Auth::user()->organization_id)->get();
        $departments = Department::whereNull('organization_id')->orWhere('organization_id',Auth::user()->organization_id)->get();
        $employees = Employee::where('organization_id',Auth::user()->organization_id)->get();
        return view('pdf.p9Select',compact('employees','branches','departments'));
    });

    Route::get('payrolldashboard', function() {
        return View::make('payrolldashboard');
    });
});




// ================= API - AJAX  ROUTES ================================ //
Route::get('api/dropdown', function (Request $request) {
    $id = $request->get('option');
    $bbranch = Bank::find($id)->bankbranch;
    return $bbranch->lists('bank_branch_name', 'id');
});

Route::get('api/purchases', function(){
    $id = 2;//$request->get('option');
    $erporderitems = Erporder::join('erporderitems','erporders.id','=','erporderitems.erporder_id')
        ->join('items','erporderitems.item_id','=','items.id')
        ->where('client_id',$id)
        ->groupBy('erporders.id')
        ->where('erporders.status','new')
        ->havingRaw('balance > 0 or balance is null')
        ->select('erporders.id',  DB::raw('CONCAT(order_number," : ",name," (Actual amount: ", sum(price * quantity),")") AS erporder'), DB::raw('(SELECT (sum(price * quantity) - sum(amount_paid)) FROM payments t WHERE t.erporder_id=erporders.id and t.client_id='.$id.') AS balance'))
        ->get('erporder', 'id');
    return $erporderitems;
});

Route::get('api/purchasesdue', function(Request $request){
    $id = $request->get('option');
    $price = Erporderitem::join('items','erporderitems.item_id','=','items.id')
        ->where('erporder_id',$id)->select(DB::raw('sum(price * quantity) AS total'))->first();
    $payment = Payment::where('erporder_id',$id)->sum('amount_paid');

    return ($price->total) - $payment;
});

Route::get('api/orders', function (Request $request) {
    $id = $request->get('option');
    $projects = DB::table('project_orders')
        ->join('members', 'project_orders.member_id', '=', 'members.id')
        ->join('projects', 'project_orders.project_id', '=', 'projects.id')
        ->where('member_id', $id)
        ->select('project_orders.id as id', 'projects.name as name')
        ->lists('name', 'id');

    return $projects;
});

Route::get('api/getaccountno', function(Request $request){
    $member = Member::findOrFail($request->get('member'));
    $id = $request->get('product');
    $savingproduct = Savingproduct::findOrFail($id);

    return $acc_no = $savingproduct->shortname.'000000'.$member->membership_no;
});

Route::get('api/label', function (Request $request) {
    $id = $request->get('option');
    $currency = Currency::find(1);
    $loanproduct = Loanproduct::find($id);
    return $currency->shortname . '. ' . number_format($loanproduct->auto_loan_limit, 2);
});

Route::get('api/loans', function (Request $request) {
    $id = $request->get('option');
    $loans = DB::table('loanaccounts')
        ->join('loanproducts', 'loanaccounts.loanproduct_id', '=', 'loanproducts.id')
        ->where('member_id', $id)
        ->select('loanaccounts.id as id', 'loanproducts.name as name')
        ->lists('name', 'id');

    return $loans;
});

Route::get('api/leavetypes', function(){
    $leavetypes = Leavetype::where('organization_id',Auth::user()->organization_id)->get();
    return $leavetypes->lists('name', 'id');
});

Route::get('api/site', function(Request $request){
    $sid = $request->get('option');
    $site = array();


    $site = Site::select('id', 'name')
        ->where('organization_id',Auth::user()->organization_id)
        ->where('period',$sid)
        ->lists('name', 'id');


    return $site;
});


Route::get('api/branchemployee', function(Request $request){
    $bid = $request->get('option');
    $did = $request->get('deptid');
    $seltype = $request->get('type');
    $employee = array();
    $department = Department::where('department_name','Management')
        ->where(function($query){
            $query->whereNull('organization_id')
                ->orWhere('organization_id',Auth::user()->organization_id);
        })->first();


    $jgroup = Jobgroup::where(function($query){
        $query->whereNull('organization_id')
            ->orWhere('organization_id',Auth::user()->organization_id);
    })->where('job_group_name','Management')
        ->first();

    if(($bid == 'All' || $bid == '' || $bid == 0) && ($did == 'All' || $did == '' || $did == 0)){
        if(Gate::allows('manager_payroll')){
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('organization_id',Auth::user()->organization_id)
                ->lists('full_name', 'id');
        }else{
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('organization_id',Auth::user()->organization_id)
                ->where('job_group_id','!=',$jgroup->id)
                ->lists('full_name', 'id');
        }
    }else if(($bid != 'All' || $bid != '' || $bid != 0) && ($did == 'All' || $did == '' || $did == 0)){
        if(Gate::allows('manager_payroll')){
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('branch_id',$bid)
                ->where('organization_id',Auth::user()->organization_id)
                ->lists('full_name', 'id');
        }else{
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('branch_id',$bid)
                ->where('organization_id',Auth::user()->organization_id)
                ->where('job_group_id','!=',$jgroup->id)
                ->lists('full_name', 'id');
        }
    }else if(($did != 'All' || $did != '' || $did != 0) && ($bid != 'All' || $bid != '' || $bid != 0) ){
        if(Gate::allows('manager_payroll')){
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('branch_id',$bid)
                ->where('organization_id',Auth::user()->organization_id)
                ->where('department_id',$did)
                ->lists('full_name', 'id');
        }else{
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('branch_id',$bid)
                ->where('organization_id',Auth::user()->organization_id)
                ->where('job_group_id','!=',$jgroup->id)
                ->where('department_id',$did)
                ->lists('full_name', 'id');
        }
    }else if(($did != 'All' || $did != '' || $did != 0) && ($bid == 'All' || $bid == '' || $bid == 0)){
        if(Gate::allows('manager_payroll')){
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('department_id',$did)
                ->where('organization_id',Auth::user()->organization_id)
                ->lists('full_name', 'id');
        }else{
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('department_id',$did)
                ->where('organization_id',Auth::user()->organization_id)
                ->where('job_group_id','!=',$jgroup->id)
                ->lists('full_name', 'id');
        }
    }
    return $employee;
});

Route::get('api/deptemployee', function(Request $request){
    $did = $request->get('option');
    $bid = $request->get('bid');
    $seltype = $request->get('type');
    $employee = array();
    $department = Department::where('department_name','Management')
        ->where(function($query){
            $query->whereNull('organization_id')
                ->orWhere('organization_id',Auth::user()->organization_id);
        })->first();


    $jgroup = Jobgroup::where(function($query){
        $query->whereNull('organization_id')
            ->orWhere('organization_id',Auth::user()->organization_id);
    })->where('job_group_name','Management')
        ->first();

    if(($did == 'All' || $did == '' || $did == 0) && ($bid == 'All' || $bid == '' || $bid == 0)){
        if(Gate::allows('manager_payroll')){
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('organization_id',Auth::user()->organization_id)
                ->lists('full_name', 'id');
        }else{
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('organization_id',Auth::user()->organization_id)
                ->where('job_group_id','!=',$jgroup->id)
                ->lists('full_name', 'id');
        }
    }else if(($did != 'All' || $did != '' || $did != 0) && ($bid == 'All' || $bid == '' || $bid == 0)){
        if(Gate::allows('manager_payroll')){
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('department_id',$did)
                ->where('organization_id',Auth::user()->organization_id)
                ->lists('full_name', 'id');
        }else{
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('department_id',$did)
                ->where('organization_id',Auth::user()->organization_id)
                ->where('job_group_id','!=',$jgroup->id)
                ->lists('full_name', 'id');
        }
    }else if(($did != 'All' || $did != '' || $did != 0) && ($bid != 'All' || $bid != '' || $bid != 0) ){
        if(Gate::allows('manager_payroll')){
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('branch_id',$bid)
                ->where('organization_id',Auth::user()->organization_id)
                ->where('department_id',$did)
                ->lists('full_name', 'id');
        }else{
            $employee = Employee::select('id', DB::raw('CONCAT(personal_file_number, " : ", first_name," ",middle_name," ",last_name) AS full_name'))
                ->where('branch_id',$bid)
                ->where('organization_id',Auth::user()->organization_id)
                ->where('job_group_id','!=',$jgroup->id)
                ->where('department_id',$did)
                ->lists('full_name', 'id');
        }
    }
    return $employee;
});


Route::get('api/getDays', function(Request $request){
    $id = $request->get('employee');
    $lid = $request->get('leave');
    $d = $request->get('option');
    $sdate = $request->get('sdate');
    $weekends = $request->get('weekends');
    $holidays = $request->get('holidays');

    Leaveapplication::checkBalance($id, $lid,$d);
    if(Leaveapplication::checkBalance($id, $lid,$d)<0){
        return Leaveapplication::checkBalance($id, $lid,$d);
    }else{

        $enddate = Leaveapplication::getEndDate($sdate,$d,$weekends,$holidays);

        return $enddate;
        //Leaveapplication::checkHoliday($sdate);
    }

    //return Leaveapplication::checkBalance($id, $lid,$d);
});

Route::get('api/getDaysDynamic', function(Request $request){
    $id = $request->get('employee');
    $lid = $request->get('leave');
    $d = $request->get('option');
    $sdate = $request->get('sdate');
    $weekends = $request->get('weekends');
    $holidays = $request->get('holidays');

    Leaveapplication::checkBalance($id, $lid,$d);
    if(Leaveapplication::checkBalance($id, $lid,$d)<0){
        return Leaveapplication::checkBalance($id, $lid,$d);
    }else{

        $enddate = Leaveapplication::getEndDate($sdate,$d,$weekends,$holidays);

        return $enddate;
        //Leaveapplication::checkHoliday($sdate);
    }

    //return Leaveapplication::checkBalance($id, $lid,$d);
});

Route::get('api/score', function(Request $request){
    $id = $request->get('option');
    $rate = Appraisalquestion::find($id);
    return $rate->rate;
});

Route::get('api/pay', function(Request $request){
    $id = $request->get('option');
    $employee = Employee::find($id);
    return number_format($employee->basic_pay,2);
});

Route::get('ajaxfetchleaveEnd',function(Request $request){
    $fdate = date("Y-m-d",strtotime($request->get('fdate')));
    $leave_id= $request->get('leavetype');
    $fdate2= date("Y-m-d",strtotime($fdate));
    $leave= Leavetype::find($leave_id);
    $leave_days= $leave->days;
    $off_weekends= $leave->off_weekends;
    $off_holidays= $leave->off_holidays;
    for($i=1; $i<=$leave_days; $i++){
        if($i==1){
            $holidate= Holiday::where("date","=",$fdate)->count();
            $weekend=date("w",strtotime($fdate));
            if($weekend==6 && $off_weekends==1){
                $curr_date=date('Y-m-d',strtotime($fdate . '+ 2 days'));
            }else if($weekend==0 && $off_weekends==1){
                $curr_date=date('Y-m-d',strtotime($fdate . '+ 1 days'));
            }else if($holidate>0 && $off_holidays==1){
                $curr_date=date('Y-m-d',strtotime($fdate . '+ 1 days'));
            }else{$curr_date=$fdate;}
        }else{
            $curr_date=$_SESSION['curr_date'];
            $next_date=date('Y-m-d',strtotime($curr_date . '+ 1 days'));
            $holidate= Holiday::where("date","=",$next_date)->count();
            $weekend=date("w",strtotime($next_date));
            if($weekend==6 && $off_weekends==1){
                $curr_date=date('Y-m-d',strtotime($curr_date . '+ 3 days'));
            }elseif($holidate>0 && $off_holidays==1){
                $curr_date=date('Y-m-d',strtotime($curr_date . '+ 2 days'));
            }else{
                $curr_date=date('Y-m-d',strtotime($curr_date . '+ 1 days'));
            }
        }
        $_SESSION['curr_date']=$curr_date;
    }
    session_unset();
    return $curr_date;
});

// ================== END API ROUTES ================================//
